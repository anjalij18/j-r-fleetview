import { Component, OnInit, ViewChild, ElementRef, ViewChildren, QueryList } from '@angular/core';
import { SMS } from '@ionic-native/sms';
import * as moment from 'moment';
import * as io from 'socket.io-client';   // with ES6 import
import { IonicPage, NavController, NavParams, AlertController, ToastController, ModalController, PopoverController, ViewController, Navbar, Platform, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { TranslateService } from '@ngx-translate/core';
import { ImmobilizeModelPage } from './immobilize-modal';
import { DrawerState } from 'ion-bottom-drawer';
import { TimePickerModal } from '../profile/settings/notif-setting/time-picker/time-picker';
import { CallNumber } from '@ionic-native/call-number';
import { PushObject, Push, PushOptions } from '@ionic-native/push';
@IonicPage()
@Component({
  selector: 'page-add-devices',
  templateUrl: 'add-devices.html',
})
export class AddDevicesPage implements OnInit {
  public cartCount: number = 0;
  @ViewChildren("step") steps: QueryList<ElementRef>;
  public loadProgress: number = 0;
  @ViewChildren('myGauge') myGauge: QueryList<ElementRef>;
  islogin: any;
  isDealer: any;
  islogindealer: string;
  stausdevice: string;
  stausdevices: string;
  device_types: { vehicle: string; name: string; }[];
  GroupType: { vehicle: string; name: string; }[];
  GroupStatus: { name: string; }[];
  allDevices: any = [];
  allDevicesSearch: any[] = [];
  ddata:any;
  socket: any;
  veh: any;
  isdevice: string;
  userPermission: any;
  option_switch: boolean = false;
  dataEngine: any;
  DeviceConfigStatus: any;
  messages: string;
  editdata: any;
  responseMessage: string;
  searchCountryString = ''; // initialize your searchCountryString string empty
  countries: any;
  latLang: any;
  TotalDevice: any[];

  @ViewChild('popoverContent', { read: ElementRef }) content: ElementRef;
  @ViewChild('popoverText', { read: ElementRef }) text: ElementRef;
  // @ViewChild('foo', { read: ElementRef }) foo : ElementRef;
  // fooChart: any;
  @ViewChild(Navbar) navBar: Navbar;
  page: number = 0;
  limit: number = 8;
  ndata: any;
  support_contact:any;
  searchItems: any;
  searchQuery: string = '';
  items: any[];
  deivceId: any;
  immobType: any;
  respMsg: any;
  commandStatus: any;
  intervalID: any;
  now: any;
  datePipe: any;
  isSuperAdmin: any;
  clicked: boolean = false;
  checkedPass: string;
  datetimeEnd: string;
  datetimeStart: string;
  resp_data:any=[];
  // superAdmin: boolean;
  dealer_Permission: boolean;
  checkIfStat: string = "ALL";
  public toggled: boolean = false;
  progressIntervalId: any;
  drawerData: any;
  intervalid123: any;
  devIcon: string;
  all_devices: any = [];
  singleVehicleCount: number;
  t_veicle_count: any;
  showEmptyStatus: boolean = false;
  distanceReport: any;
  totaldevices: any;
  devices: any;
  Running: any;
  IDLING: any;
  Stopped: any;
  OutOffReach: any;
  NogpsDevice: any;
  expiredDevices: any;
  nodata: any;
  totalVechiles: any;
  to: string;
  from: string;
  token: string;
  isPayAlert:any=false;
  to_odo:any;
  isPayNow:any= false;
  devicedetail:any;
  setselect :any= false;
  selectedDate:any;
  isPaymentPage:any[]=[];
  selectedArray:any[] = [];
  public toggle(): void {
    this.toggled = !this.toggled;
  }
  cancelSearch() {
    // this.toggle();
    this.toggled = false;
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private sms: SMS,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    private geocoderApi: GeocoderProvider,
    private plt: Platform,
    public translate: TranslateService,
    private events: Events,
    private callNumber: CallNumber,
    public push: Push,

  ) {
    //this.getDistanceReport();
    //this.getDashboarddevices();

    this.callBaseURL();
    this.toggled = false;
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.apiCall.getRechargePlanApi(this.islogin.supAdmin).subscribe((respData) => {
      console.log("recharge data --------->", respData);
      this.isPaymentPage =respData
    });
    var url = this.apiCall.mainUrl + "users/get_user_setting";
    var payload = {
      uid: this.islogin._id,
    };
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdatatwo(url, payload).subscribe(
      (respData) => {
        this.ddata = respData;
        this.ddata = JSON.parse(this.ddata._body).service1;
        this.apiCall.stopLoading();
        console.log("respData of add device data--:-------- ",this.ddata);
      },
      (err) => {
        console.log("oops got error: ", err);
        this.apiCall.stopLoading();
      }
    );
    this.events.publish('user:updated', this.islogin);
    console.log("islogin devices => " + JSON.stringify(this.islogin));
    this.datetimeStart = moment({ hours: 0 }).format();
    console.log("start", this.datetimeStart);
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log("end", this.datetimeEnd);
    this.to = new Date().toISOString();
    var d = new Date();
    var a = d.setHours(0, 0, 0, 0)
    this.from = new Date(a).toISOString();
    this.selectedDate = new Date().toISOString();;
    // this.datetimeStart = moment({ hours: 0 }).format(); // yesterday date with 12:00 am
    // console.log("today time: ", this.datetimeStart)
    // this.datetimeEnd = moment({ hours: 0 }).format(); // today date and time with 12:00am
    if (localStorage.getItem('Total_Vech') !== null) {
      this.t_veicle_count = JSON.parse(localStorage.getItem('Total_Vech'));
    }
    // this.superAdmin = this.islogin.isSuperAdmin;
    // this.adbtn = localStorage.getItem("dlrchk");
    // this.dealer_Permission = this.islogin.device_add_permission;

    this.isDealer = this.islogin.isDealer;
    this.isSuperAdmin = this.islogin.isSuperAdmin;
    this.islogindealer = localStorage.getItem('isDealervalue');
    if (this.isDealer == false && this.isSuperAdmin == false) {
      this.dealer_Permission = false;
    } else {
      this.dealer_Permission = this.islogin.device_add_permission;
      console.log("dealer_Permission devices => " + this.dealer_Permission);
    }
    if (navParams.get("label") && navParams.get("value")) {
      this.stausdevice = localStorage.getItem('status');
      this.checkIfStat = this.stausdevice;
      this.singleVehicleCount = navParams.get("value");
    } else {
      this.stausdevice = undefined;
    }


    console.log("navparam test: " +navParams.get("param"));
    this.stausdevice = navParams.get("param");
    //this.segmentChanged = navParams.get("param");
    console.log("cheack status and go", this.stausdevice);
    this.checkIfStat = this.stausdevice

    ////////////
    this.events.subscribe("Released:Dismiss", () => {
      this.getdevices();
      // this.getdevicesTemp();
    });
    //this.getDistanceReport();
  }


  callBaseURL() {
    if (localStorage.getItem("ENTERED_BASE_URL") === null) {
      let url = "https://www.oneqlik.in/pullData/getUrlnew";
      this.apiCall.getSOSReportAPI(url)
        .subscribe((data) => {
          console.log("base url: ", data);
          if (data.url) {
            localStorage.setItem("BASE_URL", JSON.stringify(data.url));
          }
          if (data.socket) {
            localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
          }
          this.getSocketUrl();
        });
    }
  }
  socketurl: any;
  getSocketUrl() {
    if (localStorage.getItem('SOCKET_URL') !== null) {
      this.socketurl = JSON.parse(localStorage.getItem('SOCKET_URL'));
      ///////////////=======Push notifications========////////////////
      this.events.subscribe('cart:updated', (count) => {
        this.cartCount = count;
      });

      this.socket = io(this.socketurl + '/notifIOV2?userId=' + this.islogin._id, {
        transports: ['websocket', 'polling']
      });

      this.socket.on('connect', () => {
        // console.log('IO Connected tabs');
        // console.log("socket connected tabs", this.socket.connected)
      });

      this.socket.on(this.islogin._id, (msg) => {
        this.cartCount += 1;
      })
    }
  }
  showDrawer: boolean = false;
  shouldBounce = true;
  dockedHeight = 185;
  distanceTop = 556;
  drawerState = DrawerState.Docked;
  states = DrawerState;
  minimumHeight = 0;

  showBottomDrawer(data) {
    console.log('drawerData', data)
    this.drawerData = data;
    // if(this.showDrawer) {
    //   this.showDrawer = false;
    // }
    //  setTimeout(() => {
    this.showDrawer = true;
    this.drawerState = DrawerState.Docked;
    //  }, 500);
  }

  fonctionTest(d) {
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    const modal = this.modalCtrl.create(TimePickerModal, {
      data: d,
      key: 'parking'
    });
    modal.onDidDismiss((data) => {
      // console.log(data);
      // this.getdevicesTemp123();
      this.getdevices();
    })
    modal.present();
  }

  towAlertCall(d) {
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    const modal = this.modalCtrl.create(TimePickerModal, {
      data: d,
      key: 'tow'
    });

    modal.onDidDismiss((data) => {
      // console.log(data);
      // this.getdevicesTemp123();
      this.getdevices();
    })
    modal.present();
  }

  ngOnInit() {
    this.now = new Date().toISOString();
    this.getDashboarddevices();
    //setInterval (() => {  this.getdevices();}, 10000);
    //this.getDistanceReport();
    // this.getdevicesTemp();
    // this.getDataFromSQLiteDB();

    // this.intervalid123 = setInterval(() => {
    //   this.page = 0;
    //   this.getdevicesTemp123();
    // }, 30000);
    // localStorage.setItem('getDevicesInterval_ID', JSON.stringify(this.intervalid123))
  }

  ngOnDestroy() { }
  hideMe: boolean = false;
  // ionViewDidLoad() {
  //   this.getdevices();
  // }
  ionViewDidEnter() {
    this.getDefaultUserSettings();
    this.navBar.backButtonClick = (ev: UIEvent) => {
      console.log('this will work in Ionic 3 +');
      this.hideMe = true;
      this.navCtrl.pop({
        animate: true, animation: 'transition-ios', direction: 'back'
      });
    }
    this.getdevices();
    this.cancelSearch();
    // this.getdevicesTemp();

    if (localStorage.getItem("SCREEN") != null) {
      this.navBar.backButtonClick = (e: UIEvent) => {
        if (localStorage.getItem("SCREEN") != null) {
          if (localStorage.getItem("SCREEN") === 'live') {
            this.navCtrl.setRoot('LivePage');
          } else {
            if (localStorage.getItem("SCREEN") === 'dashboard') {
              // this.navCtrl.setRoot('DashboardPage')
              this,this.navCtrl.setRoot('AddDevicesPage')
            }
          }
        }
      }
    }

  }
  measurementUnit: string = 'MKS';
  getDefaultUserSettings() {
    var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        this.resp_data=resp;
        console.log("check lang key: ", resp)
        if (resp.unit_measurement !== undefined) {
          this.measurementUnit = resp.unit_measurement;
        } else {
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        }
      },
        err => {
          console.log(err);
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        });
        this.apiCall.getDealerInfo()
        .subscribe(response => {
          this.support_contact=response.support_contact;
            console.log("response demo data -----support_contact>",response.support_contact)
        })
  }


  segmentChanged(ev: any) {
    this.selectedArray = [];
    console.log('Segment changed', ev);
    // if (this.showDrawer) {
    //   this.showDrawer = false;
    // }
    // this.checkIfStat = ev._value;
    // if (this.checkIfStat == 'ALL') {
    //   this.stausdevice = ev._value
    // } else {
    //   this.stausdevice = undefined;
    // }
    // if(ev==="ALL"){
    //   this.allDevicesSearch = this.ndata

    // }
    if(ev ==='Expired'){
      this.isPayNow = true;
    }
    else{
      this.isPayNow = false;
    }
    console.log("ispay now---->",this.isPayNow)
    this.stausdevices = ev
    this.page = 0;
    this.loadProgress = 100;
    this.getdevices();
  }

  getdevices() {
    this.showEmptyStatus = false;
    this.showDrawer = false;
    var baseURLp;

    let mainUrlNew
    if (localStorage.getItem('BASE_URL') != null) {
      mainUrlNew = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
    }
    debugger
    if (this.stausdevices) {
      baseURLp = (mainUrlNew ? mainUrlNew : this.apiCall.mainUrl) + 'devices/getDeviceByUserMobile?email=' + this.islogin.email + '&id=' + this.islogin._id + '&statuss=' + this.stausdevices + '&skip=' + this.page + '&limit=' + this.limit;
    }
    else {
      baseURLp = (mainUrlNew ? mainUrlNew : this.apiCall.mainUrl) + 'devices/getDeviceByUserMobile?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
    }

    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.progressBar();
    if (this.loadProgress == 100) {
      this.loadProgress = 0;
    }
    if(this.stausdevices=="ALL")
    {
      baseURLp =(mainUrlNew ? mainUrlNew : this.apiCall.mainUrl) + 'devices/getDeviceByUserMobile?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
    }
    // this.apiCall.toastMsgStarted();
    // this.apiCall.startLoading().present();
    debugger
    console.log("bbbbbbbbbbbbbbbbbbbbbb------------->",baseURLp)
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        // this.apiCall.stopLoading();
        // this.apiCall.toastMsgDismised();
        console.log("ssssssssss------------->",data);
        let that = this;
        that.loadProgress = 100;
        clearInterval(that.progressIntervalId);
        //this.allDevicesSearch = [];
        that.ndata = data.devices;
        that.allDevices = that.ndata;
        that.allDevicesSearch =  that.ndata;
       //this.selectedArray = [];
                ///// add push token to server //////////////
                this.addPushNotify();
                /////////////////////////////////////////////

        //vehicleid = this.n
        // this.allDevicesSearch = this.ndata;
        if (this.allDevicesSearch.length === 0) {
          this.showEmptyStatus = true;
        }
        that.userPermission = JSON.parse(localStorage.getItem('details'));
        if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
          that.option_switch = true;
        } else {
          if (localStorage.getItem('isDealervalue') == 'true') {
            that.option_switch = true;
          } else {
            if (that.userPermission.isDealer == false) {
              that.option_switch = false;
            }
          }
        }
      },
        err => {
          console.log("error=> ", err);
          let that = this;
          this.showEmptyStatus = false;
          clearInterval(that.progressIntervalId);
        });
  }

  AllData: any = [];
  runningData: any = [];
  stoppedData: any = [];
  idlingData: any = [];
  outOfReachData: any = [];

  getdevicesTemp123() {
    var baseURLp;
    if (this.checkIfStat != undefined && this.checkIfStat !== 'ALL') {
      baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.checkIfStat + '&skip=' + this.page + '&limit=' + this.limit  + '&supAdmin=' + this.islogin._id ;
    } else {
      this.limit = 10;
      baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit  + '&supAdmin=' + this.islogin._id;
    }

    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.progressBar();
    if (this.loadProgress == 100) {
      this.loadProgress = 0;
    }
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.loadProgress = 100;
        this.ndata = data.devices;
        this.allDevices = data.devices;
        console.log("deviceslist", this.allDevices);
        // this.allDevicesSearch = this.ndata;
        let that = this;
        ////////////////////////////////////////////
        this.AllData = [];
        this.runningData = [];
        this.stoppedData = [];
        this.idlingData = [];
        this.outOfReachData = [];
        let expiredDevices = [];
        let nodataDevices = [];
        that.all_devices = [];
        for (var i = 0; i < data.devices.length; i++) {
          // console.log('icontype: ', res.rows.item(i).iconType);
          if(data.devices[i].status === 'ALL'){
            that.AllData.push(data.devices[i]);
          }
           if (data.devices[i].status === 'RUNNING') {
            that.runningData.push(data.devices[i]);
          } else if (data.devices[i].status === 'IDLING') {
            that.idlingData.push(data.devices[i]);
          } else if (data.devices[i].status === 'STOPPED') {
            that.stoppedData.push(data.devices[i]);
          } else if (data.devices[i].status === 'OUT OF REACH') {
            that.outOfReachData.push(data.devices[i]);
          } else if (data.devices[i].status === 'Expired') {
            expiredDevices.push(data.devices[i]);
          } else if (data.devices[i].status === 'NO DATA') {
            nodataDevices.push(data.devices[i]);
          }
          that.all_devices.push(data.devices[i])
           console.log("dddddddddddddd-------",that.all_devices);
        }
        if (this.checkIfStat === 'ALL') {
          this.allDevicesSearch = this.ndata;
        } else if (this.checkIfStat === 'RUNNING') {
          this.allDevicesSearch = this.runningData;
        } else if (this.checkIfStat === 'IDLING') {
          this.allDevicesSearch = this.idlingData;
        } else if (this.checkIfStat === 'OUT OF REACH') {
          this.allDevicesSearch = this.outOfReachData;
        } else if (this.checkIfStat === 'STOPPED') {
          this.allDevicesSearch = this.stoppedData;
        } else if (this.checkIfStat === 'Expired') {
          this.allDevicesSearch = expiredDevices;
        } else if (this.checkIfStat === 'NO DATA') {
          this.allDevicesSearch = nodataDevices;
        }
        console.log('running data: ', that.runningData.length);
        console.log('idling data: ', that.idlingData.length);
        console.log('out of reach data: ', that.outOfReachData.length);
        console.log('stopped data: ', that.stoppedData.length);
        ////////////////////////////////////////////
        that.userPermission = JSON.parse(localStorage.getItem('details'));
        if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
          that.option_switch = true;
        } else {
          if (localStorage.getItem('isDealervalue') == 'true') {
            that.option_switch = true;
          } else {
            if (that.userPermission.isDealer == false) {
              that.option_switch = false;
            }
          }
        }
      },
        err => {
          console.log("error=> ", err);
        });
  }

  activateVehicle(data) {
    // this.navCtrl.push("PaytmwalletloginPage", {
    //   "param": data
    // })
    let that =this;
    let e =""
    if(this.stausdevices !=="Expired"){
      let Exp = "Expired"
      let alert = that.alertCtrl.create({
        message: "GPS subscription for this vehicle expired.Do you want to renew ?",
        buttons: [
          {
            text: "cancel",
            handler: () => {
              (err) => {
                console.log(err);
              };
            },
          },
          {
            text: "Renew",
            handler: () => {
              that.segmentChanged(Exp);
             // that.selected(e,data);
              (err) => {
                console.log(err);
              };
            },
          },

        ],
      });
      alert.present();
    }


  }

  selected(event,d){
  //   console.log("selected----->",d);
  //   this.isPayAlert=true;
  //   event.stopPropagation();
  //   const isSelected = this.selectedArray.findIndex(item => item.Device_ID == d.Device_ID);
  //   console.log("isSelected----->",isSelected);
  //   if (isSelected > -1) {
  //     this.selectedArray = this.selectedArray.filter(item => item.Device_ID != d.Device_ID);
  //   } else {
  //     this.selectedArray.push(d)
  //   }

  //  console.log("datassss--->",this.selectedArray)
  //  console.log("icon type--->",d.iconType);

  //  this.setselect = true
  console.log("selected----abc->",d);
  if(this.stausdevices !=="Expired"){
    this.activateVehicle(d);
  }
  else{
    this.isPayAlert=true;
    event.stopPropagation();
    const isSelected = this.selectedArray.findIndex(item => item.Device_ID == d.Device_ID);
    console.log("isSelected----->",isSelected);
    if (isSelected > -1) {
      this.selectedArray = this.selectedArray.filter(item => item.Device_ID != d.Device_ID);
    } else {
      this.selectedArray.push(d)
    }

   console.log("datassss--->",this.selectedArray)
   console.log("icon type--->",d.iconType);

   this.setselect = true
  }

  }

  isSelected(d) {
    const item = this.selectedArray.filter(item => item.Device_ID == d.Device_ID);

    return item.length > 0;
  }

  activateVehicles(){
    console.log("multipalSelects---->yhfhh");
    if(this.isPayAlert==false){
      let alert = this.alertCtrl.create({
        message: "Please Select Vehicle",
        buttons: [
          {
            text: "ok",
            handler: () => {
              this.ionViewDidEnter();
              (err) => {
                console.log(err);
              };
            },
          },
        ],
      });
      alert.present();
    }
    //

    if(this.isPayAlert){
    if(this.resp_data.paymentgateway == true && this.isPaymentPage.length > 0){
    this.navCtrl.push("PaytmwalletloginPage",{
      'param':this.selectedArray});
    }
    else{
      this.navCtrl.push(PaymantgatweyPage);
    }

  }

  }
  supportCall(){
    let number = this.ddata;
    console.log("check phone number: ", number)
    if (number !== "" || number !== undefined) {
      if (this.plt.is('android')) {
        console.log("is android")
        window.open('tel:' + number, '_system');
      } else if (this.plt.is('ios')) {
        console.log("is ios")
        // alert(number)
        this.callNumber.callNumber(number.toString(), true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
      }
    } else {
      this.toastCtrl.create({
        message: 'Contact number not found!',
        position: 'middle',
        duration: 2000
      }).present();
    }
  }

  addPushNotify() {
    console.log("VeryFirstLoginUser: ", localStorage.getItem("VeryFirstLoginUser"));
    let that = this;
    that.token = "";
    var pushdata;
    that.token = localStorage.getItem("DEVICE_TOKEN");
    var v_id = localStorage.getItem("VeryFirstLoginUser");
    if (that.token == null) {
      this.pushSetup();
    } else {
      if (this.plt.is('android')) {
        pushdata = {
          "uid": that.islogin._id,
          "token": that.token,
          "os": "android"
        }
      } else {
        pushdata = {
          "uid": that.islogin._id,
          "token": that.token,
          "os": "ios"
        }
      }

      if (v_id == that.islogin._id) {
        that.apiCall.pushnotifyCall(pushdata)
          .subscribe(data => {
            console.log("push notifications updated " + data.message)
          },
            error => {
              console.log(error);
            });
      }
      else {
        console.log("Token already updated!!")
      }
    }
  }

  pushSetup() {
    this.push.createChannel({
      id: "ignitionon",
      description: "ignition on description",
      sound: 'ignitionon',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('Channel created'));
    this.push.createChannel({
      id: "ignitionoff",
      description: "ignition off description",
      sound: 'ignitionoff',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('ignitionoff Channel created'));

    this.push.createChannel({
      id: "devicepowerdisconnected",
      description: "devicepowerdisconnected description",
      sound: 'devicepowerdisconnected',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('devicepowerdisconnected Channel created'));

    this.push.createChannel({
      id: "default",
      description: "default description",
      sound: 'default',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('default Channel created'));

    this.push.createChannel({
      id: "notif",
      description: "default description",
      sound: 'notif',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('sos Channel created'));
    // Delete a channel (Android O and above)
    // this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));

    // Return a list of currently configured channels
    this.push.listChannels().then((channels) => console.log('List of channels', channels))
    let that = this;
    const options: PushOptions = {
      android: {
        senderID: '644983599736',
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = that.push.init(options);

    pushObject.on('notification').subscribe((notification: any) => {
      if (notification.additionalData.foreground) {
        const toast = this.toastCtrl.create({
          message: notification.message,
          duration: 2000
        });
        toast.present();
      }
    });

    pushObject.on('registration')
      .subscribe((registration: any) => {
        console.log("dashboard entered device token => " + registration.registrationId);
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
        that.addPushNotify();
      });

    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin', error)
    });
  }

  timeoutAlert() {
    let alerttemp = this.alertCtrl.create({
      message: "the server is taking much time to respond. Please try in some time.",
      buttons: [{
        text: this.translate.instant('Okay'),
        handler: () => {
          this.navCtrl.setRoot("AddDevicesPage");
        }
      }]
    });
    alerttemp.present();
  }

  showVehicleDetails(vdata) {

    this.navCtrl.push('VehicleDetailsPage', {
      param: vdata,
      option_switch: this.option_switch
    });
    this.showDrawer = false;
  }

  doRefresh(refresher) {
    let that = this;
    that.page = 0;
    that.limit = 5;
    console.log('Begin async operation', refresher);
    this.getdevices();
    this.getdevicesTemp123();
    refresher.complete();
  }

  shareVehicle(d_data) {
    let that = this;
    const prompt = this.alertCtrl.create({
      title: 'Share Vehicle',
      inputs: [
        {
          name: 'device_name',
          value: d_data.Device_Name
        },
        {
          name: 'shareId',
          placeholder: this.translate.instant('Enter Email Id/Mobile Number')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          handler: () => {
          }
        },
        {
          text: this.translate.instant('Share'),
          handler: (data) => {
            that.sharedevices(data, d_data)

          }
        }
      ]
    });
    prompt.present();
  }

  sharedevices(data, d_data) {
    let that = this;
    var devicedetails = {
      "did": d_data._id,
      "email": data.shareId,
      "uid": that.islogin._id
    }

    that.apiCall.startLoading().present();
    that.apiCall.deviceShareCall(devicedetails)
      .subscribe(data => {
        that.apiCall.stopLoading();
        let toast = that.toastCtrl.create({
          message: data.message,
          position: 'bottom',
          duration: 2000
        });

        toast.present();
      },
        err => {
          that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
  }

  showDeleteBtn(b) {
    // debugger;
    let that = this;
    if (localStorage.getItem('isDealervalue') == 'true') {
      return false;
    } else {
      if (b) {
        var u = b.split(",");
        for (let p = 0; p < u.length; p++) {
          if (that.islogin._id == u[p]) {
            return true;
          }
        }
      }
      else {
        return false;
      }
    }

  }

  sharedVehicleDelete(device) {
    let that = this;
    that.deivceId = device;
    let alert = that.alertCtrl.create({
      message: this.translate.instant('Do you want to delete this share vehicle ?'),
      buttons: [{
        text: this.translate.instant('YES PROCEED'),
        handler: () => {
          that.removeDevice(that.deivceId._id);
        }
      },
      {
        text: this.translate.instant('NO')
      }]
    });
    alert.present();
  }

  callObjFunc(d) {
    let that = this;
    var _bUrl = that.apiCall.mainUrl + 'devices/getDevicebyId?deviceId=' + d.Device_ID;
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(_bUrl)
      .subscribe(resp => {
        this.apiCall.stopLoading();
        console.log("updated device object=> " + resp);
        if (!resp) {
          return;
        } else {
          that.drawerData = resp;
        }
      })
  }
  removeDevice(did) {
    this.apiCall.startLoading().present();
    this.apiCall.dataRemoveFuncCall(this.islogin._id, did)
      .subscribe(data => {
        console.log(data)
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Shared Device was deleted successfully!'),
          duration: 1500
        });
        toast.onDidDismiss(() => {
          // this.getdevices();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        });
  }
  seeNotifications() {
    this.navCtrl.push('AllNotificationsPage',
    {param:this.allDevicesSearch});
    console.log("add device components");

  }
  showSharedBtn(a, b) {
    // debugger
    if (b) {
      return !(b.split(",").indexOf(a) + 1);
    }
    else {
      return true;
    }
  }

  presentPopover(ev, data) {
    console.log("populated=> " + JSON.stringify(data))
    let popover = this.popoverCtrl.create(PopoverPage,
      {
        vehData: data
      },
      {
        cssClass: 'contact-popover'
      });

    popover.onDidDismiss(() => {
      this.getdevices();
    })

    popover.present({
      ev: ev
    });
  }

  getDashboarddevices() {
    console.log("getdevices");
    var _baseUrl = this.apiCall.mainUrl + 'gps/getDashboard?email=' + this.islogin.email + '&from=' + this.from + '&to=' + this.to + '&id=' + this.islogin._id;

    if (this.islogin.isSuperAdmin == true) {
      _baseUrl += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        _baseUrl += '&dealer=' + this.islogin._id;
      }
    }
    this.progressBar();
    if (this.loadProgress == 100) {
      this.loadProgress = 0;
    }
    this.apiCall.dashboardcall(_baseUrl)
      .subscribe(data => {
        console.log("bordddddddd----->",data);
        let that = this;
        that.loadProgress = 100;
        clearInterval(that.progressIntervalId);

        this.totaldevices = data.Total_Vech;
        ///// add push token to server //////////////
        this.addPushNotify();
        /////////////////////////////////////////////

        this.TotalDevice = [];
        this.devices = data;
        console.log("devicelist", this.devices);
        this.Running = this.devices.running_devices;
        this.TotalDevice.push(this.Running)
        this.IDLING = this.devices["Ideal Devices"]
        this.TotalDevice.push(this.IDLING);
        this.Stopped = this.devices["OFF Devices"]
        this.TotalDevice.push(this.Stopped);
        // this.Maintance = this.devices["Maintance Device"]
        // this.TotalDevice.push(this.Maintance);
        this.OutOffReach = this.devices["OutOfReach"]
        this.TotalDevice.push(this.OutOffReach);
        this.NogpsDevice = this.devices["no_gps_fix_devices"]
        this.TotalDevice.push(this.NogpsDevice);
        this.expiredDevices = this.devices["expire_status"];
        this.TotalDevice.push(this.expiredDevices);
        this.nodata = this.devices["no_data"]
        console.log("no data devices", this.nodata);
        this.TotalDevice.push(this.devices["no_data"]);
        this.totalVechiles = this.Running + this.IDLING + this.Stopped + this.OutOffReach + this.NogpsDevice + this.expiredDevices + this.devices["no_data"];
        console.log("data we want to populate", this.TotalDevice);
      },
        err => {
          console.log(err);
          let that = this;
          clearInterval(that.progressIntervalId);
        });
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.page = that.page + 1;
    setTimeout(() => {
      var baseURLp;
      if (that.stausdevices) {
         // If status all
         if(that.stausdevices==='ALL'){
           baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email  + '&skip=' + that.page + '&limit=' + that.limit;
         }else{
          baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevices + '&skip=' + that.page + '&limit=' + that.limit;
         }
      }
      else {
        baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit ;
      }

      if (this.islogin.isSuperAdmin == true) {
        baseURLp += '&supAdmin=' + this.islogin._id;
      } else {
        if (this.islogin.isDealer == true) {
          baseURLp += '&dealer=' + this.islogin._id;
        }
      }
      that.ndata = [];
      that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
        .subscribe(
          res => {
            if (res.devices.length > 0) {
              that.ndata = res.devices;
              for (let i = 0; i < that.ndata.length; i++) {
                // that.allDevices.push(that.ndata[i]);
                that.allDevicesSearch.push(that.ndata[i]);
              }
              // that.allDevicesSearch = that.allDevices;

            }
            infiniteScroll.complete();
          },
          error => {
            console.log(error);
          });

    }, 100);
  }

  progressBar() {
    // Test interval to show the progress bar
    this.progressIntervalId = setInterval(() => {
      if (this.loadProgress < 100) {
        this.loadProgress += 1;
        // console.log("progress: ", this.loadProgress);
      }
      else {
        clearInterval(this.loadProgress);
      }
    }, 100);
  }

  livetrack(device) {
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    localStorage.setItem("LiveDevice", "LiveDevice");
    let animationsOptions;
    if (this.plt.is('android')) {
      this.navCtrl.push('LiveSingleDevice', { device: device });
    } else {
      if (this.plt.is('ios')) {
        animationsOptions = {
          animation: 'ios-transition',
          duration: 1000
        }
        this.navCtrl.push('LiveSingleDevice', { device: device }, animationsOptions);
      }
    }
  }

  showHistoryDetail(device) {

    this.navCtrl.push('HistoryDevicePage', {
      device: device
    });
    if (this.showDrawer) {
      this.showDrawer = false;
    }
  }
  device_address(device, index) {
    let that = this;
    let tempcord = {};
    if (!device.last_location) {
      that.allDevicesSearch[index].address = "N/A";
      if (!device.last_lat && !device.last_lng) {
        that.allDevicesSearch[index].address = "N/A";
      } else {
        tempcord = {
          "lat": device.last_lat,
          "long": device.last_lng
        }
        this.apiCall.getAddress(tempcord)
          .subscribe(res => {
            if (res.message === "Address not found in databse") {
              this.geocoderApi.reverseGeocode(device.last_lat, device.last_lng)
                .then(res => {
                  var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                  that.saveAddressToServer(str, device.last_lat, device.last_lng);
                  that.allDevicesSearch[index].address = str;
                  // console.log("inside", that.address);
                })
            } else {
              that.allDevicesSearch[index].address = res.address;
            }
          })
      }
    } else {
      tempcord = {
        "lat": device.last_location.lat,
        "long": device.last_location.long
      }
      this.apiCall.getAddress(tempcord)
        .subscribe(res => {
          if (res.message === "Address not found in databse") {
            this.geocoderApi.reverseGeocode(device.last_location.lat, device.last_location.long)
              .then(res => {
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, device.last_location.lat, device.last_location.long);
                // that.allDevices[index].address = str;
                that.allDevicesSearch[index].address = str;
                // console.log("inside", that.address);
              })
          } else {
            // that.allDevices[index].address = res.address;
            that.allDevicesSearch[index].address = res.address;
          }
        })
    }
  }

  // device_address(device, index) {
  //   let that = this;
  //   if (!device.last_location) {
  //     that.allDevices[index].address = "N/A";
  //     return;
  //   }
  //   let tempcord = {
  //     "lat": device.last_location.lat,
  //     "long": device.last_location.long
  //   }
  //   this.apiCall.getAddress(tempcord)
  //     .subscribe(res => {
  //       if (res.message === "Address not found in databse") {
  //         this.geocoderApi.reverseGeocode(device.last_location.lat, device.last_location.long)
  //           .then(res => {
  //             var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
  //             that.saveAddressToServer(str, device.last_location.lat, device.last_location.long);
  //             that.allDevices[index].address = str;
  //             // console.log("inside", that.address);
  //           })
  //       } else {
  //         that.allDevices[index].address = res.address;
  //       }
  //     })
  // }
  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apiCall.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

  getDuration(device, index) {
    let that = this;
    that.allDevicesSearch[index].duration = "0hrs 0mins";
    if (!device.status_updated_at) {
      // console.log("did not found last ping on")
      that.allDevicesSearch[index].duration = "0hrs 0mins";
    } else if (device.status_updated_at) {
      // console.log("found last ping on")
      var a = moment(new Date().toISOString());//now
      var b = moment(new Date(device.status_updated_at).toISOString());
      var mins;
      mins = a.diff(b, 'minutes') % 60;
      that.allDevicesSearch[index].duration = a.diff(b, 'hours') + 'hrs ' + mins + 'mins';
    }
  }

  // getDuration(device, index) {
  //   let that = this;
  //   that.allDevices[index].duration = "0hrs 0mins";
  //   if (!device.status_updated_at) {
  //     // console.log("did not found last ping on")
  //     that.allDevices[index].duration = "0hrs 0mins";
  //   } else if (device.status_updated_at) {
  //     // console.log("found last ping on")
  //     var a = moment(new Date().toISOString());//now
  //     var b = moment(new Date(device.status_updated_at).toISOString());
  //     var mins;
  //     mins = a.diff(b, 'minutes') % 60;
  //     that.allDevices[index].duration = a.diff(b, 'hours') + 'hrs ' + mins + 'mins';
  //   }
  // }

  callSearch(ev) {
    var searchKey = ev.target.value;
    var _baseURL;

    if (this.islogin.isDealer == true) {
      _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&dealer=" + this.islogin._id;
    } else {
      if (this.islogin.isSuperAdmin == true) {
        _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&supAdmin=" + this.islogin._id;
      } else {
        _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey;
      }
    }

    this.apiCall.callSearchService(_baseURL)
      .subscribe(data => {
        // this.apiCall.stopLoading();
        console.log("search result=> " + JSON.stringify(data))
        this.allDevicesSearch = data.devices;
        this.allDevices = data.devices;
        // console.log("fuel percentage: " + data.devices[0].fuel_percent)
      },
        err => {
          console.log(err);
          // this.apiCall.stopLoading();
        });
  }

  getDistanceReport(device, i) {

    //this.apiCall.startLoading().present();
    console.log("device id", device._id);
    this.devicedetail = device._id;
    var distanceReport = [];
    var user = device.user;
    if (typeof user === 'object') {
      var user_id = user._id;
    } else {
      var user_id = user;
    }
    // this.apiCall.getDistanceReportApi(new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), user_id, device._id)
    //   .subscribe(data => {

    //     console.log("dataa", data)
    //     distanceReport = data;
    //     var dd123 = (distanceReport.length > 0) ? parseFloat(distanceReport[0].distance).toFixed(2) : 0;
    //     console.log("dataaaaaa", dd123);

    //     device['distance'] = dd123;
    //     // this.apiCall.stopLoading();
    //   }, error => {
    //     // this.apiCall.stopLoading();
    //     device['distance'] = 0;
    //     console.log(error);
    //   })
    let _bUrl = this.apiCall.mainUrl + 'user_trip/StoppageLogs?date=' + this.selectedDate + '&device=' + this.devicedetail._id;
    this.apiCall.getSOSReportAPI(_bUrl)
      .subscribe(resp => {
        console.log("got response:- " + resp.result)
        // this.to_running = this.parseMillisecondsIntoReadableTime(resp.today_running);
        // this.to_stopped = this.parseMillisecondsIntoReadableTime(resp.today_stopped);
        this.to_odo = this.meterIntoKelometer(resp.todays_odo);
      },
        err => {
          console.log(" got error: ", err);
        });
  }

  meterIntoKelometer(value) {
    var km = value / 1000;
    return km.toFixed(1);
    // alert(km.toFixed(1) + " km"); // 1613.8 km
  }

  innerFunc(distanceReport) {
    let outerthis = this;
    var i = 0, howManyTimes = distanceReport.length;
    function f() {

      outerthis.allDevicesSearch.push({
        'distance': distanceReport[i].distance,
      });
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
    console.log("final data", this.allDevicesSearch);
  }

  details(key) {
    console.log('cheack key', key)
    this.navCtrl.push('AddDevicesPage', {param: key});
}

  checkImmobilizePassword() {
    const rurl = this.apiCall.mainUrl + 'users/get_user_setting';
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(rurl, Var)
      .subscribe(data => {
        if (!data.engine_cut_psd) {
          this.checkedPass = 'PASSWORD_NOT_SET';
        } else {
          this.checkedPass = 'PASSWORD_SET';
        }
      })
  }

  checkPointsAvailability() {
    var url = this.apiCall.mainUrl + "users/checkPointAvailablity?u_id=" + this.islogin._id + "&demanded_points=0";
    this.apiCall.getSOSReportAPI(url)
      .subscribe((resp) => {
        console.log("points availability: ", resp);
      },
        err => {
          console.log("error ", err);
        })
  }

  // IgnitionOnOff(d) {
  //   // debugger
  //   if (this.showDrawer) {
  //     this.showDrawer = false;
  //   }
  //   let pModal = this.modalCtrl.create(ImmobilizeModelPage, {
  //     param: d
  //   });
  //   pModal.onDidDismiss(() => {
  //     this.getdevices();
  //   })
  //   pModal.present();

  // };

  IgnitionOnOff(d) {
    let that = this;
    debugger
    if (d.last_ACC != null || d.last_ACC != undefined) {
      if (that.clicked) {
        let alert = this.alertCtrl.create({
          message: 'Process ongoing..',
          buttons: ['Okay']
        });
        alert.present();
      } else {
        this.messages = undefined;
        this.dataEngine = d;
        var baseURLp = 'https://www.oneqlik.in/deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
        this.apiCall.startLoading().present();
        this.apiCall.ignitionoffCall(baseURLp)
          .subscribe(data => {
            this.apiCall.stopLoading();
            this.DeviceConfigStatus = data;
            this.immobType = data[0].imobliser_type;
            if (this.dataEngine.ignitionLock == '1') {
              this.messages = 'Do you want to unlock the engine?'
            } else {
              if (this.dataEngine.ignitionLock == '0') {
                this.messages = 'Do you want to lock the engine?'
              }
            }
            let alert = this.alertCtrl.create({
              message: this.messages,
              buttons: [{
                text: 'YES',
                handler: () => {
                  if (this.immobType == 0 || this.immobType == undefined) {
                    that.clicked = true;
                    var devicedetail = {
                      "_id": this.dataEngine._id,
                      "engine_status": !this.dataEngine.engine_status
                    }
                    // this.apiCall.startLoading().present();
                    this.apiCall.deviceupdateCall(devicedetail)
                      .subscribe(response => {
                        // this.apiCall.stopLoading();
                        this.editdata = response;
                        const toast = this.toastCtrl.create({
                          message: response.message,
                          duration: 2000,
                          position: 'top'
                        });
                        toast.present();
                        this.responseMessage = "Edit successfully";
                        this.getdevices();

                        var msg;
                        if (!this.dataEngine.engine_status) {
                          msg = this.DeviceConfigStatus[0].resume_command;
                        }
                        else {
                          msg = this.DeviceConfigStatus[0].immoblizer_command;
                        }

                        this.sms.send(d.sim_number, msg);
                        const toast1 = this.toastCtrl.create({
                          message: 'SMS sent successfully',
                          duration: 2000,
                          position: 'bottom'
                        });
                        toast1.present();
                        that.clicked = false;
                      },
                        error => {
                          that.clicked = false;
                          // this.apiCall.stopLoading();
                          console.log(error);
                        });
                  } else {
                    console.log("Call server code here!!")
                    that.clicked = true;
                    var data = {
                      "imei": d.Device_ID,
                      "_id": this.dataEngine._id,
                      "engine_status": d.ignitionLock,
                      "protocol_type": d.device_model.device_type
                    }
                    // this.apiCall.startLoading().present();
                    this.apiCall.serverLevelonoff(data)
                      .subscribe(resp => {
                        // this.apiCall.stopLoading();
                        console.log("ignition on off=> ", resp)
                        this.respMsg = resp;

                        // this.apiCall.startLoadingnew(this.dataEngine.ignitionLock).present();
                        this.intervalID = setInterval(() => {
                          this.apiCall.callResponse(this.respMsg._id)
                            .subscribe(data => {
                              console.log("interval=> " + data)
                              this.commandStatus = data.status;

                              if (this.commandStatus == 'SUCCESS') {
                                clearInterval(this.intervalID);
                                that.clicked = false;
                                // this.apiCall.stopLoadingnw();
                                const toast1 = this.toastCtrl.create({
                                  message: 'process has completed successfully!',
                                  duration: 1500,
                                  position: 'bottom'
                                });
                                toast1.present();
                                this.getdevices();
                              }
                            })
                        }, 5000);
                      },
                        err => {
                          this.apiCall.stopLoading();
                          console.log("error in onoff=>", err);
                          that.clicked = false;
                        });
                  }
                }
              },
              {
                text: 'No'
              }]
            });
            alert.present();
          },
            error => {
              this.apiCall.stopLoading();
              console.log("some error: ", error._body.message);
            });
      }
    }

  };

  // askForPassword(d) {
  //   const prompt = this.alertCtrl.create({
  //     title: 'Enter Password',
  //     message: "Enter password for engine cut",
  //     inputs: [
  //       {
  //         name: 'password',
  //         placeholder: 'Password'
  //       }
  //     ],
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         handler: data => {
  //           console.log('Cancel clicked');
  //         }
  //       },
  //       {
  //         text: 'Proceed',
  //         handler: data => {
  //           console.log('Saved clicked');
  //           console.log("data: ", data)
  //           // if (data.password !== data.cpassword) {
  //           //   this.toastmsg("Entered password and confirm password did not match.")
  //           //   return;
  //           // }
  //           this.verifyPassword(data, d);
  //         }
  //       }
  //     ]
  //   });
  //   prompt.present();
  // }

  // verifyPassword(pass, d) {
  //   const ryurl = this.apiCall.mainUrl + "users/verify_EngineCut_Password";
  //   var payLd = {
  //     "uid": this.islogin._id,
  //     "psd": pass.password
  //   }
  //   this.apiCall.startLoading().present();
  //   this.apiCall.urlpasseswithdata(ryurl, payLd)
  //     .subscribe(resp => {
  //       this.apiCall.stopLoading();
  //       console.log(resp);
  //       if (resp.message === 'password not matched') {
  //         this.toastmsg(resp.message)
  //         return;
  //       }
  //       this.serverLevelOnOff(d);
  //     },
  //       err => {
  //         this.apiCall.stopLoading();
  //       });
  // }

  toastmsg(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    }).present();
  }

  // serverLevelOnOff(d) {
  //   let that = this;
  //   that.clicked = true;
  //   var data = {
  //     "imei": d.Device_ID,
  //     "_id": this.dataEngine._id,
  //     "engine_status": d.ignitionLock,
  //     "protocol_type": d.device_model.device_type
  //   }
  //   // this.apiCall.startLoading().present();
  //   this.apiCall.serverLevelonoff(data)
  //     .subscribe(resp => {
  //       // this.apiCall.stopLoading();
  //       console.log("ignition on off=> ", resp)
  //       this.respMsg = resp;

  //       // this.apiCall.startLoadingnew(this.dataEngine.ignitionLock).present();
  //       this.intervalID = setInterval(() => {
  //         this.apiCall.callResponse(this.respMsg._id)
  //           .subscribe(data => {
  //             console.log("interval=> " + data)
  //             this.commandStatus = data.status;

  //             if (this.commandStatus == 'SUCCESS') {
  //               clearInterval(this.intervalID);
  //               that.clicked = false;
  //               // this.apiCall.stopLoadingnw();
  //               const toast1 = this.toastCtrl.create({
  //                 message: this.translate.instant('process has been completed successfully!'),
  //                 duration: 1500,
  //                 position: 'bottom'
  //               });
  //               toast1.present();
  //               // this.getdevices();
  //             }
  //           })
  //       }, 5000);
  //     },
  //       err => {
  //         this.apiCall.stopLoading();
  //         console.log("error in onoff=>", err);
  //         that.clicked = false;
  //       });
  // }

  dialNumber(number) {
    if (this.showDrawer) {
      this.showDrawer = false;
    }
    console.log("check phone number: ", number)
    if (number !== "" || number !== undefined) {
      if (this.plt.is('android')) {
        console.log("is android")
        window.open('tel:' + number, '_system');
      } else if (this.plt.is('ios')) {
        console.log("is ios")
        // alert(number)
        this.callNumber.callNumber(number.toString(), true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
      }
    } else {
      this.toastCtrl.create({
        message: 'Contact number not found!',
        position: 'middle',
        duration: 2000
      }).present();
    }
  }

  getItems(ev: any) {
    const val = ev.target.value.trim();
    this.allDevicesSearch = this.allDevices.filter((item) => {
      return (item.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    });
    console.log("search====", this.allDevicesSearch);
  }

  onClear(ev) {
    this.getdevices();
    // this.getdevicesTemp();
    ev.target.value = '';
    // this.toggled = false;
  }

  openAdddeviceModal(cust) {
    let profileModal = this.modalCtrl.create('AddDeviceModalPage');
    //NavParams: this.allDevicesSearch;
    profileModal.onDidDismiss(data => {
      console.log(data);
      this.getdevices();
    });
    profileModal.present();
  }

  // IgnitionOnOff() {
  //   let pModal = this.modalCtrl.create('ImmobilizeModelPage');
  //   pModal.onDidDismiss(data => {
  //     console.log(data);
  //   })
  // }

  upload(vehData) {
    this.navCtrl.push('UploadDocPage', { vehData: vehData });
  }
  iconCheck(status, iconType) {

    var devStatus = status.split(" ");

    if ((iconType == 'car') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/car_blue_icon.png";
      return this.devIcon;
    } else if ((iconType == 'car') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/car_green_icon.png";
      return this.devIcon;
    } else if ((iconType == 'car') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/car_red_icon.png";
      return this.devIcon;
    } else if ((iconType == 'car') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/car_yellow_icon.png";
      return this.devIcon;
    } else if ((iconType == 'car') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/car_grey_icon.png";
      return this.devIcon;
    }
    else if ((iconType == 'bike') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/bike_blue_icon.png";
      return this.devIcon;
    } else if ((iconType == 'bike') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/bike_green_icon.png";
      return this.devIcon;
    } else if ((iconType == 'bike') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/bike_red_icon.png";
      return this.devIcon;
    } else if ((iconType == 'bike') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/bike_yellow_icon.png";
      return this.devIcon;
    } else if ((iconType == 'bike') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/bike_grey_icon.png";
      return this.devIcon;
    }
    else if ((iconType == 'bus') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/bus_blue.png";
      return this.devIcon;
    } else if ((iconType == 'bus') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/bus_green.png";
      return this.devIcon;
    } else if ((iconType == 'bus') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/bus_red.png";
      return this.devIcon;
    } else if ((iconType == 'bus') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/bus_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'bus') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/bus_gray.png";
      return this.devIcon;
    }
    else if ((iconType == 'truck') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/truck_icon_blue.png";
      return this.devIcon;
    } else if ((iconType == 'truck') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/truck_icon_green.png";
      return this.devIcon;
    } else if ((iconType == 'truck') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/truck_icon_red.png";
      return this.devIcon;
    } else if ((iconType == 'truck') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/truck_icon_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'truck') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/truck_icon_grey.png";
      return this.devIcon;
    }
    else if ((iconType == 'tractor') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/tractor_blue.png";
      return this.devIcon;
    } else if ((iconType == 'tractor') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/tractor_green.png";
      return this.devIcon;
    } else if ((iconType == 'tractor') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/tractor_red.png";
      return this.devIcon;
    } else if ((iconType == 'tractor') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/tractor_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'tractor') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/tractor_gray.png";
      return this.devIcon;
    }
    else if ((!iconType) && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/car_blue_icon.png";
      return this.devIcon;
    } else if ((!iconType) && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/car_green_icon.png";
      return this.devIcon;
    } else if ((!iconType) && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/car_red_icon.png";
      return this.devIcon;
    } else if ((!iconType) && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/car_yellow_icon.png";
      return this.devIcon;
    } else if ((!iconType) && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/car_grey_icon.png";
      return this.devIcon;
    }
    else if ((iconType == 'user') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    } else if ((iconType == 'user') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    } else if ((iconType == 'user') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    } else if ((iconType == 'user') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    } else if ((iconType == 'user') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/user.png";
      return this.devIcon;
    }
    else if ((iconType == 'jcb') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/jcb_blue.png";
      return this.devIcon;
    } else if ((iconType == 'jcb') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/jcb_green.png";
      return this.devIcon;
    } else if ((iconType == 'jcb') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/jcb_red.png";
      return this.devIcon;
    } else if ((iconType == 'jcb') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/jcb_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'jcb') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/jcb_gray.png";
      return this.devIcon;
    }
    else if ((iconType == 'ambulance') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/ambulance_blue.png";
      return this.devIcon;
    } else if ((iconType == 'ambulance') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/ambulance_green.png";
      return this.devIcon;
    } else if ((iconType == 'ambulance') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/ambulance_red.png";
      return this.devIcon;
    } else if ((iconType == 'ambulance') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/ambulance_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'ambulance') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/ambulance_gray.png";
      return this.devIcon;
    }
    else if ((iconType == 'auto') && (devStatus[0] == "OUT")) {
      this.devIcon = "../../assets/imgs/auto_blue.png";
      return this.devIcon;
    } else if ((iconType == 'auto') && (devStatus[0] == "RUNNING")) {
      this.devIcon = "../../assets/imgs/auto_green.png";
      return this.devIcon;
    } else if ((iconType == 'auto') && (devStatus[0] == "STOPPED")) {
      this.devIcon = "../../assets/imgs/auto_red.png";
      return this.devIcon;
    } else if ((iconType == 'auto') && (devStatus[0] == "IDLING")) {
      this.devIcon = "../../assets/imgs/auto_yellow.png";
      return this.devIcon;
    } else if ((iconType == 'auto') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
      this.devIcon = "../../assets/imgs/auto_gray.png";
      return this.devIcon;
    }
    else {
      this.devIcon = "../../assets/imgs/noIcon.png";
      return this.devIcon
    }

  }
}

@Component({
  template: `
    <ion-list>
      <ion-item class="text-palatino" (click)="editItem()">
        <ion-icon name="create"></ion-icon>&nbsp;&nbsp;{{'edit' | translate}}
      </ion-item>
      <ion-item class="text-san-francisco" (click)="deleteItem()">
        <ion-icon name="trash"></ion-icon>&nbsp;&nbsp;{{'delete' | translate}}
      </ion-item>
      <ion-item class="text-seravek" (click)="shareItem()">
        <ion-icon name="share"></ion-icon>&nbsp;&nbsp;{{'share' | translate}}
      </ion-item>
    </ion-list>
  `
})


export class PopoverPage implements OnInit {
  contentEle: any;
  textEle: any;
  vehData: any;
  islogin: any;
  veh: any;
  constructor(
    navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public translate: TranslateService
  ) {
    this.vehData = navParams.get("vehData");
    console.log("popover data=> ", this.vehData);

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + this.islogin);
  }

  ngOnInit() { }

  editItem() {
    console.log("edit")
    let modal = this.modalCtrl.create('UpdateDevicePage', {
      vehData: this.vehData
    });
    modal.onDidDismiss(() => {
      console.log("modal dismissed!")
      this.viewCtrl.dismiss();
      // this.getdevices();
    })
    modal.present();
  }

  deleteItem() {
    let that = this;
    console.log("delete")
    let alert = this.alertCtrl.create({
      message: this.translate.instant('Do you want to delete this vehicle ?'),
      buttons: [{
        text: this.translate.instant('YES PROCEED'),
        handler: () => {
          console.log(that.vehData.Device_ID)
          that.deleteDevice(that.vehData.Device_ID);
        }
      },
      {
        text: this.translate.instant('NO')
      }]
    });
    alert.present();
  }


  deleteDevice(d_id) {
    this.apiCall.startLoading().present();
    this.apiCall.deleteDeviceCall(d_id)
      .subscribe(data => {
        this.apiCall.stopLoading();
        var DeletedDevice = data;
        console.log(DeletedDevice);

        let toast = this.toastCtrl.create({
          message: this.translate.instant('Vehicle deleted successfully!'),
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          this.viewCtrl.dismiss();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
  }

  shareItem() {
    let that = this;
    console.log("share")
    const prompt = this.alertCtrl.create({
      title: this.translate.instant('Share Vehicle'),
      // message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'device_name',
          value: that.vehData.Device_Name
        },
        {
          name: 'shareId',
          placeholder: this.translate.instant('Enter Email Id/Mobile Number')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('Share'),
          handler: data => {
            console.log('Saved clicked');
            console.log("clicked=> ", data)

            that.sharedevices(data)

          }
        }
      ]
    });
    prompt.present();
  }

  // valScreen() {
  //   let that = this
  //   console.log("selected=>", that.vehData);
  //   localStorage.selectedVal = JSON.stringify(that.vehData);
  //   this.navCtrl.push('DailyReportNewPage');
  //   // if(this.vehData == "Device_Name" ) {
  //   //   console.log(this.vehData);
  //   //   this.navCtrl.push('DailyReportNewPage');
  //   // }
  // }


  sharedevices(data) {
    let that = this;
    console.log(data.shareId);
    var devicedetails = {
      "did": that.vehData._id,
      "email": data.shareId,
      "uid": that.islogin._id
    }

    that.apiCall.startLoading().present();
    that.apiCall.deviceShareCall(devicedetails)
      .subscribe(data => {
        that.apiCall.stopLoading();
        // var editdata = data;
        let toast = that.toastCtrl.create({
          message: data.message,
          position: 'bottom',
          duration: 2000
        });

        toast.present();
      },
        err => {
          that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
  }
}

@Component({
  templateUrl: "./paymantgatwey-false.html",
  styles: [
    `
      .col {
        padding: 0px;
      }
    `,
  ],
})
export class PaymantgatweyPage implements OnInit {
  fdate: any;
  tdate: any;
  uid: any;
  paramData: any = {};
  ddata: any = [];
  vname: any;
  customerId: any;
  paymentHistory: any[] = [];
  date: any;
  expDate: any[] = [];
  support_contact:any;
  islogin:any;
  constructor(public apiCall: ApiServiceProvider, public navParam: NavParams,public navCtrl: NavController, private callNumbers: CallNumber,public plt: Platform,public toastCtrl: ToastController,) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
  }

  ngOnInit() {
    this.getDetails();
  }

  callNumber(number){
    console.log("check phone number: ", number)
    if (number !== "" || number !== undefined) {
      if (this.plt.is('android')) {
        console.log("is android")
        window.open('tel:' + number, '_system');
      } else if (this.plt.is('ios')) {
        console.log("is ios")
        // alert(number)
        this.callNumbers.callNumber(number.toString(), true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
      }
    } else {
      this.toastCtrl.create({
        message: 'Contact number not found!',
        position: 'middle',
        duration: 2000
      }).present();
    }
  }
  getDetails() {
    // let isSuperAdmin = JSON.parse(localStorage.getItem("details"));
    // this.apiCall.getCustomerPayment(isSuperAdmin._id).subscribe((respData) => {
    //   this.paymentHistory = respData;
    //   this.paymentHistory.map((item) => {
    //     item.createdAt = moment(new Date(item.createdAt), "DD-MM-YYYY").format(
    //       "YYYY-MM-DD"
    //     );
    //     return item;
    //   });
    // });


    var url = this.apiCall.mainUrl + "users/get_user_setting";
    var payload = {
      uid: this.islogin._id,
    };
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdatatwo(url, payload).subscribe(
      (respData) => {
        this.ddata = respData;
        this.ddata = JSON.parse(this.ddata._body).Service;
        this.apiCall.stopLoading();
        console.log("respData of expiry date updation:-------- ",this.ddata);
      },
      (err) => {
        console.log("oops got error: ", err);
        this.apiCall.stopLoading();
      }
    );
  }
}
