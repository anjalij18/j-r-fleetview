import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDocPage } from './add-doc';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AddDocPage,
  ],
  imports: [
    IonicPageModule.forChild(AddDocPage),
    TranslateModule.forChild()
  ],
})
export class AddDocPageModule {}
