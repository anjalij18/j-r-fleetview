import { Component, OnInit } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController,
  ToastController,
  AlertController,
} from "ionic-angular";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import * as moment from "moment";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { getLocaleTimeFormat } from "@angular/common";
import {PaymantgatweyPage} from '../add-devices'
declare var RazorpayCheckout: any;

@IonicPage()
@Component({
  selector: "page-paytmwalletlogin",
  templateUrl: "paytmwalletlogin.html",
})
export class PaytmwalletloginPage {
  paytmmail: any;
  paytmnumber: any;
  paytmotp: number;
  successresponse: boolean = false;
  inputform: boolean = false;
  razor_key: string = "rzp_live_eZdWnBRmYRCLWo";
  paymentAmount: any;
  currency: any = "INR";
  parameters: any;
  islogin: any;
  device_name: any[] = [];
  rechargeData: any[] = [];
  total: number = 0;
  ingnitionStat: string;
  isSuperAdmin: any;
  Amount: any;
  isAlert:any=false;
  Duration_days:any ='90';
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public apiCall: ApiServiceProvider,
    private menu: MenuController,
    public toast: ToastController,
    private alertCtrl: AlertController
  ) {
    // this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    // console.log("islogin devices => " + JSON.stringify(this.islogin));
    this.islogin = localStorage.getItem("details")
    ? JSON.parse(localStorage.getItem("details"))
    : "";
    this.apiCall.getRechargePlanApi(this.islogin.supAdmin).subscribe((respData) => {
      console.log("recharge data --------->", respData);
      if(respData.length ==0 ){
        this.navCtrl.push(PaymantgatweyPage)
      }
      this.total = respData[0].Amount;

      this.rechargeData = respData;
      this.ingnitionStat = this.rechargeData[0].Name
      this.Duration_days = this.rechargeData[0].Duration_days;
      this.total =this.rechargeData[0]. Amount;
    this.paymentAmount = this.total * this.device_name.length;
      console.log("recharge -------->", this.rechargeData);
    });
    console.log("user details", this.islogin);
    this.paytmnumber = this.islogin.phn;
    // this.parameters = navParams.get('param');
    console.log("parameters", this.parameters);
    this.device_name = navParams.get("param");
   // this.device_id.push(this.device_name._id)
    console.log("device name datas --->", this.device_name);
    //  let lenmult =this.device_name.length
    //     this.paymentAmount =lenmult;
    //console.log("paymentAmount ----->",lenmult);
    console.log("paymentAmount ---fcghf-->", this.paymentAmount);
    if (localStorage.getItem("default_curreny") !== null) {
      this.currency = localStorage.getItem("default_curreny");
    }

    if (this.islogin.isSuperAdmin == true) {
      let alert = this.alertCtrl.create({
        message: "Do you want to Renew the vehicle?",
        buttons: [
          {
            text: "Yes",
            handler: () => {
              this.updateExpDate();
              (err) => {
                console.log(err);
              };
            },
          },
          {
            text: "No",
            handler: () => {
              // this.menuCtrl.close();
            },
          },
        ],
      });
      alert.present();
    }
  }
  onItemChange(item: any, days:any) {
    console.log("ssssstype of ",typeof(days))
    this.isAlert = true;
    console.log("renew--id---->",this.islogin.Dealer_ID._id)
    this.Duration_days = days;
    console.log("Duration_days  -------days-->", this.Duration_days);
    this.total = item;
    this.paymentAmount = this.total * this.device_name.length;
    console.log("paymentAmount ---fcghf---apiiiii->", this.paymentAmount);
  }
  ionViewDidEnter() {
    this.Amount = this.device_name.length * this.total;
    console.log("Amount data--->", this.Amount);
    this.menu.enable(true);
    this.apiCall.getRechargePlanApi(this.islogin.supAdmin).subscribe((respData) => {
      console.log("recharge data --------->", respData);
      this.rechargeData = respData;
      this.ingnitionStat = this.rechargeData[0].Name
      this.Duration_days = this.rechargeData[0].Duration_days;
      this.total =this.rechargeData[0]. Amount;
    this.paymentAmount = this.total * this.device_name.length;
      if(this.rechargeData.length==0){
        this.navCtrl.push('PaymantgatweyPage')
      }
      console.log("recharge -------->", this.rechargeData);
    });
  }

  paytmSignupOTPResponse: any;

  payWithRazor() {
    console.log("numnber-->", this.paytmnumber);
    if(this.isAlert==false){
      let alert = this.alertCtrl.create({
        message: "Please Select Recharge Plan",
        buttons: [
          {
            text: "ok",
            handler: () => {
              this.ionViewDidEnter();
              (err) => {
                console.log(err);
              };
            },
          },
        ],
      });
      alert.present();
    }
    var orderId;
    // debugger
    var options = {
      description: "Credits towards consultation",
      image: "https://i.imgur.com/GO0jiDP.jpg",
      currency: this.currency, // your 3 letter currency code
      key: this.razor_key, // your Key Id from Razorpay dashboard
     // key: "rzp_test_prTPBJTO1qZAoD",
      amount: this.paymentAmount * 100, // Payment amount in smallest denomiation e.g. cents for USD
      //name: this.parameters.Device_Name,
      // order_id: '123dknkdslnflk',
      prefill: {
        email: this.islogin.email,
        contact: this.paytmnumber,
        name: this.islogin.fn + " " + this.islogin.ln,
      },
      theme: {
        color: "#ee612e",
      },
      modal: {
        ondismiss: function () {
          console.log("dismissed");
        },
      },
    };
    debugger;
    let that = this;
    var successCallback = function (success) {
      that.isSuperAdmin = JSON.parse(localStorage.getItem("details"));
      console.log("dddddddd----dadtaaa", that.device_name);
      let VehicleNo = that.device_name.map((item) => item.Device_Name);
      console.log("vehicle numner---->", VehicleNo);

      let data = {
        Amount: that.paymentAmount,
        Customer_ID: that.isSuperAdmin._id,
        Status: "Paid",
        TransactionID: success.razorpay_payment_id,
        Remarks: "455",
        VehicleNo: VehicleNo,
        Items: "SIM + Software",
        PaymentMode: "UPI",
        supAdmin: that.isSuperAdmin.supAdmin,
        date:new Date()
      };
      that.apiCall.addCustomerPayment(data).subscribe((respData) => {});
      // alert('order_id: ' + JSON.stringify(success));
      that.showAlert(success.razorpay_payment_id);

      orderId = success.razorpay_order_id;
      var signature = success.razorpay_signature;
    };

    var cancelCallback = function (error) {
      console.log("razorpay error: ", error);
      that.onFailed(error);
      // alert(error.description + ' (Error ' + error.code + ')')
    };
    console.log("optionssss--->", options);
    // RazorpayCheckout.open(options, this.successCallback(), cancelCallback);
    RazorpayCheckout.on("payment.success", successCallback);
    RazorpayCheckout.on("payment.cancel", cancelCallback);
    if(this.isAlert){
    RazorpayCheckout.open(options);
    }
  }
  success() {
    var tempdate = new Date();
    var Duration_days =this.Duration_days;

   let tempExp =    moment(tempdate).add('days', Duration_days);
   let ISOtempdate  = tempExp.toISOString();
   console.log("tempExp date--->",ISOtempdate)
  // let ISOtempdate = tempdate.toISOString();
    let device_id = this.device_name.map((item) => item._id);

   let data ={
    device: device_id,
    renew_by:this.islogin._id,
    expDate: ISOtempdate,
    }
    console.log("renew object---->",data);
    this.apiCall.addRenewDevices(data).subscribe((respData) => {
      console.log("Renew ------Devicesss----->",respData)
    });
    this.navCtrl.push(SuccessDetailPage, {
      param: this.paymentAmount,
      vname: this.device_name,
      tdate: "datetimeEnd",
      superAdmin: this.islogin.isSuperAdmin,
    });
    console.log("success dataaaa---->");
    //this.updateExpDate();
  }
  showAlert(id) {
    let alert = this.alertCtrl.create({
      message: "Payment successfull...",
      buttons: [
        {
          text: "Okay",
          handler: () => {
            // this.updateExpDate();
            console.log("okk success-->");
            this.success();
          },
        },
      ],
    });
    alert.present();
  }

  onFailed(err) {
    console.log(err);
    let alert = this.alertCtrl.create({
      message: "Transaction Failed! Please try again with valid credentials",
      buttons: [
        {
          text: "Try Again",
          handler: () => {
            this.goBack();
          },
        },
      ],
    });
    alert.present();
  }

  goBack() {
    this.navCtrl.pop();
  }

  updateExpDate() {
    debugger;
    var tempdate = new Date();
    tempdate.setDate(tempdate.getDate() + 365);
    var expDate = moment(new Date(tempdate), "DD-MM-YYYY").format("YYYY-MM-DD");
    console.log("updated expiry date: ", expDate);
    var url = this.apiCall.mainUrl + "devices/editDevMaster";
    var payload = {
      // _id: this.parameters.Device_ID,
      _id: this.parameters._id,
      expiration_date: new Date(expDate).toISOString(),
    };
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdatatwo(url, payload).subscribe(
      (respData) => {
        this.apiCall.stopLoading();
        console.log("respData of expiry date updation: ", respData);
        let toast = this.toastCtrl.create({
          message: "The expired device is renewed successfully!!",
          duration: 2000,
          position: "middle",
        });
        toast.onDidDismiss(() => {
          this.navCtrl.pop();
        });
        toast.present();
      },
      (err) => {
        console.log("oops got error: ", err);
        this.apiCall.stopLoading();
      }
    );
  }
}

@Component({
  templateUrl: "./success-Detail.html",
  styles: [
    `
      .col {
        padding: 0px;
      }
    `,
  ],
})
export class SuccessDetailPage implements OnInit {
  fdate: any;
  Amount: any;
  razor_key: string = "rzp_live_eZdWnBRmYRCLWo";
  tdate: any;
  uid: any;
  paramData: any = {};
  ddata: any[] = [];
  vname: any[] = [];
  customerId: any;
  paymentHistory: any[] = [];
  constructor(
    public apiCall: ApiServiceProvider,
    public navParam: NavParams,
    public navCtrl: NavController
  ) {
    // this.apiCall.addCustomerPayment("234")
    console.log(
      "param parameters:------>paramdata ",
      this.navParam.get("param")
    );
    this.Amount = this.navParam.get("param");
    this.vname = this.navParam.get("vname");
    console.log("vvvvname--->", this.vname);
    this.customerId = this.vname[0].Dealer;

    // this.tdate = this.navParam.get('tdate');
    // this.uid = this.navParam.get('uid');
  }

  ngOnInit() {
    this.getDetails();
  }
  paymenthistory() {
    let isSuperAdmin = JSON.parse(localStorage.getItem("details"));
    console.log("customer-----id-->", this.customerId);
    this.apiCall
      .getCustomerPayment(isSuperAdmin._id)

      .subscribe((respData) => {
        this.paymentHistory = respData;
        console.log("data response paymentHistory--->", respData);
        // this.rechargeData = respData;
        // console.log("recharge -------->",this.rechargeData);
      });
    this.navCtrl.push(PaymentHistoryPage, {
      param: "50",
      fdate: "datetimeStart",
      tdate: "datetimeEnd",
      uid: "islogin",
      customerId: this.customerId,
      paymentHistory: this.paymentHistory,
    });
  }
  getDetails() {
    // debugger
    // this.apiCall.startLoading().present();
    // this.apiCall.getDetailACReportAPI(new Date(this.fdate).toISOString(), new Date(this.tdate).toISOString(), this.uid, this.paramData._id.imei)
    //   .subscribe(data => {
    //     this.apiCall.stopLoading();
    //     console.log("detailed ac report data: ", data[0].s);
    //     this.ddata = data[0].s;
    //   })
  }
}

@Component({
  templateUrl: "./payment-History.html",
  styles: [
    `
      .col {
        padding: 0px;
      }
    `,
  ],
})
export class PaymentHistoryPage implements OnInit {
  fdate: any;
  tdate: any;
  uid: any;
  paramData: any = {};
  ddata: any[] = [];
  vname: any;
  customerId: any;
  paymentHistory: any[] = [];
  date: any;
  expDate: any[] = [];
  constructor(public apiCall: ApiServiceProvider, public navParam: NavParams) {}

  ngOnInit() {
    this.getDetails();
  }

  getDetails() {
    let isSuperAdmin = JSON.parse(localStorage.getItem("details"));
    this.apiCall.getCustomerPayment(isSuperAdmin._id).subscribe((respData) => {
      this.paymentHistory = respData;
      this.paymentHistory.map((item) => {
        item.createdAt = moment(new Date(item.createdAt), "DD-MM-YYYY").format(
          "YYYY-MM-DD"
        );
        return item;
      });
    });
  }
}
