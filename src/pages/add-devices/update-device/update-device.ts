import { Component, OnInit } from "@angular/core";
import {
  ViewController,
  NavParams,
  ToastController,
  AlertController,
  IonicPage,
} from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import * as moment from "moment";
import { SMS } from "@ionic-native/sms";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { AndroidPermissions } from "@ionic-native/android-permissions";
@IonicPage()
@Component({
  selector: "page-update-device",
  templateUrl: "./update-device.html",
})
export class UpdateDevicePage implements OnInit {
  updatevehForm: FormGroup;
  vehData: any;
  selectUser: any;
  allVehicle: any;
  deviceModel: any;
  allGroup: any[] = [];
  allGroupName: any;
  islogin: any;
  submitAttempt: boolean;
  groupstaus: any;
  userdata: any;
  modeldata: any;
  vehType: any;
  groupstaus_id: any;
  devicedetail: any = {};
  vehicleType: any;
  modeldata_id: any;
  userdata_id: any;
  eDate: any;
  vehicleType_id: any;
  currentYear: any;
  minDate: any;
  speed: any;
  d_user: any;
  smdData: any;

  constructor(
    public apiCall: ApiServiceProvider,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    navPar: NavParams,
    public toastCtrl: ToastController,
    public alerCtrl: AlertController,
    private sms: SMS,
    private androidPermissions: AndroidPermissions
  ) {
    this.vehData = navPar.get("vehData");
    console.log("vehicle data=> ", this.vehData.device_model._id);

    // this.apiCall.getDevModelById(this.updatevehForm.value._id)
    //   .subscribe(data => {
    //     console.log("data--->---sms---->",data)
    //     this.device_timezone =data.device_timezone;
    //     this.sms_apn =data.sms_apn;
    //     this.sms_ip =data.sms_ip;
    //   })

    this.islogin = JSON.parse(localStorage.getItem("details")) || {};
    console.log("islogin devices => " + this.islogin);
    var tempdate = new Date();
    tempdate.setDate(tempdate.getDate() + 365);
    this.currentYear = moment(new Date(tempdate), "DD-MM-YYYY").format(
      "YYYY-MM-DD"
    );

    if (this.vehData.expiration_date == null) {
      this.eDate = this.currentYear;
    } else {
      this.eDate = moment(this.vehData.expiration_date).format("YYYY-MM-DD");
    }
    var d_type, v_type, g_name;
    if (this.vehData.device_model != undefined) {
      d_type = this.vehData.device_model["device_type"];
    } else {
      d_type = "";
    }
    if (this.vehData.user != undefined) {
      // this.d_user = this.vehData.user;
      this.d_user = this.vehData.user["first_name"];
    } else {
      this.d_user = "";
    }
    debugger;
    if (
      this.vehData.vehicleType != undefined ||
      this.vehData.iconType != undefined
    ) {
      v_type = this.vehData.vehicleType
        ? this.vehData.vehicleType["brand"]
        : this.vehData.iconType;
    } else {
      v_type = "";
    }

    if (this.vehData.vehicleGroup != undefined) {
      g_name = this.vehData.vehicleGroup["name"];
    } else {
      g_name = "";
    }

    // ============== one month later date from current date ================

    var today: any = new Date();
    var dd: any = today.getDate();
    var mm: any = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = "0" + dd;
    }
    if (mm < 10) {
      mm = "0" + mm;
    }

    today = yyyy + "-" + mm + "-" + dd;
    this.minDate = today;
    console.log("minimum date: ", today);
    // =============== end
    this.updatevehForm = formBuilder.group({
      device_name: [this.vehData.Device_Name, Validators.required],
      device_id: [this.vehData.Device_ID, Validators.required],
      driver: [this.vehData.driver_name],
      sim_number: [this.vehData.sim_number, Validators.required],
      contact_number: [this.vehData.contact_number, Validators],
      SpeedLimit: [this.vehData.SpeedLimit],
      ExipreDate: [this.eDate, Validators.required],
      gName: [g_name],
      device_type: [d_type],
      first_name: [this.d_user],
      brand: [v_type],
    });
    console.log(
      "date format=> " +
        moment(this.vehData.expiration_date).format("YYYY-MM-DD")
    );
  }

  ngOnInit() {
    this.getGroup();
    this.getDeviceModel();
    this.getSelectUser();
    this.getVehicleType();
  }
  // sendSms() {
  //     let options = {
  //       replaceLineBreaks: false, // true to replace \n by a new line, false by default
  //       android: {
  //           intent: ''  // send SMS with the native android SMS messaging
  //           // intent: '' // send SMS without opening any other app
  //       }
  //   };
  //     this.sms.send('+917737383167', 'SMS Works', options).then(val => {
  //       alert('It works');
  //     });
  //   }

  checkSMSPermission() {
    this.androidPermissions
      .checkPermission(this.androidPermissions.PERMISSION.SEND_SMS)
      .then(
        (
            result //alert(result.hasPermission),
          ) =>
          (err) =>
            this.androidPermissions.requestPermission(
              this.androidPermissions.PERMISSION.SEND_SMS
            )
      );
  }
  requestSMSPermission() {
    // tslint:disable-next-line: max-line-length
    this.androidPermissions.requestPermissions([
      this.androidPermissions.PERMISSION.SEND_SMS,
      this.androidPermissions.PERMISSION.BROADCAST_SMS,
    ]);
  }

  sendSms() {
    this.checkSMSPermission();
    // Check mobile number
    if (this.updatevehForm.controls.sim_number.value) {
      // First Message
      //alert(this.smdData.sms_ip+ ' --- '+ this.smdData.sms_apn+ ' -- '+  this.smdData.sms_timezone)
      if (this.smdData && this.smdData.sms_ip) {
        this.onSendsms(
          this.updatevehForm.controls.sim_number.value,
          this.smdData.sms_ip
        );
      }

      // Second Message
      if (this.smdData && this.smdData.sms_apn) {
        setTimeout(() => {
          this.onSendsms(
            this.updatevehForm.controls.sim_number.value,
            this.smdData.sms_apn
          );
        }, 5000);
      }

      // Third Message
      if (this.smdData && this.smdData.sms_timezone) {
        setTimeout(() => {
          this.onSendsms(
            this.updatevehForm.controls.sim_number.value,
            this.smdData.sms_timezone
          );
        }, 10000);
      }
    }
  }
  onSendsms(mobileNumber: string, sms: string) {
    const options = {
      replaceLineBreaks: true, // true to replace \n by a new line, false by default
      android: {
        // intent: 'INTENT'  // send SMS with the native android SMS messaging
        intent: "", // send SMS without opening any other app
      },
    };
    this.sms
      .send('+91'+mobileNumber, sms, options)
      .then(() => {
        alert("message has been sent");
      })
      .catch((error) => {
        alert(error);
      });
  }

  // sendSms(){
  //     var options = {
  //         replaceLineBreaks: false, // true to replace \n by a new line, false by default
  //         android: {
  //             intent: 'INTENT'  // send SMS with the native android SMS messaging
  //             //intent: '' // send SMS without opening any other app, require : android.permission.SEND_SMS and android.permission.READ_PHONE_STATE
  //         }
  //     };
  //     let success = function () { alert('Message sent successfully'); };
  //     let error = function (e) { alert('Message Failed:' + e); };
  //     this.sms.send('+917737383167', 'Hello world!');

  //     alert('sendSms');
  // }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  updateDevices() {
    let that = this;
    that.submitAttempt = true;
    if (that.updatevehForm.valid) {
      ///////////////////////////
      // debugger;
      console.log("form valuess=> ", that.updatevehForm.value);
      if (that.updatevehForm.value.device_type != "") {
        if (
          that.vehData.device_model != null ||
          that.vehData.device_model != undefined
        ) {
          if (
            that.updatevehForm.value.device_type ==
            that.vehData.device_model["device_type"]
          ) {
            that.modeldata_id = that.vehData.device_model["_id"];
          } else {
            that.modeldata_id = that.modeldata._id;
          }
        } else {
          if (that.modeldata._id != undefined) {
            that.modeldata_id = that.modeldata._id;
          } else {
            that.modeldata_id = null;
          }
        }
      } else {
        that.modeldata_id = null;
      }
      if (that.updatevehForm.value.gName != "") {
        if (
          that.vehData.vehicleGroup != null ||
          that.vehData.vehicleGroup != undefined
        ) {
          if (
            that.updatevehForm.value.gName == that.vehData.vehicleGroup["name"]
          ) {
            that.groupstaus_id = that.vehData.vehicleGroup["_id"];
          } else {
            that.groupstaus_id = that.groupstaus._id;
          }
        } else {
          if (that.groupstaus._id != undefined) {
            that.groupstaus_id = that.groupstaus._id;
          } else {
            that.groupstaus_id = null;
          }
        }
      } else {
        that.groupstaus_id = null;
      }
      if (that.updatevehForm.value.first_name != "") {
        if (that.vehData.user != null || that.vehData.user != undefined) {
          if (
            that.updatevehForm.value.first_name ==
            that.vehData.user["first_name"]
          ) {
            that.userdata_id = that.vehData.user["_id"];
          } else {
            that.userdata_id = that.userdata._id;
          }
        } else {
          if (that.userdata._id != undefined) {
            that.userdata_id = that.userdata._id;
          } else {
            that.userdata_id = null;
          }
        }
      } else {
        that.userdata_id = null;
      }
      if (that.updatevehForm.value.brand != "") {
        debugger;
        if (
          that.vehData.iconType != null ||
          that.vehData.iconType != undefined ||
          that.vehData.vehicleType != null ||
          that.vehData.vehicleType != undefined
        ) {
          if (
            that.updatevehForm.value.brand == this.vehData.vehicleType
              ? this.vehData.vehicleType["brand"]
              : this.vehData.iconType
          ) {
            that.vehicleType_id = that.updatevehForm.value.brand._id;
          } else {
            that.vehicleType_id = that.vehicleType._id;
          }
        } else {
          if (that.vehicleType_id != undefined) {
            that.vehicleType_id = that.vehicleType._id;
          } else {
            that.vehicleType_id = null;
          }
        }
      } else {
        that.vehicleType_id = null;
      }

      if (that.updatevehForm.value.SpeedLimit != "") {
        that.speed = that.updatevehForm.value.SpeedLimit;
      } else {
        that.speed = 1;
      }

      ///////////////////////////

      that.devicedetail = {
        _id: that.vehData._id,
        devicename: that.updatevehForm.value.device_name,
        drname: that.updatevehForm.value.driver,
        deviceid: that.updatevehForm.value.device_id,
        sim: that.updatevehForm.value.sim_number,
        iconType: null,
        dphone: that.updatevehForm.value.contact_number,
        speed: that.speed,
        vehicleGroup: that.groupstaus_id,
        device_model: that.modeldata_id,
        expdate: new Date(that.updatevehForm.value.ExipreDate).toISOString(),
        user: that.userdata_id,
        vehicleType: that.vehicleType_id,
        updatedBy: that.islogin._id,
      };

      console.log("all details=> " + that.devicedetail);
      that.apiCall.startLoading().present();
      that.apiCall.deviceupdateCall(that.devicedetail).subscribe(
        (data) => {
          console.log("dataataa---detailssss--->", data);
          that.apiCall.stopLoading();
          var editdata = data;
          console.log("editdata=> " + editdata);
          let toast = that.toastCtrl.create({
            message: editdata.message,
            position: "top",
            duration: 2000,
          });

          toast.onDidDismiss(() => {
            console.log("Dismissed toast");
            that.viewCtrl.dismiss(editdata);
          });

          toast.present();
        },
        (err) => {
          this.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          console.log("error occured 1=> ", msg);
          let alert = this.alerCtrl.create({
            title: "Oops!",
            message:
              "Device is already into system. Please delete from system and then try again.",
            buttons: ["OK"],
          });
          alert.present();
        }
      );
    }
  }

  deviceModelata(deviceModel) {
    // debugger
    console.log("deviceModel" + this.updatevehForm.value.device_type);
    this.modeldata = this.updatevehForm.value.device_type;
    console.log("modal data device_type=> " + this.modeldata.device_type);
  }

  GroupStatusdata(status) {
    console.log(status);
    this.groupstaus = status;
    console.log("groupstaus=> " + this.groupstaus._id);
  }

  userselectData(userselect) {
    console.log(userselect);
    this.userdata = this.updatevehForm.value.first_name;
    console.log("userdata=> " + this.userdata.first_name);
  }

  vehicleTypeselectData(vehicletype) {
    // debugger
    console.log(this.updatevehForm.value.brand._id);
    this.vehicleType = this.updatevehForm.value.brand;
    console.log("vehType=> " + this.vehicleType._id);
  }

  getGroup() {
    console.log("get group");
    var baseURLp =
      this.apiCall.mainUrl + "groups/getGroups_list?uid=" + this.islogin._id;
    this.apiCall.startLoading().present();
    this.apiCall.groupsCall(baseURLp).subscribe(
      (data) => {
        this.apiCall.stopLoading();
        if (data["group_details"] !== undefined) {
          if (data["group_details"].length > 0) {
            this.allGroup = data["group_details"];
            for (var i = 0; i < this.allGroup.length; i++) {
              this.allGroupName = this.allGroup[i].name;
              // console.log("allGroupName=> "+this.allGroupName);
            }
            console.log("allGroupName=> " + this.allGroupName);
          }
        }
      },
      (err) => {
        console.log(err);
        this.apiCall.stopLoading();
      }
    );
  }

  getDeviceModel() {
    var baseURLp = this.apiCall.mainUrl + "deviceModel/getDeviceModel";
    this.apiCall.getDeviceModelCall(baseURLp).subscribe(
      (data) => {
        this.deviceModel = data;
      },
      (err) => {
        console.log(err);
      }
    );
    this.apiCall
      .getDevModelById(this.vehData.device_model._id)
      .subscribe((data) => {
        if (data && data.length) {
          this.smdData = data[0];
        }
      });
  }

  getSelectUser() {
    console.log("get user");

    var baseURLp =
      this.apiCall.mainUrl + "users/getAllUsers?dealer=" + this.islogin._id;

    this.apiCall.getAllUsersCall(baseURLp).subscribe(
      (data) => {
        this.selectUser = data;
        console.log("selected user=> ", this.selectUser);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getVehicleType() {
    console.log("get getVehicleType");

    var baseURLp =
      this.apiCall.mainUrl +
      "vehicleType/getVehicleTypes?user=" +
      this.islogin._id;
    // this.apiCall.startLoading().present();
    this.apiCall.getVehicleTypesCall(baseURLp).subscribe(
      (data) => {
        // this.apiCall.stopLoading();
        this.allVehicle = data;
        console.log("all vehicles=> ", this.allVehicle);
      },
      (err) => {
        console.log(err);
        // this.apiCall.stopLoading();
      }
    );
  }
}
