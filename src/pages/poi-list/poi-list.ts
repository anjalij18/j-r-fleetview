import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { GoogleMaps } from '@ionic-native/google-maps';
import { TranslateService } from '@ngx-translate/core';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';

@IonicPage()
@Component({
  selector: 'page-poi-list',
  templateUrl: 'poi-list.html',
})
export class PoiListPage {
  islogin: any;
  poilist: any[] = [];
  allData: any = {};
  mapid: any;
  stdid: any;
  staticImageUrl: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public events: Events,
    private translate: TranslateService,
    private geocoderApi: GeocoderProvider) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.events.subscribe('reloadpoilist', () => {
      this.poilist = [];
      this.getpois();
    })
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter PoiListPage');
  }

  ngOnInit() {
    this.getpois();
  }

  getpois() {
    this.apiCall.getPoisAPI(this.islogin._id)
      .subscribe(data => {
        this.poilist = data;
        this.allData._poiListData = [];
        var i = 0, howManyTimes = data.length;
        let that = this;
        function f() {
          that.allData._poiListData.push({
            "poiname": (data[i].poi.poiname ? data[i].poi.poiname : null),
            "radius": data[i].radius ? data[i].radius : 'N/A',
            "_id": data[i]._id,
            "start_location": {
              'lat': data[i].poi.location.coordinates[1],
              'long': data[i].poi.location.coordinates[0]
            }
          });
          // debugger
          that.start_address(that.allData._poiListData[i], i);
          
          that.allData._poiListData[that.allData._poiListData.length - 1].mapid = "a" + i;
          that.allData.map = GoogleMaps.create(that.allData._poiListData[that.allData._poiListData.length - 1].mapid, {
            camera: {
              target: {
                lat: data[i].poi.location.coordinates[1],
                lng: data[i].poi.location.coordinates[0]
              },
              zoom: 12
            }
          })

          that.allData.map.addMarker({
            position: {
              lat: data[i].poi.location.coordinates[1],
              lng: data[i].poi.location.coordinates[0]
            }
          })

          i++;
          if (i < howManyTimes) {
            setTimeout(f, 200);
          }
        } f();
      },
        err => {
          console.log("error in pois: ", err)
        })
  }

  start_address(item, index) {
    let that = this;
    that.allData._poiListData[index].StartLocation = "N/A";
    if (!item.start_location) {
      that.allData._poiListData[index].StartLocation = "N/A";
    } else if (item.start_location) {
      this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
        .then((res) => {
          console.log("test" ,res)
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.allData._poiListData[index].addres = str;
        })
    }
  }

  editPOI(data) {
    console.log("edited data: " + JSON.stringify(data));
    this.navCtrl.push('AddPoiPage', {
      "param": data
    });
  }

  deletePOI(data) {
    console.log("delete data: ", data)
    let alert = this.alertCtrl.create({
      message: this.translate.instant('Do you want to delete this', { value: 'POI' }),
      buttons: [{
        text: this.translate.instant('Back')
      }, {
        text: this.translate.instant('YES PROCEED'),
        handler: () => {
          this.apiCall.startLoading().present();
          this.apiCall.deletePOIAPI(data._id)
            .subscribe(data => {
              this.apiCall.stopLoading();
              let toast = this.toastCtrl.create({
                message: this.translate.instant('Deleted successfully.', { value: 'POI' }),
                position: "top",
                duration: 2000
              })
              toast.present();
              this.getpois();
            },
              err => {
                this.apiCall.stopLoading();
                console.log("error occured while deleting POI: ", err)
                let toast = this.toastCtrl.create({
                  message: this.translate.instant('Deleted successfully.', { value: 'POI' }),
                  position: "top",
                  duration: 2000
                })
                toast.present();
                this.getpois();
              })
        }
      }]
    });
    alert.present();
  }

  addPOI() {
    this.navCtrl.push('AddPoiPage');
  }

  getStaticImage(lat: number, lng: number, zoom: number) {
    return `https://maps.googleapis.com/maps/api/staticmap?center=${lat},${lng}&zoom=${zoom}&size=600x300&maptype=roadmap&markers=color:red%7Clabel:Place%7C${lat},${lng}&key=AIzaSyA2meHC0Wgy8gn8eozMGDs-TI-oBan-V4k`;
    // return `https://maps.googleapis.com/maps/api/staticmap?center=Brooklyn+Bridge,New+York,NY&zoom=13&size=600x300&maptype=roadmap
    // &markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318
    // &markers=color:red%7Clabel:C%7C40.718217,-73.998284
    // &key=AIzaSyA2meHC0Wgy8gn8eozMGDs-TI-oBan-V4k`
  }

}
