import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, Navbar, ModalController, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { GoogleMaps, ILatLng, Circle } from '@ionic-native/google-maps';

@IonicPage()
@Component({
  selector: 'page-geofence',
  templateUrl: 'geofence.html',
})
export class GeofencePage implements OnInit, OnDestroy {
  @ViewChild(Navbar) navBar: Navbar;
  islogin: any;
  setsmsforotp: string;
  isdevice: string;
  devices: any[] = [];
  DeletedDevice: any;
  statusofgeofence: any;
  creation_type: any = "circular";
  allData: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    public alerCtrl: AlertController,
    public modalCtrl: ModalController,
    public events: Events) {
    this.allData._poiListData = [];
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.setsmsforotp = localStorage.getItem('setsms');
    this.isdevice = localStorage.getItem('cordinates');
    this.events.subscribe('reloadDetails', () => { });
  }

  ngOnInit() {
    if (localStorage.getItem("SCREEN") != null) {
      this.navBar.backButtonClick = (e: UIEvent) => {
        console.log("back button poped")
        if (localStorage.getItem("SCREEN") != null) {
          if (localStorage.getItem("SCREEN") === 'live') {
            this.navCtrl.setRoot('LivePage');
          } else {
            if (localStorage.getItem("SCREEN") === 'dashboard') {
              this.navCtrl.setRoot('AddDevicesPage');
            }
          }
        }
      }
    }

    if (this.creation_type == 'circular') {
      this.devices = [];
      this.getCircularGeofence();
    }
    // this.getgeofence();
  }

  ngOnDestroy() {
    if (this.allData.map) {
      this.allData.map.remove();
    }
  }

  radioChecked(key) {
    if (key == "circular") {
      this.devices = [];
      this.getCircularGeofence();
    } else {
      if (key == "polygon") {
        this.allData._poiListData = [];
        if (this.allData.map) {
          this.allData.map.remove();
        }
        this.getgeofence();
      }
    }
  }

  getCircularGeofence() {
    this.apiCall.getPoisAPI(this.islogin._id)
      .subscribe(data => {
        // this.circularlist = data;
        this.allData._poiListData = [];
        var i = 0, howManyTimes = data.length;
        let that = this;
        function f() {
          that.allData._poiListData.push({
            "geonamename": data[i].poi.poiname,
            "radius": data[i].radius ? data[i].radius : 'N/A',
            "address": data[i].poi.address ? data[i].poi.address : 'N/A',
            "_id": data[i]._id
          });

          that.allData._poiListData[that.allData._poiListData.length - 1].mapid = "a" + i;
          that.allData.map = GoogleMaps.create(that.allData._poiListData[that.allData._poiListData.length - 1].mapid, {
            camera: {
              target: {
                lat: data[i].poi.location.coordinates[1],
                lng: data[i].poi.location.coordinates[0]
              },
              zoom: 13
            }
          })

          that.allData.map.addMarker({
            position: {
              lat: data[i].poi.location.coordinates[1],
              lng: data[i].poi.location.coordinates[0]
            }
          })

          let ltln: ILatLng = { "lat": data[i].poi.location.coordinates[1], "lng": data[i].poi.location.coordinates[0] };
          that.allData.map.addCircleSync({
            'center': ltln,
            'radius': data[i].radius,
            'strokeColor': '#ee612e',
            'strokeWidth': 2,
            'fillColor': 'rgb(255, 128, 128, 0.5)'
          });
          // let circle: Circle = that.allData.map.addCircleSync({
          //   'center': ltln,
          //   'radius': data[i].radius,
          //   'strokeColor': '#ee612e',
          //   'strokeWidth': 2,
          //   'fillColor': 'rgb(255, 128, 128, 0.5)'
          // });

          i++;
          if (i < howManyTimes) {
            setTimeout(f, 200);
          }
        } f();
      },
        err => {
          // debugger
          if(JSON.parse(err._body).message === "no poi list found") {
            let toast = this.toastCtrl.create({
              message: "Oops..POI list not found.",
              duration: 1800,
              position: 'bottom'
            });
            toast.present();
          } else {
            return
          }

          // console.log("error in pois: ", JSON.parse(JSON.stringify(err)).message)
          console.log("error in pois: ", JSON.parse(err._body).message)
        })
  }

  addgeofence() {
    this.navCtrl.push('AddGeofencePage');
  }


  getgeofence() {
    this.apiCall.startLoading().present();
    this.apiCall.getallgeofenceCall(this.islogin._id)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.devices = data;
      },
        err => {
          this.apiCall.stopLoading();
          console.log("error => ", err);
        });
  }

  deleteGeo(_id) {
    // this.apiCall.startLoading().present();
    this.apiCall.deleteGeoCall(_id).
      subscribe(data => {
        // this.apiCall.stopLoading();
        this.DeletedDevice = data;
        let toast = this.toastCtrl.create({
          message: 'Deleted Geofence Area successfully.',
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          this.getgeofence();
        });

        toast.present();
      },
        err => {
          // this.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alerCtrl.create({
            title: 'Oops!',
            message: msg.message,
            buttons: ['OK']
          });
          alert.present();
        });
  }

  DelateGeofence(_id) {
    let alert = this.alerCtrl.create({
      message: 'Do you want to delete this geofence area?',
      buttons: [{
        text: 'No'
      },
      {
        text: 'YES',
        handler: () => {
          this.deleteGeo(_id);
        }
      }]
    });
    alert.present();
  }

  DisplayDataOnMap(item) {
    // console.log(item);
    // var baseURLp = 'http://13.126.36.205:3000/geofencing/geofencestatus?gid=' + item._id + '&status=' + item.status + '&entering=' + item.entering + '&exiting=' + item.exiting;
    this.apiCall.geofencestatusCall(item._id, item.status, item.entering, item.exiting)
      .subscribe(data => {
        this.statusofgeofence = data;
        // console.log(this.statusofgeofence);
      },
        err => {
          console.log(err);
        });
  }

  geofenceShow(item) {
    this.navCtrl.push('GeofenceShowPage', {
      param: item
    });
  }

  deletePOI(data) {
    console.log("delete data: ", data)
    let alert = this.alerCtrl.create({
      message: "Do you want to delete this Geofence?",
      buttons: [{
        text: 'BACK'
      }, {
        text: 'YES PROCEED',
        handler: () => {
          this.apiCall.startLoading().present();
          this.apiCall.deletePOIAPI(data._id)
            .subscribe(data => {
              this.apiCall.stopLoading();
              let toast = this.toastCtrl.create({
                message: "Geofence deleted successfully!",
                position: "top",
                duration: 2000
              })
              toast.present();
              this.getCircularGeofence();
            },
              err => {
                this.apiCall.stopLoading();
                console.log("error occured while deleting POI: ", err)
                let toast = this.toastCtrl.create({
                  message: "Geofence deleted successfully!",
                  position: "top",
                  duration: 2000
                })
                toast.present();
                this.getCircularGeofence();
              })
        }
      }]
    });
    alert.present();
  }
}
