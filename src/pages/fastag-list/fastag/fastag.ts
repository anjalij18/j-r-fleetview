import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { TranslateService } from '@ngx-translate/core';
declare var RazorpayCheckout: any;

@IonicPage()
@Component({
  selector: 'page-fastag',
  templateUrl: 'fastag.html',
})
export class FastagPage {
  buttonColor: string = '#fff'; //Default Color
  buttonColor1: string = '#fff';
  buttonColor2: string;
  buttonColor3: string;
  buttonColor4: string;
  buttonColor5: string;
  islogin: any;
  vehicleType: string;
  numFastag: number;
  mainCost: number = 200;
  totalPayable: number;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiServiceProvider,
    private toastCtrl: ToastController,
    private translate: TranslateService) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter FastagPage');
  }

  calc() {
    this.totalPayable = this.mainCost * this.numFastag;
  }
  addEvent() {
    this.vehicleType = 'Truck';
    this.buttonColor1 = '#fff';
    this.buttonColor2 = '#fff';
    this.buttonColor3 = '#fff';
    this.buttonColor4 = '#fff';
    this.buttonColor5 = '#fff';
    this.buttonColor = '#FBCCD2'; //desired Color
  }

  addEvent1() {
    this.vehicleType = 'Bus';
    this.buttonColor = '#fff';
    this.buttonColor2 = '#fff';
    this.buttonColor3 = '#fff';
    this.buttonColor4 = '#fff';
    this.buttonColor5 = '#fff';
    this.buttonColor1 = '#FBCCD2'; //desired Color
  }

  addEvent2() {
    this.vehicleType = 'Taxi';
    this.buttonColor1 = '#fff';
    this.buttonColor = '#fff';
    this.buttonColor3 = '#fff';
    this.buttonColor4 = '#fff';
    this.buttonColor5 = '#fff';
    this.buttonColor2 = '#FBCCD2'; //desired Color
  }

  addEvent3() {
    this.vehicleType = 'Car';
    this.buttonColor2 = '#fff';
    this.buttonColor1 = '#fff';
    this.buttonColor = '#fff';
    this.buttonColor4 = '#fff';
    this.buttonColor5 = '#fff';
    this.buttonColor3 = '#FBCCD2'; //desired Color
  }
  addEvent4() {
    this.vehicleType = 'Bike';
    this.buttonColor2 = '#fff';
    this.buttonColor1 = '#fff';
    this.buttonColor = '#fff';
    this.buttonColor3 = '#fff';
    this.buttonColor5 = '#fff';
    this.buttonColor4 = '#FBCCD2'; //desired Color
  }

  addEvent5() {
    this.vehicleType = 'Other';
    this.buttonColor4 = '#fff';
    this.buttonColor2 = '#fff';
    this.buttonColor1 = '#fff';
    this.buttonColor = '#fff';
    this.buttonColor3 = '#fff';
    this.buttonColor5 = '#FBCCD2'; //desired Color
  }

  fastagReq() {
    if (this.vehicleType === undefined || this.numFastag === undefined) {
      this.toastCtrl.create({
        message: this.translate.instant('Please select the vehicle type and add number of requests.'),
        duration: 2000,
        position: 'bottom'
      }).present();
      return;
    }
    var url = this.apiCall.mainUrl + "fastTag/addRequest";
    var payload = {};
    // debugger
    if (this.islogin.Dealer_ID === undefined) {
      payload = {
        user: this.islogin._id,
        Dealer: this.islogin.supAdmin,
        supAdmin: this.islogin.supAdmin,
        date: new Date().toISOString(),
        quantity: this.numFastag,
        vehicle_type: this.vehicleType
      }
    } else {
      payload = {
        user: this.islogin._id,
        Dealer: this.islogin.Dealer_ID._id,
        supAdmin: this.islogin.supAdmin,
        date: new Date().toISOString(),
        quantity: this.numFastag,
        vehicle_type: this.vehicleType
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(url, payload)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        console.log('response data: ', respData);
        if (respData.message === 'saved succesfully') {
          this.toastCtrl.create({
            message: 'Fastag added successfully.',
            duration: 2000,
            position: 'bottom'
          }).present();
          this.navCtrl.pop();
        }
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  payNow() {
    this.navCtrl.push(FasttagPayNow, {
      param: {
        amount: this.totalPayable,
        fastagType: this.vehicleType,
        quantity: this.numFastag
      }
    })
  }
}

@Component({
  templateUrl: './paynow.html'
})

export class FasttagPayNow {
  razor_key: string = 'rzp_live_eZdWnBRmYRCLWo';
  paymentAmount: number;
  currency: any = 'INR';
  data: any = {};
  islogin: any;
  constructor(private navParams: NavParams) {
    this.islogin = localStorage.getItem('details') ? JSON.parse(localStorage.getItem('details')) : "";
    this.data = this.navParams.get('param');
    this.paymentAmount = this.data.amount * 100;
    console.log(this.navParams.get('param'))
    if (localStorage.getItem('default_curreny') !== null) {
      this.currency = localStorage.getItem('default_curreny');
    }
  }
  payWithRazor() {
    var options = {
      description: 'Credits towards consultation',
      image: 'https://i.imgur.com/GO0jiDP.jpg',
      currency: this.currency, // your 3 letter currency code
      key: this.razor_key, // your Key Id from Razorpay dashboard
      amount: this.paymentAmount, // Payment amount in smallest denomiation e.g. cents for USD
      name: this.data.fastagType,
      prefill: {
        email: this.islogin.email,
        contact: this.islogin.phn,
        name: this.islogin.fn + ' ' + this.islogin.ln
      },
      theme: {
        color: '#ee612e'
      },
      modal: {
        ondismiss: function () {
          console.log('dismissed')
        }
      }
    };

    var successCallback = function (payment_id) {
      alert('payment_id: ' + payment_id);
    };

    var cancelCallback = function (error) {
      alert(error.description + ' (Error ' + error.code + ')');
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
  }
}
