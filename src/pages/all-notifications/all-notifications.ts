import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, PopoverController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as io from 'socket.io-client';   // with ES6 import
import { FilterPage } from './filter/filter';

@IonicPage()
@Component({
  selector: 'page-all-notifications',
  templateUrl: 'all-notifications.html',
})
export class AllNotificationsPage implements OnInit {
  islogin: any;
  NotifyData: any = [];
  MassArray1: any = [];
  notiftime: any;
  time: string;
  date: string;
  socket: any;
  page: number = 1;

  items = [];
  limit: number = 8;
  ndata: any;
  portstemp: any = [];
  keyData: any = [];
  dates: any;
  selectedVehicle: any={};
  vehicleData: any={};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public events: Events,
    public popoverCtrl: PopoverController) {
     
    this.callBaseURL();
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.events.publish('cart:updated', 0);
    this.getVehicleList(); 
    for (let i = 0; i < 30; i++) {
      this.items.push(this.items.length);
    }

    if (navParams.get('param') != null) {
      this.vehicleData = navParams.get('param');
     
    }

  }

  ngOnInit() {
    localStorage.removeItem("filterByType");
    localStorage.removeItem("filterByDate");
    console.log(this.vehicleData)
    if (this.vehicleData == undefined) {
      console.log("vehicle undefined")
      this.getUsersOnScroll();
      this.getVehicleList();
    } else {
      console.log("vehicle defined")
      this.temp(this.vehicleData);
    }
  }

  callBaseURL() {
    // debugger
    if (localStorage.getItem("ENTERED_BASE_URL") === null) {
      let url = "https://www.oneqlik.in/pullData/getUrlnew";
      this.apiCall.getSOSReportAPI(url)
        .subscribe((data) => {
          console.log("base url: ", data);
          if (data.url) {
            localStorage.setItem("BASE_URL", JSON.stringify(data.url));
          }
          if (data.socket) {
            localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
          }
          // this.getSocketUrl();
        });
    }
  }
  // socketurl: any;
  // getSocketUrl() {
  //   if (localStorage.getItem('SOCKET_URL') !== null) {
  //     this.socketurl = JSON.parse(localStorage.getItem('SOCKET_URL'));
  //     this.socket = io(this.socketurl + '/notifIOV2?userId=' + this.islogin._id, {
  //       transports: ['websocket', 'polling']
  //     });
  //     this.socket.on('connect', () => {
  //       console.log('IO Connected page');
  //       console.log("socket connected page ", this.socket.connected)
  //     });

  //     this.socket.on(this.islogin._id, (msg) => {
  //       this.NotifyData.push(msg);
  //     });
  //   }
  // }

  onNotifMap(notif) {
    console.log("notif : ", this.vehicleData);
    this.navCtrl.push('NotifMapPage', {
      navParams: notif
    })

  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.getUsersOnScroll();
    refresher.complete();
  }

  filterby(ev) {
    let popover = this.popoverCtrl.create(FilterPage,
      {
        cssClass: 'iosPop-popover'
      });

    popover.present({
      ev: ev
    });

    popover.onDidDismiss(data => {
      console.log("notification----->",data,localStorage.getItem("types"))
      if (localStorage.getItem("types") != null) {
        console.log("filter datassss---->",data);
        var typeArr = [];
        if (data != null) {
          if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
              typeArr.push(data[i].filterValue)
            }
          }
          this.keyData = typeArr;
          console.log("keyData=> ", this.keyData)
          this.filterByType();
        }
      } else {
        if (localStorage.getItem("dates") != null) {
          this.dates = data;
          this.filterByDate()
        }
      }

    })
  }

    filterByType() {
    console.log("selecteddddd---->",this.selectedVehicle)
    let that = this;
    console.log("device iddd--->",this.selectedVehicle.Device_ID)
    localStorage.setItem("filterByType", "filterByType");
    var baseUrl;
    console.log("vehicleData------->",this.selectedVehicle)
    debugger
    if (that.vehicleData != undefined || that.selectedVehicle != undefined) {
      var dev;
      if (that.vehicleData) {
        dev = that.vehicleData.Device_ID;
      } else {
        dev = this.selectedVehicle.Device_ID;
      }

      baseUrl = this.apiCall.mainUrl + 'notifs/getNotifiLimit?user=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&type=' + that.keyData + '&device=' + this.selectedVehicle.Device_ID;
    } else {
      baseUrl = this.apiCall.mainUrl + 'notifs/getNotifiLimit?user=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&type=' + that.keyData;
    }
    that.apiCall.startLoading().present();
    console.log("baseurl ------>",baseUrl)
    that.apiCall.filterByType(baseUrl)

      .subscribe(data => {
        that.apiCall.stopLoading();
        that.ndata = data;
        console.log("ndata----->",this.ndata)
        that.MassArray1 = that.ndata;
        localStorage.removeItem("types");
      },
        err => {
          that.apiCall.stopLoading();
          console.log(err)
        });
  }

  filterByDate() {
    let that = this;
    localStorage.setItem("filterByDate", "filterByDate");
    that.apiCall.startLoading().present();
    that.apiCall.filterByDateCall(that.islogin._id, that.page, that.limit, that.dates)
      .subscribe(data => {
        that.apiCall.stopLoading();
        that.ndata = data;
        console.log("filter by date------>",data);
        that.MassArray1 = that.ndata;
        localStorage.removeItem("dates");
      },
        err => {
          that.apiCall.stopLoading();
          console.log(err)
        });
  }

  temp(key) {
    console.log("temp key--->",key)
    this.selectedVehicle = key;
    this.page = 1;
    let that = this;
    var baseUrl2 = this.apiCall.mainUrl + 'notifs/getNotifiLimit?user=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&type=' + that.keyData + '&device=' ;
    console.log("base url---------ssss>",baseUrl2)
    console.log("filete by type ---->",localStorage.getItem("filterByType"))
    if (localStorage.getItem("filterByType") == null) {
      let that = this;
      console.log("filter is null----->")
      var baseUrl2 = this.apiCall.mainUrl + 'notifs/getNotifiLimit?user=' + that.islogin._id
      that.apiCall.startLoading().present();
      that.apiCall.filterByType(baseUrl2)
        .subscribe(data => {
          console.log("dddddddddd------>",data);
          that.apiCall.stopLoading();
          that.ndata = data;
          that.MassArray1 = that.ndata;
          localStorage.removeItem("types");
        },
          err => {
            that.apiCall.stopLoading();
            console.log(err)
          });
    } else {
      this.apiCall.startLoading().present();
      this.apiCall.getFilteredcall(this.islogin._id, this.page, this.limit, key.Device_ID)
        .subscribe(data => {
          this.apiCall.stopLoading();
          console.log("data---response-->",data);
          this.ndata = data;
          this.MassArray1 = this.ndata;
        },
          err => {
            this.apiCall.stopLoading();
            console.log(err)
          });
    }

  }

  getUsersOnScroll() {
    this.apiCall.getDataOnScroll(this.islogin._id, this.page, this.limit)
      .subscribe(
        res => {
          this.ndata = res;
          this.MassArray1 = this.ndata;
        },
        error => {
          console.log(error);
        });
  }

  getVehicleList() {
   // console.log("dddddd------>",this.islogin.isSuperAdmin)
    let that = this;
    var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email;
    if (that.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + that.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + that.islogin._id;
      }
    }
    // that.apiCall.getVehicleListCall(that.islogin._id, that.islogin.email)
    that.apiCall.getVehicleListCall(baseURLp)
      .subscribe(data => {
        that.portstemp = data.devices;
        
      },
        err => {
          console.log(err);
        });
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.page = that.page + 1;
    setTimeout(() => {
      if (localStorage.getItem("filterByType") != null) {
        // when filter by type is selected
        localStorage.setItem("filterByType", "filterByType");
        var baseUrl1;
        if (that.vehicleData != undefined || that.selectedVehicle != undefined) {
          var dev;
          if (that.vehicleData) {
            dev = that.vehicleData.Device_ID;
          } else {
            dev = that.selectedVehicle.Device_ID;
          }

          baseUrl1 = this.apiCall.mainUrl + 'notifs/getNotifiLimit?user=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&type=' + that.keyData + '&device=' + dev;
        } else {
          baseUrl1 = this.apiCall.mainUrl + 'notifs/getNotifiLimit?user=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&type=' + that.keyData;
        }
        that.apiCall.filterByType(baseUrl1)
          .subscribe(data => {
            that.ndata = data;
            for (let i = 0; i < that.ndata.length; i++) {
              that.MassArray1.push(that.ndata[i]);
            }
            localStorage.removeItem("types");
          },
            err => {
              console.log(err)
            });
      } else {
        if (localStorage.getItem("filterByDate") != null) {
          // when filter by date is applied
          that.apiCall.filterByDateCall(that.islogin._id, that.page, that.limit, that.dates)
            .subscribe(data => {
              that.ndata = data;
              for (let i = 0; i < that.ndata.length; i++) {
                that.MassArray1.push(that.ndata[i]);
              }
              localStorage.removeItem("dates");
            },
              err => {
                console.log(err)
              });
        } else {
          if (that.selectedVehicle != undefined || that.vehicleData != undefined) {
            var keyTemp;
            if (that.selectedVehicle == undefined) {
              keyTemp = that.vehicleData;
            } else {
              keyTemp = that.selectedVehicle;
            }
            // when applying sorting by selected vehicle
            that.apiCall.getFilteredcall(that.islogin._id, that.page, that.limit, keyTemp.Device_ID)
              .subscribe(data => {
                console.log("data ---responese--->",data)
                that.ndata = data;
                for (let i = 0; i < that.ndata.length; i++) {
                  that.MassArray1.push(that.ndata[i]);
                }
              },
                err => {
                  console.log(err)
                });
          } else {
            // when no filter applied
            that.apiCall.getDataOnScroll(that.islogin._id, that.page, that.limit)
              .subscribe(
                res => {
                  that.ndata = res;
                  for (let i = 0; i < that.ndata.length; i++) {
                    that.MassArray1.push(that.ndata[i]);
                  }
                },
                error => {
                  console.log(error);
                });
          }
        }
      }
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 100);
  }
}
