import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MsgUtilityPage } from './msg-utility';

@NgModule({
  declarations: [
    MsgUtilityPage,
  ],
  imports: [
    IonicPageModule.forChild(MsgUtilityPage),
  ],
})
export class MsgUtilityPageModule {}
