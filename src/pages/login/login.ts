import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, PopoverController, ViewController, Events, Platform } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { TranslateService } from '@ngx-translate/core';
import { CallNumber } from '@ionic-native/call-number';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm: FormGroup;
  submitAttempt: boolean;
  otpMess: any;
  data:any= {};
  logindata: any;
  userDetails: string;
  details: any;
  showPassword: boolean = false;
  languages: any;
  sales_contact:any;
  support_contact:any;
  // supAdmin: any;
  constructor(

    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public apiservice: ApiServiceProvider,
    public toastCtrl: ToastController,
    public popoverCtrl: PopoverController,
    public translate: TranslateService,
    public plt: Platform,
    private callNumber: CallNumber,
    public events: Events,
  ) {
    this.callBaseURL();
    localStorage.removeItem("ENTERED_BASE_URL");
    this.apiservice.getDealerInfo()
    .subscribe(response => {
      this.support_contact=response.support_contact;
      this.sales_contact = response.sales_contact;
      // this.supAdmin = response.supAdmin;
        console.log("response demo data ----->",response)
        console.log("response demo data ----->",response.demo_id)
        console.log("response demo data ----->",response.demo_pass)
    })
    this.loginForm = formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      // url: []
    })

  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter")
    // if (localStorage.getItem("LANG_KEY") == null) {
    //   this.presentPopover();
    // }
  }

  ionViewDidEnter() {
    console.log("ionViewDidEnter")
  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  callBaseURL() {
    // debugger
    if (localStorage.getItem("ENTERED_BASE_URL") === null) {
      let url = "https://www.oneqlik.in/pullData/getUrlnew";
      this.apiservice.getSOSReportAPI(url)
        .subscribe((data) => {
          console.log("base url: ", data);
          if (data.url) {
            localStorage.setItem("BASE_URL", JSON.stringify(data.url));
          }
          if (data.socket) {
            localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
          }
          this.checkVersion();
        });
    }
  }

  checkVersion() {
    let that = this;
    that.apiservice.getDealerInfo()
      .subscribe(response => {
        console.log("response demo data ----->", response)
        if (response.app_version || response.ios_version) {
          this.events.publish('latest:version', response);
        }
        if (response.supAdmin) {
          localStorage.setItem("DealerInfo", response.supAdmin)
        }
      })
  }

  userlogin() {
    this.submitAttempt = true;

    if (this.loginForm.value.username == "") {
      /*alert("invalid");*/
      return false;
    }
    else if (this.loginForm.value.password == "") {
      /*alert("invalid");*/
      return false;
    }
    function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }

    var isEmail = validateEmail(this.loginForm.value.username);
    var isName = isNaN(this.loginForm.value.username);
    var strNum;


    if (isName == false) {
      strNum = this.loginForm.value.username.trim();
    }

    if (isEmail == false && isName == false && strNum.length == 10) {

      this.data = {

        "psd": this.loginForm.value.password,
        "ph_num": this.loginForm.value.username
      };
    }
    else if (isEmail) {

      this.data = {

        "psd": this.loginForm.value.password,
        "emailid": this.loginForm.value.username
      };
    }
    else {

      this.data = {

        "psd": this.loginForm.value.password,
        "user_id": this.loginForm.value.username
      };
    }

    if(localStorage.getItem("DealerInfo") != null){
      let superAdmin = localStorage.getItem("DealerInfo");
      this.data.supAdmin = superAdmin;
    }

    this.apiservice.startLoading();
    this.apiservice.loginApi(this.data)
      .subscribe(response => {
        this.logindata = response;
        this.logindata = JSON.stringify(response);
        var logindetails = JSON.parse(this.logindata);
        this.userDetails = window.atob(logindetails.token.split('.')[1]);
        this.details = JSON.parse(this.userDetails);
        console.log(this.details.email);
        localStorage.setItem("loginflag", "loginflag");
        localStorage.setItem('details', JSON.stringify(this.details));
        localStorage.setItem('condition_chk', this.details.isDealer);
        localStorage.setItem('VeryFirstLoginUser', this.details._id)
        localStorage.setItem("INTRO", "INTRO")

        this.apiservice.stopLoading();

        let toast = this.toastCtrl.create({
          message: this.translate.instant("Welcome! You're logged In successfully."),
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          this.navCtrl.setRoot('AddDevicesPage');
        });

        toast.present();
      },
        error => {
          localStorage.removeItem("ENTERED_BASE_URL");
          // console.log("login error => "+error)
          var body = error._body;
          var msg = JSON.parse(body);
          if (msg.message == "Mobile Phone Not Verified") {
            let confirmPopup = this.alertCtrl.create({
              title: 'Login failed!',
              message: msg.message = !msg.message ? '  Do you Want to verify it?' : msg.message + '  Do you Want to verify it?',
              buttons: [
                { text: 'Cancel' },
                {
                  text: this.translate.instant('Okay'),
                  handler: () => {
                    // this.navCtrl.push(MobileVerify);
                  }
                }
              ]
            });
            confirmPopup.present();
          } else {
            // Do something on error
            let alertPopup = this.alertCtrl.create({
              title: 'Login failed!',
              message: msg.message,
              buttons: [this.translate.instant('Okay')]
            });
            alertPopup.present();
          }
          this.apiservice.stopLoading();
        });
  }

  presentToast(msg) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 2500
    });
    toast.present();
  }

  gotosignuppage() {
    this.navCtrl.push('SignupPage');
  }

  gotodemopage() {
    this.navCtrl.push('DemoPage');
  }

  forgotPassFunc() {

    const prompt = this.alertCtrl.create({
      title: this.translate.instant('Forgot Password'),
      message: this.translate.instant('Enter your Registered Mobile Number and we will send you a confirmation code.'),
      inputs: [
        {
          name: 'mobno',
          placeholder: this.translate.instant('Mobile Number')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('SEND CONFIRMATION CODE'),
          handler: (data) => {
            var forgotdata = {
              "cred": data.mobno
            }
            this.apiservice.startLoading();
            this.apiservice.forgotPassApi(forgotdata)
              .subscribe(data => {
                this.apiservice.stopLoading();
                this.presentToast(data.message);
                this.otpMess = data;
                this.PassWordConfirmPopup();
              },
                error => {
                  this.apiservice.stopLoading();
                  var body = error._body;
                  var msg = JSON.parse(body);
                  let alert = this.alertCtrl.create({
                    title: this.translate.instant('Forgot Password Failed!'),
                    message: msg.message,
                    buttons: [this.translate.instant('Okay')]
                  });
                  alert.present();
                })
          }
        }
      ]
    });
    prompt.present();
  }

  dialNumber(number) {
    // if (this.showDrawer) {
    //   this.showDrawer = false;
    // }
    console.log("check phone number: ", number)
    if (number !== "" || number !== undefined) {
      if (this.plt.is('android')) {
        console.log("is android")
        window.open('tel:' + number, '_system');
      } else if (this.plt.is('ios')) {
        console.log("is ios")
        // alert(number)
        this.callNumber.callNumber(number.toString(), true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
      }
    } else {
      this.toastCtrl.create({
        message: 'Contact number not found!',
        position: 'middle',
        duration: 2000
      }).present();
    }
  }

  PassWordConfirmPopup() {
    const prompt = this.alertCtrl.create({
      title: this.translate.instant('Reset Password'),
      // message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'newpass',
          placeholder: this.translate.instant('Password')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('Back'),
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('SAVE'),
          handler: data => {
            if (!data.newpass || !data.mobilenum || !data.confCode) {
              let alertPopup = this.alertCtrl.create({
                title: this.translate.instant('Warning'),
                message: this.translate.instant('Fill all mandatory fields!'),
                buttons: [this.translate.instant('Okay')]
              });
              alertPopup.present();
            } else {
              if (data.newpass == data.mobilenum && data.newpass && data.mobilenum) {
                if (data.newpass.length < 6 || data.newpass.length > 12) {
                  var Popup = this.alertCtrl.create({
                    title: this.translate.instant('Warning'),
                    message: this.translate.instant('Password length should be 6 - 12'),
                    buttons: [this.translate.instant('Okay')]
                  });
                  Popup.present();
                } else {
                  var Passwordset = {
                    "newpwd": data.newpass,
                    "otp": data.confCode,
                    "phone": this.otpMess,
                    "cred": this.otpMess
                  }
                  this.apiservice.startLoading();
                  this.apiservice.forgotPassMobApi(Passwordset)
                    .subscribe(data => {
                      this.apiservice.stopLoading();
                      this.presentToast(data.message);
                      this.navCtrl.setRoot(LoginPage);
                    },
                      error => {
                        this.apiservice.stopLoading();
                        var body = error._body;
                        var msg = JSON.parse(body);
                        let alert = this.alertCtrl.create({
                          title: this.translate.instant('Forgot Password Failed!'),
                          message: msg.message,
                          buttons: [this.translate.instant('Okay')]
                        });
                        alert.present();
                      });
                }
              } else {
                let alertPopup = this.alertCtrl.create({
                  title: this.translate.instant('Warning'),
                  message: this.translate.instant('New Password and Confirm Password Not Matched'),
                  buttons: [this.translate.instant('Okay')]
                });
                alertPopup.present();
              }
              if (!data.newpass || !data.mobilenum || !data.confCode) {
                //don't allow the user to close unless he enters model...
                return false;
              }
            }
          }
        }
      ]
    });
    prompt.present();
  }
}

@Component({
  template: `
    <ion-list>
      <ion-list-header style="text-align: center;">{{'Select Language' | translate}}</ion-list-header>
      <button ion-item (click)="setLanguage('en')">{{'English' | translate}}</button>
      <button ion-item (click)="setLanguage('hi')">{{'Hindi' | translate}}</button>
      <button ion-item (click)="setLanguage('bn')">{{'Bangali' | translate}}</button>
      <button ion-item (click)="setLanguage('te')">{{'Telugu' | translate}}</button>
      <button ion-item (click)="setLanguage('ta')">{{'Tamil' | translate}}</button>
      <button ion-item (click)="setLanguage('gu')">{{'Gujarati' | translate}}</button>
      <button ion-item (click)="setLanguage('kn')">{{'Kannada' | translate}}</button>
      <button ion-item (click)="setLanguage('mr')">{{'Marathi' | translate}}</button>
      <button ion-item (click)="setLanguage('ml')">{{'Malayalam' | translate}}</button>
      <button ion-item (click)="setLanguage('sp')">{{'Spanish' | translate}}</button>
      <button ion-item (click)="setLanguage('fa')">{{'Persian' | translate}}</button>
      <button ion-item (click)="setLanguage('ar')">{{'Arabic' | translate}}</button>
      <button ion-item (click)="setLanguage('ne')">{{'Nepali' | translate}}</button>
      <button ion-item (click)="setLanguage('fr')">{{'French' | translate}}</button>
      <button ion-item (click)="setLanguage('pn')">{{'Punjabi' | translate}}</button>
    </ion-list>
  `
})
export class LanguagesPage {
  islogin: any;
  constructor(
    public viewCtrl: ViewController,
    public translate: TranslateService,
    private apiCall: ApiServiceProvider,
    private toastCtrl: ToastController,
    private events: Events) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
  }

  close() {
    this.viewCtrl.dismiss();
  }

  setLanguage(key) {
    var payload = {
      uid: this.islogin._id,
      lang: key
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
      .subscribe((resp) => {
        console.log('response language code ' + resp);
        localStorage.setItem("LanguageKey", key);
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: resp.message,
          duration: 1500,
          position: 'bottom'
        });
        toast.present();
      }, err => {
        localStorage.setItem("LanguageKey", key);
      });
    this.events.publish('lang:key', key);
    localStorage.setItem('LanguageKey', key);
    this.viewCtrl.dismiss();
  }
}
