import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, Navbar, Events, Platform, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GoogleMaps, Marker, LatLng, Spherical, GoogleMapsEvent, LatLngBounds, Geocoder, GeocoderResult, GoogleMapsMapTypeId, Polyline } from '@ionic-native/google-maps';
import { TranslateService } from '@ngx-translate/core';
import { DrawerState } from 'ion-bottom-drawer';
import { ModalPage } from './modal';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
// import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { DatePipe } from '@angular/common';
declare var $: any;

@IonicPage()
@Component({
  selector: 'page-history-device',
  templateUrl: 'history-device.html',
})
export class HistoryDevicePage implements OnInit, OnDestroy {
  @ViewChild(Navbar) navBar: Navbar;


  shouldBounce = true;
  dockedHeight = 100;
  distanceTop = 378;
  drawerState = DrawerState.Docked;
  states = DrawerState;
  minimumHeight = 0;

  showActionSheet: boolean = false;
  transition: any = ['0.5s', 'ease-in-out'];

  device: any;
  trackerId: any;
  trackerType: any;
  DeviceId: any;
  datetimeStart: any;
  datetimeEnd: any;
  hideplayback: boolean;
  trackerName: any;
  avg_speed: string;
  total_dis: string;
  data2: any = {};
  latlongObjArr: any;
  locations: any = [];
  islogin: any;
  dataArrayCoords: any = [];
  mapData: any[];
  speed: number;
  flightPath: any;
  arrival: Date;
  departure: Date;
  target: number;
  playing: boolean;
  coordreplaydata: any;
  speedMarker: any;
  updatetimedate: any;
  showDropDown: boolean;
  SelectVehicle: string = 'Select Vehicle';

  portstemp: any;
  selectedVehicle: any;
  totime: string;
  fromtime: string;
  allData: any = {};
  startPos: any[];
  showZoom: boolean = false;
  address: any;
  arrTime: any;
  depTime: any;
  addressofstudent: any;
  drawerHidden1: boolean;
  arrivalTime: string;
  departureTime: string;
  addressof: string;
  durations: string;
  menuActive: boolean;
  mapKey: string;
  idleLocations: any[];
  addressof123: string;
  latLngArray: any = [];
  devices: any = [];
  markersArray: any = [];
  fraction: number = 0;
  intevalId: any;
  zoomLevel: number = 15;
  direction: number = 1;
  vehicle_speed: number;
  cumu_distance: number = 0;
  recenterMeLat: any;
  recenterMeLng: any;
  addressLine: string = 'N/A';
  twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().format("YYYY-MM-DD");
  battery: number = 0;

  seekBarValue: number = 200;
  sliderValue: number = 0;
  indexValue: any;
  historyPolyline: Polyline;

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public apiCall: ApiServiceProvider,
    private plt: Platform,
    private translate: TranslateService,
    private modalCtrl: ModalController,
    private geocoderApi: GeocoderProvider,
    // private sqlite: SQLite,
    private datePipe: DatePipe,
    public elementRef: ElementRef,
  ) {
    var selectedMapKey;
    if (localStorage.getItem('MAP_KEY') != null) {
      selectedMapKey = localStorage.getItem('MAP_KEY');
      if (selectedMapKey == this.translate.instant('Hybrid')) {
        this.mapKey = 'MAP_TYPE_HYBRID';
      } else if (selectedMapKey == this.translate.instant('Normal')) {
        this.mapKey = 'MAP_TYPE_NORMAL';
      } else if (selectedMapKey == this.translate.instant('Terrain')) {
        this.mapKey = 'MAP_TYPE_TERRAIN';
      } else if (selectedMapKey == this.translate.instant('Satellite')) {
        this.mapKey = 'MAP_TYPE_HYBRID';
      }
    } else {
      this.mapKey = 'MAP_TYPE_NORMAL';
    }
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);
  }

  checkScreen() {
    this.navBar.backButtonClick = (e: UIEvent) => {
      // todo something
      // this.navController.pop();
      console.log("back button poped")
      if (localStorage.getItem("SCREEN") != null) {
        if (localStorage.getItem("SCREEN") === 'live') {
          this.navCtrl.setRoot('LivePage');
        } else {
          if (localStorage.getItem("SCREEN") === 'dashboard') {
            this.navCtrl.setRoot('AddDevicesPage')
          }
        }
      }
    }
  }
  hideMe: boolean = false;
  backBtnEvent() {
    this.navBar.backButtonClick = (ev: UIEvent) => {
      this.hideMe = true;
      console.log('this will work in Ionic 3 +');
      this.navCtrl.pop({
        animate: true, animation: 'transition-ios', direction: 'back'
      });
    }
  }
  speedValue123: any = 1;
  changeSpeed(t) {
    console.log(t);
    let that = this;
    that.speed = t * 100;
    // that.events.publish("SpeedValue:Updated", that.speed)
  }
  data: any;
  measurementUnit: any = 'MKS';
  ionViewDidEnter() {
    this.backBtnEvent();
    ////////////////////////////////////////
    this.getDefaultUserSettings();
    /////////////////////////////////////////
    this.allData.playFlag = 'start';
    this.allData.flag2 = 'init';

    this.initMap();

    if (localStorage.getItem("SCREEN") != null) {
      console.log("screen printed: ", localStorage.getItem('SCREEN'));
      this.checkScreen();
    }
    localStorage.removeItem("markerTarget");
    // localStorage.removeItem('HistoryFlag');
    if (this.navParams.get('device') !== null && this.navParams.get('device') !== undefined) {
      this.device = this.navParams.get('device');
      console.log("passed params: ", this.device);
      this.trackerId = this.device.Device_ID;
      this.trackerType = this.device.iconType;
      this.DeviceId = this.device._id;
      this.trackerName = this.device.Device_Name;
      this.btnClicked();
    } else {
      this.showDropDown = true;
      this.getdevices();
      // this.getDataFromSQLiteDB();
    }
    this.hideplayback = false;
    this.target = 0;
  }

  getDefaultUserSettings() {
    var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        if (resp.unit_measurement !== undefined) {
          this.measurementUnit = resp.unit_measurement;
        } else {
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        }
      },
        err => {
          console.log(err);
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        });
  }
  ngOnInit() { }

  ionViewDidLeave() {
    localStorage.removeItem("markerTarget");
    // localStorage.removeItem("speedMarker");
    // localStorage.removeItem("updatetimedate");
    localStorage.removeItem("MainHistory");
    if (this.intevalId) {
      clearInterval(this.intevalId);
    }
  }


  ngOnDestroy() { }

  changeformat(date) {
    console.log("date=> " + new Date(date).toISOString())
    // this.btnClicked();
  }
  setDocHeight() {
    console.log("dockerchage event")
    this.dockedHeight = 150;
    this.distanceTop = 46;
  }

  closeDocker() {
    let that = this;
    that.showActionSheet = false;
  }

  getdevices() {
    var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.portstemp = data.devices;
      },
        error => {
          this.apiCall.stopLoading();
          console.log(error);
        });
  }

  onChangedSelect(item) {
    debugger
    let that = this;
    // if (that.allData.start2) {
    //   clearTimeout(that.allData.start2);
    //   console.log("timeout cleared!!!!!!!!!!!")
    // }
    // that.allData.flag2 = 'init';
    that.trackerId = item.Device_ID;
    that.trackerType = item.iconType;
    that.DeviceId = item._id;
    that.trackerName = item.Device_Name;
    // if (that.allData.map) {
    //   that.allData.map.clear();
    //   that.allData.map.remove();
    // }
    this.btnClicked();
  }

  reCenterMe() {
    // console.log("getzoom level: " + this.allData.map.getCameraZoom());
    this.allData.map.moveCamera({
      target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
      zoom: this.allData.map.getCameraZoom()
    }).then(() => {

    })
  }
  flag: any;

  getIconUrl() {
    let that = this;
    var iconUrl;
    if (that.plt.is('ios')) {
      iconUrl = 'www/assets/imgs/vehicles/running' + that.trackerType + '.png';
    } else if (that.plt.is('android')) {
      iconUrl = './assets/imgs/vehicles/running' + that.trackerType + '.png';
    }
    console.log("icon url: ", iconUrl);
    return iconUrl;
  }
  start: any;
  play2() {
    let that = this;
    that.allData.speed = 50;
    var coord = that.dataArrayCoords[that.target];
    that.coordreplaydata = coord;
    var lat = coord[0];
    var lng = coord[1];
    debugger
    if (that.allData.flag2 == 'init') {
      localStorage.setItem("AlreadyClicked", "AlreadyClicked")
      if (that.allData.mark == undefined) {
        var icicon;
        if (that.plt.is('ios')) {
          icicon = 'www/assets/imgs/vehicles/running' + that.trackerType + '.png';
        } else if (that.plt.is('android')) {
          icicon = './assets/imgs/vehicles/running' + that.trackerType + '.png';
        }

        that.allData.map.addMarker({
          icon: {
            url: icicon,
            size: {
              width: 20,
              height: 40
            }
          },
          styles: {
            'text-align': 'center',
            'font-style': 'italic',
            'font-weight': 'bold',
            'color': 'green'
          },
          position: new LatLng(lat, lng),
        }).then((marker: Marker) => {
          that.allData.mark = marker;
          that.allData.map.animateCamera({
            target: marker.getPosition(),
            duration: 1500,
            zoom: 16
          })
          that.animateMarker2(that.allData.map, that.allData.mark, null, that.dataArrayCoords);
          that.allData.flag2 = 'stop';
        });
      } else {
        that.allData.mark.setPosition({ lat: lat, lng: lng });
        that.allData.map.setCameraTarget({ lat: lat, lng: lng });
        that.animateMarker2(that.allData.map, that.allData.mark, null, that.dataArrayCoords);
        that.allData.flag2 = 'stop';
      }

    } else
      if (that.allData.flag2 == 'start') {
        that._moveMarker2();
        that.allData.flag2 = 'stop';
      } else
        if (that.allData.flag2 == 'stop') {
          //  dmap.speed = 0;
          clearTimeout(that.allData.start2);
          that.allData.flag2 = 'start';
        }
    if (that.allData.flag2 == 'reset') {
      console.log("flag2 is: ", that.allData.flag2);
      console.log("check reset coords: " + that.dataArrayCoords[0][0])
      that.allData.mark.setPosition({ lat: that.dataArrayCoords[0][0], lng: that.dataArrayCoords[0][1] });
      that.allData.map.setCameraTarget({ lat: that.dataArrayCoords[0][0], lng: that.dataArrayCoords[0][1] });
      that.seekBarValue = 0;
      clearTimeout(that.allData.start2);
      that.allData.flag2 = 'init';
    }
    return that.allData.flag2;
  };
  _goToPoint2: () => void;
  _moveMarker2: () => void;
  animateMarker2(map, mark, icons, coords) {
    let that = this;
    that.cumu_distance = 0;
    that.allData.speed = 50;

    that.allData.delay = 100;
    if (that.allData.start2)
      clearTimeout(that.allData.start2);
    var target = 0;
    that._goToPoint2 = function () {
      if (that.speed) {
        that.allData.speed = that.speed;
      }
      ///////////////////////////////////////////////
      if (that.rangeDetector === true) {
        a = that.indexValue;
        target = that.indexValue;
        that.sliderValue = that.indexValue;
      }
      ///////////////////////////////////////////////
      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;
      var step = (that.allData.speed * 1000 * that.allData.delay) / 3600000; // in meters

      if (coords[target] === undefined) {
        if (that.allData.start2)
          clearTimeout(that.allData.start2);

        that.allData.flag2 = 'init';
        that.sliderValue = 0;
        return;
      }
      var dest = new LatLng(coords[target][0], coords[target][1]);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target][0] - lat) / numStep;
      var deltaLng = (coords[target][1] - lng) / numStep;
      function changeMarker(mark, deg) {
        console.log("check marker: ", mark)
        if (Number.isNaN(parseInt(deg))) {
          console.log("check degree: " + parseInt(deg))
        } else {
          console.log("check not: " + parseInt(deg))
          if (mark) {
            mark.setRotation(deg);
          }
        }
      }
      that._moveMarker2 = function () {
        var head;
        that.sliderValue = a;
        that.cumu_distance = coords[target][5].cumu_dist;

        console.log("cumulative distance: ", that.cumu_distance);
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng))
          if ((head !== 0) || (head !== NaN)) {
            changeMarker(mark, head);
          }
          mark.setPosition(new LatLng(lat, lng));
          map.setCameraTarget({ lat: lat, lng: lng });
          that.getAddress(lat, lng);
          that.allData.start2 = setTimeout(that._moveMarker2, that.allData.delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          if ((head !== 0) || (head !== NaN)) {
            changeMarker(mark, head);
          }
          mark.setPosition(dest);
          map.setCameraTarget(dest);
          that.getAddress(dest.lat, dest.lng);
          target++;
          if (target == coords.length) {
            that.allData.flag2 = 'reset';
            clearTimeout(that.allData.start2);
          }
          that.allData.start2 = setTimeout(that._goToPoint2, that.allData.delay);
        }
      }
      a++

      that.rangeDetector = false;
      console.log("aaaaaaaaaaaaaaaaa " + a);
      console.log("coords length " + coords.length);
      if (a > coords.length) {
        console.log("inside this aaaaaaaaaaaaaaaaa " + a);
      }
      else {
        console.log("coords target: ", coords[target])
        that.speedMarker = coords[target][3].speed;
        that.updatetimedate = coords[target][2].time;
        // that.cumu_distance = coords[target][5].cumu_dist;
        that.battery = coords[target][6].battery;

        console.log('move marker running')
        that._moveMarker2();
      }
    }
    var a = 0;
    that._goToPoint2();
  }

  getAddress(lat, lng) {
    let that = this;
    var coordinates = {
      lat: lat,
      long: lng
    };
    if (!coordinates) {
      that.addressLine = 'N/A';
      return;
    }
    this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        // that.saveAddressToServer(str, coordinates.lat, coordinates.long);
        that.addressLine = str;
      })
  }

  zoomin() {
    let that = this;
    that.allData.map.animateCameraZoomIn()
    // that.allData.map.moveCameraZoomIn();
  }
  zoomout() {
    let that = this;
    that.allData.map.animateCameraZoomOut();
  }

  showRoute: boolean = true;
  hidePlayRoute() {
    let that = this;
    that.showRoute = !that.showRoute;
    if (!that.showRoute) {
      if (that.historyPolyline) {
        that.historyPolyline.remove();
      }
    } else {
      that.historyPolyline = undefined;
      that.allData.map.addPolyline({
        points: that.mapData,
        color: '#635400',
        width: 3,
        geodesic: true
      }).then((polyline: Polyline) => {
        that.historyPolyline = polyline;
      })
    }

  }

  btnClicked() {
    this.speedMarker = undefined;
    this.updatetimedate = undefined;
    this.cumu_distance = undefined;
    this.battery = undefined;
    this.data2.Distance = undefined;

    let dev = this.navParams.get('device');
    if (dev === null || dev === undefined) {
      if (this.selectedVehicle === undefined) {
        let alert = this.alertCtrl.create({
          title: 'Alert',
          message: 'Please select the vehicle first and try again',
          buttons: ['Okay']
        });
        alert.present();
      } else {
        if (this.mapData !== undefined) {
          if (this.mapData.length > 0) {
            if (this.allData.map) {
              this.allData.map.remove();
              if (this.allData.mark)
                this.allData.mark.remove();
              ///////////////////////////////////////
              let that = this;
              if (that.allData.start2) {
                clearTimeout(that.allData.start2);
                console.log("timeout cleared!!!!!!!!!!!")
              }
              /////////////////////////////
              this.allData = {};

              that.allData.flag2 = 'init';
              this.initMap();
              this.maphistory();
            }
          } else {
            this.maphistory();
          }
        } else {
          this.maphistory();
        }

      }
    } else {
      if (this.mapData !== undefined) {
        if (this.mapData.length > 0) {
          if (this.allData.map) {
            this.allData.map.remove();
            if (this.allData.mark)
              this.allData.mark.remove();
            ///////////////////////////////////////
            let that = this;
            if (that.allData.start2) {
              clearTimeout(that.allData.start2);
              console.log("timeout cleared!!!!!!!!!!!")
            }
            /////////////////////////////
            this.allData = {};
            that.allData.flag2 = 'init';
            this.initMap();
            this.maphistory();
          }
        } else {
          this.maphistory();
        }
      } else {
        this.maphistory();
      }
    }
  }

  maphistory() {
    let that = this;
    that.mapData = [];
    that.latLngLine = [];
    that.sliderValue = 0;
    that.dataArrayCoords = [];
    this.data2 = {};
    // that.allData
    this.latlongObjArr = undefined;

    let from1 = new Date(this.datetimeStart);
    this.fromtime = from1.toISOString();
    let to1 = new Date(this.datetimeEnd);
    this.totime = to1.toISOString();

    if (this.totime >= this.fromtime) {

    } else {
      let alert = this.alertCtrl.create({
        title: 'Select Correct Time',
        message: 'To time always greater than From Time',
        buttons: ['ok']
      });
      alert.present();
      return false;
    }

    this.getHistoryData();
  }
  datePipeString: string;
  getHistoryData() {
    let that = this;
    this.apiCall.startLoading().present();
    that.apiCall.gpsCall(this.trackerId, new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString())
      .subscribe(data3 => {
        that.apiCall.stopLoading();
        if (data3.length > 1) {   // to draw polyline at least need two points
          that.gps(data3.reverse());
          that.getDistance();
        } else {
          let alert = that.alertCtrl.create({
            title: 'No Data Found',
            message: 'Vehicle has not moved from ' + this.datePipe.transform(new Date(this.datetimeStart), 'medium') + ' to ' + this.datePipe.transform(new Date(this.datetimeEnd), 'medium'),
            buttons: [{
              text: 'OK',
              handler: () => {
                that.hideplayback = false;
              }
            }]
          });
          alert.present();
        }
      },
        err => {
          that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = that.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
    // }
  }

  getDistance() {
    // this.apiCall.startLoading().present();
    this.apiCall.getDistanceSpeedCall(this.trackerId, new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString())
      .subscribe(data3 => {
        debugger
        this.data2 = data3;
        this.latlongObjArr = data3;
        // debugger
        if (isNaN(this.data2["Average Speed"])) {
          this.data2.AverageSpeed = 0;
        } else {
          this.data2.AverageSpeed = this.data2["Average Speed"];
        }

        this.data2.IdleTime = this.data2["Idle Time"];
        this.hideplayback = true;

        //////////////////////////////////
        // this.callgpsFunc(this.fromtime, this.totime);

        // this.locations = [];
        // this.stoppages(timeStart, timeEnd);
        ////////////////////////////////
      },
        error => {
          // this.apiCall.stopLoading();
          console.log("error in getdistancespeed =>  ", error)
          var body = error._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: ['okay']
          });
          alert.present();
        });

  }

  stoppages() {
    this.locations = [];
    let that = this;
    that.apiCall.stoppage_detail(this.islogin._id, new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), this.DeviceId)
      .subscribe(res => {
        console.log('stoppage data', res)
        var arr = [];
        for (var i = 0; i < res.length; i++) {

          this.arrivalTime = new Date(res[i].arrival_time).toLocaleString();
          this.departureTime = new Date(res[i].departure_time).toLocaleString();

          var fd = new Date(this.arrivalTime).getTime();
          var td = new Date(this.departureTime).getTime();
          var time_difference = td - fd;
          var total_min = time_difference / 60000;
          var hours = total_min / 60
          var rhours = Math.floor(hours);
          var minutes = (hours - rhours) * 60;
          var rminutes = Math.round(minutes);
          var Durations = rhours + 'Hours' + ':' + rminutes + 'Min';

          arr.push({
            lat: res[i].lat,
            lng: res[i].long,
            arrival_time: res[i].arrival_time,
            departure_time: res[i].departure_time,
            device: res[i].device,
            address: res[i].address,
            user: res[i].user,
            duration: Durations
          });

          that.locations.push(arr);
          if (that.locations[0] != undefined) {              // check if there is stoppages or not
            for (var k = 0; k < that.locations[0].length; k++) {
              that.setStoppages(that.locations[0][k]);
            }
          }

        }
        console.log('stoppage data locations', that.locations)
        // this.callgpsFunc(this.fromtime, this.totime);
      },
        err => {
          // this.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: ['okay']
          });
          alert.present();
        });
  }

  callgpsFunc(fromtime, totime) {
    let that = this;
    that.apiCall.gpsCall(this.trackerId, fromtime, totime)
      .subscribe(data3 => {
        // that.apiCall.stopLoading();
        if (data3.length > 0) {
          if (data3.length > 1) {   // to draw polyline at least need two points
            that.gps(data3.reverse());
          } else {
            let alert = that.alertCtrl.create({
              message: 'No Data found for selected vehicle..',
              buttons: [{
                text: 'OK',
                handler: () => {
                  // that.datetimeStart = moment({ hours: 0 }).format();
                  // console.log('start date', this.datetimeStart)
                  // that.datetimeEnd = moment().format();//new Date(a).toISOString();
                  // console.log('stop date', this.datetimeEnd);

                  // that.selectedVehicle = undefined;
                  that.hideplayback = false;
                }
              }]
            });
            alert.present();
          }
        } else {
          let alert = that.alertCtrl.create({
            message: 'No Data found for selected vehicle..',
            buttons: [{
              text: 'OK',
              handler: () => {
                // that.datetimeStart = moment({ hours: 0 }).format();
                // console.log('start date', this.datetimeStart)
                // that.datetimeEnd = moment().format();//new Date(a).toISOString();
                // console.log('stop date', this.datetimeEnd);
                // that.selectedVehicle = undefined;
                that.hideplayback = false;
              }
            }]
          });
          alert.present();
        }

      },
        err => {
          // that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = that.alertCtrl.create({
            message: msg.message,
            buttons: [this.translate.instant('Okay')]
          });
          alert.present();
        });
  }

  replayHistory() {
    let that = this;
    that.allData.flag2 = 'reset';
    that.speedMarker = 0;
    that.updatetimedate = undefined;
    that.cumu_distance = 0;
    that.battery = 0;
    that.play2();
  }

  changeDate(dateid) {
    if (dateid == 'today') {
      var td = new Date();
      let rt = new Date(td.setHours(0, 0, 0));
      this.datetimeStart = moment(rt).format();
      this.datetimeEnd = moment().format();
      this.btnClicked();
    }
    if (dateid == 'yest') {
      let yt: any;
      yt = moment().subtract(1, 'days');
      var ptt = new Date(yt).setHours(0, 0, 0);
      this.datetimeStart = moment(new Date(ptt)).format();
      var ttt = new Date(yt).setHours(23, 59, 59);
      this.datetimeEnd = moment(new Date(ttt)).format();
      this.btnClicked();
    }
    if (dateid == 'week') {
      var wd: any;
      wd = moment().startOf('week')
      this.datetimeStart = moment(new Date(wd)).format();
      this.datetimeEnd = moment().format();
      this.btnClicked();
    }
    if (dateid == 'hour') {
      // var md = moment().startOf('hour').format();
      var md = moment.utc().subtract(1, "hours").format();
      this.datetimeStart = moment(new Date(md)).format();
      console.log("start hour: ", this.datetimeStart)
      this.datetimeEnd = moment().format();
      console.log("end hour: ", this.datetimeEnd)

      this.btnClicked();
    }
  }

  initMap() {
    if (this.allData.map != undefined) {
      this.allData.map.remove();
    }
    let mapOptions = {
      gestures: {
        rotate: false,
        tilt: false,
        compass: false
      },
      mapType: this.mapKey
    }
    this.allData.map = GoogleMaps.create('map_canvas', mapOptions);
  }

  gps(data3) {
    let that = this;

    that.mapData = data3.map(function (d) {
      return { lat: d.lat, lng: d.lng };
    })
    that.mapData.reverse();
    let bounds = new LatLngBounds(that.mapData);
    that.allData.map.moveCamera({
      target: bounds
    })
    that.latlongObjArr = data3;
    var cumulativeDistance = 0;
    for (var i = 0; i < that.latlongObjArr.length; i++) {
      if (that.latlongObjArr[i].lat && that.latlongObjArr[i].lng) {
        var arr = [];

        var startdatetime = new Date(that.latlongObjArr[i].insertionTime);
        arr.push(that.latlongObjArr[i].lat);
        arr.push(that.latlongObjArr[i].lng);
        arr.push({ "time": startdatetime.toLocaleString() });
        arr.push({ "speed": that.latlongObjArr[i].speed });
        arr.push({ "imei": that.latlongObjArr[i].imei })
        debugger
        if (that.latlongObjArr[i].isPastData != true) {
          if (i === 0) {
            cumulativeDistance += 0;
          } else {
            cumulativeDistance += that.latlongObjArr[i].distanceFromPrevious ? parseFloat(that.latlongObjArr[i].distanceFromPrevious) : 0;
          }
          that.latlongObjArr[i]['cummulative_distance'] = (cumulativeDistance);
          arr.push({ "cumu_dist": that.latlongObjArr[i]['cummulative_distance'] });
        } else {
          that.latlongObjArr[i]['cummulative_distance'] = (cumulativeDistance);
          arr.push({ "cumu_dist": that.latlongObjArr[i]['cummulative_distance'] });
        }

        arr.push({ "battery": that.latlongObjArr[i].external_Battery });
        let cord = {
          lat: that.latlongObjArr[i].lat,
          lng: that.latlongObjArr[i].lng
        }
        // console.log("check battery: ", data3[i]['external_Battery'])
        that.dataArrayCoords.push(arr);
        that.latLngLine.push(cord);
      }
    }

    that.seekBarValue = that.dataArrayCoords.length;

    this.allData.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(
      (data) => {
        console.log('Click MAP');

        that.drawerHidden1 = true;
      }
    );

    let start_icon; let stop_icon;
    if (this.plt.is('android')) {
      start_icon = './assets/imgs/greenFlag.png';
      stop_icon = './assets/imgs/redFlag.png';
    } else if (this.plt.is('ios')) {
      start_icon = 'www/assets/imgs/greenFlag.png';
      stop_icon = 'www/assets/imgs/redFlag.png';
    }

    that.allData.map.addMarker({
      title: 'D',
      position: that.mapData[0],
      icon: {
        url: stop_icon,
        size: {
          height: 40,
          width: 40
        }
      },
      styles: {
        'text-align': 'center',
        'font-style': 'italic',
        'font-weight': 'bold',
        'color': 'red'
      },
    }).then((marker: Marker) => {
      // marker.showInfoWindow();

      that.allData.map.addMarker({
        title: 'S',
        position: that.mapData[that.mapData.length - 1],
        icon: {
          url: start_icon,
          size: {
            height: 40,
            width: 40
          }
        },
        styles: {
          'text-align': 'center',
          'font-style': 'italic',
          'font-weight': 'bold',
          'color': 'green'
        },
      }).then((marker: Marker) => {
        // marker.showInfoWindow();
      });
    });

    that.allData.map.addPolyline({
      points: that.mapData,
      color: '#635400',
      width: 3,
      geodesic: true
    }).then((polyline: Polyline) => {
      that.historyPolyline = polyline;
    })
    that.hideplayback = true;

    //////////////
    var playerSeekbar: any;
    $(document).ready(function () {
      debugger
      playerSeekbar = document.getElementById('slider1');
      console.log("ready!", playerSeekbar['value']);
      playerSeekbar.oninput = function () {

        // zoomToObject(flightPath);
        that.changeRange();

      }
    });
    //////////////
    that.stoppages();
  }
  rangeDetector: boolean = false;
  latLngLine = [];

  changeRange() {
    // debugger
    let that = this;
    clearTimeout(that.allData.start2);
    that.rangeDetector = true;
    var rangeVal = document.getElementById("slider1");

    this.indexValue = rangeVal['value'];

    if (that.allData.flag2 == 'stop') {
      that.allData.mark.setPosition({ lat: this.latLngLine[this.indexValue].lat, lng: this.latLngLine[this.indexValue].lng });
      // this.historyMap.setCenter({ lat: this.latLngLine[this.indexValue].lat, lng: this.latLngLine[this.indexValue].lng });
      that.allData.map.moveCamera({
        target: {
          lat: this.latLngLine[this.indexValue].lat,
          lng: this.latLngLine[this.indexValue].lng
        }
      });
      //////////////////
      that.speedMarker = this.dataArrayCoords[this.indexValue][3].speed;
      that.updatetimedate = this.dataArrayCoords[this.indexValue][2].time;
      that.cumu_distance = this.dataArrayCoords[this.indexValue][5].cumu_dist;
      that.battery = this.dataArrayCoords[this.indexValue][6].battery;

      console.log("check speed value: ", that.speedMarker)
      console.log("check updatetimedate value: ", that.updatetimedate)
      console.log("check cumu_distance value: ", that.cumu_distance)
      console.log("check speed battery: ", that.battery)
      //////////////////
      that._goToPoint2();

    }
    if (that.allData.flag2 == 'start') {
      that.allData.mark.setPosition({ lat: this.latLngLine[this.indexValue].lat, lng: this.latLngLine[this.indexValue].lng });
      // this.historyMap.setCenter({ lat: this.latLngLine[this.indexValue].lat, lng: this.latLngLine[this.indexValue].lng });
      that.allData.map.moveCamera({
        target: {
          lat: this.latLngLine[this.indexValue].lat,
          lng: this.latLngLine[this.indexValue].lng
        }
      });
      that.allData.flag2 = 'stop';

      //////////////////
      that.speedMarker = this.dataArrayCoords[this.indexValue][3].speed;
      that.updatetimedate = this.dataArrayCoords[this.indexValue][2].time;
      that.cumu_distance = this.dataArrayCoords[this.indexValue][5].cumu_dist;
      that.battery = this.dataArrayCoords[this.indexValue][6].battery;
      //////////////////
      console.log("check speed value123: ", that.speedMarker)
      console.log("check updatetimedate value123: ", that.updatetimedate)
      console.log("check cumu_distance value123: ", that.cumu_distance)
      console.log("check speed battery123: ", that.battery)
      that._goToPoint2();
    }

  }

  zoomSet() {
    // this.allData.map.setZoom(15);
    this.allData.map.moveCamera({
      zoom: 15,
      // tilt: 30,
      // bearing: 45
    });
  }


  setStoppages(pdata) {
    let that = this;
    ///////////////////////////////
    // let htmlInfoWindow = new HtmlInfoWindow();
    // let frame: HTMLElement = document.createElement('div');
    // frame.innerHTML = [
    //   '<p style="font-size: 7px;">Address:- ' + pdata.address + '</p>',
    //   '<p style="font-size: 7px;">Arrival Time:- ' + moment(new Date(pdata.arrival_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a") + '</p>',
    //   '<p style="font-size: 7px;">Departure Time:- ' + moment(new Date(pdata.departure_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a") + '</p>'
    // ].join("");

    // htmlInfoWindow.setContent(frame, { width: "220px", height: "100px" });
    ///////////////////////////////////////////////////

    if (pdata != undefined)
      (function (data) {
        // console.log("inside for data=> ", data)

        var centerMarker = data;
        let location = new LatLng(centerMarker.lat, centerMarker.lng);
        var markicon;
        if (that.plt.is('ios')) {
          markicon = 'www/assets/imgs/park.png';
        } else if (that.plt.is('android')) {
          markicon = './assets/imgs/park.png';
        }
        let markerOptions = {
          position: location,
          icon: {
            url: markicon,
            size: {
              height: 22,
              width: 22
            }
          }
        };
        that.allData.map.addMarker(markerOptions)
          .then((marker: Marker) => {
            // console.log('centerMarker.ID' + centerMarker.ID)
            marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
              .subscribe(e => {
                that.showActionSheet = true;
                // that.drawerHidden1 = false;
                that.drawerState = DrawerState.Docked;
                Geocoder.geocode({
                  "position": {
                    lat: e[0].lat,
                    lng: e[0].lng
                  }
                }).then((results: GeocoderResult[]) => {
                  if (results.length == 0) {
                    return null;
                  }
                  that.addressof = results[0].extra.lines[0];
                });

                setTimeout(function () {

                  that.address = that.addressof;
                  console.log("pickup location new ", that.address);
                  that.arrTime = moment(new Date(data.arrival_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a");
                  that.depTime = moment(new Date(data.departure_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a");

                  var fd = new Date(data.arrival_time).getTime();
                  var td = new Date(data.departure_time).getTime();
                  var time_difference = td - fd;
                  var total_min = time_difference / 60000;
                  var hours = total_min / 60
                  var rhours = Math.floor(hours);
                  var minutes = (hours - rhours) * 60;
                  var rminutes = Math.round(minutes);
                  that.durations = rhours + 'hours' + ':' + rminutes + 'mins'
                }, 500);

              });
          });

      })(pdata)
  }

  onIdle() {
    this.presentModal();

  }
  presentModal() {
    const modal = this.modalCtrl.create(ModalPage);
    modal.present();

    modal.onDidDismiss((data) => {
      console.log("onDidDismiss", data);
      this.getIdlePoints(data);
    })
  }

  getIdlePoints(min) {
    this.idleLocations = [];
    var urlbase = this.apiCall.mainUrl + 'stoppage/trip_idle?uId=' + this.islogin._id + '&from_date=' + new Date(this.datetimeStart).toISOString() + '&to_date=' + new Date(this.datetimeEnd).toISOString() + '&device=' + this.DeviceId + '&min_time=' + min;
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(urlbase)
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log("idle data=> " + data);
        if (data.length > 0) {
          for (var y = 0; y <= data.length; y++) {
            this.idleLocations.push(data[y]);
          }

          if (this.idleLocations.length > 0) {              // check if there is stoppages or not
            for (var k = 0; k < this.idleLocations.length; k++) {
              this.setIdlePoints(this.idleLocations[k]);
            }
          }
        }
      })
  }

  setIdlePoints(pdata) {
    let that = this;
    if (pdata != undefined)
      (function (data) {
        console.log("inside for data=> ", data)

        var centerMarker = data;
        let location = new LatLng(centerMarker.idle_location.lat, centerMarker.idle_location.long);
        var markicon;
        if (that.plt.is('ios')) {
          markicon = 'www/assets/imgs/idle.png';
        } else if (that.plt.is('android')) {
          markicon = './assets/imgs/idle.png';
        }
        let markerOptions = {
          position: location,
          icon: {
            url: markicon,
            size: {
              height: 22,
              width: 22
            }
          }
        };
        that.allData.map.addMarker(markerOptions)
          .then((marker: Marker) => {
            // console.log('centerMarker.ID' + centerMarker.ID)
            marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
              .subscribe(e => { });
          });

      })(pdata)
  }

  onClickMainMenu(item) {
    this.menuActive = !this.menuActive;
  }
  onClickMap(maptype) {
    let that = this;
    if (maptype == 'SATELLITE') {
      that.allData.map.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
    } else {
      if (maptype == 'TERRAIN') {
        that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
      } else {
        if (maptype == 'NORMAL') {
          that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
        }
      }
    }
  }

}
