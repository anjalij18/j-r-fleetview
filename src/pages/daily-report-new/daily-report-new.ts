import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-daily-report-new',
  templateUrl: 'daily-report-new.html',
})
export class DailyReportNewPage {
  to: string;
  page: number = 0;
  vehicleData: any;
  limit: number = 10;
  islogin: any;
  from: string;
  deviceReport: any[] = [];
  selectedVehicle: any;
  portstemp: any[] = [];
  dataParam: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public apicalldaily: ApiServiceProvider) {
    this.dataParam = navParams.get("item");
    console.log("selected vehicle", this.dataParam);
    if(this.dataParam[0] != null ) {
    this.vehicleData = this.dataParam[0]
    console.log("cheid", this.vehicleData);
    // this.selectedVehicle = this.dataParam[0]
    // console.log("ch", this.selectedVehicle.Device_Name);
  }
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    this.from = moment({ hours: 0 }).format();
    console.log('start date', this.from)
    this.to = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.to);

    if (navParams.get('param') != null) {
      this.vehicleData = navParams.get('param');
    }
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter DailyReportNewPage');
    this.getdevices();
    this.getDailyReportData();
    this.getDefaultUserSettings();
  }

  measurementUnit: string = 'MKS';
  getDefaultUserSettings() {
    var b_url = this.apicalldaily.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apicalldaily.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        if (resp.unit_measurement !== undefined) {
          this.measurementUnit = resp.unit_measurement;
        } else {
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        }
      },
        err => {
          console.log(err);
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        });
  }
  getSelectedId(pdata) {
    console.log(pdata)
    this.vehicleData = pdata;
    this.getDailyReportData();
  }

  getdevices() {
    var baseURLp = this.apicalldaily.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }

    this.apicalldaily.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        // this.apicalldaily.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          // this.apicalldaily.stopLoading();
          console.log(err);
        });
  }

  getDailyReportData() {
    console.log("entered");
    this.page = 0;
    var baseUrl;
    baseUrl = this.apicalldaily.mainUrl + "devices/daily_report";
    let that = this;
    var currDay = new Date().getDay();
    var currMonth = new Date().getMonth();
    var currYear = new Date().getFullYear();
    var selectedDay = new Date(that.to).getDay();
    var selectedMonth = new Date(that.to).getMonth();
    var selectedYear = new Date(that.to).getFullYear();
    var devname, devid, today_odo, today_running, today_stopped, t_idling, t_ofr, today_trips, maxSpeed, mileage;
    if ((currDay == selectedDay) && (currMonth == selectedMonth) && (currYear == selectedYear)) {
      devname = "Device_Name";
      devid = "Device_ID";
      today_odo = "today_odo";
      today_running = "today_running";
      today_stopped = "today_stopped";
      t_idling = "t_idling"
      t_ofr = "t_ofr";
      today_trips = "today_trips";
      maxSpeed = "maxSpeed";
      mileage = "Mileage"
    } else {
      console.log("else block called");
      devid = "imei";
      devname = "ID.Device_Name";
      today_odo = "today_odo";
      today_running = "today_running";
      today_stopped = "today_stopped";
      t_idling = "t_idling"
      t_ofr = "t_ofr";
      today_trips = "today_trips";
      maxSpeed = "ID.maxSpeed";
      mileage = "ID.Mileage"
    }
    // debugger
    var payload = {};
    if (this.vehicleData == undefined) {
      payload = {
        "draw": 2,
        "columns": [
          {
            "data": devname
          },
          {
            "data": devid
          },
          {
            "data": today_odo
          },
          {
            "data": today_running
          },
          {
            "data": today_stopped
          },
          {
            "data": t_idling
          },
          {
            "data": t_ofr
          },
          {
            "data": today_trips
          },
          {
            "data": maxSpeed
          },
          {
            "data": mileage
          },
          { "data": "t_running" },
          { "data": "t_stopped" },
          { "data": "t_idling" },
          { "data": "t_ofr" },
          { "data": "t_noGps" },
          {
            "data": null,
            "defaultContent": ""
          }
        ],
        "order": [
          {
            "column": 0,
            "dir": "asc"
          }
        ],
        "start": 0,
        "length": this.limit,
        "search": {
          "value": "",
          "regex": false
        },
        "op": {},
        "select": [],
        "find": {
          "user_id": this.islogin._id,
          "date": new Date(this.to).toISOString()
        }
      }
    } else {
      payload = {
        "draw": 2,
        "columns": [
          {
            "data": devname
          },
          {
            "data": devid
          },
          {
            "data": today_odo
          },
          {
            "data": today_running
          },
          {
            "data": today_stopped
          },
          {
            "data": t_idling
          },
          {
            "data": t_ofr
          },
          {
            "data": today_trips
          },
          {
            "data": mileage
          },
          {
            "data": maxSpeed
          },
          { "data": "t_running" },
          { "data": "t_stopped" },
          { "data": "t_idling" },
          { "data": "t_ofr" },
          { "data": "t_noGps" },
          {
            "data": null,
            "defaultContent": ""
          }
        ],
        "order": [
          {
            "column": 0,
            "dir": "asc"
          }
        ],
        "start": 0,
        "length": this.limit,
        "search": {
          "value": "",
          "regex": false
        },
        "op": {},
        "select": [],
        "find": {
          "user_id": this.islogin._id,
          "devId": this.vehicleData.Device_ID,
          "date": new Date(this.to).toISOString()
        }
      }
    }
    this.deviceReport = [];
    this.apicalldaily.startLoading().present();
    this.apicalldaily.getDailyReport1(baseUrl, payload)
      .subscribe(data => {
        this.apicalldaily.stopLoading();
        console.log("daily report data: ", data)
        for (var i = 0; i < data.data.length; i++) {
debugger
          this.deviceReport.push({
            _id: data.data[i]._id,
            Device_ID: data.data[i].Device_ID ? data.data[i].Device_ID : data.data[i].imei,
            Device_Name: data.data[i].Device_Name ? data.data[i].Device_Name : (data.data[i].ID ? data.data[i].ID.Device_Name : 'N/A'),
            maxSpeed: data.data[i].maxSpeed ? data.data[i].maxSpeed : (data.data[i].ID ? data.data[i].ID.maxSpeed : '0'),
            today_odo: data.data[i].today_odo,
            today_running: this.millisToMinutesAndSeconds(data.data[i].today_running),
            today_stopped: this.millisToMinutesAndSeconds(data.data[i].today_stopped),
            t_idling: this.millisToMinutesAndSeconds(data.data[i].t_idling),
            t_ofr: this.millisToMinutesAndSeconds(data.data[i].t_ofr),
            today_trips: data.data[i].today_trips,
            mileage: data.data[i].Mileage ? ((data.data[i].today_odo) / Number(data.data[i].Mileage)).toFixed(2) : (data.data[i].ID ? ((data.data[i].today_odo) / Number(data.data[i].ID.Mileage)).toFixed(2) : 'N/A')
          })
        }

      }, error => {
        this.apicalldaily.stopLoading();
        console.log("error in service=> " + error);
      })
  }

  millisToMinutesAndSeconds(millis) {
    var ms = millis;
    ms = 1000 * Math.round(ms / 1000); // round to nearest second
    var d = new Date(ms);
    // debugger
    var min1;
    var min = d.getUTCMinutes();
    if ((min).toString().length == 1) {
      min1 = '0' + (d.getUTCMinutes()).toString();
    } else {
      min1 = min;
    }

    return d.getUTCHours() + ':' + min1;
  }
}
