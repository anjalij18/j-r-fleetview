// import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';


// @IonicPage()
// @Component({
//   selector: 'page-reports-collection',
//   templateUrl: 'reports-collection.html',
// })
// export class ReportsCollectionPage {

//   constructor(public navCtrl: NavController, public navParams: NavParams) {
//   }

//   ionViewDidLoad() {
//     console.log('ionViewDidLoad ReportsCollectionPage');
//   }

// }

import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

/**
 * Generated class for the ReportsCollectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reports-collection',
  templateUrl: 'reports-collection.html',
})
export class ReportsCollectionPage implements OnInit {
  selectedVehicle: any;
  overSpeeddevice_id: any = [];
  portstemp: any;
  islogin: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public apicalloverspeed: ApiServiceProvider) {
      this.islogin = JSON.parse(localStorage.getItem('details')) || {};
      console.log("email => " + this.islogin._id);
  }

  getOverspeeddevice(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.overSpeeddevice_id = selectedVehicle
    // this.overSpeeddevice_id = [];
    // if (selectedVehicle.length > 0) {
    //   if (selectedVehicle.length > 1) {
    //     for (var t = 0; t < selectedVehicle.length; t++) {
    //       this.overSpeeddevice_id.push(selectedVehicle[t].Device_ID)
    //     }
    //   } else {
    //     this.overSpeeddevice_id.push(selectedVehicle[0].Device_ID)
    //   }
    // } else return;
    // console.log("selectedVehicle=> ", this.overSpeeddevice_id)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportsCollectionPage');
  }

  acReport() {
    this.navCtrl.push("AcReportPage", {
      item: this.overSpeeddevice_id
    })
  }

  dailyReport() {
    this.navCtrl.push("DailyReportPage", {
      item: this.overSpeeddevice_id
    })

  }

  disance() {
    this.navCtrl.push("DistanceReportPage", {
      item: this.overSpeeddevice_id
    })

  }

  geo() {
    this.navCtrl.push("GeofenceReportPage", {
      item: this.overSpeeddevice_id
    })
  }

  iginition() {
    this.navCtrl.push("IgnitionReportPage", {
      item: this.overSpeeddevice_id
    })
  }

  stoppage() {
    this.navCtrl.push("StoppagesRepoPage", {
      item: this.overSpeeddevice_id
    })

  }

  trip() {
    this.navCtrl.push("TripReportPage", {
      item: this.overSpeeddevice_id
    })
  }

  working() {
    this.navCtrl.push("WorkingHoursReportPage", {
      item: this.overSpeeddevice_id
    })
  }

  overspeed() {
    this.navCtrl.push("OverSpeedRepoPage", {
      item: this.overSpeeddevice_id
    })
  }

  value() {
    this.navCtrl.push("DailyReportNewPage", {
      item: this.overSpeeddevice_id
    })
  }

  ngOnInit() {
    this.getdevices();
  }

  getdevices() {
    var baseURLp = this.apicalloverspeed.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicalloverspeed.startLoading().present();
    this.apicalloverspeed.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicalloverspeed.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          this.apicalloverspeed.stopLoading();
          console.log(err);
        });
  }


}
