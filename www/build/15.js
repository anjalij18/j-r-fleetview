webpackJsonp([15],{

/***/ 1079:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnCreate2Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dummy2_directive__ = __webpack_require__(1080);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var OnCreate2Module = /** @class */ (function () {
    function OnCreate2Module() {
    }
    OnCreate2Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__dummy2_directive__["a" /* OnCreate2 */]],
            exports: [__WEBPACK_IMPORTED_MODULE_1__dummy2_directive__["a" /* OnCreate2 */]]
        })
    ], OnCreate2Module);
    return OnCreate2Module;
}());

//# sourceMappingURL=dummy2-directive.module.js.map

/***/ }),

/***/ 1080:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnCreate2; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OnCreate2 = /** @class */ (function () {
    function OnCreate2() {
        this.onCreate2 = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    OnCreate2.prototype.ngOnInit = function () {
        this.onCreate2.emit('dummy');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], OnCreate2.prototype, "onCreate2", void 0);
    OnCreate2 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[onCreate2]'
        }),
        __metadata("design:paramtypes", [])
    ], OnCreate2);
    return OnCreate2;
}());

//# sourceMappingURL=dummy2-directive.js.map

/***/ }),

/***/ 1081:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FuelEventsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_geocoder_geocoder__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import { ApiServiceProvider } from '../../../providers/api-service/api-service';



var FuelEventsComponent = /** @class */ (function () {
    function FuelEventsComponent(
    // private apiservice: ApiServiceProvider,
    navParams, viewCtrl, geocoderApi, translate) {
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.geocoderApi = geocoderApi;
        this.translate = translate;
        this.fuelData = [];
        console.log("param maps: ", this.navParams.get("paramMaps"));
        // if (!this.navParams.get('paramMaps') || !this.navParams.get('event')) {
        if (!this.navParams.get('paramMaps')) {
            this.viewCtrl.dismiss();
            return;
        }
        this.fuelData = this.navParams.get("paramMaps");
        this.eventType = this.fuelData[0].pour;
        // this.getData();
    }
    FuelEventsComponent.prototype.getAddress = function (fuelData, index) {
        var that = this;
        that.fuelData[index].address = "N/A";
        if (!fuelData.lat) {
            that.fuelData[index].address = "N/A";
        }
        else if (fuelData.lat) {
            this.geocoderApi.reverseGeocode(Number(fuelData.lat), Number(fuelData.long))
                .then(function (res) {
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.fuelData[index].address = str;
            });
        }
    };
    FuelEventsComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    FuelEventsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuel-events',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/fuel/fuel-events/fuel-events.html"*/'<div style="height: 100%; background: rgba(0, 0, 0, 0.65);">\n  <!-- <div style="height: 80%; background: white;"> -->\n    <ion-grid>\n      <ion-row>\n        <ion-col text-center size="12" size-sm="8" offeset-sm="2">\n          <ion-card>\n            <ion-card-header>\n              <ion-row>\n                <ion-col col-2></ion-col>\n                <ion-col col-8\n                  ><ion-card-title\n                    >FUEL {{ eventType }} EVENTS</ion-card-title\n                  ></ion-col\n                >\n                <ion-col col-2>\n                  <button ion-button clear (click)="dismiss()">\n                    <ion-icon icon-only color="dark" name="close"></ion-icon>\n                  </button>\n                </ion-col>\n              </ion-row>\n            </ion-card-header>\n            <ion-card-content no-padding>\n              <ion-card no-padding *ngFor="let fuel of fuelData; let i = index">\n                <ion-grid>\n                  <ion-row>\n                    <ion-col size="12" size-sm="8" offeset-sm="2">\n                      <ion-grid style="padding: 8px">\n                        <ion-row text-left style="background-color: gainsboro; font-size: 1em;">\n                          <ion-col col-5>{{ fuel.vehicleName }}</ion-col>\n                          <ion-col col-7>{{\n                            fuel.timestamp | date: "mediumDate"\n                          }}, {{\n                            fuel.timestamp | date: "shortTime"\n                          }}</ion-col>\n                        </ion-row>\n                        <ion-row text-left style="background-color: #fff">\n                          <ion-col col-5>Fuel Change(L)</ion-col>\n                          <ion-col col-7>{{ fuel.litres }}</ion-col>\n                        </ion-row>\n                        <ion-row text-left style="background-color: #fff">\n                          <ion-col col-1>\n                            <ion-icon name="pin" color="primary"></ion-icon>\n                          </ion-col>\n                          <ion-col col-11 (onCreate2)="getAddress(fuel, i)">\n                            <p>{{ fuel.address }}</p>\n                          </ion-col>\n                        </ion-row>\n                      </ion-grid>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n              </ion-card>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  <!-- </div> -->\n</div>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/fuel/fuel-events/fuel-events.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */]])
    ], FuelEventsComponent);
    return FuelEventsComponent;
}());

//# sourceMappingURL=fuel-events.js.map

/***/ }),

/***/ 986:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelEventsComponentModule", function() { return FuelEventsComponentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__fuel_consumption_report_dummy2_directive_module__ = __webpack_require__(1079);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fuel_events__ = __webpack_require__(1081);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var FuelEventsComponentModule = /** @class */ (function () {
    function FuelEventsComponentModule() {
    }
    FuelEventsComponentModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fuel_events__["a" /* FuelEventsComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__fuel_events__["a" /* FuelEventsComponent */]),
                __WEBPACK_IMPORTED_MODULE_1__fuel_consumption_report_dummy2_directive_module__["a" /* OnCreate2Module */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ]
        })
    ], FuelEventsComponentModule);
    return FuelEventsComponentModule;
}());

//# sourceMappingURL=fuel-events.module.js.map

/***/ })

});
//# sourceMappingURL=15.js.map