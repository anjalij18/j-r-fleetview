webpackJsonp([38],{

/***/ 1004:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddReminderPageModule", function() { return AddReminderPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_reminder__ = __webpack_require__(1098);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var AddReminderPageModule = /** @class */ (function () {
    function AddReminderPageModule() {
    }
    AddReminderPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_reminder__["a" /* AddReminderPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__add_reminder__["a" /* AddReminderPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], AddReminderPageModule);
    return AddReminderPageModule;
}());

//# sourceMappingURL=add-reminder.module.js.map

/***/ }),

/***/ 1098:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddReminderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddReminderPage = /** @class */ (function () {
    function AddReminderPage(navCtrl, navParams, viewCtrl, apiCall, toastCtrl, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.translate = translate;
        this.portstemp = [];
        this.reminderTypes = [];
        this.notifTypes = [];
        this.priorDay = 0;
        this.today = __WEBPACK_IMPORTED_MODULE_2_moment__().add(5, 'year').format("YYYY-MM-DD");
        this.twoMonthsLater = __WEBPACK_IMPORTED_MODULE_2_moment__().format("YYYY-MM-DD");
        this.remDate = __WEBPACK_IMPORTED_MODULE_2_moment__().format();
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        if (navParams.get('param') !== null) {
            this.selectedVehicle = navParams.get('param');
        }
        this.reminderTypes = [{
                viewValue: this.translate.instant("Service"),
                value: "Service",
            },
            {
                viewValue: this.translate.instant("Oil Change"),
                value: "Oil Change",
            },
            {
                viewValue: this.translate.instant("Tyres"),
                value: "Tyres",
            },
            {
                viewValue: this.translate.instant("Maintenance"),
                value: "Maintenance",
            },
            {
                viewValue: this.translate.instant("Auto Repair"),
                value: "Auto Repair",
            },
            {
                viewValue: this.translate.instant("Body Work"),
                value: "Body Work",
            },
            {
                viewValue: this.translate.instant("Diagnostics"),
                value: "Diagnostics",
            },
            {
                viewValue: this.translate.instant("Tune Up"),
                value: "Tune Up",
            },
            {
                viewValue: this.translate.instant("Brake Job"),
                value: "Brake Job",
            },
            {
                viewValue: this.translate.instant("Oil & Oil Filter Change"),
                value: "Oil & Oil Filter Change",
            },
            {
                viewValue: this.translate.instant("Tyer Care"),
                value: "Tyer Care",
            },
            {
                viewValue: this.translate.instant("Towing"),
                value: "Towing",
            },
            {
                viewValue: this.translate.instant("Wheel Balance & Alignment"),
                value: "Wheel Balance & Alignment",
            },
            {
                viewValue: this.translate.instant("Fleet"),
                value: "Fleet",
            },
            {
                viewValue: this.translate.instant("Auto Tracking"),
                value: "Auto Tracking",
            },
            {
                viewValue: this.translate.instant("A/C Repair"),
                value: "A/C Repair",
            },
            {
                viewValue: this.translate.instant("Others"),
                value: "Others",
            }
        ];
        // this.reminderTypes = [{
        //   viewValue: this.translate.instant("Service"),
        //   value: "Service",
        // }, {
        //   viewValue: this.translate.instant("Oil Changed"),
        //   value: "oil_change",
        // }, {
        //   viewValue: this.translate.instant("Tyres"),
        //   value: "Tyres",
        // }, {
        //   viewValue: this.translate.instant("Maintenance"),
        //   value: "Maintenance",
        // },
        // {
        //   viewValue: this.translate.instant("Others"),
        //   value: "Others",
        // }
        // ];
        this.notifTypes = [
            { view: this.translate.instant("SMS"), value: "SMS" }, { view: this.translate.instant("EMAIL_1"), value: "EMAIL" }, { view: this.translate.instant("PUSH NOTIFICATION"), value: "PUSH_NOTIFICATION" }
        ];
    }
    AddReminderPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    AddReminderPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter AddReminderPage');
    };
    AddReminderPage.prototype.onChange = function (key) {
        console.log(key);
    };
    AddReminderPage.prototype.onDismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddReminderPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    AddReminderPage.prototype.onSeletChange = function () {
    };
    AddReminderPage.prototype.onSeletChange1 = function () {
    };
    AddReminderPage.prototype.addReminder = function () {
        var _this = this;
        if (this.notifType == undefined || this.reminderType == undefined || this.selectedVehicle == undefined) {
            this.showToast(this.translate.instant('Please fill all the mandatory fields..'));
            return;
        }
        var url = this.apiCall.mainUrl + 'reminder/addReminder';
        var payload = {};
        if (this.notifType === 'SMS') {
            payload = {
                "created_by": this.islogin._id,
                "user": this.selectedVehicle.user,
                "device": this.selectedVehicle._id,
                "reminder_type": this.reminderType,
                "notification_type": {
                    "SMS": true,
                    "EMAIL": false,
                    "PUSH_NOTIFICATION": false
                },
                "reminder_date": new Date(this.remDate).toISOString(),
                "prior_reminder": this.priorDay,
                "note": (this.note ? this.note : null),
                "status": "Pending"
            };
        }
        else if (this.notifType === 'EMAIL') {
            payload = {
                "created_by": this.islogin._id,
                "user": this.selectedVehicle.user,
                "device": this.selectedVehicle._id,
                "reminder_type": this.reminderType,
                "notification_type": {
                    "SMS": false,
                    "EMAIL": true,
                    "PUSH_NOTIFICATION": false
                },
                "reminder_date": new Date(this.remDate).toISOString(),
                "prior_reminder": this.priorDay,
                "note": (this.note ? this.note : null),
                "status": "Pending"
            };
        }
        else if (this.notifType === 'PUSH_NOTIFICATION') {
            payload = {
                "created_by": this.islogin._id,
                "user": this.selectedVehicle.user,
                "device": this.selectedVehicle._id,
                "reminder_type": this.reminderType,
                "notification_type": {
                    "SMS": false,
                    "EMAIL": false,
                    "PUSH_NOTIFICATION": true
                },
                "reminder_date": new Date(this.remDate).toISOString(),
                "prior_reminder": this.priorDay,
                "note": (this.note ? this.note : null),
                "status": "Pending"
            };
        }
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(url, payload)
            .subscribe(function (resData) {
            _this.apiCall.stopLoading();
            console.log("Resp Data=> ", resData);
            if (resData.message === 'Data Saved') {
                _this.showToast('Reminder added successfully..');
                _this.viewCtrl.dismiss();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    AddReminderPage.prototype.showToast = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'bottom'
        }).present();
    };
    AddReminderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-reminder',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/maintenance-reminder/add-reminder/add-reminder.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{"Add Reminder" | translate}}</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="onDismiss()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <!-- <form #remForm="ngForm"> -->\n  <ion-list>\n    <ion-item style="background-color: #fafafa;">\n      <ion-label style="margin-top: 15px;">{{\n            "Select Vehicle" | translate\n          }} *</ion-label>\n      <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n        itemTextField="Device_Name" [canSearch]="true" (onChange)="onChange(selectedVehicle)">\n      </select-searchable>\n    </ion-item>\n\n    <ion-item>\n      <ion-label floating>{{"Date" | translate}} *</ion-label>\n      <ion-datetime displayFormat="MMMM DD YYYY hh:mm a" pickerFormat="DD/MM/YYYY" [(ngModel)]="remDate" [min]="twoMonthsLater" [max]="today">\n      </ion-datetime>\n\n    </ion-item>\n\n    <ion-item>\n      <ion-label floating>{{"Type number of days to get reminder for your services" | translate}} </ion-label>\n      <ion-input type="number" [(ngModel)]="priorDay"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label floating>{{"Reminder Type" | translate}} *</ion-label>\n      <ion-select [(ngModel)]="reminderType" (ionChange)="onSeletChange(reminderType)">\n        <ion-option *ngFor="let rem of reminderTypes" [value]="rem.value">{{rem.viewValue}}</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item>\n      <ion-label floating>{{"Notification Type" | translate}} *</ion-label>\n      <ion-select [(ngModel)]="notifType" (ionChange)="onSeletChange1(notifType)">\n        <ion-option *ngFor="let notif of notifTypes" [value]="notif.value">{{notif.view}}</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item>\n      <ion-label floating>{{"Additional Notes" | translate}}</ion-label>\n      <ion-input type="text" [(ngModel)]="note"></ion-input>\n    </ion-item>\n  </ion-list>\n  <!-- </form> -->\n</ion-content>\n\n<ion-footer class="footSty">\n  <ion-toolbar>\n    <ion-row>\n      <ion-col style="text-align: center;">\n        <button ion-button clear color="light" (click)="addReminder()">{{"Add Reminder" | translate}}</button>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/maintenance-reminder/add-reminder/add-reminder.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */]])
    ], AddReminderPage);
    return AddReminderPage;
}());

//# sourceMappingURL=add-reminder.js.map

/***/ })

});
//# sourceMappingURL=38.js.map