webpackJsonp([66],{

/***/ 1045:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDocPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_path__ = __webpack_require__(547);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_transfer__ = __webpack_require__(548);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(546);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_api_service_api_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AddDocPage = /** @class */ (function () {
    function AddDocPage(navCtrl, navParams, event, camera, file, platform, filePath, toastCtrl, loadingCtrl, transfer, transferObj, apiCall, actionSheetCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.event = event;
        this.camera = camera;
        this.file = file;
        this.platform = platform;
        this.filePath = filePath;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.transfer = transfer;
        this.transferObj = transferObj;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.lastImage = null;
        this.minDate = new Date().toISOString();
        this.maxData = (new Date()).getFullYear() + 5;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.vehData = navParams.get("vehData");
    }
    AddDocPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter AddDocPage');
    };
    AddDocPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    AddDocPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imagePath) {
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            // this.presentToast('Error while selecting image.');
            console.log("Error while selecting image.", err);
        });
    };
    AddDocPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    AddDocPage.prototype.pathForImage = function (img) {
        console.log("Image=>", img);
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    AddDocPage.prototype.removeImg = function () {
        console.log("coming soon");
        this.lastImage = null;
    };
    AddDocPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    AddDocPage.prototype.uploadImage = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + "users/uploadImage";
        var targetPath = this.pathForImage(this.lastImage);
        var filename = this.lastImage;
        var options = {
            fileKey: "photo",
            fileName: filename,
            chunkedMode: false,
            mimeType: "image/jpeg",
            params: { 'fileName': filename }
        };
        this.transferObj = this.transfer.create();
        this.Imgloading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.Imgloading.present();
        this.transferObj.upload(targetPath, url, options).then(function (data) {
            _this.Imgloading.dismissAll();
            _this.dlUpdate(data.response);
        }, function (err) {
            console.log("uploadError=>", err);
            _this.lastImage = null;
            _this.Imgloading.dismissAll();
            _this.presentToast('Error while uploading file, Please try again !!!');
        });
    };
    AddDocPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    AddDocPage.prototype.dlUpdate = function (dllink) {
        var _this = this;
        var that = this;
        var imageDoc = {
            "doctype": that.docType,
            "image": dllink,
            "phone": that.islogin.phn,
            "docdate": new Date(that.docexp_date).toISOString(),
            "docname": that.docname
        };
        var dlObj = {
            "_id": that.vehData._id,
            "imageDoc": imageDoc
        };
        this.apiCall.startLoading();
        this.apiCall.deviceupdateCall(dlObj)
            .subscribe(function (res) {
            _this.apiCall.stopLoading();
            _this.presentToast('Image succesful uploaded.');
            that.lastImage = null;
            _this.event.publish("reloaddoclist");
            that.navCtrl.pop();
        }, function (err) {
            _this.apiCall.stopLoading();
            _this.presentToast('Internal server Error !!!');
        });
    };
    AddDocPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-doc',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/add-devices/upload-doc/add-doc/add-doc.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{ "Upload New Document" | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <form #docForm="ngForm" padding>\n    <button *ngIf="lastImage == null" ion-button block outline color="gpsc" (click)="presentActionSheet()">\n      {{ "Select Document" | translate }}\n    </button>\n    <ion-item>\n      <ion-label>{{ "Expiry Date:" | translate }}</ion-label>\n      <ion-datetime \n      displayFormat="DD/MM/YYYY" \n      pickerFormat="DD/MM/YYYY" \n      [(ngModel)]="docexp_date" \n      [max]="maxData"\n      [min]="minDate"\n      name="docexp_date">\n      </ion-datetime>\n    </ion-item>\n    <ion-item>\n      <ion-label>{{ "Select Doc Type" | translate }}</ion-label>\n      <ion-select [(ngModel)]="docType" name="docType">\n        <ion-option value="RC">{{ "RC" | translate }}</ion-option>\n        <ion-option value="PUC">{{ "PUC" | translate }}</ion-option>\n        <ion-option value="Insurance">{{ "Insurance" | translate }}</ion-option>\n        <ion-option value="Licence">{{ "Driving Licence" | translate }}</ion-option>\n        <ion-option value="Fitment">{{ "Fitment Certificate" | translate }}</ion-option>\n        <ion-option value="Permit">{{ "Permit Certificate" | translate }}</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item *ngIf="docType">\n      <ion-label>{{docType}} {{ "Number" | translate }} </ion-label>\n      <ion-input style="text-align: right;" type="text" [(ngModel)]="docname" name="docname"></ion-input>\n    </ion-item>\n    <ion-row *ngIf="lastImage !== null">\n      <ion-col>\n        <button ion-button block outline color="gpsc" (click)="uploadImage()">{{ "Upload Doc" | translate }}</button>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf="lastImage !== null" padding-left padding-right>\n      <ion-col col-11>\n        {{ "Selected Doc" | translate }} <span style="color: blue;">{{lastImage}}</span>\n      </ion-col>\n      <ion-col col-1 (click)="removeImg()" style="text-align: right;">\n        <ion-icon name="close-circle"></ion-icon>\n      </ion-col>\n    </ion-row>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/add-devices/upload-doc/add-doc/add-doc.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_transfer__["a" /* Transfer */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_transfer__["b" /* TransferObject */],
            __WEBPACK_IMPORTED_MODULE_6__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"]])
    ], AddDocPage);
    return AddDocPage;
}());

//# sourceMappingURL=add-doc.js.map

/***/ }),

/***/ 955:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDocPageModule", function() { return AddDocPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_doc__ = __webpack_require__(1045);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AddDocPageModule = /** @class */ (function () {
    function AddDocPageModule() {
    }
    AddDocPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_doc__["a" /* AddDocPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__add_doc__["a" /* AddDocPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], AddDocPageModule);
    return AddDocPageModule;
}());

//# sourceMappingURL=add-doc.module.js.map

/***/ })

});
//# sourceMappingURL=66.js.map