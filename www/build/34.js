webpackJsonp([34],{

/***/ 1013:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotifSettingPageModule", function() { return NotifSettingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notif_setting__ = __webpack_require__(1107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { MatDatepickerModule } from '@angular/material/datepicker';
var NotifSettingPageModule = /** @class */ (function () {
    function NotifSettingPageModule() {
    }
    NotifSettingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__notif_setting__["a" /* NotifSettingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__notif_setting__["a" /* NotifSettingPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild(),
            ],
        })
    ], NotifSettingPageModule);
    return NotifSettingPageModule;
}());

//# sourceMappingURL=notif-setting.module.js.map

/***/ }),

/***/ 1107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotifSettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__notif_modal__ = __webpack_require__(572);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NotifSettingPage = /** @class */ (function () {
    function NotifSettingPage(navCtrl, navParams, apiCall, toastCtrl, modalCtrl, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.platform = platform;
        this.fData = {};
        this.isAddEmail = false;
        this.isAddPhone = false;
        this.shownotifdiv = true;
        this.newArray = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        this.getCustDetails();
        // console.log("check here: ", this.notifArray);
        // this.sortArray();
        // The Native Audio plugin can only be called once the platform is ready
        // this.platform.ready().then(() => {
        //   console.log("platform ready");
        //   // This is used to unload the track. It's useful if you're experimenting with track locations
        //   this.nativeAudio.unload('trackID').then(function () {
        //     console.log("unloaded audio!");
        //   }, function (err) {
        //     console.log("couldn't unload audio... " + err);
        //   });
        //   // 'trackID' can be anything
        //   this.nativeAudio.preloadComplex('trackID', 'assets/audio/test.mp3', 1, 1, 0).then(function () {
        //     console.log("audio loaded!");
        //   }, function (err) {
        //     console.log("audio failed: " + err);
        //   });
        // });
    }
    NotifSettingPage.prototype.getCustDetails = function () {
        var _this = this;
        var _bUrl = this.apiCall.mainUrl + 'users/getCustumerDetail?uid=' + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(_bUrl)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            // debugger
            console.log("resp data: ", respData);
            if (respData.length === 0) {
                return;
            }
            else {
                if (respData.cust.alert) {
                    var result = Object.keys(respData.cust.alert).map(function (key) {
                        return [key, respData.cust.alert[key]];
                    });
                    _this.newArray = result.map(function (r) {
                        return {
                            key: r[0],
                            imgUrl: 'assets/notificationIcon/' + r[0] + '.png',
                            keys: r[1]
                        };
                    });
                    console.log("someArr: " + _this.newArray);
                }
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    NotifSettingPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter NotifSettingPage');
    };
    NotifSettingPage.prototype.onChange = function (ev) {
        console.log("event: ", ev);
    };
    NotifSettingPage.prototype.tab = function (key1, key2) {
        var _this = this;
        if (key1 && key2) {
            if (key1.keys[key2] === false) {
                key1.keys[key2] = false;
            }
            else {
                key1.keys[key2] = true;
            }
        }
        for (var t = 0; t < this.newArray.length; t++) {
            if (this.newArray[t].key === key1.key) {
                this.newArray[t].keys = key1.keys;
            }
        }
        var temp = [];
        for (var e = 0; e < this.newArray.length; e++) {
            temp.push((_a = {},
                _a[this.newArray[e].key] = this.newArray[e].keys,
                _a.key = this.newArray[e].key,
                _a));
        }
        /* covert an array into object */
        var res = {};
        temp.forEach(function (a) { res[a.key] = a[a.key]; });
        this.fData.contactid = this.islogin._id;
        this.fData.alert = res;
        var url = this.apiCall.mainUrl + 'users/editUserDetails';
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(url, this.fData)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            console.log("check stat: ", respData);
            if (respData) {
                _this.toastCtrl.create({
                    message: 'Setting Updated',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
        var _a;
    };
    NotifSettingPage.prototype.addEmail = function (noti) {
        // use AudioProvider to control selected track 
        this.notifType = noti.key;
        this.isAddEmail = true;
        var data = {
            "buttonClick": 'email',
            "notifType": this.notifType,
            "compData": (_a = {},
                _a[this.notifType] = noti.keys,
                _a),
            "newArray": this.newArray
        };
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__notif_modal__["a" /* NotifModalPage */], {
            "notifData": data
        });
        modal.present();
        var _a;
    };
    NotifSettingPage.prototype.addPhone = function (noti) {
        this.notifType = noti.key;
        this.isAddPhone = true;
        // var data = {
        //   buttonClick: 'phone',
        //   "notifType": this.notifType,
        //   "compData": { [this.notifType]: noti.keys }
        // }
        var data = {
            "buttonClick": 'phone',
            "notifType": this.notifType,
            "compData": (_a = {},
                _a[this.notifType] = noti.keys,
                _a),
            "newArray": this.newArray
        };
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__notif_modal__["a" /* NotifModalPage */], {
            "notifData": data
        });
        modal.present();
        var _a;
    };
    NotifSettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-notif-setting',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/profile/settings/notif-setting/notif-setting.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{ "Notification Settings"  | translate}}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-card *ngFor="let item of newArray">\n    <ion-card-content>\n      <ion-row>\n        <ion-col col-1 style="margin: auto;">\n          <img [src]="item.imgUrl" style="width: 20px; height: 20px;" />\n        </ion-col>\n        <ion-col col-3 style="margin: auto;">\n          <p style="padding: 6px 0px 0px 0px; color: #000; font-size: 12px;\n          font-weight: 500;">{{item.key | titlecase}}</p>\n        </ion-col>\n        <ion-col col-6 no-padding>\n          <ion-row style="margin-top: -10px;">\n            <ion-col col-4 no-padding>\n              <ion-item no-padding>\n                <ion-label style="font-size: 12px;">{{ \'Email\' | translate }}</ion-label>\n              </ion-item>\n            </ion-col>\n            <ion-col col-4 no-padding>\n              <ion-item no-padding>\n                <ion-label style="font-size: 12px;">{{ \'SMS\' | translate }}</ion-label>\n              </ion-item>\n            </ion-col>\n            <ion-col col-4 no-padding>\n              <ion-item no-padding>\n                <ion-label style="font-size: 12px;">{{ \'Notification\' | translate }}</ion-label>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n\n          <ion-row style="margin-top: -21px;margin-bottom: -21px;">\n            <ion-col col-4 style="padding-left: 0px;">\n              <ion-item no-padding>\n                <ion-toggle item-start color="gpsc" [(ngModel)]="item.keys.email_status"\n                  *ngIf="islogin.isSuperAdmin || islogin.isDealer" (ionChange)="tab(item,\'email_status\')"></ion-toggle>\n                <ion-toggle item-start color="gpsc" [(ngModel)]="item.keys.email_status"\n                  *ngIf="!islogin.isSuperAdmin && !islogin.isDealer" (ionChange)="tab(item,\'email_status\')"\n                  disabled="true"></ion-toggle>\n              </ion-item>\n            </ion-col>\n            <ion-col col-4 style="padding-left: 0px;">\n              <ion-item no-padding>\n                <ion-toggle item-start color="gpsc" [(ngModel)]="item.keys.sms_status"\n                  *ngIf="islogin.isSuperAdmin || islogin.isDealer" (ionChange)="tab(item,\'sms_status\')"></ion-toggle>\n                <ion-toggle item-start color="gpsc" [(ngModel)]="item.keys.sms_status"\n                  *ngIf="!islogin.isSuperAdmin && !islogin.isDealer" (ionChange)="tab(item,\'sms_status\')"\n                  disabled="true"></ion-toggle>\n              </ion-item>\n            </ion-col>\n            <ion-col col-4 style="padding-left: 0px;">\n              <ion-item no-padding>\n                <ion-toggle item-start color="gpsc" [(ngModel)]="item.keys.notif_status"\n                  (ionChange)="tab(item,\'notif_status\')"></ion-toggle>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n        <ion-col col-1 (click)="addEmail(item)">\n          <ion-icon name="mail" color="primary"></ion-icon>\n        </ion-col>\n        <ion-col col-1 (click)="addPhone(item)">\n          <ion-icon name="call" color="secondary"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/profile/settings/notif-setting/notif-setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"]])
    ], NotifSettingPage);
    return NotifSettingPage;
}());

//# sourceMappingURL=notif-setting.js.map

/***/ })

});
//# sourceMappingURL=34.js.map