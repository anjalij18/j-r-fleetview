webpackJsonp([27],{

/***/ 1019:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup__ = __webpack_require__(1113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { Ng2TelInputModule } from 'ng2-tel-input';
var SignupPageModule = /** @class */ (function () {
    function SignupPageModule() {
    }
    SignupPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild(),
            ],
        })
    ], SignupPageModule);
    return SignupPageModule;
}());

//# sourceMappingURL=signup.module.js.map

/***/ }),

/***/ 1113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SignupPage = /** @class */ (function () {
    function SignupPage(navCtrl, navParams, formBuilder, apiService, toastCtrl, alertCtrl, platform, keyboard) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.apiService = apiService;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.keyboard = keyboard;
        this.type1 = "password";
        this.countryCodeArray = [];
        this.selectedCountryCode = 'us';
        this.countryCodes = ['us', 'lu', 'de', 'bs', 'br', 'pt'];
        this.type = "password";
        this.show = false;
        this.show1 = false;
        this.getCountryCode();
        // this.signupForm = formBuilder.group({
        //   mob_num: ["", Validators.compose([Validators.minLength(10), Validators.maxLength(13), Validators.required])],
        //   email_add: ["", Validators.email],
        //   Name: ["", Validators.required],
        //   pass: ["", Validators.required],
        //   cnfrm_passwrd: ["", Validators.required],
        //   std_code: ["", Validators.required]
        //   // mob_num: ['', [Validators.required, Validators.minLength(10)]]
        //   // selectedCode: [""]
        // });
        this.signupForm = formBuilder.group({
            mob_num: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].minLength(10), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].maxLength(13)])],
            email_add: [null],
            Name: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            pass: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            cnfrm_passwrd: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            lName: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            userId: [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            std_code: [null]
        });
    }
    SignupPage.prototype.getCountryCode = function () {
        var _this = this;
        this.apiService.getCountryCode().subscribe(function (data) {
            _this.countryCodeArray = data.countries;
        });
    };
    SignupPage.prototype.onSelect = function (con) {
        console.log("country selected: ", con);
    };
    SignupPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter SignupPage');
    };
    SignupPage.prototype.changeSelectedCountryCode = function (value) {
        this.selectedCountryCode = value;
    };
    SignupPage.prototype.doLogin = function () {
        this.navCtrl.setRoot("LoginPage");
    };
    SignupPage.prototype.IsDealer = function (check) {
        this.isdealer = check;
    };
    // getotp() {
    //   debugger
    //   this.isdealer = false;
    //   this.submitAttempt = true;
    //   if (this.signupForm.valid) {
    //     this.usersignupdetails = this.signupForm.value;
    //     localStorage.setItem('usersignupdetails', this.usersignupdetails);
    //     this.signupDetails = localStorage.getItem("usersignupdetails");
    //     if (this.signupForm.value.cnfrm_passwrd && this.signupForm.value.email_add && this.signupForm.value.mob_num && this.signupForm.value.Name && this.signupForm.value.pass) {
    //       if (this.signupForm.value.pass == this.signupForm.value.cnfrm_passwrd) {
    //         var usersignupdata = {
    //           "first_name": this.signupForm.value.Name,
    //           "last_name": '',
    //           "email": this.signupForm.value.email_add,
    //           "password": this.signupForm.value.pass,
    //           "confirmpass": this.signupForm.value.cnfrm_passwrd,
    //           "phone": String(this.signupForm.value.mob_num),
    //           "status": false,
    //           "supAdmin": "59cbbdbe508f164aa2fef3d8",
    //           "isDealer": false,
    //           "std_code": this.conCode
    //         }
    //         this.apiService.startLoading();
    //         this.apiService.signupApi(usersignupdata)
    //           .subscribe(response => {
    //             var phone = usersignupdata.phone;
    //             localStorage.setItem("mobnum", phone)
    //             this.apiService.stopLoading();
    //             this.signupUseradd = response;
    //             let toast = this.toastCtrl.create({
    //               message: response.message,
    //               duration: 3000,
    //               position: 'top'
    //             });
    //             toast.onDidDismiss(() => {
    //               if (response.message === 'Email ID or Mobile Number already exists') {
    //                 this.navCtrl.push("LoginPage");
    //               } else if (response.message === "OTP sent successfully") {
    //                 this.navCtrl.push('SignupOtpPage');
    //               }
    //             });
    //             toast.present();
    //           },
    //             err => {
    //               this.apiService.stopLoading();
    //               let toast = this.toastCtrl.create({
    //                 message: "Something went wrong. Please check your net connection..",
    //                 duration: 2500,
    //                 position: "top"
    //               })
    //               toast.present();
    //             })
    //       } else {
    //         var alertPopup = this.alertCtrl.create({
    //           title: 'Warning!',
    //           message: "Password and Confirm Password Not Matched",
    //           buttons: ['OK']
    //         });
    //         alertPopup.present();
    //       }
    //     }
    //   }
    // }
    SignupPage.prototype.getotp = function () {
        var _this = this;
        debugger;
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
        this.isdealer = false;
        this.submitAttempt = true;
        if (this.signupForm.valid) {
            this.usersignupdetails = this.signupForm.value;
            localStorage.setItem('usersignupdetails', this.usersignupdetails);
            this.signupDetails = localStorage.getItem("usersignupdetails");
            // if (this.signupForm.value.cnfrm_passwrd && this.signupForm.value.email_add && this.signupForm.value.mob_num && this.signupForm.value.Name && this.signupForm.value.pass) {
            // if (this.signupForm.value.cnfrm_passwrd && this.signupForm.value.email_add && this.signupForm.value.Name && this.signupForm.value.pass) {
            if (this.signupForm.value.email_add !== null) {
                var isEmail = validateEmail(this.signupForm.value.email_add);
                if (!isEmail) {
                    var toast = this.toastCtrl.create({
                        message: 'Please enter valid email address and try again',
                        duration: 3000,
                        position: 'top'
                    });
                    toast.present();
                    return;
                }
            }
            if (this.signupForm.value.mob_num !== null) {
                if (this.signupForm.value.std_code == null) {
                    this.toastCtrl.create({
                        message: 'Please select the country code.',
                        duration: 3000,
                        position: 'middle'
                    }).present();
                    return;
                }
            }
            debugger;
            if (this.signupForm.value.pass === this.signupForm.value.cnfrm_passwrd) {
                var usersignupdata = {
                    "first_name": this.signupForm.value.Name,
                    "last_name": this.signupForm.value.lName,
                    "password": this.signupForm.value.pass,
                    "org_name": null,
                    "email": (this.signupForm.value.email_add ? this.signupForm.value.email_add : null),
                    "phone": (this.signupForm.value.mob_num ? this.signupForm.value.mob_num : null),
                    // "expdate": "2021-04-02T06:56:41.356Z",
                    "supAdmin": "59cbbdbe508f164aa2fef3d8",
                    "isDealer": false,
                    "custumer": true,
                    "user_id": this.signupForm.value.userId,
                    "imageDoc": [],
                    "std_code": (this.signupForm.value.std_code ? this.signupForm.value.std_code : null)
                };
                this.apiService.startLoading();
                this.apiService.signupApi(usersignupdata)
                    .subscribe(function (response) {
                    debugger;
                    _this.apiService.stopLoading();
                    if (usersignupdata.phone !== null) {
                        var phone = usersignupdata.phone;
                        localStorage.setItem("mobnum", phone);
                        _this.signupUseradd = response;
                    }
                    var toast = _this.toastCtrl.create({
                        message: response.message,
                        duration: 3000,
                        position: 'top'
                    });
                    toast.onDidDismiss(function () {
                        if (response.message === 'Email ID or Mobile Number already exists') {
                            _this.navCtrl.push("LoginPage");
                        }
                        else if (response.message === "OTP sent successfully") {
                            _this.navCtrl.push('SignupOtpPage');
                        }
                        else if (response.message === "Registered Sucessfully") {
                            _this.navCtrl.push("LoginPage");
                        }
                    });
                    toast.present();
                }, function (err) {
                    _this.apiService.stopLoading();
                    var toast = _this.toastCtrl.create({
                        message: "Something went wrong. Please check your net connection..",
                        duration: 2500,
                        position: "top"
                    });
                    toast.present();
                });
            }
            else {
                var alertPopup = this.alertCtrl.create({
                    title: 'Warning!',
                    message: "Password and Confirm Password Not Matched",
                    buttons: ['OK']
                });
                alertPopup.present();
            }
            // }
        }
    };
    SignupPage.prototype.gotoOtp = function () {
        this.navCtrl.push('SignupOtpPage');
    };
    SignupPage.prototype.gotoLogin = function () {
        this.navCtrl.push("LoginPage");
    };
    SignupPage.prototype.toggleShow = function (ev) {
        if (ev == 0) {
            this.show = !this.show;
            if (this.show) {
                this.type = "text";
            }
            else {
                this.type = "password";
            }
        }
        else {
            this.show1 = !this.show1;
            if (this.show1) {
                this.type1 = "text";
            }
            else {
                this.type1 = "password";
            }
        }
    };
    SignupPage.prototype.upload = function () {
        this.navCtrl.push("DrivingLicensePage");
    };
    SignupPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-signup',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/signup/signup.html"*/'<ion-content no-padding>\n  <div width="100%" height="100%">\n    <div style="background: #ee612e;padding-top: 15%;padding-bottom: 10%;" height="50%">\n      <ion-card style="margin: 0px;width: 28%;border-radius: 15px; padding-top: 0%;margin-left: 37%;">\n        <img src="assets/imgs/icon.png" style="width: 60%;margin: 18%;" />\n      </ion-card>\n      <h5\n        style="color: white;font-weight: 500;text-align: center;font-size: 3.6rem;padding-top: 0%;padding-bottom: 14%;">\n        {{ "SIGN UP" | translate }}\n      </h5>\n    </div>\n    <div>\n      <div class="row">\n        <ion-card class="col-sm-12 col-12 col-md-12"\n          style="margin-top: -21%;border-radius: 4%;height: auto;overflow-y: auto;padding-bottom: 7%;">\n          <form [formGroup]="signupForm">\n            <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n              <ion-col col-10 no-padding>\n                <ion-input placeholder="{{ \'User ID*\' | translate }}" type="text" formControlName="userId"></ion-input>\n              </ion-col>\n              <ion-col col-2 no-padding style="text-align: right;margin-top: 2%; color: gray;">\n                <ion-icon name="person" style="font-size: 2.8rem"></ion-icon>\n              </ion-col>\n            </ion-row>\n            <p *ngIf="\n            !signupForm.controls.userId.valid &&\n            (signupForm.controls.userId.dirty || submitAttempt)\n          " class="err">\n              {{\'UserId is required!\' | translate}}\n            </p>\n            <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n              <ion-col col-10 no-padding>\n                <ion-input placeholder="{{ \'First Name*\' | translate }}" type="text" formControlName="Name"></ion-input>\n              </ion-col>\n              <ion-col col-2 no-padding style="text-align: right;margin-top: 2%; color: gray;">\n                <ion-icon name="person" style="font-size: 2.8rem"></ion-icon>\n              </ion-col>\n            </ion-row>\n            <p *ngIf="\n            !signupForm.controls.Name.valid &&\n            (signupForm.controls.Name.dirty || submitAttempt)\n          " class="err">\n              {{\'First name is required!\' | translate}}\n            </p>\n            <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n              <ion-col col-10 no-padding>\n                <ion-input placeholder="{{ \'Last Name*\' | translate }}" type="text" formControlName="lName"></ion-input>\n              </ion-col>\n              <ion-col col-2 no-padding style="text-align: right;margin-top: 2%; color: gray;">\n                <ion-icon name="person" style="font-size: 2.8rem"></ion-icon>\n              </ion-col>\n            </ion-row>\n            <p *ngIf="\n            !signupForm.controls.lName.valid &&\n            (signupForm.controls.lName.dirty || submitAttempt)\n          " class="err">\n              {{\'Last name is required!\' | translate}}\n            </p>\n\n            <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n              <ion-col col-4 no-padding>\n                <ion-label style="color: gray;">Select country</ion-label>\n              </ion-col>\n              <ion-col col-8 no-padding style="text-align: right;margin-top: 2%;color: gray;">\n                <ion-select style="float: right;max-width: 100%;" formControlName="std_code">\n                  <ion-option *ngFor="let con of countryCodeArray" [value]="con.code" (ionSelect)="onSelect(con)">\n                    {{con.code}} {{con.name}}</ion-option>\n                </ion-select>\n              </ion-col>\n            </ion-row>\n\n            <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n              <ion-col col-10 no-padding>\n                <ion-input placeholder="{{ \'Mobile Number\' | translate }}" type="number" formControlName="mob_num"\n                  minlength="10" maxlength="13">\n                </ion-input>\n              </ion-col>\n              <ion-col col-2 no-padding style="text-align: right;margin-top: 2%;color: gray;">\n                <ion-icon name="call" style="font-size:2.8rem"></ion-icon>\n              </ion-col>\n            </ion-row>\n\n            <p *ngIf="\n                !signupForm.controls.mob_num.valid &&\n                (signupForm.controls.mob_num.dirty || submitAttempt)\n              " class="err">\n              {{ "mobile number should be 10 digits!" | translate }}\n            </p>\n            <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n              <ion-col col-10 no-padding>\n                <ion-input type="email" placeholder="{{ \'Email ID\' | translate }}" formControlName="email_add">\n                </ion-input>\n              </ion-col>\n              <ion-col col-2 no-padding style="text-align: right;margin-top: 2%;color: gray;">\n                <ion-icon name="mail" style="font-size:2.8rem"></ion-icon>\n              </ion-col>\n            </ion-row>\n            <p *ngIf="\n                !signupForm.controls.email_add.valid &&\n                (signupForm.controls.email_add.dirty || submitAttempt)\n              " class="err">\n              {{ "please enter valid email id!" | translate }}\n            </p>\n\n            <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n              <ion-col col-10 no-padding>\n                <ion-input placeholder="{{\'Password*\' | translate}}" type="{{ type }}" formControlName="pass">\n                </ion-input>\n              </ion-col>\n              <ion-col col-2 no-padding style="text-align: right;margin-top: 2%;color: gray;position:initial"\n                (click)="toggleShow(0)">\n                <ion-icon name="eye" style="font-size:2.8rem"></ion-icon>\n              </ion-col>\n            </ion-row>\n            <p *ngIf="\n                !signupForm.controls.pass.valid &&\n                (signupForm.controls.pass.dirty || submitAttempt)\n              " class="err">\n              {{\'Password required!\' | translate}}\n            </p>\n            <ion-row style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;">\n              <ion-col col-10 no-padding>\n                <ion-input type="{{ type1 }}" placeholder="{{\'Confirm Password*\' | translate}}"\n                  formControlName="cnfrm_passwrd"></ion-input>\n              </ion-col>\n              <ion-col col-2 no-padding style="text-align: right;margin-top: 2%;color: gray;" (click)="toggleShow(1)">\n                <ion-icon name="eye" style="font-size:2.8rem"></ion-icon>\n              </ion-col>\n            </ion-row>\n            <p *ngIf="\n                !signupForm.controls.cnfrm_passwrd.valid &&\n                (signupForm.controls.cnfrm_passwrd.dirty || submitAttempt)\n              " class="err">\n              {{\'Please enter Confirm Password\' | translate}}\n            </p>\n          </form>\n        </ion-card>\n        <div class="col-sm-12 col-12 col-md-12 " style="margin:auto">\n          <button ion-button round\n            style="width: 18.2rem;color:white;background: #ee612e;margin:-46px 0px 0px 2px;font-weight:400;text-transform: none;"\n            (click)="getotp()">\n            <b>{{\'SIGN UP\' | translate}}</b>\n          </button>\n          <div (click)="gotoLogin()">\n            <p style="color:grey;margin-left: -13px;">\n              {{\'Already have an account?\' | translate}}<span\n                style="color: black;margin-left: 5px;">{{\'SIGN IN\' | translate}}</span>\n            </p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/signup/signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Keyboard"]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ })

});
//# sourceMappingURL=27.js.map