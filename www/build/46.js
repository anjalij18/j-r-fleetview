webpackJsonp([46],{

/***/ 1088:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeofenceShowPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GeofenceShowPage = /** @class */ (function () {
    function GeofenceShowPage(navParam, apiCall) {
        this.apiCall = apiCall;
        this.geoshape = {};
        this.alldata = {};
        this.items = navParam.get("param");
        this.gefence = this.items._id;
        this.gefencename = this.items.geoname;
        console.log("gefencename=> " + this.gefencename);
    }
    GeofenceShowPage.prototype.ngOnInit = function () {
        this.getgeofence();
    };
    GeofenceShowPage.prototype.newMap = function () {
        var mapOptions = {
            camera: {
                zoom: 15
            },
            controls: {
                zoom: true
            },
            mapTypeControlOptions: {
                mapTypeIds: [__WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].ROADMAP, 'map_style']
            },
            gestures: {
                rotate: false,
                tilt: false
            }
        };
        var map = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["b" /* GoogleMaps */].create('mapShowGeofence', mapOptions);
        return map;
    };
    GeofenceShowPage.prototype.getgeofence = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'geofencing/getdevicegeofence?gid=' + this.gefence;
        this.apiCall.startLoading().present();
        this.apiCall.getdevicegeofenceCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("get geofence data: ", data);
            _this.geofenceShowdata = data;
            _this.geofenceShowdata = data[0].geofence.coordinates[0];
            console.log(_this.geofenceShowdata);
            _this.geoshape = [];
            _this.geoshape = _this.geofenceShowdata.map(function (p) {
                return {
                    lat: p[1], lng: p[0],
                };
            });
            var temp = JSON.stringify(_this.geoshape);
            var temp2 = JSON.parse(temp);
            console.log(temp2);
            // this.draw(temp2);
            _this.drawPolygon(temp2);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    GeofenceShowPage.prototype.drawPolygon = function (polyData) {
        var that = this;
        that.alldata.map = that.newMap();
        that.mapdata = [];
        that.mapdata = polyData.map(function (d) {
            return { lat: d.lat, lng: d.lng };
        });
        var bounds = new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["g" /* LatLngBounds */](that.mapdata);
        that.alldata.map.moveCamera({
            target: bounds
        });
        var GORYOKAKU_POINTS = that.mapdata;
        that.alldata.map.addPolygon({
            'points': GORYOKAKU_POINTS,
            'strokeColor': '#AA00FF',
            'fillColor': '#00FFAA',
            'strokeWidth': 3
        }).then(function (polygon) {
            polygon.on(__WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["d" /* GoogleMapsEvent */].POLYGON_CLICK).subscribe(function (param) {
                console.log("polygon data=> " + param);
                console.log("polygon data=> " + param[1]);
                var contentString = '<b>Geo Fence</b><br>' +
                    'Name: ' + that.gefencename;
                var marker = param[1];
                marker.showInfoWindow();
                alert(contentString);
            });
        });
    };
    GeofenceShowPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/geofence/geofence-show/geofence-show.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>{{items.geoname}}</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content>\n    <div id="mapShowGeofence"></div>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/geofence/geofence-show/geofence-show.html"*/,
            styles: ["\n    #mapShowGeofence{\n        height: 100%;\n        width: 100%;\n     }"]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], GeofenceShowPage);
    return GeofenceShowPage;
}());

//# sourceMappingURL=geofence-show.js.map

/***/ }),

/***/ 993:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeofenceShowPageModule", function() { return GeofenceShowPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__geofence_show__ = __webpack_require__(1088);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { TranslateModule } from '@ngx-translate/core';
var GeofenceShowPageModule = /** @class */ (function () {
    function GeofenceShowPageModule() {
    }
    GeofenceShowPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__geofence_show__["a" /* GeofenceShowPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__geofence_show__["a" /* GeofenceShowPage */])
            ],
        })
    ], GeofenceShowPageModule);
    return GeofenceShowPageModule;
}());

//# sourceMappingURL=geofence-show.module.js.map

/***/ })

});
//# sourceMappingURL=46.js.map