webpackJsonp([56],{

/***/ 1058:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailyReportNewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DailyReportNewPage = /** @class */ (function () {
    function DailyReportNewPage(navCtrl, navParams, apicalldaily) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalldaily = apicalldaily;
        this.page = 0;
        this.limit = 10;
        this.deviceReport = [];
        this.portstemp = [];
        this.measurementUnit = 'MKS';
        this.dataParam = navParams.get("item");
        console.log("selected vehicle", this.dataParam);
        if (this.dataParam[0] != null) {
            this.vehicleData = this.dataParam[0];
            console.log("cheid", this.vehicleData);
            // this.selectedVehicle = this.dataParam[0]
            // console.log("ch", this.selectedVehicle.Device_Name);
        }
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.from = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.from);
        this.to = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.to);
        if (navParams.get('param') != null) {
            this.vehicleData = navParams.get('param');
        }
    }
    DailyReportNewPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter DailyReportNewPage');
        this.getdevices();
        this.getDailyReportData();
        this.getDefaultUserSettings();
    };
    DailyReportNewPage.prototype.getDefaultUserSettings = function () {
        var _this = this;
        var b_url = this.apicalldaily.mainUrl + "users/get_user_setting";
        var Var = { uid: this.islogin._id };
        this.apicalldaily.urlpasseswithdata(b_url, Var)
            .subscribe(function (resp) {
            console.log("check lang key: ", resp);
            if (resp.unit_measurement !== undefined) {
                _this.measurementUnit = resp.unit_measurement;
            }
            else {
                if (localStorage.getItem('MeasurementType') !== null) {
                    var measureType = localStorage.getItem('MeasurementType');
                    _this.measurementUnit = measureType;
                }
                else {
                    _this.measurementUnit = 'MKS';
                }
            }
        }, function (err) {
            console.log(err);
            if (localStorage.getItem('MeasurementType') !== null) {
                var measureType = localStorage.getItem('MeasurementType');
                _this.measurementUnit = measureType;
            }
            else {
                _this.measurementUnit = 'MKS';
            }
        });
    };
    DailyReportNewPage.prototype.getSelectedId = function (pdata) {
        console.log(pdata);
        this.vehicleData = pdata;
        this.getDailyReportData();
    };
    DailyReportNewPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apicalldaily.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicalldaily.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            // this.apicalldaily.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            // this.apicalldaily.stopLoading();
            console.log(err);
        });
    };
    DailyReportNewPage.prototype.getDailyReportData = function () {
        var _this = this;
        console.log("entered");
        this.page = 0;
        var baseUrl;
        baseUrl = this.apicalldaily.mainUrl + "devices/daily_report";
        var that = this;
        var currDay = new Date().getDay();
        var currMonth = new Date().getMonth();
        var currYear = new Date().getFullYear();
        var selectedDay = new Date(that.to).getDay();
        var selectedMonth = new Date(that.to).getMonth();
        var selectedYear = new Date(that.to).getFullYear();
        var devname, devid, today_odo, today_running, today_stopped, t_idling, t_ofr, today_trips, maxSpeed, mileage;
        if ((currDay == selectedDay) && (currMonth == selectedMonth) && (currYear == selectedYear)) {
            devname = "Device_Name";
            devid = "Device_ID";
            today_odo = "today_odo";
            today_running = "today_running";
            today_stopped = "today_stopped";
            t_idling = "t_idling";
            t_ofr = "t_ofr";
            today_trips = "today_trips";
            maxSpeed = "maxSpeed";
            mileage = "Mileage";
        }
        else {
            console.log("else block called");
            devid = "imei";
            devname = "ID.Device_Name";
            today_odo = "today_odo";
            today_running = "today_running";
            today_stopped = "today_stopped";
            t_idling = "t_idling";
            t_ofr = "t_ofr";
            today_trips = "today_trips";
            maxSpeed = "ID.maxSpeed";
            mileage = "ID.Mileage";
        }
        // debugger
        var payload = {};
        if (this.vehicleData == undefined) {
            payload = {
                "draw": 2,
                "columns": [
                    {
                        "data": devname
                    },
                    {
                        "data": devid
                    },
                    {
                        "data": today_odo
                    },
                    {
                        "data": today_running
                    },
                    {
                        "data": today_stopped
                    },
                    {
                        "data": t_idling
                    },
                    {
                        "data": t_ofr
                    },
                    {
                        "data": today_trips
                    },
                    {
                        "data": maxSpeed
                    },
                    {
                        "data": mileage
                    },
                    { "data": "t_running" },
                    { "data": "t_stopped" },
                    { "data": "t_idling" },
                    { "data": "t_ofr" },
                    { "data": "t_noGps" },
                    {
                        "data": null,
                        "defaultContent": ""
                    }
                ],
                "order": [
                    {
                        "column": 0,
                        "dir": "asc"
                    }
                ],
                "start": 0,
                "length": this.limit,
                "search": {
                    "value": "",
                    "regex": false
                },
                "op": {},
                "select": [],
                "find": {
                    "user_id": this.islogin._id,
                    "date": new Date(this.to).toISOString()
                }
            };
        }
        else {
            payload = {
                "draw": 2,
                "columns": [
                    {
                        "data": devname
                    },
                    {
                        "data": devid
                    },
                    {
                        "data": today_odo
                    },
                    {
                        "data": today_running
                    },
                    {
                        "data": today_stopped
                    },
                    {
                        "data": t_idling
                    },
                    {
                        "data": t_ofr
                    },
                    {
                        "data": today_trips
                    },
                    {
                        "data": mileage
                    },
                    {
                        "data": maxSpeed
                    },
                    { "data": "t_running" },
                    { "data": "t_stopped" },
                    { "data": "t_idling" },
                    { "data": "t_ofr" },
                    { "data": "t_noGps" },
                    {
                        "data": null,
                        "defaultContent": ""
                    }
                ],
                "order": [
                    {
                        "column": 0,
                        "dir": "asc"
                    }
                ],
                "start": 0,
                "length": this.limit,
                "search": {
                    "value": "",
                    "regex": false
                },
                "op": {},
                "select": [],
                "find": {
                    "user_id": this.islogin._id,
                    "devId": this.vehicleData.Device_ID,
                    "date": new Date(this.to).toISOString()
                }
            };
        }
        this.deviceReport = [];
        this.apicalldaily.startLoading().present();
        this.apicalldaily.getDailyReport1(baseUrl, payload)
            .subscribe(function (data) {
            _this.apicalldaily.stopLoading();
            console.log("daily report data: ", data);
            for (var i = 0; i < data.data.length; i++) {
                debugger;
                _this.deviceReport.push({
                    _id: data.data[i]._id,
                    Device_ID: data.data[i].Device_ID ? data.data[i].Device_ID : data.data[i].imei,
                    Device_Name: data.data[i].Device_Name ? data.data[i].Device_Name : (data.data[i].ID ? data.data[i].ID.Device_Name : 'N/A'),
                    maxSpeed: data.data[i].maxSpeed ? data.data[i].maxSpeed : (data.data[i].ID ? data.data[i].ID.maxSpeed : '0'),
                    today_odo: data.data[i].today_odo,
                    today_running: _this.millisToMinutesAndSeconds(data.data[i].today_running),
                    today_stopped: _this.millisToMinutesAndSeconds(data.data[i].today_stopped),
                    t_idling: _this.millisToMinutesAndSeconds(data.data[i].t_idling),
                    t_ofr: _this.millisToMinutesAndSeconds(data.data[i].t_ofr),
                    today_trips: data.data[i].today_trips,
                    mileage: data.data[i].Mileage ? ((data.data[i].today_odo) / Number(data.data[i].Mileage)).toFixed(2) : (data.data[i].ID ? ((data.data[i].today_odo) / Number(data.data[i].ID.Mileage)).toFixed(2) : 'N/A')
                });
            }
        }, function (error) {
            _this.apicalldaily.stopLoading();
            console.log("error in service=> " + error);
        });
    };
    DailyReportNewPage.prototype.millisToMinutesAndSeconds = function (millis) {
        var ms = millis;
        ms = 1000 * Math.round(ms / 1000); // round to nearest second
        var d = new Date(ms);
        // debugger
        var min1;
        var min = d.getUTCMinutes();
        if ((min).toString().length == 1) {
            min1 = '0' + (d.getUTCMinutes()).toString();
        }
        else {
            min1 = min;
        }
        return d.getUTCHours() + ':' + min1;
    };
    DailyReportNewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-daily-report-new',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/daily-report-new/daily-report-new.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{"Value Screen" | translate}}</ion-title>\n    <ion-buttons end>\n      <ion-datetime class="dateStyle" displayFormat="DD/MM/YYYY" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="to"\n        (ionChange)="getDailyReportData()" style="color: white;"></ion-datetime>\n    </ion-buttons>\n  </ion-navbar>\n  <ion-item style="background-color: #fafafa;">\n    <ion-label style="margin-top: 15px;">{{"Select Vehicle" | translate}}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getSelectedId(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-grid *ngFor="let item of deviceReport">\n    <ion-row>\n      <ion-col col-12 offset-sm="3">\n        <ion-card\n          style="background-image: linear-gradient(to right top, #ff5177, #ff6c88, #ff849a, #fc9aab, #f9afbb);padding: 5px">\n          <p style="color: white;">{{"Total Distance" | translate}}</p>\n          <ion-row>\n            <ion-col col-12>\n              <p>\n                <span style="font-size: xx-large; color: white;" *ngIf="item.today_odo">{{\n                      item.today_odo | number: "1.0-2"\n                    }}</span>\n                <span style="font-size: xx-large; color: white;" *ngIf="!item.today_odo">0.00</span>\n                <span\n                  style="color: white;">{{ (measurementUnit == \'MKS\') ? ("Kms" | translate) : ("Miles" | translate)}}</span>\n                <ion-buttons style="float: right;\n                    font-size: xx-large; color: white;"><img src="assets/imgs/distan.png" style="width: 50px;">\n                </ion-buttons>\n              </p>\n            </ion-col>\n          </ion-row>\n          <p style="color: white;">{{"Total distance travelled" | translate}}</p>\n        </ion-card>\n\n        <ion-card\n          style="background-image: linear-gradient(to right top, #1ef015, #40f144, #57f260, #6bf278, #7ef28d); padding: 5px;">\n          <p style="color: white;">{{"Running" | translate}}(Hr:Min)</p>\n          <ion-row>\n            <ion-col col-12>\n              <p>\n                <span style="font-size: xx-large; color: white;"\n                  *ngIf="item.today_running">{{ item.today_running }}</span>\n                <span *ngIf="!item.today_running">0.00</span>\n                <span style="color: white;"></span>\n                <ion-buttons style="float: right;\n                    font-size: xx-large; color: white;"><img src="assets/imgs/playbtn.png" style="width: 50px;">\n                </ion-buttons>\n              </p>\n            </ion-col>\n          </ion-row>\n          <p style="color: white;">{{"Total running time" | translate}}</p>\n        </ion-card>\n\n        <ion-card\n          style="background-image: linear-gradient(to right top, #f0151f, #ee2932, #ea3842, #e64550, #e0515d); padding: 5px;">\n          <p style="color: white;">{{"Stop" | translate}}(Hr:Min)</p>\n          <ion-row>\n            <ion-col col-12>\n              <p>\n                <span style="font-size: xx-large; color: white;"\n                  *ngIf="item.today_stopped">{{ item.today_stopped }}</span>\n                <span *ngIf="!item.today_stopped">00.00</span>\n                <span style="color: white;"></span>\n                <ion-buttons style="float: right;\n                        font-size: xx-large; color: white;"><img src="assets/imgs/stopping.png" style="width: 50px;">\n                </ion-buttons>\n              </p>\n            </ion-col>\n          </ion-row>\n          <p style="color: white;">{{"Total stopped time" | translate}}</p>\n        </ion-card>\n\n        <ion-card\n          style="background-image: linear-gradient(to right top, #cbd500, #cada26, #cadf3a, #cae44a, #cae959); padding: 5px;">\n          <p style="color: white;">{{"Idle" | translate}}</p>\n          <ion-row>\n            <ion-col col-12>\n              <p>\n                <span style="font-size: xx-large; color: white;" *ngIf="item.t_idling">{{ item.t_idling }}</span>\n                <span *ngIf="!item.t_idling">0.00</span>\n                <span style="color: white;"></span>\n                <ion-buttons style="float: right;\n                        font-size: xx-large; color: white;"><img src="assets/imgs/steering-wheel.png"\n                    style="width: 50px;"></ion-buttons>\n              </p>\n            </ion-col>\n          </ion-row>\n          <p style="color: white;">{{"Total idle time" | translate}}</p>\n        </ion-card>\n\n        <ion-card\n          style="background-image: linear-gradient(to right top, #72bee7, #6cbce7, #66bbe8, #60b9e8, #59b7e9); padding: 5px;">\n          <p style="color: white;">{{"Out of reach" | translate}}</p>\n          <ion-row>\n            <ion-col col-12>\n              <p>\n                <span style="font-size: xx-large; color: white;" *ngIf="item.t_ofr">{{ item.t_ofr }}</span>\n                <span *ngIf="!item.t_ofr">0.00</span>\n                <span style="color: white;"></span>\n                <ion-buttons style="float: right;\n                        font-size: xx-large; color: white;"><img src="assets/imgs/no-wifi.png" style="width: 50px;">\n                </ion-buttons>\n              </p>\n            </ion-col>\n          </ion-row>\n          <p style="color: white;">{{"Total Out of reach" | translate}}</p>\n        </ion-card>\n\n        <ion-card\n          style="background-image: linear-gradient(to right top, #5ce0e0, #5ee1cf, #6ae0bc, #7bdfa8, #8fdc93); padding: 5px;">\n          <p style="color: white;">\n            {{"Fuel Consumption" | translate}}({{ (measurementUnit == \'MKS\') ? ("Litre" | translate) : ("Gallon" | translate)}})\n          </p>\n          <ion-row>\n            <ion-col col-12>\n              <p>\n                <span style="font-size: xx-large; color: white;">{{ item.mileage }}</span>\n                <span style="color: white;"></span>\n                <ion-buttons style="float: right;\n                    font-size: xx-large; color: white;"><img src="assets/imgs/fuelcon.png" style="width: 50px;">\n                </ion-buttons>\n              </p>\n            </ion-col>\n          </ion-row>\n          <p style="color: white;">{{"Total fuel consumption" | translate}}</p>\n        </ion-card>\n\n        <ion-card\n          style="background-image: linear-gradient(to right top, #29e081, #7fdb7e, #a7d586, #c0d097, #cdcdae); padding: 5px;">\n          <p style="color: white;">{{"Max Speed" | translate}}({{ (measurementUnit == \'MKS\') ? ("Km/hr" | translate) : ("Miles/hr" | translate)}})</p>\n          <ion-row>\n            <ion-col col-12>\n              <p>\n                <span style="font-size: xx-large; color: white;" *ngIf="item.maxSpeed">{{ item.maxSpeed }}</span>\n                <span style="font-size: xx-large; color: white;" *ngIf="!item.maxSpeed">0.00</span>\n                <ion-buttons style="float: right;\n                            font-size: xx-large; color: white;"><img src="assets/imgs/max.png" style="width: 50px;">\n                </ion-buttons>\n              </p>\n            </ion-col>\n          </ion-row>\n          <p style="color: white;">{{"Max speed" | translate}}</p>\n        </ion-card>\n\n        <ion-card\n          style="background-image: linear-gradient(to right top, #9f94c0, #88a7d9, #66bce6, #49cee4, #57ded2); padding: 5px;">\n          <p style="color: white;">{{"Trips" | translate}}</p>\n          <ion-row>\n            <ion-col col-12>\n              <p>\n                <span style="font-size: xx-large; color: white;" *ngIf="item.today_trips">{{ item.today_trips }}</span>\n                <span style="font-size: xx-large; color: white;" *ngIf="!item.today_trips">00.00</span>\n                <span style="color: white;"></span>\n                <ion-buttons style="float: right;\n                            font-size: xx-large; color: white;"><img src="assets/imgs/trip.png" style="width: 50px;">\n                </ion-buttons>\n              </p>\n            </ion-col>\n          </ion-row>\n          <p style="color: white;">{{"Total trips" | translate}}</p>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/daily-report-new/daily-report-new.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], DailyReportNewPage);
    return DailyReportNewPage;
}());

//# sourceMappingURL=daily-report-new.js.map

/***/ }),

/***/ 968:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DailyReportNewPageModule", function() { return DailyReportNewPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__daily_report_new__ = __webpack_require__(1058);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var DailyReportNewPageModule = /** @class */ (function () {
    function DailyReportNewPageModule() {
    }
    DailyReportNewPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__daily_report_new__["a" /* DailyReportNewPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__daily_report_new__["a" /* DailyReportNewPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], DailyReportNewPageModule);
    return DailyReportNewPageModule;
}());

//# sourceMappingURL=daily-report-new.module.js.map

/***/ })

});
//# sourceMappingURL=56.js.map