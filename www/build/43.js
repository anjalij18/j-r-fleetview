webpackJsonp([43],{

/***/ 1090:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryDevicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ion_bottom_drawer__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modal__ = __webpack_require__(564);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_geocoder_geocoder__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_common__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









// import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

var HistoryDevicePage = /** @class */ (function () {
    function HistoryDevicePage(events, navCtrl, navParams, alertCtrl, toastCtrl, apiCall, plt, translate, modalCtrl, geocoderApi, 
    // private sqlite: SQLite,
    datePipe, elementRef) {
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.apiCall = apiCall;
        this.plt = plt;
        this.translate = translate;
        this.modalCtrl = modalCtrl;
        this.geocoderApi = geocoderApi;
        this.datePipe = datePipe;
        this.elementRef = elementRef;
        this.shouldBounce = true;
        this.dockedHeight = 100;
        this.distanceTop = 378;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_6_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.states = __WEBPACK_IMPORTED_MODULE_6_ion_bottom_drawer__["a" /* DrawerState */];
        this.minimumHeight = 0;
        this.showActionSheet = false;
        this.transition = ['0.5s', 'ease-in-out'];
        this.data2 = {};
        this.locations = [];
        this.dataArrayCoords = [];
        this.SelectVehicle = 'Select Vehicle';
        this.allData = {};
        this.showZoom = false;
        this.latLngArray = [];
        this.devices = [];
        this.markersArray = [];
        this.fraction = 0;
        this.zoomLevel = 15;
        this.direction = 1;
        this.cumu_distance = 0;
        this.addressLine = 'N/A';
        this.twoMonthsLater = __WEBPACK_IMPORTED_MODULE_3_moment__().subtract(2, 'month').format("YYYY-MM-DD");
        this.today = __WEBPACK_IMPORTED_MODULE_3_moment__().format("YYYY-MM-DD");
        this.battery = 0;
        this.seekBarValue = 200;
        this.sliderValue = 0;
        this.hideMe = false;
        this.speedValue123 = 1;
        this.measurementUnit = 'MKS';
        this.showRoute = true;
        this.rangeDetector = false;
        this.latLngLine = [];
        var selectedMapKey;
        if (localStorage.getItem('MAP_KEY') != null) {
            selectedMapKey = localStorage.getItem('MAP_KEY');
            if (selectedMapKey == this.translate.instant('Hybrid')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
            else if (selectedMapKey == this.translate.instant('Normal')) {
                this.mapKey = 'MAP_TYPE_NORMAL';
            }
            else if (selectedMapKey == this.translate.instant('Terrain')) {
                this.mapKey = 'MAP_TYPE_TERRAIN';
            }
            else if (selectedMapKey == this.translate.instant('Satellite')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
        }
        else {
            this.mapKey = 'MAP_TYPE_NORMAL';
        }
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }
    HistoryDevicePage.prototype.checkScreen = function () {
        var _this = this;
        this.navBar.backButtonClick = function (e) {
            // todo something
            // this.navController.pop();
            console.log("back button poped");
            if (localStorage.getItem("SCREEN") != null) {
                if (localStorage.getItem("SCREEN") === 'live') {
                    _this.navCtrl.setRoot('LivePage');
                }
                else {
                    if (localStorage.getItem("SCREEN") === 'dashboard') {
                        _this.navCtrl.setRoot('AddDevicesPage');
                    }
                }
            }
        };
    };
    HistoryDevicePage.prototype.backBtnEvent = function () {
        var _this = this;
        this.navBar.backButtonClick = function (ev) {
            _this.hideMe = true;
            console.log('this will work in Ionic 3 +');
            _this.navCtrl.pop({
                animate: true, animation: 'transition-ios', direction: 'back'
            });
        };
    };
    HistoryDevicePage.prototype.changeSpeed = function (t) {
        console.log(t);
        var that = this;
        that.speed = t * 100;
        // that.events.publish("SpeedValue:Updated", that.speed)
    };
    HistoryDevicePage.prototype.ionViewDidEnter = function () {
        this.backBtnEvent();
        ////////////////////////////////////////
        this.getDefaultUserSettings();
        /////////////////////////////////////////
        this.allData.playFlag = 'start';
        this.allData.flag2 = 'init';
        this.initMap();
        if (localStorage.getItem("SCREEN") != null) {
            console.log("screen printed: ", localStorage.getItem('SCREEN'));
            this.checkScreen();
        }
        localStorage.removeItem("markerTarget");
        // localStorage.removeItem('HistoryFlag');
        if (this.navParams.get('device') !== null && this.navParams.get('device') !== undefined) {
            this.device = this.navParams.get('device');
            console.log("passed params: ", this.device);
            this.trackerId = this.device.Device_ID;
            this.trackerType = this.device.iconType;
            this.DeviceId = this.device._id;
            this.trackerName = this.device.Device_Name;
            this.btnClicked();
        }
        else {
            this.showDropDown = true;
            this.getdevices();
            // this.getDataFromSQLiteDB();
        }
        this.hideplayback = false;
        this.target = 0;
    };
    HistoryDevicePage.prototype.getDefaultUserSettings = function () {
        var _this = this;
        var b_url = this.apiCall.mainUrl + "users/get_user_setting";
        var Var = { uid: this.islogin._id };
        this.apiCall.urlpasseswithdata(b_url, Var)
            .subscribe(function (resp) {
            console.log("check lang key: ", resp);
            if (resp.unit_measurement !== undefined) {
                _this.measurementUnit = resp.unit_measurement;
            }
            else {
                if (localStorage.getItem('MeasurementType') !== null) {
                    var measureType = localStorage.getItem('MeasurementType');
                    _this.measurementUnit = measureType;
                }
                else {
                    _this.measurementUnit = 'MKS';
                }
            }
        }, function (err) {
            console.log(err);
            if (localStorage.getItem('MeasurementType') !== null) {
                var measureType = localStorage.getItem('MeasurementType');
                _this.measurementUnit = measureType;
            }
            else {
                _this.measurementUnit = 'MKS';
            }
        });
    };
    HistoryDevicePage.prototype.ngOnInit = function () { };
    HistoryDevicePage.prototype.ionViewDidLeave = function () {
        localStorage.removeItem("markerTarget");
        // localStorage.removeItem("speedMarker");
        // localStorage.removeItem("updatetimedate");
        localStorage.removeItem("MainHistory");
        if (this.intevalId) {
            clearInterval(this.intevalId);
        }
    };
    HistoryDevicePage.prototype.ngOnDestroy = function () { };
    HistoryDevicePage.prototype.changeformat = function (date) {
        console.log("date=> " + new Date(date).toISOString());
        // this.btnClicked();
    };
    HistoryDevicePage.prototype.setDocHeight = function () {
        console.log("dockerchage event");
        this.dockedHeight = 150;
        this.distanceTop = 46;
    };
    HistoryDevicePage.prototype.closeDocker = function () {
        var that = this;
        that.showActionSheet = false;
    };
    HistoryDevicePage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.portstemp = data.devices;
        }, function (error) {
            _this.apiCall.stopLoading();
            console.log(error);
        });
    };
    HistoryDevicePage.prototype.onChangedSelect = function (item) {
        debugger;
        var that = this;
        // if (that.allData.start2) {
        //   clearTimeout(that.allData.start2);
        //   console.log("timeout cleared!!!!!!!!!!!")
        // }
        // that.allData.flag2 = 'init';
        that.trackerId = item.Device_ID;
        that.trackerType = item.iconType;
        that.DeviceId = item._id;
        that.trackerName = item.Device_Name;
        // if (that.allData.map) {
        //   that.allData.map.clear();
        //   that.allData.map.remove();
        // }
        this.btnClicked();
    };
    HistoryDevicePage.prototype.reCenterMe = function () {
        // console.log("getzoom level: " + this.allData.map.getCameraZoom());
        this.allData.map.moveCamera({
            target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
            zoom: this.allData.map.getCameraZoom()
        }).then(function () {
        });
    };
    HistoryDevicePage.prototype.getIconUrl = function () {
        var that = this;
        var iconUrl;
        if (that.plt.is('ios')) {
            iconUrl = 'www/assets/imgs/vehicles/running' + that.trackerType + '.png';
        }
        else if (that.plt.is('android')) {
            iconUrl = './assets/imgs/vehicles/running' + that.trackerType + '.png';
        }
        console.log("icon url: ", iconUrl);
        return iconUrl;
    };
    HistoryDevicePage.prototype.play2 = function () {
        var that = this;
        that.allData.speed = 50;
        var coord = that.dataArrayCoords[that.target];
        that.coordreplaydata = coord;
        var lat = coord[0];
        var lng = coord[1];
        debugger;
        if (that.allData.flag2 == 'init') {
            localStorage.setItem("AlreadyClicked", "AlreadyClicked");
            if (that.allData.mark == undefined) {
                var icicon;
                if (that.plt.is('ios')) {
                    icicon = 'www/assets/imgs/vehicles/running' + that.trackerType + '.png';
                }
                else if (that.plt.is('android')) {
                    icicon = './assets/imgs/vehicles/running' + that.trackerType + '.png';
                }
                that.allData.map.addMarker({
                    icon: {
                        url: icicon,
                        size: {
                            width: 20,
                            height: 40
                        }
                    },
                    styles: {
                        'text-align': 'center',
                        'font-style': 'italic',
                        'font-weight': 'bold',
                        'color': 'green'
                    },
                    position: new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](lat, lng),
                }).then(function (marker) {
                    that.allData.mark = marker;
                    that.allData.map.animateCamera({
                        target: marker.getPosition(),
                        duration: 1500,
                        zoom: 16
                    });
                    that.animateMarker2(that.allData.map, that.allData.mark, null, that.dataArrayCoords);
                    that.allData.flag2 = 'stop';
                });
            }
            else {
                that.allData.mark.setPosition({ lat: lat, lng: lng });
                that.allData.map.setCameraTarget({ lat: lat, lng: lng });
                that.animateMarker2(that.allData.map, that.allData.mark, null, that.dataArrayCoords);
                that.allData.flag2 = 'stop';
            }
        }
        else if (that.allData.flag2 == 'start') {
            that._moveMarker2();
            that.allData.flag2 = 'stop';
        }
        else if (that.allData.flag2 == 'stop') {
            //  dmap.speed = 0;
            clearTimeout(that.allData.start2);
            that.allData.flag2 = 'start';
        }
        if (that.allData.flag2 == 'reset') {
            console.log("flag2 is: ", that.allData.flag2);
            console.log("check reset coords: " + that.dataArrayCoords[0][0]);
            that.allData.mark.setPosition({ lat: that.dataArrayCoords[0][0], lng: that.dataArrayCoords[0][1] });
            that.allData.map.setCameraTarget({ lat: that.dataArrayCoords[0][0], lng: that.dataArrayCoords[0][1] });
            that.seekBarValue = 0;
            clearTimeout(that.allData.start2);
            that.allData.flag2 = 'init';
        }
        return that.allData.flag2;
    };
    ;
    HistoryDevicePage.prototype.animateMarker2 = function (map, mark, icons, coords) {
        var that = this;
        that.cumu_distance = 0;
        that.allData.speed = 50;
        that.allData.delay = 100;
        if (that.allData.start2)
            clearTimeout(that.allData.start2);
        var target = 0;
        that._goToPoint2 = function () {
            if (that.speed) {
                that.allData.speed = that.speed;
            }
            ///////////////////////////////////////////////
            if (that.rangeDetector === true) {
                a = that.indexValue;
                target = that.indexValue;
                that.sliderValue = that.indexValue;
            }
            ///////////////////////////////////////////////
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (that.allData.speed * 1000 * that.allData.delay) / 3600000; // in meters
            if (coords[target] === undefined) {
                if (that.allData.start2)
                    clearTimeout(that.allData.start2);
                that.allData.flag2 = 'init';
                that.sliderValue = 0;
                return;
            }
            var dest = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](coords[target][0], coords[target][1]);
            var distance = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target][0] - lat) / numStep;
            var deltaLng = (coords[target][1] - lng) / numStep;
            function changeMarker(mark, deg) {
                console.log("check marker: ", mark);
                if (Number.isNaN(parseInt(deg))) {
                    console.log("check degree: " + parseInt(deg));
                }
                else {
                    console.log("check not: " + parseInt(deg));
                    if (mark) {
                        mark.setRotation(deg);
                    }
                }
            }
            that._moveMarker2 = function () {
                var head;
                that.sliderValue = a;
                that.cumu_distance = coords[target][5].cumu_dist;
                console.log("cumulative distance: ", that.cumu_distance);
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                if (i < distance) {
                    head = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    if ((head !== 0) || (head !== NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    map.setCameraTarget({ lat: lat, lng: lng });
                    that.getAddress(lat, lng);
                    that.allData.start2 = setTimeout(that._moveMarker2, that.allData.delay);
                }
                else {
                    head = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    if ((head !== 0) || (head !== NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(dest);
                    map.setCameraTarget(dest);
                    that.getAddress(dest.lat, dest.lng);
                    target++;
                    if (target == coords.length) {
                        that.allData.flag2 = 'reset';
                        clearTimeout(that.allData.start2);
                    }
                    that.allData.start2 = setTimeout(that._goToPoint2, that.allData.delay);
                }
            };
            a++;
            that.rangeDetector = false;
            console.log("aaaaaaaaaaaaaaaaa " + a);
            console.log("coords length " + coords.length);
            if (a > coords.length) {
                console.log("inside this aaaaaaaaaaaaaaaaa " + a);
            }
            else {
                console.log("coords target: ", coords[target]);
                that.speedMarker = coords[target][3].speed;
                that.updatetimedate = coords[target][2].time;
                // that.cumu_distance = coords[target][5].cumu_dist;
                that.battery = coords[target][6].battery;
                console.log('move marker running');
                that._moveMarker2();
            }
        };
        var a = 0;
        that._goToPoint2();
    };
    HistoryDevicePage.prototype.getAddress = function (lat, lng) {
        var that = this;
        var coordinates = {
            lat: lat,
            long: lng
        };
        if (!coordinates) {
            that.addressLine = 'N/A';
            return;
        }
        this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
            .then(function (res) {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            // that.saveAddressToServer(str, coordinates.lat, coordinates.long);
            that.addressLine = str;
        });
    };
    HistoryDevicePage.prototype.zoomin = function () {
        var that = this;
        that.allData.map.animateCameraZoomIn();
        // that.allData.map.moveCameraZoomIn();
    };
    HistoryDevicePage.prototype.zoomout = function () {
        var that = this;
        that.allData.map.animateCameraZoomOut();
    };
    HistoryDevicePage.prototype.hidePlayRoute = function () {
        var that = this;
        that.showRoute = !that.showRoute;
        if (!that.showRoute) {
            if (that.historyPolyline) {
                that.historyPolyline.remove();
            }
        }
        else {
            that.historyPolyline = undefined;
            that.allData.map.addPolyline({
                points: that.mapData,
                color: '#635400',
                width: 3,
                geodesic: true
            }).then(function (polyline) {
                that.historyPolyline = polyline;
            });
        }
    };
    HistoryDevicePage.prototype.btnClicked = function () {
        this.speedMarker = undefined;
        this.updatetimedate = undefined;
        this.cumu_distance = undefined;
        this.battery = undefined;
        this.data2.Distance = undefined;
        var dev = this.navParams.get('device');
        if (dev === null || dev === undefined) {
            if (this.selectedVehicle === undefined) {
                var alert_1 = this.alertCtrl.create({
                    title: 'Alert',
                    message: 'Please select the vehicle first and try again',
                    buttons: ['Okay']
                });
                alert_1.present();
            }
            else {
                if (this.mapData !== undefined) {
                    if (this.mapData.length > 0) {
                        if (this.allData.map) {
                            this.allData.map.remove();
                            if (this.allData.mark)
                                this.allData.mark.remove();
                            ///////////////////////////////////////
                            var that = this;
                            if (that.allData.start2) {
                                clearTimeout(that.allData.start2);
                                console.log("timeout cleared!!!!!!!!!!!");
                            }
                            /////////////////////////////
                            this.allData = {};
                            that.allData.flag2 = 'init';
                            this.initMap();
                            this.maphistory();
                        }
                    }
                    else {
                        this.maphistory();
                    }
                }
                else {
                    this.maphistory();
                }
            }
        }
        else {
            if (this.mapData !== undefined) {
                if (this.mapData.length > 0) {
                    if (this.allData.map) {
                        this.allData.map.remove();
                        if (this.allData.mark)
                            this.allData.mark.remove();
                        ///////////////////////////////////////
                        var that = this;
                        if (that.allData.start2) {
                            clearTimeout(that.allData.start2);
                            console.log("timeout cleared!!!!!!!!!!!");
                        }
                        /////////////////////////////
                        this.allData = {};
                        that.allData.flag2 = 'init';
                        this.initMap();
                        this.maphistory();
                    }
                }
                else {
                    this.maphistory();
                }
            }
            else {
                this.maphistory();
            }
        }
    };
    HistoryDevicePage.prototype.maphistory = function () {
        var that = this;
        that.mapData = [];
        that.latLngLine = [];
        that.sliderValue = 0;
        that.dataArrayCoords = [];
        this.data2 = {};
        // that.allData
        this.latlongObjArr = undefined;
        var from1 = new Date(this.datetimeStart);
        this.fromtime = from1.toISOString();
        var to1 = new Date(this.datetimeEnd);
        this.totime = to1.toISOString();
        if (this.totime >= this.fromtime) {
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: 'Select Correct Time',
                message: 'To time always greater than From Time',
                buttons: ['ok']
            });
            alert_2.present();
            return false;
        }
        this.getHistoryData();
    };
    HistoryDevicePage.prototype.getHistoryData = function () {
        var _this = this;
        var that = this;
        this.apiCall.startLoading().present();
        that.apiCall.gpsCall(this.trackerId, new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString())
            .subscribe(function (data3) {
            that.apiCall.stopLoading();
            if (data3.length > 1) { // to draw polyline at least need two points
                that.gps(data3.reverse());
                that.getDistance();
            }
            else {
                var alert_3 = that.alertCtrl.create({
                    title: 'No Data Found',
                    message: 'Vehicle has not moved from ' + _this.datePipe.transform(new Date(_this.datetimeStart), 'medium') + ' to ' + _this.datePipe.transform(new Date(_this.datetimeEnd), 'medium'),
                    buttons: [{
                            text: 'OK',
                            handler: function () {
                                that.hideplayback = false;
                            }
                        }]
                });
                alert_3.present();
            }
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = that.alertCtrl.create({
                message: msg.message,
                buttons: [_this.translate.instant('Okay')]
            });
            alert.present();
        });
        // }
    };
    HistoryDevicePage.prototype.getDistance = function () {
        var _this = this;
        // this.apiCall.startLoading().present();
        this.apiCall.getDistanceSpeedCall(this.trackerId, new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString())
            .subscribe(function (data3) {
            debugger;
            _this.data2 = data3;
            _this.latlongObjArr = data3;
            // debugger
            if (isNaN(_this.data2["Average Speed"])) {
                _this.data2.AverageSpeed = 0;
            }
            else {
                _this.data2.AverageSpeed = _this.data2["Average Speed"];
            }
            _this.data2.IdleTime = _this.data2["Idle Time"];
            _this.hideplayback = true;
            //////////////////////////////////
            // this.callgpsFunc(this.fromtime, this.totime);
            // this.locations = [];
            // this.stoppages(timeStart, timeEnd);
            ////////////////////////////////
        }, function (error) {
            // this.apiCall.stopLoading();
            console.log("error in getdistancespeed =>  ", error);
            var body = error._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: ['okay']
            });
            alert.present();
        });
    };
    HistoryDevicePage.prototype.stoppages = function () {
        var _this = this;
        this.locations = [];
        var that = this;
        that.apiCall.stoppage_detail(this.islogin._id, new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), this.DeviceId)
            .subscribe(function (res) {
            console.log('stoppage data', res);
            var arr = [];
            for (var i = 0; i < res.length; i++) {
                _this.arrivalTime = new Date(res[i].arrival_time).toLocaleString();
                _this.departureTime = new Date(res[i].departure_time).toLocaleString();
                var fd = new Date(_this.arrivalTime).getTime();
                var td = new Date(_this.departureTime).getTime();
                var time_difference = td - fd;
                var total_min = time_difference / 60000;
                var hours = total_min / 60;
                var rhours = Math.floor(hours);
                var minutes = (hours - rhours) * 60;
                var rminutes = Math.round(minutes);
                var Durations = rhours + 'Hours' + ':' + rminutes + 'Min';
                arr.push({
                    lat: res[i].lat,
                    lng: res[i].long,
                    arrival_time: res[i].arrival_time,
                    departure_time: res[i].departure_time,
                    device: res[i].device,
                    address: res[i].address,
                    user: res[i].user,
                    duration: Durations
                });
                that.locations.push(arr);
                if (that.locations[0] != undefined) { // check if there is stoppages or not
                    for (var k = 0; k < that.locations[0].length; k++) {
                        that.setStoppages(that.locations[0][k]);
                    }
                }
            }
            console.log('stoppage data locations', that.locations);
            // this.callgpsFunc(this.fromtime, this.totime);
        }, function (err) {
            // this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: ['okay']
            });
            alert.present();
        });
    };
    HistoryDevicePage.prototype.callgpsFunc = function (fromtime, totime) {
        var _this = this;
        var that = this;
        that.apiCall.gpsCall(this.trackerId, fromtime, totime)
            .subscribe(function (data3) {
            // that.apiCall.stopLoading();
            if (data3.length > 0) {
                if (data3.length > 1) { // to draw polyline at least need two points
                    that.gps(data3.reverse());
                }
                else {
                    var alert_4 = that.alertCtrl.create({
                        message: 'No Data found for selected vehicle..',
                        buttons: [{
                                text: 'OK',
                                handler: function () {
                                    // that.datetimeStart = moment({ hours: 0 }).format();
                                    // console.log('start date', this.datetimeStart)
                                    // that.datetimeEnd = moment().format();//new Date(a).toISOString();
                                    // console.log('stop date', this.datetimeEnd);
                                    // that.selectedVehicle = undefined;
                                    that.hideplayback = false;
                                }
                            }]
                    });
                    alert_4.present();
                }
            }
            else {
                var alert_5 = that.alertCtrl.create({
                    message: 'No Data found for selected vehicle..',
                    buttons: [{
                            text: 'OK',
                            handler: function () {
                                // that.datetimeStart = moment({ hours: 0 }).format();
                                // console.log('start date', this.datetimeStart)
                                // that.datetimeEnd = moment().format();//new Date(a).toISOString();
                                // console.log('stop date', this.datetimeEnd);
                                // that.selectedVehicle = undefined;
                                that.hideplayback = false;
                            }
                        }]
                });
                alert_5.present();
            }
        }, function (err) {
            // that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = that.alertCtrl.create({
                message: msg.message,
                buttons: [_this.translate.instant('Okay')]
            });
            alert.present();
        });
    };
    HistoryDevicePage.prototype.replayHistory = function () {
        var that = this;
        that.allData.flag2 = 'reset';
        that.speedMarker = 0;
        that.updatetimedate = undefined;
        that.cumu_distance = 0;
        that.battery = 0;
        that.play2();
    };
    HistoryDevicePage.prototype.changeDate = function (dateid) {
        if (dateid == 'today') {
            var td = new Date();
            var rt = new Date(td.setHours(0, 0, 0));
            this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__(rt).format();
            this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format();
            this.btnClicked();
        }
        if (dateid == 'yest') {
            var yt = void 0;
            yt = __WEBPACK_IMPORTED_MODULE_3_moment__().subtract(1, 'days');
            var ptt = new Date(yt).setHours(0, 0, 0);
            this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(ptt)).format();
            var ttt = new Date(yt).setHours(23, 59, 59);
            this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(ttt)).format();
            this.btnClicked();
        }
        if (dateid == 'week') {
            var wd;
            wd = __WEBPACK_IMPORTED_MODULE_3_moment__().startOf('week');
            this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(wd)).format();
            this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format();
            this.btnClicked();
        }
        if (dateid == 'hour') {
            // var md = moment().startOf('hour').format();
            var md = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"]().subtract(1, "hours").format();
            this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(md)).format();
            console.log("start hour: ", this.datetimeStart);
            this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format();
            console.log("end hour: ", this.datetimeEnd);
            this.btnClicked();
        }
    };
    HistoryDevicePage.prototype.initMap = function () {
        if (this.allData.map != undefined) {
            this.allData.map.remove();
        }
        var mapOptions = {
            gestures: {
                rotate: false,
                tilt: false,
                compass: false
            },
            mapType: this.mapKey
        };
        this.allData.map = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas', mapOptions);
    };
    HistoryDevicePage.prototype.gps = function (data3) {
        var that = this;
        that.mapData = data3.map(function (d) {
            return { lat: d.lat, lng: d.lng };
        });
        that.mapData.reverse();
        var bounds = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["g" /* LatLngBounds */](that.mapData);
        that.allData.map.moveCamera({
            target: bounds
        });
        that.latlongObjArr = data3;
        var cumulativeDistance = 0;
        for (var i = 0; i < that.latlongObjArr.length; i++) {
            if (that.latlongObjArr[i].lat && that.latlongObjArr[i].lng) {
                var arr = [];
                var startdatetime = new Date(that.latlongObjArr[i].insertionTime);
                arr.push(that.latlongObjArr[i].lat);
                arr.push(that.latlongObjArr[i].lng);
                arr.push({ "time": startdatetime.toLocaleString() });
                arr.push({ "speed": that.latlongObjArr[i].speed });
                arr.push({ "imei": that.latlongObjArr[i].imei });
                debugger;
                if (that.latlongObjArr[i].isPastData != true) {
                    if (i === 0) {
                        cumulativeDistance += 0;
                    }
                    else {
                        cumulativeDistance += that.latlongObjArr[i].distanceFromPrevious ? parseFloat(that.latlongObjArr[i].distanceFromPrevious) : 0;
                    }
                    that.latlongObjArr[i]['cummulative_distance'] = (cumulativeDistance);
                    arr.push({ "cumu_dist": that.latlongObjArr[i]['cummulative_distance'] });
                }
                else {
                    that.latlongObjArr[i]['cummulative_distance'] = (cumulativeDistance);
                    arr.push({ "cumu_dist": that.latlongObjArr[i]['cummulative_distance'] });
                }
                arr.push({ "battery": that.latlongObjArr[i].external_Battery });
                var cord = {
                    lat: that.latlongObjArr[i].lat,
                    lng: that.latlongObjArr[i].lng
                };
                // console.log("check battery: ", data3[i]['external_Battery'])
                that.dataArrayCoords.push(arr);
                that.latLngLine.push(cord);
            }
        }
        that.seekBarValue = that.dataArrayCoords.length;
        this.allData.map.on(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MAP_CLICK).subscribe(function (data) {
            console.log('Click MAP');
            that.drawerHidden1 = true;
        });
        var start_icon;
        var stop_icon;
        if (this.plt.is('android')) {
            start_icon = './assets/imgs/greenFlag.png';
            stop_icon = './assets/imgs/redFlag.png';
        }
        else if (this.plt.is('ios')) {
            start_icon = 'www/assets/imgs/greenFlag.png';
            stop_icon = 'www/assets/imgs/redFlag.png';
        }
        that.allData.map.addMarker({
            title: 'D',
            position: that.mapData[0],
            icon: {
                url: stop_icon,
                size: {
                    height: 40,
                    width: 40
                }
            },
            styles: {
                'text-align': 'center',
                'font-style': 'italic',
                'font-weight': 'bold',
                'color': 'red'
            },
        }).then(function (marker) {
            // marker.showInfoWindow();
            that.allData.map.addMarker({
                title: 'S',
                position: that.mapData[that.mapData.length - 1],
                icon: {
                    url: start_icon,
                    size: {
                        height: 40,
                        width: 40
                    }
                },
                styles: {
                    'text-align': 'center',
                    'font-style': 'italic',
                    'font-weight': 'bold',
                    'color': 'green'
                },
            }).then(function (marker) {
                // marker.showInfoWindow();
            });
        });
        that.allData.map.addPolyline({
            points: that.mapData,
            color: '#635400',
            width: 3,
            geodesic: true
        }).then(function (polyline) {
            that.historyPolyline = polyline;
        });
        that.hideplayback = true;
        //////////////
        var playerSeekbar;
        $(document).ready(function () {
            debugger;
            playerSeekbar = document.getElementById('slider1');
            console.log("ready!", playerSeekbar['value']);
            playerSeekbar.oninput = function () {
                // zoomToObject(flightPath);
                that.changeRange();
            };
        });
        //////////////
        that.stoppages();
    };
    HistoryDevicePage.prototype.changeRange = function () {
        // debugger
        var that = this;
        clearTimeout(that.allData.start2);
        that.rangeDetector = true;
        var rangeVal = document.getElementById("slider1");
        this.indexValue = rangeVal['value'];
        if (that.allData.flag2 == 'stop') {
            that.allData.mark.setPosition({ lat: this.latLngLine[this.indexValue].lat, lng: this.latLngLine[this.indexValue].lng });
            // this.historyMap.setCenter({ lat: this.latLngLine[this.indexValue].lat, lng: this.latLngLine[this.indexValue].lng });
            that.allData.map.moveCamera({
                target: {
                    lat: this.latLngLine[this.indexValue].lat,
                    lng: this.latLngLine[this.indexValue].lng
                }
            });
            //////////////////
            that.speedMarker = this.dataArrayCoords[this.indexValue][3].speed;
            that.updatetimedate = this.dataArrayCoords[this.indexValue][2].time;
            that.cumu_distance = this.dataArrayCoords[this.indexValue][5].cumu_dist;
            that.battery = this.dataArrayCoords[this.indexValue][6].battery;
            console.log("check speed value: ", that.speedMarker);
            console.log("check updatetimedate value: ", that.updatetimedate);
            console.log("check cumu_distance value: ", that.cumu_distance);
            console.log("check speed battery: ", that.battery);
            //////////////////
            that._goToPoint2();
        }
        if (that.allData.flag2 == 'start') {
            that.allData.mark.setPosition({ lat: this.latLngLine[this.indexValue].lat, lng: this.latLngLine[this.indexValue].lng });
            // this.historyMap.setCenter({ lat: this.latLngLine[this.indexValue].lat, lng: this.latLngLine[this.indexValue].lng });
            that.allData.map.moveCamera({
                target: {
                    lat: this.latLngLine[this.indexValue].lat,
                    lng: this.latLngLine[this.indexValue].lng
                }
            });
            that.allData.flag2 = 'stop';
            //////////////////
            that.speedMarker = this.dataArrayCoords[this.indexValue][3].speed;
            that.updatetimedate = this.dataArrayCoords[this.indexValue][2].time;
            that.cumu_distance = this.dataArrayCoords[this.indexValue][5].cumu_dist;
            that.battery = this.dataArrayCoords[this.indexValue][6].battery;
            //////////////////
            console.log("check speed value123: ", that.speedMarker);
            console.log("check updatetimedate value123: ", that.updatetimedate);
            console.log("check cumu_distance value123: ", that.cumu_distance);
            console.log("check speed battery123: ", that.battery);
            that._goToPoint2();
        }
    };
    HistoryDevicePage.prototype.zoomSet = function () {
        // this.allData.map.setZoom(15);
        this.allData.map.moveCamera({
            zoom: 15,
        });
    };
    HistoryDevicePage.prototype.setStoppages = function (pdata) {
        var that = this;
        ///////////////////////////////
        // let htmlInfoWindow = new HtmlInfoWindow();
        // let frame: HTMLElement = document.createElement('div');
        // frame.innerHTML = [
        //   '<p style="font-size: 7px;">Address:- ' + pdata.address + '</p>',
        //   '<p style="font-size: 7px;">Arrival Time:- ' + moment(new Date(pdata.arrival_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a") + '</p>',
        //   '<p style="font-size: 7px;">Departure Time:- ' + moment(new Date(pdata.departure_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a") + '</p>'
        // ].join("");
        // htmlInfoWindow.setContent(frame, { width: "220px", height: "100px" });
        ///////////////////////////////////////////////////
        if (pdata != undefined)
            (function (data) {
                // console.log("inside for data=> ", data)
                var centerMarker = data;
                var location = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](centerMarker.lat, centerMarker.lng);
                var markicon;
                if (that.plt.is('ios')) {
                    markicon = 'www/assets/imgs/park.png';
                }
                else if (that.plt.is('android')) {
                    markicon = './assets/imgs/park.png';
                }
                var markerOptions = {
                    position: location,
                    icon: {
                        url: markicon,
                        size: {
                            height: 22,
                            width: 22
                        }
                    }
                };
                that.allData.map.addMarker(markerOptions)
                    .then(function (marker) {
                    // console.log('centerMarker.ID' + centerMarker.ID)
                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                        .subscribe(function (e) {
                        that.showActionSheet = true;
                        // that.drawerHidden1 = false;
                        that.drawerState = __WEBPACK_IMPORTED_MODULE_6_ion_bottom_drawer__["a" /* DrawerState */].Docked;
                        __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["a" /* Geocoder */].geocode({
                            "position": {
                                lat: e[0].lat,
                                lng: e[0].lng
                            }
                        }).then(function (results) {
                            if (results.length == 0) {
                                return null;
                            }
                            that.addressof = results[0].extra.lines[0];
                        });
                        setTimeout(function () {
                            that.address = that.addressof;
                            console.log("pickup location new ", that.address);
                            that.arrTime = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(data.arrival_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a");
                            that.depTime = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(data.departure_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a");
                            var fd = new Date(data.arrival_time).getTime();
                            var td = new Date(data.departure_time).getTime();
                            var time_difference = td - fd;
                            var total_min = time_difference / 60000;
                            var hours = total_min / 60;
                            var rhours = Math.floor(hours);
                            var minutes = (hours - rhours) * 60;
                            var rminutes = Math.round(minutes);
                            that.durations = rhours + 'hours' + ':' + rminutes + 'mins';
                        }, 500);
                    });
                });
            })(pdata);
    };
    HistoryDevicePage.prototype.onIdle = function () {
        this.presentModal();
    };
    HistoryDevicePage.prototype.presentModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__modal__["a" /* ModalPage */]);
        modal.present();
        modal.onDidDismiss(function (data) {
            console.log("onDidDismiss", data);
            _this.getIdlePoints(data);
        });
    };
    HistoryDevicePage.prototype.getIdlePoints = function (min) {
        var _this = this;
        this.idleLocations = [];
        var urlbase = this.apiCall.mainUrl + 'stoppage/trip_idle?uId=' + this.islogin._id + '&from_date=' + new Date(this.datetimeStart).toISOString() + '&to_date=' + new Date(this.datetimeEnd).toISOString() + '&device=' + this.DeviceId + '&min_time=' + min;
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(urlbase)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("idle data=> " + data);
            if (data.length > 0) {
                for (var y = 0; y <= data.length; y++) {
                    _this.idleLocations.push(data[y]);
                }
                if (_this.idleLocations.length > 0) { // check if there is stoppages or not
                    for (var k = 0; k < _this.idleLocations.length; k++) {
                        _this.setIdlePoints(_this.idleLocations[k]);
                    }
                }
            }
        });
    };
    HistoryDevicePage.prototype.setIdlePoints = function (pdata) {
        var that = this;
        if (pdata != undefined)
            (function (data) {
                console.log("inside for data=> ", data);
                var centerMarker = data;
                var location = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](centerMarker.idle_location.lat, centerMarker.idle_location.long);
                var markicon;
                if (that.plt.is('ios')) {
                    markicon = 'www/assets/imgs/idle.png';
                }
                else if (that.plt.is('android')) {
                    markicon = './assets/imgs/idle.png';
                }
                var markerOptions = {
                    position: location,
                    icon: {
                        url: markicon,
                        size: {
                            height: 22,
                            width: 22
                        }
                    }
                };
                that.allData.map.addMarker(markerOptions)
                    .then(function (marker) {
                    // console.log('centerMarker.ID' + centerMarker.ID)
                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                        .subscribe(function (e) { });
                });
            })(pdata);
    };
    HistoryDevicePage.prototype.onClickMainMenu = function (item) {
        this.menuActive = !this.menuActive;
    };
    HistoryDevicePage.prototype.onClickMap = function (maptype) {
        var that = this;
        if (maptype == 'SATELLITE') {
            that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].HYBRID);
        }
        else {
            if (maptype == 'TERRAIN') {
                that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].TERRAIN);
            }
            else {
                if (maptype == 'NORMAL') {
                    that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].NORMAL);
                }
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], HistoryDevicePage.prototype, "navBar", void 0);
    HistoryDevicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-history-device',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/history-device/history-device.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title *ngIf="device">{{ device.Device_Name }}</ion-title>\n    <ion-title *ngIf="!device">{{ "View History" | translate }}</ion-title>\n  </ion-navbar>\n  <ion-item *ngIf="showDropDown">\n    <ion-label>{{ SelectVehicle }}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="onChangedSelect(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n  <ion-row style="background: white;">\n    <ion-col width-50 padding-left class="col1">\n      <ion-avatar item-start class="avtar">\n        <img src="assets/imgs/clock.svg" align="left" />\n      </ion-avatar>\n      <ion-label style="margin-top: 1px;">\n        <span style="font-size: 11px">{{ "From Date" | translate }}</span>\n        <ion-datetime displayFormat="DD/MM/YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [min]="twoMonthsLater"\n          [max]="today" [(ngModel)]="datetimeStart" (ionChange)="changeformat(datetimeStart)" style="font-size: 10px;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n    <ion-col width-50 class="col1">\n      <ion-avatar item-start class="avtar">\n        <img src="assets/imgs/clock.svg" align="left" />\n      </ion-avatar>\n      <ion-label style="margin-top: 1px;">\n        <span style="font-size: 11px">{{ "To Date" | translate }}</span>\n        <ion-datetime displayFormat="DD/MM/YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n          (ionChange)="changeformat(datetimeEnd)" style="font-size: 10px;"></ion-datetime>\n      </ion-label>\n    </ion-col>\n    <ion-col ion-text text-right padding-right>\n      <ion-icon ios="ios-search" md="md-search" style="font-size:30px;" (tap)="btnClicked()">\n      </ion-icon>\n    </ion-col>\n  </ion-row>\n  <ion-row style="background-color: #fff; margin-top: -10px;">\n    <ion-col style="text-align: center;">\n      <button ion-button round small style="background-color: #f0810f; font-size: 0.8em; padding: 10px;width: 21%;"\n        (click)="changeDate(\'hour\')">{{"1 Hour" | translate}}</button>\n      <button ion-button round small style="background-color: #f0810f; font-size: 0.8em; padding: 10px;width: 21%;"\n        (click)="changeDate(\'today\')">{{"Today" | translate}}</button>\n      <button ion-button round small style="background-color: #f0810f; font-size: 0.8em; padding: 10px;width: 21%;"\n        (click)="changeDate(\'yest\')">{{"Yesterday" | translate}}</button>\n      <button ion-button round small style="background-color: #f0810f; font-size: 0.8em; padding: 10px;width: 21%;"\n        (click)="changeDate(\'week\')">{{"Week" | translate}}</button>\n    </ion-col>\n  </ion-row>\n</ion-header>\n<ion-content *ngIf="!hideMe">\n  <div id="map_canvas">\n    <ion-fab top right style="margin-top: 17%;" *ngIf="showDropDown">\n      <button ion-fab color="light" mini (click)="onClickMainMenu()">\n        <ion-icon color="gpsc" name="map"></ion-icon>\n      </button>\n      <ion-fab-list side="bottom">\n        <button ion-fab (click)="onClickMap(\'SATELLITE\')" color="gpsc">\n          S\n        </button>\n        <button ion-fab (click)="onClickMap(\'TERRAIN\')" color="gpsc">\n          T\n        </button>\n        <button ion-fab (click)="onClickMap(\'NORMAL\')" color="gpsc">\n          N\n        </button>\n      </ion-fab-list>\n    </ion-fab>\n    <ion-fab top left style="margin-top: 17%;" *ngIf="showDropDown">\n      <button ion-fab color="light" mini>\n        <ion-icon color="gpsc" name="arrow-round-forward"></ion-icon>\n      </button>\n      <ion-fab-list side="bottom">\n        <button ion-fab (click)="stoppages()" color="gpsc">\n          P\n        </button>\n        <button ion-fab (click)="onIdle()" color="gpsc">\n          I\n        </button>\n      </ion-fab-list>\n    </ion-fab>\n\n    <ion-fab top right *ngIf="!showDropDown">\n      <button ion-fab color="light" mini (click)="onClickMainMenu()">\n        <ion-icon color="gpsc" name="map"></ion-icon>\n      </button>\n      <ion-fab-list side="bottom">\n        <button ion-fab (click)="onClickMap(\'SATELLITE\')" color="gpsc">\n          S\n        </button>\n        <button ion-fab (click)="onClickMap(\'TERRAIN\')" color="gpsc">\n          T\n        </button>\n        <button ion-fab (click)="onClickMap(\'NORMAL\')" color="gpsc">\n          N\n        </button>\n      </ion-fab-list>\n    </ion-fab>\n    <ion-fab top left *ngIf="!showDropDown">\n      <button ion-fab color="light" mini>\n        <ion-icon color="gpsc" name="arrow-round-forward"></ion-icon>\n      </button>\n      <ion-fab-list side="bottom">\n        <button ion-fab (click)="stoppages()" color="gpsc">\n          P\n        </button>\n        <button ion-fab (click)="onIdle()" color="gpsc">\n          I\n        </button>\n      </ion-fab-list>\n    </ion-fab>\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 68%" *ngIf="showZoom">\n      <button ion-fab mini (click)="zoomin()" color="gpsc">\n        <ion-icon name="add" color="black"></ion-icon>\n      </button>\n    </ion-fab>\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 80%" *ngIf="showZoom">\n      <button ion-fab mini (click)="zoomout()" color="gpsc">\n        <ion-icon name="remove" color="black"></ion-icon>\n      </button>\n    </ion-fab>\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 92%" *ngIf="showZoom">\n      <button ion-fab color="gpsc" mini (click)="reCenterMe()">\n        <ion-icon name="locate"></ion-icon>\n      </button>\n    </ion-fab>\n  </div>\n</ion-content>\n\n<div *ngIf="showActionSheet" class="divPlan">\n\n  <ion-bottom-drawer [(state)]="drawerState" [dockedHeight]="dockedHeight" [shouldBounce]="shouldBounce"\n    [distanceTop]="distanceTop" [transition]="transition" [minimumHeight]="minimumHeight" (click)="setDocHeight()">\n    <div class="drawer-content">\n      <ion-row style="margin-bottom:-10%;">\n        <ion-col col-12 text-right>\n          <ion-icon style="font-size: 1em; font-weight: bold;" name="close" (click)="closeDocker()"></ion-icon>\n        </ion-col>\n        <ion-col style="text-align:center;">\n          <p style="font-size: 20px;color:black;text-align: center;" *ngIf="!durations">\n            N/A\n          </p>\n          <p style="font-size: 20px;color:black;text-align: center;" *ngIf="durations">\n            {{ durations }}\n          </p>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-bottom: -6%;">\n        <ion-col col-50>\n          <p style="font-size: 13px;color:green;margin-left: 4%;" *ngIf="!arrTime">\n            <ion-icon name="time" width="55" height="55" style="margin-top: 9%;color: green;"></ion-icon>&nbsp;&nbsp;N/A\n          </p>\n          <p style="font-size: 13px;color:green;margin-left: 4%;" *ngIf="arrTime">\n            <ion-icon name="time" width="55" height="55" style="margin-top: 9%;color:green;"></ion-icon>\n            &nbsp;&nbsp;{{ arrTime }}\n          </p>\n        </ion-col>\n        <ion-col col-50>\n          <p style="font-size: 13px;color:#ac0031;margin-left: 4%;" *ngIf="!depTime">\n            <ion-icon name="time" width="55" height="55" style="margin-top: 9%;color:#ac0031;"></ion-icon>\n            &nbsp;&nbsp;N/A\n          </p>\n          <p style="font-size: 13px;margin-left: 4%;color:#ac0031;" *ngIf="depTime">\n            <ion-icon name="time" width="55" height="55" style="margin-top: 9%;color: #ac0031"></ion-icon>\n            &nbsp;&nbsp;{{ depTime }}\n          </p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <p style="font-size: 13px; color:cornflowerblue; margin-left: 4%;" *ngIf="!address">\n          <ion-icon name="pin" width="55" height="55" style="margin-top: 6%"></ion-icon>\n          &nbsp;&nbsp;N/A\n        </p>\n        <p style="font-size: 13px; color:cornflowerblue; margin-left: 4%;" *ngIf="address">\n          <ion-icon name="pin" width="55" height="55" style="margin-top: 9%"></ion-icon>&nbsp;&nbsp;{{ address }}\n        </p>\n      </ion-row>\n    </div>\n  </ion-bottom-drawer>\n</div>\n<ion-footer no-border>\n  <ion-navbar class="footerStyle">\n    <ion-row *ngIf="hideplayback"\n      style="padding-bottom: 8px; border-bottom: 1px solid lightgray; background-color: transparent;">\n      <ion-col col-2 style="padding: 0px 0px 0px 18px; margin-top: 6px;">\n        <ion-icon color="secondary" name="pin" style="font-size: 1.5em;"></ion-icon>\n      </ion-col>\n      <ion-col col-7 style="max-height: 38px !important;overflow: hidden;">\n        <p style="margin: 0px; font-size: 0.9em; color: #696666;">\n          {{addressLine}}\n        </p>\n      </ion-col>\n      <ion-col col-1 style="text-align: center;">\n        <ion-icon color="gpsc" name="battery-charging" style="font-size:1.5em;"></ion-icon>\n      </ion-col>\n      <ion-col col-2 style="text-align: center;padding: 6px 10px 0px 0px;">\n        <p style="margin: 0px; font-size: 1em; color: #696666;">\n          {{ (battery ? battery : 0) }}<span style="font-size: 0.8em; font-weight: normal;">&nbsp;V</span>\n        </p>\n      </ion-col>\n    </ion-row>\n    <ion-row *ngIf="hideplayback" style="background-color: transparent;border-bottom: 1px solid lightgray;">\n      <ion-col col-2 style="padding: 0px 0px 0px 10px;margin-top: 5px;" (click)="play2()">\n        <img src="assets/imgs/hplay.png" style="width: 30px;height: 30px; margin: auto;"\n          *ngIf="allData.flag2 == \'init\'" />\n        <img src="assets/imgs/hplay.png" style="width: 30px;height: 30px; margin: auto;"\n          *ngIf="allData.flag2 == \'start\'" />\n        <img src="assets/imgs/pause.png" style="width: 30px;height: 30px; margin: auto;"\n          *ngIf="allData.flag2 == \'stop\'" />\n      </ion-col>\n      <ion-col col-7>\n        <input type="range" min="0" max={{seekBarValue}} [(ngModel)]="sliderValue" class="slider" id="slider1"\n          style="width: 100%; margin-top: 13px;">\n      </ion-col>\n      <ion-col col-3 style="margin: auto;">\n        <ion-select no-padding [(ngModel)]="speedValue123" interface="popover" (ionChange)="changeSpeed(speedValue123)"\n          style="max-width: 80%;margin-left: 5px;">\n          <ion-option value="1">1px</ion-option>\n          <ion-option value="2">2px</ion-option>\n          <ion-option value="5">5px</ion-option>\n          <ion-option value="10">10px</ion-option>\n        </ion-select>\n      </ion-col>\n    </ion-row>\n    <ion-row style="padding:3px 0px 3px 0px !important;background-color: transparent;">\n      <ion-col col-2 style="padding: 0px 0px 0px 18px;margin-top: 3px;">\n        <ion-icon name="time" style="color:#33cd5f;font-size:1.5em;"></ion-icon>\n      </ion-col>\n      <ion-col col-3>\n        <p style="color:#696666;font-size: 1em; margin: 0px;">\n          {{ updatetimedate ? updatetimedate : \'00:00:00\' }}\n        </p>\n      </ion-col>\n      <ion-col col-1 style="padding: 0px 0px 0px 0px;margin-top: 3px;">\n        <ion-icon name="speedometer" style="font-size:1.5em;" color="gpsc"></ion-icon>\n      </ion-col>\n      <ion-col col-3>\n        <p style="color:#696666; font-size: 1em; margin: 0px;">\n          {{ speedMarker ? speedMarker : 0 }}\n          <span\n            style="font-size: 0.8em; font-weight: normal;">{{ (measurementUnit == \'MKS\') ? ("Km/hr" | translate) : ("Miles/hr" | translate) }}</span>\n        </p>\n      </ion-col>\n      <ion-col col-1 style="padding: 0px 0px 0px 0px;margin-top: 3px;text-align: center;">\n        <ion-icon style="font-size:1.2em;" name="custom-milestone"></ion-icon>\n      </ion-col>\n      <ion-col col-2>\n        <p style="color:#696666;font-size: 1em; margin: 0px;">\n          {{ cumu_distance ? (cumu_distance | number: "1.0-2") : 0 }}\n          <span\n            style="font-size: 0.8em; font-weight: normal;">{{ (measurementUnit == \'MKS\') ? ("Kms" | translate) : ("Miles" | translate) }}</span>\n        </p>\n      </ion-col>\n    </ion-row>\n\n    <ion-row style="border-top: 1px solid lightgray;background-color: transparent;">\n      <ion-col col-5 style="padding: 0px 0px 0px 18px;margin-top: 10px;">\n        <p style="margin: 0px; color: #696666;font-size: 1em;" *ngIf="device">{{device.Device_Name}}</p>\n        <p style="margin: 0px; color: #696666;font-size: 1em;" *ngIf="selectedVehicle">\n          {{selectedVehicle.Device_Name}}</p>\n      </ion-col>\n      <ion-col col-1 style="padding: 0px;margin-top: 10px;">\n        <img src="assets/imgs/totalDist.png" style="width: 21px; height: 21px;" />\n      </ion-col>\n      <ion-col col-3 style="margin-top: 6px;">\n        <p style="margin: 0px; color: gray;font-size: 1em;">{{ data2.Distance ? data2.Distance : 0 }}\n          <span\n            style="font-size: 0.8em; font-weight: normal;">{{ (measurementUnit == \'MKS\') ? ("Kms" | translate) : ("Miles" | translate)}}</span>\n        </p>\n      </ion-col>\n      <ion-col col-2 style="margin-top: 3px;" (click)="replayHistory()">\n        <ion-row>\n          <ion-col col-12 no-padding style="margin-left: 5px;">\n            <img src="assets/imgs/replay.png" style="height: 18px; width: 18px;">\n          </ion-col>\n          <ion-col col-12 no-padding style="margin-top: -2px;">\n            <p style="margin: 0px; font-size: 0.7em; color: #696666;">{{"Replay" | translate}}</p>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n      <ion-col col-1 style="margin-top: 3px; margin-left: -13px;">\n        <ion-row (click)="hidePlayRoute()" *ngIf="showRoute">\n          <ion-col col-12 no-padding style="text-align: center;">\n            <img src="assets/imgs/routebtn.png" style="height: 18px; width: 18px;" />\n          </ion-col>\n          <ion-col col-12 no-padding style="text-align: center;margin-top: -2px;">\n            <p style="margin: 0px; font-size: 0.7em; color: #696666;">{{"Hide" | translate}}</p>\n          </ion-col>\n        </ion-row>\n        <ion-row (click)="hidePlayRoute()" *ngIf="!showRoute">\n          <ion-col col-12 no-padding style="text-align: center;">\n            <img src="assets/imgs/routebtn_in.png" style="height: 18px; width: 18px;" />\n          </ion-col>\n          <ion-col col-12 no-padding style="text-align: center;margin-top: -2px;">\n            <p style="margin: 0px; font-size: 0.7em; color: #696666;">{{"Hide" | translate}}</p>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n\n  </ion-navbar>\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/history-device/history-device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_8__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
            __WEBPACK_IMPORTED_MODULE_9__angular_common__["DatePipe"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]])
    ], HistoryDevicePage);
    return HistoryDevicePage;
}());

//# sourceMappingURL=history-device.js.map

/***/ }),

/***/ 995:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryDevicePageModule", function() { return HistoryDevicePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history_device__ = __webpack_require__(1090);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ion_bottom_drawer__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






// import { ModalPage } from './modal';
var HistoryDevicePageModule = /** @class */ (function () {
    function HistoryDevicePageModule() {
    }
    HistoryDevicePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__history_device__["a" /* HistoryDevicePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__history_device__["a" /* HistoryDevicePage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ion_bottom_drawer__["b" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]],
        })
    ], HistoryDevicePageModule);
    return HistoryDevicePageModule;
}());

//# sourceMappingURL=history-device.module.js.map

/***/ })

});
//# sourceMappingURL=43.js.map