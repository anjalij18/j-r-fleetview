webpackJsonp([63],{

/***/ 1050:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactUsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_pullup__ = __webpack_require__(191);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { CallNumber } from '@ionic-native/call-number';


var ContactUsPage = /** @class */ (function () {
    function ContactUsPage(navCtrl, navParams, formBuilder, api, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.contact_data = {};
        this.tickets = [];
        this.showDatePanel = false;
        this.openNum = 0;
        this.closeNum = 0;
        this.inprogressNum = 0;
        this.tempTickets = [];
        this.device_id = [];
        this.device_name = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        //this.datetimeStart = moment({ hours: 0 }).format();
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_4_moment__().startOf('month').format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_4_moment__({ hours: 0 }).add(1, 'days').format();
        //this.datetimeEnd = moment().format();
        //new Date(a).toISOString();
        this.contactusForm = formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            mail: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email],
            mobno: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            note: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    ContactUsPage_1 = ContactUsPage;
    ContactUsPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter ContactUsPage');
    };
    ContactUsPage.prototype.ngOnInit = function () {
        this.viewTicket();
        this.getdevices();
    };
    ContactUsPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.api.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        //this.api.startLoading().present();
        this.api.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            //this.api.stopLoading();
            _this.devices = data;
            _this.portstemp = data.devices;
        }, function (err) {
            _this.api.stopLoading();
            console.log(err);
        });
    };
    ContactUsPage.prototype.call = function (num) {
        // this.callNumber.callNumber(num, true)
        //   .then(res => console.log('Launched dialer!', res))
        //   .catch(err => console.log('Error launching dialer', err));
    };
    ContactUsPage.prototype.footerExpanded = function () {
        console.log('Footer expanded!');
    };
    ContactUsPage.prototype.footerCollapsed = function () {
        console.log('Footer collapsed!');
    };
    ContactUsPage.prototype.contactUs = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.contactusForm.valid) {
            // this.contact_data = {
            //   "user": this.islogin._id,
            //   // "email": this.contactusForm.value.mail,
            //   "msg": this.contactusForm.value.note,
            //   // "phone": (this.contactusForm.value.mobno).toString(),
            //   // "dealerid": this.islogin.email,
            //   // "dealerName": this.islogin.fn + ' ' + this.islogin.ln
            //   // "dealerid": "gaurav.gupta@adnatesolutions.com",
            //   // "dealerName": "Gaurav Gupta"
            // }
            // var payData = {
            //   "user": this.islogin._id,
            //   // "email": this.contactusForm.value.mail,
            //   "msg": this.contactusForm.value.note,
            // }
            var payData = {
                "user": this.islogin._id,
                // "email": this.contactusForm.value.mail,
                "msg": this.contactusForm.value.note,
                "fileName": "oneqlikvanilla.json",
                //"fileName":"sanskarvts.json",
                "DeviceIMEI": this.device_id,
                "VehicleNo": this.device_name,
                "mobile": this.contactusForm.value.mobno,
                "email": this.contactusForm.value.mail
            };
            this.api.startLoading().present();
            // this.api.contactusApi(this.contact_data)
            this.api.createTicketApi(payData)
                .subscribe(function (data) {
                _this.api.stopLoading();
                console.log(data.message);
                if (data.message == 'saved response') {
                    var toast = _this.toastCtrl.create({
                        // message: 'Your request has been submitted successfully. We will get back to you soon.',
                        message: 'Your ticket has been submitted successfully. We will get back to you soon.',
                        position: 'bottom',
                        duration: 3000
                    });
                    // toast.onDidDismiss(() => {
                    //   console.log('Dismissed toast');
                    //   // this.contactusForm.reset();
                    //   this.navCtrl.setRoot(ContactUsPage);
                    // });
                    toast.present();
                    _this.navCtrl.setRoot(ContactUsPage_1);
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: 'Something went wrong. Please try after some time.',
                        position: 'bottom',
                        duration: 3000
                    });
                    toast.onDidDismiss(function () {
                        console.log('Dismissed toast');
                        _this.navCtrl.setRoot(ContactUsPage_1);
                    });
                    toast.present();
                }
            }, function (error) {
                _this.api.stopLoading();
                console.log(error);
            });
        }
    };
    ContactUsPage.prototype.getSummaarydevice = function (selectedVehicle) {
        this.device_id = [];
        this.device_name = [];
        if (selectedVehicle.length > 0) {
            if (selectedVehicle.length > 1) {
                for (var t = 0; t < selectedVehicle.length; t++) {
                    this.device_id.push(selectedVehicle[t].Device_ID);
                    this.device_name.push(selectedVehicle[t].Device_Name);
                }
            }
            else {
                this.device_id.push(selectedVehicle[0].Device_ID);
                this.device_name.push(selectedVehicle[0].Device_Name);
            }
        }
        else
            return;
        console.log("selectedVehicle=> ", this.device_id);
    };
    ContactUsPage.prototype.addTicket = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_5_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_5_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_5_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    };
    ContactUsPage.prototype.viewTicket = function () {
        var _this = this;
        var _bUrl = this.api.mainUrl + "customer_support/getCustomerQuery";
        var payload = {
            "draw": 1,
            "columns": [
                {
                    "data": "_id"
                },
                {
                    "data": "superAdmin"
                },
                {
                    "data": "dealer"
                },
                {
                    "data": "ticketId"
                },
                {
                    "data": "message"
                },
                {
                    "data": "assigned_to"
                },
                {
                    "data": "assigned_to.first_name"
                },
                {
                    "data": "assigned_to.last_name"
                },
                {
                    "data": "support_status"
                },
                {
                    "data": "posted_on"
                },
                {
                    "data": "posted_by"
                },
                {
                    "data": "posted_by.first_name"
                },
                {
                    "data": "posted_by.last_name"
                },
                {
                    "data": "posted_by.phone"
                },
                {
                    "data": "posted_by.email"
                },
                {
                    "data": "role"
                },
                {
                    "data": "user"
                }
            ],
            "order": [
                {
                    "column": 3,
                    "dir": "desc",
                }
            ],
            "start": 0,
            "length": 50,
            "search": {
                "value": "",
                "regex": false
            },
            "op": {},
            "select": [],
            "find": {
                "posted_by": this.islogin._id,
                "posted_on": {
                    "$gte": {
                        "_eval": "date",
                        "value": new Date(this.datetimeStart).toISOString()
                    },
                    "$lte": {
                        "_eval": "date",
                        "value": new Date(this.datetimeEnd).toISOString()
                    }
                }
            }
        };
        this.tickets = [];
        this.openNum = 0;
        this.closeNum = 0;
        this.inprogressNum = 0;
        this.api.startLoading().present();
        this.api.urlpasseswithdata(_bUrl, payload)
            .subscribe(function (data) {
            _this.api.stopLoading();
            console.log("tickets: ", data);
            _this.tickets = data.data;
            _this.tempTickets = data.data;
            for (var i = 0; i < data.data.length; i++) {
                if (data.data[i].support_status == 'OPEN') {
                    _this.openNum += 1;
                }
                else if (data.data[i].support_status == 'CLOSE') {
                    _this.closeNum += 1;
                }
                else if (data.data[i].support_status == 'IN PROGRESS') {
                    _this.inprogressNum += 1;
                }
            }
        }, function (err) {
            _this.api.stopLoading();
            console.log("getting err while getting data: ", err);
        });
    };
    ContactUsPage.prototype.loadContent = function (key) {
        if (key === 'PROGRESS') {
            this.tickets = [];
            for (var i = 0; i < this.tempTickets.length; i++) {
                if (this.tempTickets[i].support_status === 'IN PROGRESS') {
                    this.tickets.push(this.tempTickets[i]);
                }
            }
        }
        else if (key === 'OPEN') {
            this.tickets = [];
            for (var r = 0; r < this.tempTickets.length; r++) {
                if (this.tempTickets[r].support_status === 'OPEN') {
                    this.tickets.push(this.tempTickets[r]);
                }
            }
        }
        else if (key === 'CLOSE') {
            this.tickets = [];
            for (var v = 0; v < this.tempTickets.length; v++) {
                if (this.tempTickets[v].support_status === 'CLOSE') {
                    this.tickets.push(this.tempTickets[v]);
                }
            }
        }
    };
    ContactUsPage.prototype.onClickChat = function () {
        this.navCtrl.push('ChatPage', {
            params: this.islogin,
            isCustomer: !this.islogin.isSuperAdmin
        });
    };
    ContactUsPage = ContactUsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-contact-us',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/contact-us/contact-us.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "Contact Us" | translate }}</ion-title>\n  </ion-navbar>\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{ "From Date" | translate }}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{ "To Date" | translate }}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right">\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="viewTicket()">\n        </ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n\n<ion-content padding-bottom>\n  <ion-row style="margin-top: 33%">\n    <ion-col col-4 tappable (click)="loadContent(\'OPEN\')">\n      <ion-row>\n        <ion-col>\n          <div\n            style="width:75px; height: 75px; border-radius: 48%; background: rgb(238, 100, 69); text-align: center;margin: auto; padding: 25px; font-size: 1.5em; color: white;">\n            <b>{{ openNum }}</b>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col style="text-align: center;">\n          {{ "OPEN" | translate }}\n        </ion-col>\n      </ion-row>\n    </ion-col>\n\n    <ion-col col-4 tappable (click)="loadContent(\'PROGRESS\')">\n      <ion-row>\n        <ion-col>\n          <div\n            style="width:75px; height: 75px; border-radius: 48%; background: rgb(233, 182, 16); text-align: center;margin: auto; padding: 25px; font-size: 1.5em; color: white;">\n            <b>{{ inprogressNum }}</b>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col style="text-align: center;">\n          {{ "IN PROGRESS" | translate }}\n        </ion-col>\n      </ion-row>\n    </ion-col>\n\n    <ion-col col-4 tappable (click)="loadContent(\'CLOSE\')">\n      <ion-row>\n        <ion-col>\n          <div\n            style="width:75px; height: 75px; border-radius: 48%; background: rgb(132, 231, 45); text-align: center;margin: auto; padding: 25px; font-size: 1.5em; color: white;">\n            <b>{{ closeNum }}</b>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col style="text-align: center;">\n          {{ "CLOSE" | translate }}\n        </ion-col>\n      </ion-row>\n    </ion-col>\n\n  </ion-row>\n  <div *ngIf="tickets.length > 0">\n    <ion-card *ngFor="let t of tickets">\n      <ion-card-content style="background: transparent;">\n        <p style="margin: 0px; padding: 0px; color: black">\n          <b>{{ \'Ticket ID\' | translate}}: </b>{{ t.ticketId ?t.ticketId : \'NA\'  }}\n        </p>\n        <ion-row>\n          <ion-col col-12 style="padding-left: 0px">\n            <ion-icon name="person"></ion-icon>&nbsp;\n            {{ t.posted_by.first_name }}\n            {{ t.posted_by.last_name }}&nbsp;&nbsp;<ion-icon name="call" color="gpsc"></ion-icon>\n          </ion-col>\n\n        </ion-row>\n        <ion-row>\n\n          <ion-col col-6 style="padding-left: 0px">\n            <ion-icon name="calendar"></ion-icon>&nbsp;&nbsp;{{\n              t.posted_on | date: "short"\n            }}\n          </ion-col>\n          <ion-col col-6 style="text-align: right">\n            <ion-badge [ngClass]="{\n                myClass: t.support_status == \'IN PROGRESS\',\n                myClass2: t.support_status == \'OPEN\',\n                myClass3: t.support_status == \'CLOSE\'\n              }">{{ t.support_status }}</ion-badge>\n          </ion-col>\n        </ion-row>\n\n        <p style="margin: 0px; padding: 0px; color: black">\n          <b>{{ \'Issue\' | translate}}: </b>{{ t.message }}\n        </p>\n      </ion-card-content>\n    </ion-card>\n  </div>\n\n</ion-content>\n\n<ion-fab bottom right *ngIf="!islogin.isSuperAdmin">\n  <button ion-fab (click)="addTicket()" color="gpsc">\n    <ion-icon name="add" *ngIf="footerState == 0"></ion-icon>\n    <ion-icon name="close" *ngIf="footerState == 1"></ion-icon>\n  </button>\n</ion-fab>\n<ion-fab bottom right style="bottom: 82px;" *ngIf="!islogin.isSuperAdmin">\n  <button ion-fab (click)="onClickChat()" color="light">\n    <img src="assets/imgs/telemarketer.png" />\n    <!-- <ion-icon name="add" *ngIf="footerState == 0"></ion-icon> -->\n    <!-- <ion-icon name="close" *ngIf="footerState == 1"></ion-icon> -->\n  </button>\n</ion-fab>\n<ion-pullup #pullup (onExpand)="footerExpanded()" (onCollapse)="footerCollapsed()" [(state)]="footerState">\n  <ion-content>\n\n    <ion-item style="height: 45px; background-color: #fafafa;">\n      <ion-label>{{ "Select Vehicle" | translate }}</ion-label>\n      <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n        itemTextField="Device_Name" [isMultiple]="true" [canSearch]="true"\n        (onChange)="getSummaarydevice(selectedVehicle)">\n      </select-searchable>\n    </ion-item>\n\n    <form class="form" [formGroup]="contactusForm" style="margin-top: -15px;">\n      <p class="para" type="Name:">\n        <input formControlName="name" type="text" placeholder="Write your name here.." />\n      </p>\n      <span class="span"\n        *ngIf="!contactusForm.controls.name.valid && submitAttempt">{{ \'Name is required and should be in valid format!\' | translate}}</span>\n      <p class="para" type="Email:">\n        <input formControlName="mail" type="text" placeholder="Let us know how to contact you back.." />\n      </p>\n      <span class="span"\n        *ngIf="!contactusForm.controls.mail.valid && submitAttempt">{{ \'Email id is required and should be in valid format!\' | translate }}</span>\n      <p class="para" type="Mobile Num.:">\n        <input formControlName="mobno" type="number" maxlength="10" minlength="10"\n          placeholder="{{ \'Let us know how to contact you back via mobile number..\' | translate}}" />\n      </p>\n      <p class="para" type="Message:">\n        <textarea rows="2" cols="50" formControlName="note"\n          placeholder="{{ \'What would you like to tell us..\' | translate }}"></textarea>\n      </p>\n      <span class="span"\n        *ngIf="!contactusForm.controls.note.valid && submitAttempt">{{ \'Please write your message!\' | translate}}</span>\n      <ion-row>\n        <ion-col col-12><button (tap)="contactUs()" class="buttonclass">\n            {{ \'Raise Ticket\' | translate}}\n          </button></ion-col>\n      </ion-row>\n    </form>\n  </ion-content>\n</ion-pullup>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/contact-us/contact-us.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], ContactUsPage);
    return ContactUsPage;
    var ContactUsPage_1;
}());

//# sourceMappingURL=contact-us.js.map

/***/ }),

/***/ 961:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsPageModule", function() { return ContactUsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_us__ = __webpack_require__(1050);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_pullup__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var ContactUsPageModule = /** @class */ (function () {
    function ContactUsPageModule() {
    }
    ContactUsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__contact_us__["a" /* ContactUsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__contact_us__["a" /* ContactUsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_6_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4_ionic_pullup__["b" /* IonPullupModule */],
                __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], ContactUsPageModule);
    return ContactUsPageModule;
}());

//# sourceMappingURL=contact-us.module.js.map

/***/ })

});
//# sourceMappingURL=63.js.map