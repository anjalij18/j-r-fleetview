webpackJsonp([60],{

/***/ 1054:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddCustomerModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(546);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__ = __webpack_require__(548);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__ = __webpack_require__(547);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var AddCustomerModal = /** @class */ (function () {
    function AddCustomerModal(navCtrl, navParams, formBuilder, apicallCustomer, alerCtrl, viewCtrl, toastCtrl, actionSheetCtrl, file, filePath, camera, transferObj, transfer, loadingCtrl, platform, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.apicallCustomer = apicallCustomer;
        this.alerCtrl = alerCtrl;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.file = file;
        this.filePath = filePath;
        this.camera = camera;
        this.transferObj = transferObj;
        this.transfer = transfer;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.translate = translate;
        this.customerdata = {};
        // isDealer: any;
        this.DlType = [{
                value: 'dl',
                viewValue: "Driving License"
            }, {
                value: 'Adhar',
                viewValue: "Adhar Card"
            }, {
                value: 'PAN',
                viewValue: "PAN Card"
            }, {
                value: 'voterCard',
                viewValue: "Voter ID Card"
            }
        ];
        this.lastImage = null;
        this.countryCodeArray = [];
        this.getCountryCode();
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.isSuperAdminStatus = this.islogin.isSuperAdmin;
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);
        this.currentYear = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        // this.addcustomerform = formBuilder.group({
        //     userId: ['', Validators.required],
        //     Firstname: ['', Validators.required],
        //     LastName: ['', Validators.required],
        //     emailid: [this.islogin.account, [Validators.required, Validators.email]],
        //     contact_num: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(10)]],
        //     password: ['', Validators.required],
        //     confpassword: [''],
        //     address: ['', Validators.required],
        //     ExipreDate: [this.currentYear, Validators.required],
        //     dealer_firstname: [''],
        //     DlNo: [""],
        //     Name: [""],
        //     std_code: ["", Validators.required]
        // })
        this.addcustomerform = formBuilder.group({
            userId: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            Firstname: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            LastName: [''],
            emailid: [''],
            contact_num: [''],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            confpassword: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            address: [''],
            ExipreDate: [this.currentYear],
            dealer_firstname: [''],
            DlNo: [""],
            Name: [""],
            std_code: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    Object.defineProperty(AddCustomerModal.prototype, "frm", {
        get: function () { return this.addcustomerform.controls; },
        enumerable: true,
        configurable: true
    });
    AddCustomerModal.prototype.getCountryCode = function () {
        var _this = this;
        this.apicallCustomer.getCountryCode().subscribe(function (data) {
            _this.countryCodeArray = data.countries;
        });
    };
    AddCustomerModal.prototype.ngOnInit = function () {
        if (this.isSuperAdminStatus) {
            this.getAllDealers();
        }
    };
    AddCustomerModal.prototype.DocumentOnChnage = function (type) {
        this.Documentdata = type;
        if (this.Documentdata.value == 'dl') {
            this.Documentdatashow = this.Documentdata.value;
        }
        else if (this.Documentdata.value == 'Adhar') {
            this.DocumentdataAdhar = this.Documentdata.value;
        }
    };
    // Create a new name for the image
    AddCustomerModal.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    AddCustomerModal.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    AddCustomerModal.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    // Always get the accurate path to your apps folder
    AddCustomerModal.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    AddCustomerModal.prototype.uploadImage = function () {
        var _this = this;
        // Destination URL
        var url = this.apicallCustomer.mainUrl + "users/uploadImage";
        var targetPath = this.pathForImage(this.lastImage);
        console.log("TargetPath=>", targetPath);
        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "photo",
            fileName: filename,
            chunkedMode: false,
            mimeType: "image/jpeg",
            params: { 'fileName': filename }
        };
        // multipart/form-data"
        this.transferObj = this.transfer.create();
        this.Imgloading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.Imgloading.present();
        this.transferObj.upload(targetPath, url, options).then(function (data) {
            _this.Imgloading.dismissAll();
        }, function (err) {
            console.log("uploadError=>", err);
            _this.lastImage = null;
            _this.Imgloading.dismissAll();
            _this.presentToast('Error while uploading file, Please try again !!!');
        });
    };
    AddCustomerModal.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            _this.presentToast('Error while selecting image.');
        });
    };
    AddCustomerModal.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    AddCustomerModal.prototype.dealerOnChnage = function (dealer) {
        console.log(dealer);
        this.dealerdata = dealer;
        console.log("dealer id=> " + this.dealerdata.dealer_id);
    };
    AddCustomerModal.prototype.onSelect = function (con) {
        console.log("country selected: ", con);
    };
    AddCustomerModal.prototype.addcustomer = function () {
        var _this = this;
        this.submitAttempt = true;
        // console.log(devicedetails);
        if (this.addcustomerform.valid) {
            debugger;
            if (this.addcustomerform.value.password === this.addcustomerform.value.confpassword) {
                if (this.islogin.isSuperAdmin == true) {
                    this.customerdata = {
                        "first_name": (this.addcustomerform.value.Firstname ? this.addcustomerform.value.Firstname : null),
                        "last_name": (this.addcustomerform.value.LastName ? this.addcustomerform.value.LastName : null),
                        "email": (this.addcustomerform.value.emailid ? this.addcustomerform.value.emailid : null),
                        "phone": (this.addcustomerform.value.contact_num ? this.addcustomerform.value.contact_num : null),
                        "password": (this.addcustomerform.value.password ? this.addcustomerform.value.password : null),
                        "isDealer": false,
                        "custumer": true,
                        "status": true,
                        "user_id": this.addcustomerform.value.userId,
                        "address": (this.addcustomerform.value.address ? this.addcustomerform.value.address : null),
                        "supAdmin": this.islogin._id,
                        "std_code": (this.addcustomerform.value.std_code ? this.addcustomerform.value.std_code : null),
                        "expdate": (this.addcustomerform.value.ExipreDate ? this.addcustomerform.value.ExipreDate : this.currentYear)
                    };
                }
                else {
                    if (this.islogin.isDealer == true) {
                        this.customerdata = {
                            "first_name": (this.addcustomerform.value.Firstname ? this.addcustomerform.value.Firstname : null),
                            "last_name": (this.addcustomerform.value.LastName ? this.addcustomerform.value.LastName : null),
                            "email": (this.addcustomerform.value.emailid ? this.addcustomerform.value.emailid : null),
                            "phone": (this.addcustomerform.value.contact_num ? this.addcustomerform.value.contact_num : null),
                            "password": (this.addcustomerform.value.password ? this.addcustomerform.value.password : null),
                            "isDealer": this.islogin.isDealer,
                            "custumer": true,
                            "status": true,
                            "user_id": this.addcustomerform.value.userId,
                            "address": (this.addcustomerform.value.address ? this.addcustomerform.value.address : null),
                            "supAdmin": this.islogin.supAdmin,
                            "std_code": (this.addcustomerform.value.std_code ? this.addcustomerform.value.std_code : null),
                            "expdate": (this.addcustomerform.value.ExipreDate ? this.addcustomerform.value.ExipreDate : this.currentYear)
                            // "Dealer": this.islogin.Dealer_ID['_id']
                        };
                    }
                }
                if (this.dealerdata != undefined) {
                    this.customerdata.Dealer = this.dealerdata.dealer_id;
                }
                else {
                    this.customerdata.Dealer = this.islogin._id;
                }
                this.apicallCustomer.startLoading().present();
                this.apicallCustomer.signupApi(this.customerdata)
                    .subscribe(function (data) {
                    _this.apicallCustomer.stopLoading();
                    // if (data.message === 'Email ID or Mobile Number already exists') {
                    //     this.toast(data.message);
                    // }
                    if (data.message != undefined) {
                        _this.toast(data.message);
                    }
                    // this.Customeradd = data;
                }, function (err) {
                    _this.apicallCustomer.stopLoading();
                    var body = err._body;
                    var msg = JSON.parse(body);
                    var namepass = [];
                    namepass = msg.split(":");
                    var name = namepass[1];
                    var alert = _this.alerCtrl.create({
                        message: name,
                        buttons: [_this.translate.instant('Okay')]
                    });
                    alert.present();
                });
            }
            else {
                var toast = this.toastCtrl.create({
                    message: 'Password and confirm password mismatched... Please try again',
                    duration: 2000,
                    position: 'middle'
                });
                toast.present();
                return;
            }
        }
    };
    AddCustomerModal.prototype.toast = function (msg) {
        var _this = this;
        var toast;
        if (msg === 'Registered') {
            toast = this.toastCtrl.create({
                // message: msg,
                message: this.translate.instant('Customer added successfully'),
                position: 'middle',
                duration: 1500
            });
            toast.onDidDismiss(function () {
                _this.viewCtrl.dismiss();
            });
            toast.present();
        }
        else {
            toast = this.toastCtrl.create({
                message: msg,
                // message: this.translate.instant('dealeradded', { value: this.translate.instant('cust') }),
                position: 'middle',
                duration: 1500
            });
            toast.onDidDismiss(function () {
                _this.viewCtrl.dismiss();
            });
            toast.present();
        }
    };
    AddCustomerModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddCustomerModal.prototype.getAllDealers = function () {
        var _this = this;
        var baseURLp = this.apicallCustomer.mainUrl + 'users/getAllDealerVehicles?supAdmin=' + this.islogin._id;
        var toast = this.toastCtrl.create({
            message: 'Loading dealers..',
            position: 'bottom',
            duration: 1500
        });
        toast.present();
        this.apicallCustomer.getAllDealerCall(baseURLp)
            .subscribe(function (data) {
            _this.selectDealer = data;
            // toast.dismiss();
        }, function (error) {
            console.log(error);
        });
    };
    AddCustomerModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-customer-model',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/customers/modals/add-customer-modal/add-customer-modal.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>{{\'Add Customer\' | translate}}</ion-title>\n        <ion-buttons end>\n            <button ion-button icon-only (click)="dismiss()">\n                <ion-icon name="close-circle"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n\n</ion-header>\n<ion-content>\n    <form [formGroup]="addcustomerform">\n\n        <!-- <ion-item>\n            <ion-label stacked>{{\'Registration Number*\' | translate}}</ion-label>\n            <ion-input formControlName="device_name" type="text"></ion-input>\n        </ion-item> -->\n        <ion-item>\n            <ion-label stacked>{{\'User ID\' | translate}}</ion-label>\n            <ion-input formControlName="userId" type="text"></ion-input>\n        </ion-item>\n\n        <ion-item class="logitem1"\n            *ngIf="!addcustomerform.controls.userId.valid && (addcustomerform.controls.userId.dirty || submitAttempt)">\n            <p>{{\'user id required!\' | translate}}</p>\n        </ion-item>\n\n        <ion-item>\n            <ion-label stacked>{{\'First Name\' | translate}}</ion-label>\n            <ion-input formControlName="Firstname" type="text"></ion-input>\n        </ion-item>\n        <ion-item class="logitem1"\n            *ngIf="!addcustomerform.controls.Firstname.valid && (addcustomerform.controls.Firstname.dirty || submitAttempt)">\n            <p>{{\'first name required!\' | translate}}</p>\n        </ion-item>\n\n        <ion-item>\n            <ion-label stacked>{{\'Last Name\' | translate}}</ion-label>\n            <ion-input formControlName="LastName" type="text"></ion-input>\n        </ion-item>\n        <!-- <ion-item class="logitem1"\n            *ngIf="!addcustomerform.controls.LastName.valid && (addcustomerform.controls.LastName.dirty || submitAttempt)">\n            <p>{{\'last name required!\' | translate}}</p>\n        </ion-item> -->\n\n        <ion-item>\n            <ion-label stacked>{{\'Email ID\'|translate}}</ion-label>\n            <ion-input formControlName="emailid" type="email"></ion-input>\n        </ion-item>\n\n        <!-- <div *ngIf="submitAttempt && frm.emailid.errors" class="logitem1">\n            <ion-item *ngIf="frm.emailid.errors.required">\n                <p class="logitem1">\n                    <ion-icon name="information-circle"></ion-icon>&nbsp;&nbsp;Email is required!\n                </p>\n            </ion-item>\n            <ion-item *ngIf="frm.emailid.errors.email">\n                <p class="logitem1">\n                    <ion-icon name="information-circle"></ion-icon>&nbsp;&nbsp;Email must be a valid email address!\n                </p>\n            </ion-item>\n        </div> -->\n\n        <ion-item>\n            <ion-label stacked>{{\'Select country*\' | translate}}</ion-label>\n            <ion-select style="float: right;max-width: 100%;" formControlName="std_code">\n                <ion-option *ngFor="let con of countryCodeArray" [value]="con.code" (ionSelect)="onSelect(con)">\n                    {{con.code}} {{con.name}}</ion-option>\n            </ion-select>\n        </ion-item>\n        <ion-item class="logitem1"\n            *ngIf="!addcustomerform.controls.std_code.valid && (addcustomerform.controls.std_code.dirty || submitAttempt)">\n            <p>{{\'country code is required!\' | translate}}</p>\n        </ion-item>\n\n        <ion-item>\n            <ion-label stacked>{{\'Mobile Number\' | translate}}</ion-label>\n            <ion-input formControlName="contact_num" type="tel"></ion-input>\n        </ion-item>\n        <!-- <div *ngIf="submitAttempt && frm.contact_num.errors" class="logitem1">\n            <ion-item *ngIf="frm.contact_num.errors.required">\n                <p class="logitem1">\n                    <ion-icon name="information-circle"></ion-icon>&nbsp;&nbsp;Mobile number is required!\n                </p>\n            </ion-item>\n            <ion-item *ngIf="frm.contact_num.errors.minlength">\n                <p class="logitem1">\n                    <ion-icon name="information-circle"></ion-icon>&nbsp;&nbsp;Mobile number must be 10 digits!\n                </p>\n            </ion-item>\n        </div> -->\n\n        <ion-item>\n            <ion-label stacked>{{\'Password\' | translate}}</ion-label>\n            <ion-input formControlName="password" type="text"></ion-input>\n        </ion-item>\n        <ion-item class="logitem1"\n            *ngIf="!addcustomerform.controls.password.valid && (addcustomerform.controls.password.dirty || submitAttempt)">\n            <p>{{\'Password required!\' | translate}}</p>\n        </ion-item>\n\n        <ion-item>\n            <ion-label stacked>{{\'Confirm Password\' | translate}}</ion-label>\n            <ion-input formControlName="confpassword" type="text"></ion-input>\n        </ion-item>\n        <ion-item class="logitem1"\n            *ngIf="!addcustomerform.controls.confpassword.valid && (addcustomerform.controls.confpassword.dirty || submitAttempt)">\n            <p>{{\'confirm password required!\' | translate}}</p>\n        </ion-item>\n\n        <!-- <ion-item>\n            <ion-label stacked style="min-width: 50% !important;">{{\'Address\' | translate}}</ion-label>\n            <ion-input formControlName="address" type="text"></ion-input>\n        </ion-item> -->\n        <!-- <ion-item class="logitem1"\n            *ngIf="!addcustomerform.controls.address.valid && (addcustomerform.controls.address.dirty || submitAttempt)">\n            <p>{{\'Address required!\' | translate}}</p>\n        </ion-item> -->\n\n        <!-- <ion-item>\n            <ion-label stacked style="min-width: 50% !important;">{{\'Expire On\' | translate}}</ion-label>\n            <ion-input type="date" formControlName="ExipreDate" style="margin-left: -2px;" min="{{minDate}}">\n            </ion-input>\n        </ion-item> -->\n        <!-- <ion-item class="logitem1"\n            *ngIf="!addcustomerform.controls.ExipreDate.valid && (addcustomerform.controls.ExipreDate.dirty || submitAttempt)">\n            <p>{{\'Date of expiry required!\' | translate}}</p>\n        </ion-item> -->\n\n        <ion-item *ngIf="isSuperAdminStatus">\n            <ion-label stacked>{{\'Dealers\' | translate}}</ion-label>\n            <ion-select formControlName="dealer_firstname" style="min-width:49%;">\n                <ion-option *ngFor="let dealer of selectDealer" [value]="dealer.dealer_firstname"\n                    (ionSelect)="dealerOnChnage(dealer)">{{dealer.dealer_firstname | titlecase}}\n                    {{dealer.dealer_lastname | titlecase}}</ion-option>\n            </ion-select>\n        </ion-item>\n\n    </form>\n</ion-content>\n<ion-footer class="footSty">\n    <ion-toolbar>\n        <ion-row no-padding>\n            <ion-col width-50 style="text-align: center;">\n                <button ion-button clear color="light" (click)="addcustomer()">{{\'ADD CUSTOMER\' | translate}}</button>\n            </ion-col>\n        </ion-row>\n    </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/customers/modals/add-customer-modal/add-customer-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__["b" /* TransferObject */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__["a" /* Transfer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__["c" /* TranslateService */]])
    ], AddCustomerModal);
    return AddCustomerModal;
}());

//# sourceMappingURL=add-customer-modal.js.map

/***/ }),

/***/ 964:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddCustomerModalModule", function() { return AddCustomerModalModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_customer_modal__ = __webpack_require__(1054);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_path__ = __webpack_require__(547);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__ = __webpack_require__(546);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__ = __webpack_require__(548);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AddCustomerModalModule = /** @class */ (function () {
    function AddCustomerModalModule() {
    }
    AddCustomerModalModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_customer_modal__["a" /* AddCustomerModal */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__add_customer_modal__["a" /* AddCustomerModal */]),
                __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__["a" /* Transfer */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__["b" /* TransferObject */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__["b" /* FileTransferObject */],
            ]
        })
    ], AddCustomerModalModule);
    return AddCustomerModalModule;
}());

//# sourceMappingURL=add-customer-modal.module.js.map

/***/ })

});
//# sourceMappingURL=60.js.map