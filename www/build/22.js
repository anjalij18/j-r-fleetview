webpackJsonp([22],{

/***/ 1051:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_socket_io_client__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_socket_io_client__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import * as moment from 'moment';

// import { NavParams, IonContent, Events } from '@ionic/angular';
var ChatPage = /** @class */ (function () {
    // user_input: string;
    // start_typing: any;
    function ChatPage(navCtrl, navParams, apiCall, plt) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.plt = plt;
        this.editorMsg = '';
        this.showEmojiPicker = false;
        this.msgList = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.callBaseURL();
        // debugger
        if (navParams.get("isCustomer")) {
            if (this.navParams.get('params')) {
                this.paramData = this.navParams.get('params').Dealer_ID;
                this.userName = this.paramData.first_name;
                console.log("param data: ", this.paramData);
                this.toUser = {
                    id: this.paramData._id,
                    name: this.paramData.first_name
                };
                // Get mock user information
                this.user = {
                    id: this.islogin._id,
                    name: this.islogin.fn
                };
            }
        }
        else {
            if (this.navParams.get('params')) {
                this.paramData = this.navParams.get('params');
                this.userName = this.paramData.first_name;
                console.log("param data: ", this.paramData);
                this.toUser = {
                    id: this.paramData._id,
                    name: this.paramData.first_name
                };
                // Get mock user information
                this.user = {
                    id: this.islogin._id,
                    name: this.islogin.fn
                };
            }
        }
    }
    ChatPage.prototype.ionViewWillLeave = function () {
    };
    ChatPage.prototype.ngAfterViewInit = function () { };
    ChatPage.prototype.ionViewDidEnter = function () {
        //get message list
        this.getMsg();
        if (this.plt.is('ios')) {
            // this.keyboard.disableScroll(true);
        }
        window.addEventListener('native.keyboardshow', keyboardShowHandler);
        window.addEventListener('native.keyboardhide', keyboardHideHandler);
        window.addEventListener('touchstart', tapCoordinates);
        var y;
        var h;
        var offsetY;
        function tapCoordinates(e) {
            y = e.touches[0].clientY;
            h = window.innerHeight;
            offsetY = (h - y);
            console.log("offset = " + offsetY);
        }
        function keyboardShowHandler(e) {
            var kH = e.keyboardHeight;
            console.log(e.keyboardHeight);
            var bodyMove = document.querySelector("ion-app"), bodyMoveStyle = bodyMove.style;
            console.log("calculating " + kH + "-" + offsetY + "=" + (kH - offsetY));
            if (offsetY < kH + 40) {
                bodyMoveStyle.bottom = (kH - offsetY + 40) + "px";
                bodyMoveStyle.top = "initial";
            }
        }
        function keyboardHideHandler() {
            console.log('gone');
            var removeStyles = document.querySelector("ion-app");
            removeStyles.removeAttribute("style");
        }
    };
    ChatPage.prototype.onFocus = function (x) {
        x.style.background = "yellow";
        this.showEmojiPicker = false;
        this.content.resize();
        this.scrollToBottom();
    };
    ChatPage.prototype.switchEmojiPicker = function () {
        this.showEmojiPicker = !this.showEmojiPicker;
        if (!this.showEmojiPicker) {
            this.focus();
        }
        else {
            this.setTextareaScroll();
        }
        this.content.resize();
        this.scrollToBottom();
    };
    /**
     * @name getMsg
     * @returns {Promise<ChatMessage[]>}
     */
    ChatPage.prototype.getMsg = function () {
        // Get mock message list
        // return this.chatService
        //   .getMsgList()
        //   .subscribe(res => {
        var _this = this;
        //     this.msgList = res;
        //     this.scrollToBottom();
        //   });
        this.apiCall.startLoading().present();
        var url = this.apiCall.mainUrl + "broadcastNotification/getchatmsg?from=" + this.islogin._id + "&to=" + this.paramData._id;
        this.apiCall.getdevicesForAllVehiclesApi(url)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            debugger;
            if (respData) {
                var res = respData;
                for (var i = 0; i < res.length; i++) {
                    if (res[i].sender === _this.toUser.id) {
                        _this.msgList.push({
                            // userId: this.User,
                            // userName: this.User,
                            // time: res[i].timestamp,
                            // message: res[i].message,
                            // id: this.msgList.length + 1
                            "messageId": _this.msgList.length + 1,
                            "userId": _this.toUser.id,
                            "userName": _this.toUser.name,
                            // "userImgUrl": "./assets/user.jpg",
                            "toUserId": _this.user.id,
                            "toUserName": _this.user.name,
                            // "userAvatar": "./assets/to-user.jpg",
                            "time": res[i].timestamp,
                            "message": res[i].message,
                            "status": "success"
                        });
                    }
                    else {
                        if (res[i].sender === _this.user.id) {
                            _this.msgList.push({
                                // userId: this.toUser,
                                // userName: this.toUser,
                                // time: res[i].timestamp,
                                // message: res[i].message,
                                // id: this.msgList.length + 1
                                "messageId": _this.msgList.length + 1,
                                "userId": _this.user.id,
                                "userName": _this.user.name,
                                // "userImgUrl": "./assets/user.jpg",
                                "toUserId": _this.toUser.id,
                                "toUserName": _this.toUser.name,
                                // "userAvatar": "./assets/to-user.jpg",
                                "time": res[i].timestamp,
                                "message": res[i].message,
                                "status": "success"
                            });
                        }
                    }
                }
                setTimeout(function () {
                    _this.scrollToBottom();
                    // this.content.scrollToBottom(100);
                }, 50);
                // this.scrollDown();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("chat err: ", err);
        });
    };
    /**
     * @name sendMsg
     */
    ChatPage.prototype.sendMsg = function () {
        if (!this.editorMsg.trim())
            return;
        var that = this;
        that._io.emit('send', this.user.id, this.toUser.id, this.editorMsg); // three parameters, from(who is sending msg), to(to whom ur sending msg), msg(message string)
        // Mock message
        var id = Date.now().toString();
        var newMsg = {
            messageId: this.msgList.length + 1,
            userId: this.user.id,
            userName: this.user.name,
            // userAvatar: this.user.avatar,
            toUserId: this.toUser.id,
            time: new Date().toISOString(),
            message: this.editorMsg,
            status: 'success'
        };
        this.pushNewMsg(newMsg);
        this.editorMsg = '';
        if (!this.showEmojiPicker) {
            this.focus();
        }
    };
    /**
     * @name pushNewMsg
     * @param msg
     */
    ChatPage.prototype.pushNewMsg = function (msg) {
        var userId = this.user.id, toUserId = this.toUser.id;
        // Verify user relationships
        if (msg.userId === userId && msg.toUserId === toUserId) {
            this.msgList.push(msg);
        }
        else if (msg.toUserId === userId && msg.userId === toUserId) {
            this.msgList.push(msg);
        }
        this.scrollToBottom();
    };
    ChatPage.prototype.getMsgIndexById = function (id) {
        return this.msgList.findIndex(function (e) { return e.messageId === id; });
    };
    ChatPage.prototype.scrollToBottom = function () {
        var that = this;
        setTimeout(function () {
            if (that.content.scrollToBottom) {
                that.content.scrollToBottom();
            }
        }, 400);
    };
    ChatPage.prototype.focus = function () {
        if (this.messageInput && this.messageInput.nativeElement) {
            this.messageInput.nativeElement.focus();
        }
    };
    ChatPage.prototype.setTextareaScroll = function () {
        var textarea = this.messageInput.nativeElement;
        textarea.scrollTop = textarea.scrollHeight;
    };
    ChatPage.prototype.ngOnInit = function () {
        this.openChatSocket();
        // this.getChatHistory();
        // this.innerWidth = window.innerWidth;
        // console.log("window test: ", this.innerWidth)
    };
    ChatPage.prototype.callBaseURL = function () {
        var _this = this;
        // debugger
        if (localStorage.getItem("ENTERED_BASE_URL") === null) {
            var url = "https://www.oneqlik.in/pullData/getUrlnew";
            this.apiCall.getSOSReportAPI(url)
                .subscribe(function (data) {
                console.log("base url: ", data);
                if (data.url) {
                    localStorage.setItem("BASE_URL", JSON.stringify(data.url));
                }
                if (data.socket) {
                    localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
                }
                _this.getSocketUrl();
            });
        }
    };
    ChatPage.prototype.getSocketUrl = function () {
        if (localStorage.getItem('SOCKET_URL') !== null) {
            this.socketurl = JSON.parse(localStorage.getItem('SOCKET_URL'));
            this._io = __WEBPACK_IMPORTED_MODULE_3_socket_io_client__(this.socketurl + '/userChat', {
                transports: ['websocket']
            });
            this._io.on('connect', function (data) {
                console.log("userChat connect data: ", data);
            });
        }
    };
    ChatPage.prototype.openChatSocket = function () {
        var that = this;
        that._io.on(that.user.id + "-" + that.toUser.id, function (d4) {
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        return;
                    }
                    that.loader = true;
                    setTimeout(function () {
                        that.msgList.push({
                            // userId: that.user.id,
                            // userName: that.User,
                            // // userAvatar: "../../assets/chat/chat5.jpg",
                            // time: new Date().toISOString(),
                            // message: data
                            messageId: that.msgList.length + 1,
                            userId: that.toUser.id,
                            userName: that.toUser.name,
                            // userAvatar: this.user.avatar,
                            toUserId: that.user.id,
                            time: new Date().toISOString(),
                            message: data,
                            status: 'success'
                        });
                        that.loader = false;
                        // that.scrollDown();
                        that.scrollToBottom();
                    }, 2000);
                    // that.scrollDown();
                })(d4);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Content"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Content"])
    ], ChatPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('chat_input'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], ChatPage.prototype, "messageInput", void 0);
    ChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-chat',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/chat/chat.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{userName}}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="message-wrap">\n    <div *ngFor="let msg of msgList" class="message" [class.left]=" msg.userId === toUser.id "\n      [class.right]="msg.userId === user.id">\n      <ion-spinner name="dots" *ngIf="msg.status === \'pending\'"></ion-spinner>\n      <div class="msg-detail">\n        <div class="msg-info">\n          <p>\n            {{msg.userName}}&nbsp;&nbsp;&nbsp;{{msg.time | relativeTime}}</p>\n        </div>\n        <div class="msg-content">\n          <span class="triangle"></span>\n          <p class="line-breaker ">{{msg.message}}</p>\n        </div>\n      </div>\n    </div>\n    <ion-spinner name="dots" *ngIf="loader"></ion-spinner>\n  </div>\n</ion-content>\n<ion-footer no-border [style.height]="showEmojiPicker ? \'255px\' : \'55px\'">\n  <div class="input-wrap">\n    <textarea #chat_input placeholder="Text Input" [(ngModel)]="editorMsg" (keyup.enter)="sendMsg()"\n      (focusin)="onFocus()">\n    </textarea>\n    <button ion-button id="myBtn" clear icon-only item-right (click)="sendMsg()">\n      <ion-icon name="ios-send" ios="ios-send" md="md-send"></ion-icon>\n    </button>\n  </div>\n</ion-footer>\n\n<!-- <ion-header>\n  <ion-toolbar class="header">\n    <ion-title color="light">Business</ion-title>\n    <ion-buttons slot="end">\n      <ion-icon name="contact" icon-only color="light"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding class="contentBg" #content id="chat-parent" overflow-scroll="true"\n  (ionScrollStart)="logScrollStart()" (ionScroll)="logScrolling($event)">\n  <div>\n    <div id="chat-container">\n      <span class="line-style"></span>\n      <hr>\n      <div class="message-wrap">\n        <div *ngFor="let msg of msgList;let i = index; let first = first; let last = last" class="message">\n\n          <div class="message-left" *ngIf="msg.userId == toUser">\n            <img class="user-img" [src]="msg.userAvatar">\n            <div class="msg-detail">\n              <div class="msg-content">\n                <p class="line-breaker ">{{msg.message}}</p>\n              </div>\n            </div>\n          </div>\n          <div class="message-right" *ngIf="msg.userId == User">\n            <ion-spinner name="dots" *ngIf="msg.status === \'pending\'"></ion-spinner>\n            <div class="msg-detail">\n              <div class="msg-info">\n              </div>\n              <div class="msg-content">\n                <p class="line-breaker  ">{{msg.message}}</p>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n  </div>\n\n\n</ion-content>\n\n<ion-footer style="flex: 1;" class="footer">\n  <ion-toolbar class="footerSections">\n    <div class="containerFooter">\n      <div class="inputConatiner">\n        <ion-input class="placeholdertext inputMessage inputBg" type="text" placeholder="Type a message"\n          [(ngModel)]="inp_text" autofocus="true" padding-start></ion-input>\n      </div>\n      <div class="send-button-parent">\n        <button ion-button (click)="sendMsg()" class="sendBtn" color="primary">\n          <ion-icon class="sendBtn-icon" name="send" color="light"></ion-icon>\n        </button>\n      </div>\n    </div>\n  </ion-toolbar>\n</ion-footer> -->'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/chat/chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"]])
    ], ChatPage);
    return ChatPage;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 1052:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RelativeTime; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

//import distanceInWordsToNow from 'date-fns/distance_in_words_to_now';
// import {distanceInWordsToNow} from 'date-fns/dis'
var RelativeTime = /** @class */ (function () {
    function RelativeTime() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    RelativeTime.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        //return distanceInWordsToNow(new Date(value), { addSuffix: true });
    };
    RelativeTime = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'relativeTime',
        })
    ], RelativeTime);
    return RelativeTime;
}());

//# sourceMappingURL=relative-time.js.map

/***/ }),

/***/ 962:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageModule", function() { return ChatPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chat__ = __webpack_require__(1051);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_relative_time__ = __webpack_require__(1052);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ChatPageModule = /** @class */ (function () {
    function ChatPageModule() {
    }
    ChatPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__chat__["a" /* ChatPage */],
                __WEBPACK_IMPORTED_MODULE_3__pipes_relative_time__["a" /* RelativeTime */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__chat__["a" /* ChatPage */]),
            ],
        })
    ], ChatPageModule);
    return ChatPageModule;
}());

//# sourceMappingURL=chat.module.js.map

/***/ })

});
//# sourceMappingURL=22.js.map