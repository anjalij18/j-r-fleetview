webpackJsonp([32],{

/***/ 1028:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportsCollectionPageModule", function() { return ReportsCollectionPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__reports_collection__ = __webpack_require__(1125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ReportsCollectionPageModule = /** @class */ (function () {
    function ReportsCollectionPageModule() {
    }
    ReportsCollectionPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__reports_collection__["a" /* ReportsCollectionPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__reports_collection__["a" /* ReportsCollectionPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], ReportsCollectionPageModule);
    return ReportsCollectionPageModule;
}());

//# sourceMappingURL=reports-collection.module.js.map

/***/ }),

/***/ 1125:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportsCollectionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
// import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// @IonicPage()
// @Component({
//   selector: 'page-reports-collection',
//template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/reports-collection/reports-collection.html"*/'<!--\n  Generated template for the ReportsCollectionPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!--\n  Generated template for the ReportsCollectionPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Reports</ion-title>\n  </ion-navbar>\n  <ion-item style="height: 45px; background-color: #fafafa;">\n    <ion-label style="margin-top: 15px;">{{\'Select Vehicle\' | translate}}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getOverspeeddevice(selectedVehicle)"  [isMultiple]="true">\n    </select-searchable>\n  </ion-item>\n</ion-header>\n\n\n<ion-content>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col (click)= "acReport()">\n          <div class="style">\n            <img src="assets/imgs/ac.png">\n            <p style="margin: auto;padding: 3px;"> AC Report</p>\n          </div>\n\n          <!-- <ion-card>\n            <ion-row>\n              <ion-col col-4>\n\n              </ion-col>\n              <ion-col col-4>\n                <img src="assets/imgs/ac.png">\n              </ion-col>\n              <ion-col col-4>\n\n              </ion-col>\n            </ion-row>\n          </ion-card> -->\n        </ion-col>\n        <ion-col (click)="dailyReport()">\n          <div class="style">\n            <img src="assets/imgs/24-hour-daily-service.png">\n            <p style="margin: auto;padding: 3px;"> Daily Report</p>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col (click)="disance()">\n          <div class="style" >\n            <img src="assets/imgs/distance.png">\n            <p style="margin: auto;padding: 3px;"> Distance Report</p>\n          </div>\n\n          <!-- <ion-card>\n            <ion-row>\n              <ion-col col-4>\n\n              </ion-col>\n              <ion-col col-4>\n                <img src="assets/imgs/ac.png">\n              </ion-col>\n              <ion-col col-4>\n\n              </ion-col>\n            </ion-row>\n          </ion-card> -->\n        </ion-col>\n        <ion-col (click)="geo()">\n          <div class="style">\n            <!-- <img src="assets/imgs/ac.png"> -->\n            <ion-icon  name="disc"></ion-icon>\n            <p style="margin: auto;padding: 3px;"> Geofence Report</p>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col (click)="iginition()">\n          <div class="style">\n            <img src="assets/imgs/power-button.png">\n            <p style="margin: auto;padding: 3px;"> Ignition Report</p>\n          </div>\n\n          <!-- <ion-card>\n            <ion-row>\n              <ion-col col-4>\n\n              </ion-col>\n              <ion-col col-4>\n                <img src="assets/imgs/ac.png">\n              </ion-col>\n              <ion-col col-4>\n\n              </ion-col>\n            </ion-row>\n          </ion-card> -->\n        </ion-col>\n        <ion-col (click)="stoppage()">\n          <div class="style" >\n            <img src="assets/imgs/stoppage.png">\n            <p style="margin: auto;padding: 3px;"> Stoppage Report</p>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col (click)="trip()">\n          <div class="style">\n            <img src="assets/imgs/destination.png">\n            <p style="margin: auto;padding: 3px;"> Trip Report</p>\n          </div>\n\n          <!-- <ion-card>\n            <ion-row>\n              <ion-col col-4>\n\n              </ion-col>\n              <ion-col col-4>\n                <img src="assets/imgs/ac.png">\n              </ion-col>\n              <ion-col col-4>\n\n              </ion-col>\n            </ion-row>\n          </ion-card> -->\n        </ion-col>\n        <ion-col (click)="working()">\n          <div class="style">\n            <img src="assets/imgs/working-time.png">\n            <p style="margin: auto;padding: 3px;"> Working Report</p>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col (click)="overspeed()">\n          <div class="style">\n            <img src="assets/imgs/speed.png">\n            <p style="margin: auto;padding: 3px;"> Overspeed Reprot</p>\n          </div>\n\n          <!-- <ion-card>\n            <ion-row>\n              <ion-col col-4>\n\n              </ion-col>\n              <ion-col col-4>\n                <img src="assets/imgs/ac.png">\n              </ion-col>\n              <ion-col col-4>\n\n              </ion-col>\n            </ion-row>\n          </ion-card> -->\n        </ion-col>\n        <ion-col (click)="value()">\n          <div class="style">\n            <img src="assets/imgs/value.png">\n            <p style="margin: auto;padding: 3px;"> Value Sceen</p>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/reports-collection/reports-collection.html"*/,
// })
// export class ReportsCollectionPage {
//   constructor(public navCtrl: NavController, public navParams: NavParams) {
//   }
//   ionViewDidLoad() {
//     console.log('ionViewDidLoad ReportsCollectionPage');
//   }
// }



/**
 * Generated class for the ReportsCollectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReportsCollectionPage = /** @class */ (function () {
    function ReportsCollectionPage(navCtrl, navParams, apicalloverspeed) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalloverspeed = apicalloverspeed;
        this.overSpeeddevice_id = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
    }
    ReportsCollectionPage.prototype.getOverspeeddevice = function (selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.overSpeeddevice_id = selectedVehicle;
        // this.overSpeeddevice_id = [];
        // if (selectedVehicle.length > 0) {
        //   if (selectedVehicle.length > 1) {
        //     for (var t = 0; t < selectedVehicle.length; t++) {
        //       this.overSpeeddevice_id.push(selectedVehicle[t].Device_ID)
        //     }
        //   } else {
        //     this.overSpeeddevice_id.push(selectedVehicle[0].Device_ID)
        //   }
        // } else return;
        // console.log("selectedVehicle=> ", this.overSpeeddevice_id)
    };
    ReportsCollectionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReportsCollectionPage');
    };
    ReportsCollectionPage.prototype.acReport = function () {
        this.navCtrl.push("AcReportPage", {
            item: this.overSpeeddevice_id
        });
    };
    ReportsCollectionPage.prototype.dailyReport = function () {
        this.navCtrl.push("DailyReportPage", {
            item: this.overSpeeddevice_id
        });
    };
    ReportsCollectionPage.prototype.disance = function () {
        this.navCtrl.push("DistanceReportPage", {
            item: this.overSpeeddevice_id
        });
    };
    ReportsCollectionPage.prototype.geo = function () {
        this.navCtrl.push("GeofenceReportPage", {
            item: this.overSpeeddevice_id
        });
    };
    ReportsCollectionPage.prototype.iginition = function () {
        this.navCtrl.push("IgnitionReportPage", {
            item: this.overSpeeddevice_id
        });
    };
    ReportsCollectionPage.prototype.stoppage = function () {
        this.navCtrl.push("StoppagesRepoPage", {
            item: this.overSpeeddevice_id
        });
    };
    ReportsCollectionPage.prototype.trip = function () {
        this.navCtrl.push("TripReportPage", {
            item: this.overSpeeddevice_id
        });
    };
    ReportsCollectionPage.prototype.working = function () {
        this.navCtrl.push("WorkingHoursReportPage", {
            item: this.overSpeeddevice_id
        });
    };
    ReportsCollectionPage.prototype.overspeed = function () {
        this.navCtrl.push("OverSpeedRepoPage", {
            item: this.overSpeeddevice_id
        });
    };
    ReportsCollectionPage.prototype.value = function () {
        this.navCtrl.push("DailyReportNewPage", {
            item: this.overSpeeddevice_id
        });
    };
    ReportsCollectionPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    ReportsCollectionPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apicalloverspeed.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicalloverspeed.startLoading().present();
        this.apicalloverspeed.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicalloverspeed.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apicalloverspeed.stopLoading();
            console.log(err);
        });
    };
    ReportsCollectionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-reports-collection',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/reports-collection/reports-collection.html"*/'<!--\n  Generated template for the ReportsCollectionPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!--\n  Generated template for the ReportsCollectionPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Reports</ion-title>\n  </ion-navbar>\n  <ion-item style="height: 45px; background-color: #fafafa;">\n    <ion-label style="margin-top: 15px;">{{\'Select Vehicle\' | translate}}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getOverspeeddevice(selectedVehicle)"  [isMultiple]="true">\n    </select-searchable>\n  </ion-item>\n</ion-header>\n\n\n<ion-content>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col (click)= "acReport()">\n          <div class="style">\n            <img src="assets/imgs/ac.png">\n            <p style="margin: auto;padding: 3px;"> AC Report</p>\n          </div>\n\n          <!-- <ion-card>\n            <ion-row>\n              <ion-col col-4>\n\n              </ion-col>\n              <ion-col col-4>\n                <img src="assets/imgs/ac.png">\n              </ion-col>\n              <ion-col col-4>\n\n              </ion-col>\n            </ion-row>\n          </ion-card> -->\n        </ion-col>\n        <ion-col (click)="dailyReport()">\n          <div class="style">\n            <img src="assets/imgs/24-hour-daily-service.png">\n            <p style="margin: auto;padding: 3px;"> Daily Report</p>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col (click)="disance()">\n          <div class="style" >\n            <img src="assets/imgs/distance.png">\n            <p style="margin: auto;padding: 3px;"> Distance Report</p>\n          </div>\n\n          <!-- <ion-card>\n            <ion-row>\n              <ion-col col-4>\n\n              </ion-col>\n              <ion-col col-4>\n                <img src="assets/imgs/ac.png">\n              </ion-col>\n              <ion-col col-4>\n\n              </ion-col>\n            </ion-row>\n          </ion-card> -->\n        </ion-col>\n        <ion-col (click)="geo()">\n          <div class="style">\n            <!-- <img src="assets/imgs/ac.png"> -->\n            <ion-icon  name="disc"></ion-icon>\n            <p style="margin: auto;padding: 3px;"> Geofence Report</p>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col (click)="iginition()">\n          <div class="style">\n            <img src="assets/imgs/power-button.png">\n            <p style="margin: auto;padding: 3px;"> Ignition Report</p>\n          </div>\n\n          <!-- <ion-card>\n            <ion-row>\n              <ion-col col-4>\n\n              </ion-col>\n              <ion-col col-4>\n                <img src="assets/imgs/ac.png">\n              </ion-col>\n              <ion-col col-4>\n\n              </ion-col>\n            </ion-row>\n          </ion-card> -->\n        </ion-col>\n        <ion-col (click)="stoppage()">\n          <div class="style" >\n            <img src="assets/imgs/stoppage.png">\n            <p style="margin: auto;padding: 3px;"> Stoppage Report</p>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col (click)="trip()">\n          <div class="style">\n            <img src="assets/imgs/destination.png">\n            <p style="margin: auto;padding: 3px;"> Trip Report</p>\n          </div>\n\n          <!-- <ion-card>\n            <ion-row>\n              <ion-col col-4>\n\n              </ion-col>\n              <ion-col col-4>\n                <img src="assets/imgs/ac.png">\n              </ion-col>\n              <ion-col col-4>\n\n              </ion-col>\n            </ion-row>\n          </ion-card> -->\n        </ion-col>\n        <ion-col (click)="working()">\n          <div class="style">\n            <img src="assets/imgs/working-time.png">\n            <p style="margin: auto;padding: 3px;"> Working Report</p>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col (click)="overspeed()">\n          <div class="style">\n            <img src="assets/imgs/speed.png">\n            <p style="margin: auto;padding: 3px;"> Overspeed Reprot</p>\n          </div>\n\n          <!-- <ion-card>\n            <ion-row>\n              <ion-col col-4>\n\n              </ion-col>\n              <ion-col col-4>\n                <img src="assets/imgs/ac.png">\n              </ion-col>\n              <ion-col col-4>\n\n              </ion-col>\n            </ion-row>\n          </ion-card> -->\n        </ion-col>\n        <ion-col (click)="value()">\n          <div class="style">\n            <img src="assets/imgs/value.png">\n            <p style="margin: auto;padding: 3px;"> Value Sceen</p>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/reports-collection/reports-collection.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], ReportsCollectionPage);
    return ReportsCollectionPage;
}());

//# sourceMappingURL=reports-collection.js.map

/***/ })

});
//# sourceMappingURL=32.js.map