webpackJsonp([65],{

/***/ 1048:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AllNotificationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__filter_filter__ = __webpack_require__(553);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AllNotificationsPage = /** @class */ (function () {
    function AllNotificationsPage(navCtrl, navParams, apiCall, events, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.events = events;
        this.popoverCtrl = popoverCtrl;
        this.NotifyData = [];
        this.MassArray1 = [];
        this.page = 1;
        this.items = [];
        this.limit = 8;
        this.portstemp = [];
        this.keyData = [];
        this.selectedVehicle = {};
        this.vehicleData = {};
        this.callBaseURL();
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.events.publish('cart:updated', 0);
        this.getVehicleList();
        for (var i = 0; i < 30; i++) {
            this.items.push(this.items.length);
        }
        if (navParams.get('param') != null) {
            this.vehicleData = navParams.get('param');
        }
    }
    AllNotificationsPage.prototype.ngOnInit = function () {
        localStorage.removeItem("filterByType");
        localStorage.removeItem("filterByDate");
        console.log(this.vehicleData);
        if (this.vehicleData == undefined) {
            console.log("vehicle undefined");
            this.getUsersOnScroll();
            this.getVehicleList();
        }
        else {
            console.log("vehicle defined");
            this.temp(this.vehicleData);
        }
    };
    AllNotificationsPage.prototype.callBaseURL = function () {
        // debugger
        if (localStorage.getItem("ENTERED_BASE_URL") === null) {
            var url = "https://www.oneqlik.in/pullData/getUrlnew";
            this.apiCall.getSOSReportAPI(url)
                .subscribe(function (data) {
                console.log("base url: ", data);
                if (data.url) {
                    localStorage.setItem("BASE_URL", JSON.stringify(data.url));
                }
                if (data.socket) {
                    localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
                }
                // this.getSocketUrl();
            });
        }
    };
    // socketurl: any;
    // getSocketUrl() {
    //   if (localStorage.getItem('SOCKET_URL') !== null) {
    //     this.socketurl = JSON.parse(localStorage.getItem('SOCKET_URL'));
    //     this.socket = io(this.socketurl + '/notifIOV2?userId=' + this.islogin._id, {
    //       transports: ['websocket', 'polling']
    //     });
    //     this.socket.on('connect', () => {
    //       console.log('IO Connected page');
    //       console.log("socket connected page ", this.socket.connected)
    //     });
    //     this.socket.on(this.islogin._id, (msg) => {
    //       this.NotifyData.push(msg);
    //     });
    //   }
    // }
    AllNotificationsPage.prototype.onNotifMap = function (notif) {
        console.log("notif : ", this.vehicleData);
        this.navCtrl.push('NotifMapPage', {
            navParams: notif
        });
    };
    AllNotificationsPage.prototype.doRefresh = function (refresher) {
        console.log('Begin async operation', refresher);
        this.getUsersOnScroll();
        refresher.complete();
    };
    AllNotificationsPage.prototype.filterby = function (ev) {
        var _this = this;
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__filter_filter__["a" /* FilterPage */], {
            cssClass: 'iosPop-popover'
        });
        popover.present({
            ev: ev
        });
        popover.onDidDismiss(function (data) {
            console.log("notification----->", data, localStorage.getItem("types"));
            if (localStorage.getItem("types") != null) {
                console.log("filter datassss---->", data);
                var typeArr = [];
                if (data != null) {
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            typeArr.push(data[i].filterValue);
                        }
                    }
                    _this.keyData = typeArr;
                    console.log("keyData=> ", _this.keyData);
                    _this.filterByType();
                }
            }
            else {
                if (localStorage.getItem("dates") != null) {
                    _this.dates = data;
                    _this.filterByDate();
                }
            }
        });
    };
    AllNotificationsPage.prototype.filterByType = function () {
        var _this = this;
        console.log("selecteddddd---->", this.selectedVehicle);
        var that = this;
        console.log("device iddd--->", this.selectedVehicle.Device_ID);
        localStorage.setItem("filterByType", "filterByType");
        var baseUrl;
        console.log("vehicleData------->", this.selectedVehicle);
        debugger;
        if (that.vehicleData != undefined || that.selectedVehicle != undefined) {
            var dev;
            if (that.vehicleData) {
                dev = that.vehicleData.Device_ID;
            }
            else {
                dev = this.selectedVehicle.Device_ID;
            }
            baseUrl = this.apiCall.mainUrl + 'notifs/getNotifiLimit?user=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&type=' + that.keyData + '&device=' + this.selectedVehicle.Device_ID;
        }
        else {
            baseUrl = this.apiCall.mainUrl + 'notifs/getNotifiLimit?user=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&type=' + that.keyData;
        }
        that.apiCall.startLoading().present();
        console.log("baseurl ------>", baseUrl);
        that.apiCall.filterByType(baseUrl)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            that.ndata = data;
            console.log("ndata----->", _this.ndata);
            that.MassArray1 = that.ndata;
            localStorage.removeItem("types");
        }, function (err) {
            that.apiCall.stopLoading();
            console.log(err);
        });
    };
    AllNotificationsPage.prototype.filterByDate = function () {
        var that = this;
        localStorage.setItem("filterByDate", "filterByDate");
        that.apiCall.startLoading().present();
        that.apiCall.filterByDateCall(that.islogin._id, that.page, that.limit, that.dates)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            that.ndata = data;
            console.log("filter by date------>", data);
            that.MassArray1 = that.ndata;
            localStorage.removeItem("dates");
        }, function (err) {
            that.apiCall.stopLoading();
            console.log(err);
        });
    };
    AllNotificationsPage.prototype.temp = function (key) {
        var _this = this;
        console.log("temp key--->", key);
        this.selectedVehicle = key;
        this.page = 1;
        var that = this;
        var baseUrl2 = this.apiCall.mainUrl + 'notifs/getNotifiLimit?user=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&type=' + that.keyData + '&device=';
        console.log("base url---------ssss>", baseUrl2);
        console.log("filete by type ---->", localStorage.getItem("filterByType"));
        if (localStorage.getItem("filterByType") == null) {
            var that_1 = this;
            console.log("filter is null----->");
            var baseUrl2 = this.apiCall.mainUrl + 'notifs/getNotifiLimit?user=' + that_1.islogin._id;
            that_1.apiCall.startLoading().present();
            that_1.apiCall.filterByType(baseUrl2)
                .subscribe(function (data) {
                console.log("dddddddddd------>", data);
                that_1.apiCall.stopLoading();
                that_1.ndata = data;
                that_1.MassArray1 = that_1.ndata;
                localStorage.removeItem("types");
            }, function (err) {
                that_1.apiCall.stopLoading();
                console.log(err);
            });
        }
        else {
            this.apiCall.startLoading().present();
            this.apiCall.getFilteredcall(this.islogin._id, this.page, this.limit, key.Device_ID)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                console.log("data---response-->", data);
                _this.ndata = data;
                _this.MassArray1 = _this.ndata;
            }, function (err) {
                _this.apiCall.stopLoading();
                console.log(err);
            });
        }
    };
    AllNotificationsPage.prototype.getUsersOnScroll = function () {
        var _this = this;
        this.apiCall.getDataOnScroll(this.islogin._id, this.page, this.limit)
            .subscribe(function (res) {
            _this.ndata = res;
            _this.MassArray1 = _this.ndata;
        }, function (error) {
            console.log(error);
        });
    };
    AllNotificationsPage.prototype.getVehicleList = function () {
        // console.log("dddddd------>",this.islogin.isSuperAdmin)
        var that = this;
        var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email;
        if (that.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + that.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + that.islogin._id;
            }
        }
        // that.apiCall.getVehicleListCall(that.islogin._id, that.islogin.email)
        that.apiCall.getVehicleListCall(baseURLp)
            .subscribe(function (data) {
            that.portstemp = data.devices;
        }, function (err) {
            console.log(err);
        });
    };
    AllNotificationsPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var that = this;
        that.page = that.page + 1;
        setTimeout(function () {
            if (localStorage.getItem("filterByType") != null) {
                // when filter by type is selected
                localStorage.setItem("filterByType", "filterByType");
                var baseUrl1;
                if (that.vehicleData != undefined || that.selectedVehicle != undefined) {
                    var dev;
                    if (that.vehicleData) {
                        dev = that.vehicleData.Device_ID;
                    }
                    else {
                        dev = that.selectedVehicle.Device_ID;
                    }
                    baseUrl1 = _this.apiCall.mainUrl + 'notifs/getNotifiLimit?user=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&type=' + that.keyData + '&device=' + dev;
                }
                else {
                    baseUrl1 = _this.apiCall.mainUrl + 'notifs/getNotifiLimit?user=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&type=' + that.keyData;
                }
                that.apiCall.filterByType(baseUrl1)
                    .subscribe(function (data) {
                    that.ndata = data;
                    for (var i = 0; i < that.ndata.length; i++) {
                        that.MassArray1.push(that.ndata[i]);
                    }
                    localStorage.removeItem("types");
                }, function (err) {
                    console.log(err);
                });
            }
            else {
                if (localStorage.getItem("filterByDate") != null) {
                    // when filter by date is applied
                    that.apiCall.filterByDateCall(that.islogin._id, that.page, that.limit, that.dates)
                        .subscribe(function (data) {
                        that.ndata = data;
                        for (var i = 0; i < that.ndata.length; i++) {
                            that.MassArray1.push(that.ndata[i]);
                        }
                        localStorage.removeItem("dates");
                    }, function (err) {
                        console.log(err);
                    });
                }
                else {
                    if (that.selectedVehicle != undefined || that.vehicleData != undefined) {
                        var keyTemp;
                        if (that.selectedVehicle == undefined) {
                            keyTemp = that.vehicleData;
                        }
                        else {
                            keyTemp = that.selectedVehicle;
                        }
                        // when applying sorting by selected vehicle
                        that.apiCall.getFilteredcall(that.islogin._id, that.page, that.limit, keyTemp.Device_ID)
                            .subscribe(function (data) {
                            console.log("data ---responese--->", data);
                            that.ndata = data;
                            for (var i = 0; i < that.ndata.length; i++) {
                                that.MassArray1.push(that.ndata[i]);
                            }
                        }, function (err) {
                            console.log(err);
                        });
                    }
                    else {
                        // when no filter applied
                        that.apiCall.getDataOnScroll(that.islogin._id, that.page, that.limit)
                            .subscribe(function (res) {
                            that.ndata = res;
                            for (var i = 0; i < that.ndata.length; i++) {
                                that.MassArray1.push(that.ndata[i]);
                            }
                        }, function (error) {
                            console.log(error);
                        });
                    }
                }
            }
            console.log('Async operation has ended');
            infiniteScroll.complete();
        }, 100);
    };
    AllNotificationsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-all-notifications',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/all-notifications/all-notifications.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "Notifications" | translate }}</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="filterby($event)">\n        <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n  <ion-item *ngIf="portstemp.length != 0" class="itemStyle">\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="temp(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n</ion-header>\n\n<ion-content [ngClass]="{\n    masters: portstemp.length != 0,\n    masters1: portstemp.length == 0\n  }">\n  <div *ngIf="MassArray1.length == 0">\n    <p padding-left>{{ "Oops.. No data found for selected vehicle.." | translate }}</p>\n  </div>\n  <div *ngIf="MassArray1.length != 0">\n    <ion-list>\n      <ion-item *ngFor="let notif of MassArray1; let i = index" (click)="onNotifMap(notif)">\n        <ion-avatar item-start>\n          <img src="assets/imgs/sos.png" *ngIf="notif.type === \'SOS\'" />\n          <img src="assets/imgs/max.png" *ngIf="notif.type === \'MAXSTOPPAGE\'" />\n          <img src="assets/imgs/speedlimit.png" *ngIf="notif.type === \'overspeed\'" />\n          <img src="assets/imgs/geofence.jpg" *ngIf="notif.type === \'Geo-Fence\'" />\n          <img src="assets/imgs/ignition-switch-icon.jpg" *ngIf="notif.type === \'IGN\'" />\n          <img src="assets/imgs/car_blue_icon.png" *ngIf="notif.type === \'route-poi\'" />\n          <img src="assets/imgs/petrolpump4.jpg" *ngIf="notif.type === \'Fuel\'" />\n          <img src="assets/imgs/system_status-noun_63767_cc.png" *ngIf="notif.type == \'status\'" />\n          <img src="assets/imgs/AccurateMapping.jpg" *ngIf="notif.type === \'Route\'" />\n          <img src="assets/imgs/Snowflakes_snow-10-512.png" *ngIf="notif.type === \'AC\'" />\n          <img src="assets/imgs/power-png-icon-6.png" *ngIf="notif.type === \'power\'" />\n          <img src="assets/imgs/key_orange.jpg" *ngIf="notif.type === \'immo\'" />\n          <img src="assets/imgs/342e07b3a6f1628afd14c9b4e6c93afe_icon.png" *ngIf="notif.type === \'theft\'" />\n          <img src="assets/imgs/bell_active.png" *ngIf="notif.type === \'Reminder\'" />\n          <div *ngIf="notif.type === \'door\'">\n            <img src="assets/imgs/statusIcons/car_door_open.png" style="width: 35px;" *ngIf="notif.switch === \'OFF\'" />\n            <img src="assets/imgs/statusIcons/car_door_close.png" *ngIf="notif.switch === \'ON\'" />\n          </div>\n        </ion-avatar>\n        <h2>{{ notif.item._type | titlecase }}</h2>\n        <p ion-text text-wrap>{{ notif.item["sentence"] }}</p>\n        <p *ngIf="notif.type === \'IGN\'">{{\'odo\' | translate}}: <span\n            [ngClass]="{ \'switchOn\': notif.switch === \'ON\',\'switchOff\':   notif.switch === \'OFF\' }">{{notif.odo | number: "1.0-2"}}\n            km(s)</span></p>\n        <ion-row item-end>\n          <ion-col width-40>\n            <div style="margin-top:19%;">\n              <p style="text-align: right;font-size: 12px;">\n                {{ notif.timestamp | date: "mediumDate" }}\n              </p>\n            </div>\n            <div style="margin-top:19%;">\n              <p style="text-align: right;font-size: 12px;">\n                {{ notif.timestamp | date: "shortTime" }}\n              </p>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n    </ion-list>\n  </div>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/all-notifications/all-notifications.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"]])
    ], AllNotificationsPage);
    return AllNotificationsPage;
}());

//# sourceMappingURL=all-notifications.js.map

/***/ }),

/***/ 959:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllNotificationsPageModule", function() { return AllNotificationsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__all_notifications__ = __webpack_require__(1048);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var AllNotificationsPageModule = /** @class */ (function () {
    function AllNotificationsPageModule() {
    }
    AllNotificationsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__all_notifications__["a" /* AllNotificationsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__all_notifications__["a" /* AllNotificationsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], AllNotificationsPageModule);
    return AllNotificationsPageModule;
}());

//# sourceMappingURL=all-notifications.module.js.map

/***/ })

});
//# sourceMappingURL=65.js.map