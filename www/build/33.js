webpackJsonp([33],{

/***/ 1014:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPageModule", function() { return SettingsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings__ = __webpack_require__(1108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SettingsPageModule = /** @class */ (function () {
    function SettingsPageModule() {
    }
    SettingsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__settings__["a" /* SettingsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__settings__["a" /* SettingsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], SettingsPageModule);
    return SettingsPageModule;
}());

//# sourceMappingURL=settings.module.js.map

/***/ }),

/***/ 1108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_text_to_speech__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_app_languages__ = __webpack_require__(573);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SettingsPage = /** @class */ (function () {
    function SettingsPage(events, tts, navCtrl, navParams, alertCtrl, toastCtrl, apiCall, translate, appLang) {
        var _this = this;
        this.events = events;
        this.tts = tts;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.apiCall = apiCall;
        this.translate = translate;
        this.appLang = appLang;
        this.isChecked = true;
        this.notif = "true";
        this.fuels = [];
        this.maps = [];
        this.vehName = true;
        this.measurementType = 'MKS';
        this.currencyJson = [];
        this.currencyArray = [];
        this.currencyValue = 'INR';
        this.fuels = ['LITRE', 'PERCENTAGE'];
        this.maps = [this.translate.instant('Normal'), this.translate.instant('Terrain'), this.translate.instant('Satellite')];
        this.languages = this.appLang.Languages.slice();
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        if (this.islogin.fuel_unit === 'LITRE') {
            this.fuelKey = 'LITRE';
        }
        else {
            this.fuelKey = 'PERCENTAGE';
        }
        if (localStorage.getItem('MAP_KEY') != null) {
            this.selectedMapKey = localStorage.getItem('MAP_KEY');
        }
        if (localStorage.getItem('NightMode') != null) {
            if (localStorage.getItem('NightMode') === 'ON') {
                this.nightMode = true;
            }
        }
        if (localStorage.getItem('TrafficMode') != null) {
            if (localStorage.getItem('TrafficMode') === 'ON') {
                this.trafficMode = true;
            }
        }
        if (localStorage.getItem('DisplayVehicleName') != null) {
            if (localStorage.getItem('DisplayVehicleName') === 'OFF') {
                this.vehName = false;
            }
        }
        this.apiCall.getCurrency().subscribe(function (data) {
            _this.currencyJson = data;
            _this.getCurrencies();
        });
    }
    SettingsPage.prototype.ngOnInit = function () {
        var _this = this;
        var b_url = this.apiCall.mainUrl + "users/get_user_setting";
        var Var = { uid: this.islogin._id };
        this.apiCall.urlpasseswithdata(b_url, Var)
            .subscribe(function (resp) {
            console.log("check lang key: ", resp);
            if (resp.language_code !== undefined) {
                _this.lankeyname = resp.language_code;
                _this.lankey = resp.language_code;
            }
            if (resp.voice_alert !== undefined) {
                _this.notif = resp.voice_alert;
                if (_this.notif == 'true') {
                    _this.isChecked = true;
                }
                else {
                    _this.isChecked = false;
                }
            }
            if (resp.unit_measurement !== undefined) {
                _this.measurementType = resp.unit_measurement;
                // this.measurementType = resp.unit_measurement;
                if (_this.measurementType === 'FPS') {
                    if (_this.islogin.fuel_unit === 'LITRE') {
                        _this.fuelKey = 'GALLON';
                    }
                    else {
                        _this.fuelKey = 'PERCENTAGE';
                    }
                }
            }
            else {
                if (localStorage.getItem('MeasurementType') !== null) {
                    var measureType = localStorage.getItem('MeasurementType');
                    _this.measurementType = measureType;
                }
                else {
                    _this.measurementType = 'MKS';
                }
            }
            if (resp.currency_code !== undefined) {
                _this.currencyValue = resp.currency_code;
            }
        }, function (err) {
            console.log(err);
        });
        if (localStorage.getItem("SCREEN") != null) {
            if (localStorage.getItem("SCREEN") == 'live') {
                this.isBooleanforLive = true;
                this.isBooleanforDash = false;
            }
            else {
                if (localStorage.getItem("SCREEN") == 'dashboard') {
                    this.isBooleanforDash = true;
                    this.isBooleanforLive = false;
                }
            }
        }
    };
    SettingsPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter SettingsPage');
    };
    SettingsPage.prototype.onChangeMeasure = function (key) {
        console.log(key);
        localStorage.setItem('MeasurementType', key);
        var payload = {
            uid: this.islogin._id,
            unit_measurement: key
        };
        // this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
            .subscribe(function (resp) {
            console.log('response language code ' + JSON.stringify(resp));
        }, function (err) {
            // this.apiCall.stopLoading();
            console.log(err);
        });
    };
    SettingsPage.prototype.setLanguage = function (key) {
        var _this = this;
        var payload = {
            uid: this.islogin._id,
            lang: key
        };
        // this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
            .subscribe(function (resp) {
            console.log('response language code ' + JSON.stringify(resp));
            localStorage.setItem('LanguageKey', key);
            // this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: resp.message,
                duration: 1500,
                position: 'bottom'
            });
            toast.present();
        }, function (err) {
            localStorage.setItem('LanguageKey', key);
            // this.apiCall.stopLoading();
            console.log(err);
        });
        this.events.publish('lang:key', key);
        localStorage.setItem('LanguageKey', key);
    };
    SettingsPage.prototype.setNotif = function (notif) {
        var _this = this;
        this.isChecked = notif;
        // if (notif === true) {
        var payload = {
            uid: this.islogin._id,
            voice_alert: notif
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
            .subscribe(function (resp) {
            console.log('response language code ' + JSON.stringify(resp));
            _this.apiCall.stopLoading();
            var msg;
            if (notif === true) {
                // msg = 'You have succesfully enabled voice notifications';
                msg = _this.translate.instant('You have succesfully enabled voice notifications');
            }
            else {
                msg = _this.translate.instant('You have succesfully disabled voice notifications');
                // msg = 'You have succesfully disabled voice notifications';
            }
            _this.tts.speak(msg)
                .then(function () { })
                .catch(function (reason) { return console.log(reason); });
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant("You have succesfully enabled voice notifications"),
                duration: 1500,
                position: 'bottom'
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error occured: ", err);
        });
        // }
    };
    SettingsPage.prototype.rootpage = function () {
        var _this = this;
        var alert = this.alertCtrl.create();
        alert.setSubTitle('Choose default screen');
        alert.addInput({
            type: 'radio',
            label: 'Dashboard',
            value: 'dashboard',
            checked: (this.isBooleanforDash ? this.isBooleanforDash : true)
        });
        alert.addInput({
            type: 'radio',
            label: 'Live Tracking',
            value: 'live',
            checked: this.isBooleanforLive
        });
        alert.addInput({
            type: 'radio',
            label: 'Vehicle List',
            value: 'vehiclelist',
            checked: this.isBooleanforVehList
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                console.log(data);
                localStorage.setItem("SCREEN", data);
                var toast = _this.toastCtrl.create({
                    message: 'Default page set to ' + data + ' page',
                    duration: 2000
                });
                toast.onDidDismiss(function () {
                    _this.events.publish("Screen:Changed", data);
                });
                toast.present();
            }
        });
        alert.present();
    };
    SettingsPage.prototype.onChangeMap = function (key) {
        console.log("map key changed: ", key);
        localStorage.setItem("MAP_KEY", key);
    };
    SettingsPage.prototype.onChangeFuel = function (key) {
        console.log("key changed: ", key);
        var newContact = {
            fname: this.islogin.fn,
            lname: this.islogin.ln,
            org: this.islogin._orgName,
            uid: this.islogin._id,
            fuel_unit: key
        };
        var _baseURl = this.apiCall.mainUrl + "users/Account_Edit";
        this.apiCall.urlpasseswithdata(_baseURl, newContact)
            .subscribe(function (data) {
            console.log("got response data: ", data);
            var logindetails = JSON.parse(JSON.stringify(data));
            var userDetails = window.atob(logindetails.token.split('.')[1]);
            var details = JSON.parse(userDetails);
            localStorage.setItem('details', JSON.stringify(details));
        });
    };
    SettingsPage.prototype.showAlert = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: this.translate.instant('Set Immobilize Password'),
            message: this.translate.instant("Enter password for engine cut"),
            inputs: [
                {
                    name: 'password',
                    placeholder: this.translate.instant('Password')
                },
                {
                    name: 'cpassword',
                    placeholder: this.translate.instant('Confirm Password')
                },
            ],
            buttons: [
                {
                    text: this.translate.instant('Cancel'),
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.translate.instant('SAVE'),
                    handler: function (data) {
                        console.log('Saved clicked');
                        if (data.password !== data.cpassword) {
                            _this.toastmsg(_this.translate.instant('Entered password and confirm password did not match.'));
                            return;
                        }
                        _this.setPassword(data);
                    }
                }
            ]
        });
        prompt.present();
    };
    SettingsPage.prototype.toastmsg = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'bottom'
        }).present();
    };
    SettingsPage.prototype.setPassword = function (data) {
        var _this = this;
        var payload = {
            uid: this.islogin._id,
            engine_cut_psd: data.password
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
            .subscribe(function (resp) {
            console.log('response language code ' + JSON.stringify(resp));
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant("Immobilize password set successfully"),
                duration: 1500,
                position: 'bottom'
            });
            toast.present();
        });
    };
    SettingsPage.prototype.update = function (ev) {
        console.log("invoking notification: ", ev);
        if (ev === true) {
            localStorage.setItem('NightMode', 'ON');
        }
        else {
            localStorage.setItem('NightMode', 'OFF');
        }
    };
    SettingsPage.prototype.trafficUpdate = function (ev) {
        console.log("invoking notification for traffic: ", ev);
        if (ev === true) {
            localStorage.setItem('TrafficMode', 'ON');
        }
        else {
            localStorage.setItem('TrafficMode', 'OFF');
        }
    };
    SettingsPage.prototype.vehNameUpdate = function (ev) {
        var _this = this;
        if (ev == false) {
            var alert_1 = this.alertCtrl.create({
                message: this.translate.instant("Are you sure, you don't want to display vehicle names on multiple live screen?"),
                buttons: [
                    {
                        text: this.translate.instant('Back'),
                        handler: function () {
                            _this.vehName = true;
                        }
                    },
                    {
                        text: this.translate.instant('Yes'),
                        handler: function () {
                            localStorage.setItem('DisplayVehicleName', 'OFF');
                        }
                    }
                ]
            });
            alert_1.present();
        }
        else {
            localStorage.setItem('DisplayVehicleName', 'ON');
        }
    };
    SettingsPage.prototype.notifSet = function () {
        this.navCtrl.push('NotifSettingPage');
    };
    SettingsPage.prototype.getCurrencies = function () {
        var currencyObj = this.currencyJson;
        for (var i in currencyObj) {
            if (i == 'INR') {
                this.currencyValue = i;
            }
            var tempObj = {
                currencyVal: i,
                country: i + '-' + currencyObj[i]
            };
            this.currencyArray.push(tempObj);
        }
    };
    SettingsPage.prototype.onChangeCurrency = function (currencyValue) {
        var _this = this;
        var url = this.apiCall.mainUrl + "users/set_user_setting";
        var payload = {
            "uid": this.islogin._id,
            "currency_code": currencyValue
        };
        this.apiCall.urlpasseswithdata(url, payload).subscribe(function (data) {
            console.log("check saved data: ", data);
            var toast = _this.toastCtrl.create({
                message: 'Currency updated successfully.',
                duration: 1500,
                position: 'top'
            });
            toast.present();
            localStorage.setItem('default_curreny', _this.currencyValue);
            // this.getDefaultUserSettings();   
        }, function (err) {
            var toast = _this.toastCtrl.create({
                message: 'Something went wrong.. try in somtime.',
                duration: 1500,
                position: 'top'
            });
            toast.present();
        });
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-settings',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/profile/settings/settings.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{\'Settings\' | translate}}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-item>\n    <ion-icon *ngIf="!isChecked" name="mic" item-start></ion-icon>\n    <ion-icon *ngIf="isChecked" name="mic-off" item-start></ion-icon>\n    <ion-label>{{\'Voice Notification\' | translate}}</ion-label>\n    <ion-toggle color="gpsc" [(ngModel)]="notif" (ngModelChange)="setNotif(notif)"></ion-toggle>\n\n    <!-- <ion-checkbox color="gpsc" [(ngModel)]="notif" (click)="setNotif(notif)" item-right></ion-checkbox> -->\n  </ion-item>\n  <ion-item (tap)="rootpage()">\n    <ion-icon name="checkmark-circle-outline" item-start></ion-icon>\n    <ion-label>{{\'Default Page\' | translate}}</ion-label>\n  </ion-item>\n\n  <ion-item>\n    <ion-icon name="map" item-start></ion-icon>\n    <ion-label color="dark">{{\'Map Type\' | translate}}</ion-label>\n    <ion-select [(ngModel)]="mapKey">\n      <ion-option *ngFor="let mapKey of maps; let idx = index" [value]="mapKey" (ionSelect)="onChangeMap(mapKey)"\n        [selected]="selectedMapKey === mapKey ? selectedMapKey : (idx==0).toString()">{{ mapKey }}</ion-option>\n    </ion-select>\n  </ion-item>\n\n  <ion-item>\n    <ion-icon name="custom-fuel" item-start style="font-size: 1em;"></ion-icon>\n    <ion-label color="dark">{{\'Fuel Unit\' | translate}}</ion-label>\n    <ion-select [(ngModel)]="fuelKey">\n      <ion-option *ngFor="let fuelKey of fuels" [value]="fuelKey" (ionSelect)="onChangeFuel(fuelKey)"\n        [selected]="islogin.fuel_unit === fuelKey">{{ fuelKey }}</ion-option>\n    </ion-select>\n  </ion-item>\n\n  <ion-item>\n    <ion-icon name="flag" item-start></ion-icon>\n    <ion-label color="dark">{{\'Language\' | translate}}</ion-label>\n    <ion-select [(ngModel)]="lankey" (ionChange)="setLanguage(lankey)">\n      <ion-option *ngFor="let lan of languages" [value]="lan.key" [selected]="lankeyname === lan.key">{{ lan.name }}\n      </ion-option>\n    </ion-select>\n  </ion-item>\n\n  <ion-item>\n    <ion-icon name="moon" item-start></ion-icon>\n    <ion-label color="dark">{{\'Night Mode\' | translate}}</ion-label>\n    <ion-toggle color="gpsc" [(ngModel)]="nightMode" (ngModelChange)="update($event)"></ion-toggle>\n  </ion-item>\n\n  <ion-item>\n    <!-- <img item-start src="assets/icon/traffic-light.png" style="width: 1.65em;"  /> -->\n    <ion-icon name="walk" item-start></ion-icon>\n    <ion-label color="dark">{{\'Traffic Mode\' | translate}}</ion-label>\n    <ion-toggle color="gpsc" [(ngModel)]="trafficMode" (ngModelChange)="trafficUpdate($event)"></ion-toggle>\n  </ion-item>\n\n  <ion-item>\n    <!-- <img item-start src="assets/icon/traffic-light.png" style="width: 1.65em;"  /> -->\n    <ion-icon name="car" item-start></ion-icon>\n    <ion-label color="dark">{{\'Display Vehicle Name\' | translate}}</ion-label>\n    <ion-toggle color="gpsc" [(ngModel)]="vehName" (ngModelChange)="vehNameUpdate($event)"></ion-toggle>\n  </ion-item>\n\n  <ion-item (click)="showAlert()">\n    <ion-icon name="lock" item-start></ion-icon>\n    <ion-label>{{\'Set Immobilize Password\' | translate}}</ion-label>\n  </ion-item>\n\n  <!-- <ion-item (click)="notifSet()" *ngIf="islogin.isSuperAdmin || islogin.isDealer">\n    <ion-icon name="notifications-outline" item-start></ion-icon>\n    <ion-label color="dark">{{\'Notification Settings\' | translate}}</ion-label>\n  </ion-item> -->\n  <ion-item (click)="notifSet()">\n    <ion-icon name="notifications-outline" item-start></ion-icon>\n    <ion-label color="dark">{{\'Notification Settings\' | translate}}</ion-label>\n  </ion-item>\n\n  <ion-item>\n    <ion-icon name="custom-lawyer" item-start></ion-icon>\n    <ion-label color="dark">{{\'Measurement Type\' | translate}}</ion-label>\n    <ion-select [(ngModel)]="measurementType" (ionChange)="onChangeMeasure(measurementType)">\n      <ion-option value="MKS" [selected]="measurementType === \'MKS\' ? true : false">MKS</ion-option>\n      <ion-option value="FPS" [selected]="measurementType === \'FPS\' ? true : false">FPS</ion-option>\n    </ion-select>\n  </ion-item>\n\n  <ion-item>\n    <ion-icon name="cash" item-start></ion-icon>\n    <ion-label color="dark">{{\'Currency Setting\' | translate}}</ion-label>\n    <ion-select [(ngModel)]="currencyValue" (ionChange)="onChangeCurrency(currencyValue)">\n      <ion-option *ngFor="let item of currencyArray" [value]="item.currencyVal">{{ item.country }}</ion-option>\n    </ion-select>\n  </ion-item>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/profile/settings/settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_text_to_speech__["a" /* TextToSpeech */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_5__providers_app_languages__["a" /* AppLanguages */]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ })

});
//# sourceMappingURL=33.js.map