webpackJsonp([67],{

/***/ 1044:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateDevicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_sms__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_android_permissions__ = __webpack_require__(194);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UpdateDevicePage = /** @class */ (function () {
    function UpdateDevicePage(apiCall, viewCtrl, formBuilder, navPar, toastCtrl, alerCtrl, sms, androidPermissions) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.sms = sms;
        this.androidPermissions = androidPermissions;
        this.allGroup = [];
        this.devicedetail = {};
        this.vehData = navPar.get("vehData");
        console.log("vehicle data=> ", this.vehData.device_model._id);
        // this.apiCall.getDevModelById(this.updatevehForm.value._id)
        //   .subscribe(data => {
        //     console.log("data--->---sms---->",data)
        //     this.device_timezone =data.device_timezone;
        //     this.sms_apn =data.sms_apn;
        //     this.sms_ip =data.sms_ip;
        //   })
        this.islogin = JSON.parse(localStorage.getItem("details")) || {};
        console.log("islogin devices => " + this.islogin);
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);
        this.currentYear = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(tempdate), "DD-MM-YYYY").format("YYYY-MM-DD");
        if (this.vehData.expiration_date == null) {
            this.eDate = this.currentYear;
        }
        else {
            this.eDate = __WEBPACK_IMPORTED_MODULE_3_moment__(this.vehData.expiration_date).format("YYYY-MM-DD");
        }
        var d_type, v_type, g_name;
        if (this.vehData.device_model != undefined) {
            d_type = this.vehData.device_model["device_type"];
        }
        else {
            d_type = "";
        }
        if (this.vehData.user != undefined) {
            // this.d_user = this.vehData.user;
            this.d_user = this.vehData.user["first_name"];
        }
        else {
            this.d_user = "";
        }
        debugger;
        if (this.vehData.vehicleType != undefined ||
            this.vehData.iconType != undefined) {
            v_type = this.vehData.vehicleType
                ? this.vehData.vehicleType["brand"]
                : this.vehData.iconType;
        }
        else {
            v_type = "";
        }
        if (this.vehData.vehicleGroup != undefined) {
            g_name = this.vehData.vehicleGroup["name"];
        }
        else {
            g_name = "";
        }
        // ============== one month later date from current date ================
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = "0" + dd;
        }
        if (mm < 10) {
            mm = "0" + mm;
        }
        today = yyyy + "-" + mm + "-" + dd;
        this.minDate = today;
        console.log("minimum date: ", today);
        // =============== end
        this.updatevehForm = formBuilder.group({
            device_name: [this.vehData.Device_Name, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            device_id: [this.vehData.Device_ID, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            driver: [this.vehData.driver_name],
            sim_number: [this.vehData.sim_number, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            contact_number: [this.vehData.contact_number, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"]],
            SpeedLimit: [this.vehData.SpeedLimit],
            ExipreDate: [this.eDate, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            gName: [g_name],
            device_type: [d_type],
            first_name: [this.d_user],
            brand: [v_type],
        });
        console.log("date format=> " +
            __WEBPACK_IMPORTED_MODULE_3_moment__(this.vehData.expiration_date).format("YYYY-MM-DD"));
    }
    UpdateDevicePage.prototype.ngOnInit = function () {
        this.getGroup();
        this.getDeviceModel();
        this.getSelectUser();
        this.getVehicleType();
    };
    // sendSms() {
    //     let options = {
    //       replaceLineBreaks: false, // true to replace \n by a new line, false by default
    //       android: {
    //           intent: ''  // send SMS with the native android SMS messaging
    //           // intent: '' // send SMS without opening any other app
    //       }
    //   };
    //     this.sms.send('+917737383167', 'SMS Works', options).then(val => {
    //       alert('It works');
    //     });
    //   }
    UpdateDevicePage.prototype.checkSMSPermission = function () {
        var _this = this;
        this.androidPermissions
            .checkPermission(this.androidPermissions.PERMISSION.SEND_SMS)
            .then(function (result //alert(result.hasPermission),
        ) {
            return function (err) {
                return _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.SEND_SMS);
            };
        });
    };
    UpdateDevicePage.prototype.requestSMSPermission = function () {
        // tslint:disable-next-line: max-line-length
        this.androidPermissions.requestPermissions([
            this.androidPermissions.PERMISSION.SEND_SMS,
            this.androidPermissions.PERMISSION.BROADCAST_SMS,
        ]);
    };
    UpdateDevicePage.prototype.sendSms = function () {
        var _this = this;
        this.checkSMSPermission();
        // Check mobile number
        if (this.updatevehForm.controls.sim_number.value) {
            // First Message
            //alert(this.smdData.sms_ip+ ' --- '+ this.smdData.sms_apn+ ' -- '+  this.smdData.sms_timezone)
            if (this.smdData && this.smdData.sms_ip) {
                this.onSendsms(this.updatevehForm.controls.sim_number.value, this.smdData.sms_ip);
            }
            // Second Message
            if (this.smdData && this.smdData.sms_apn) {
                setTimeout(function () {
                    _this.onSendsms(_this.updatevehForm.controls.sim_number.value, _this.smdData.sms_apn);
                }, 5000);
            }
            // Third Message
            if (this.smdData && this.smdData.sms_timezone) {
                setTimeout(function () {
                    _this.onSendsms(_this.updatevehForm.controls.sim_number.value, _this.smdData.sms_timezone);
                }, 10000);
            }
        }
    };
    UpdateDevicePage.prototype.onSendsms = function (mobileNumber, sms) {
        var options = {
            replaceLineBreaks: true,
            android: {
                // intent: 'INTENT'  // send SMS with the native android SMS messaging
                intent: "",
            },
        };
        this.sms
            .send('+91' + mobileNumber, sms, options)
            .then(function () {
            alert("message has been sent");
        })
            .catch(function (error) {
            alert(error);
        });
    };
    // sendSms(){
    //     var options = {
    //         replaceLineBreaks: false, // true to replace \n by a new line, false by default
    //         android: {
    //             intent: 'INTENT'  // send SMS with the native android SMS messaging
    //             //intent: '' // send SMS without opening any other app, require : android.permission.SEND_SMS and android.permission.READ_PHONE_STATE
    //         }
    //     };
    //     let success = function () { alert('Message sent successfully'); };
    //     let error = function (e) { alert('Message Failed:' + e); };
    //     this.sms.send('+917737383167', 'Hello world!');
    //     alert('sendSms');
    // }
    UpdateDevicePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UpdateDevicePage.prototype.updateDevices = function () {
        var _this = this;
        var that = this;
        that.submitAttempt = true;
        if (that.updatevehForm.valid) {
            ///////////////////////////
            // debugger;
            console.log("form valuess=> ", that.updatevehForm.value);
            if (that.updatevehForm.value.device_type != "") {
                if (that.vehData.device_model != null ||
                    that.vehData.device_model != undefined) {
                    if (that.updatevehForm.value.device_type ==
                        that.vehData.device_model["device_type"]) {
                        that.modeldata_id = that.vehData.device_model["_id"];
                    }
                    else {
                        that.modeldata_id = that.modeldata._id;
                    }
                }
                else {
                    if (that.modeldata._id != undefined) {
                        that.modeldata_id = that.modeldata._id;
                    }
                    else {
                        that.modeldata_id = null;
                    }
                }
            }
            else {
                that.modeldata_id = null;
            }
            if (that.updatevehForm.value.gName != "") {
                if (that.vehData.vehicleGroup != null ||
                    that.vehData.vehicleGroup != undefined) {
                    if (that.updatevehForm.value.gName == that.vehData.vehicleGroup["name"]) {
                        that.groupstaus_id = that.vehData.vehicleGroup["_id"];
                    }
                    else {
                        that.groupstaus_id = that.groupstaus._id;
                    }
                }
                else {
                    if (that.groupstaus._id != undefined) {
                        that.groupstaus_id = that.groupstaus._id;
                    }
                    else {
                        that.groupstaus_id = null;
                    }
                }
            }
            else {
                that.groupstaus_id = null;
            }
            if (that.updatevehForm.value.first_name != "") {
                if (that.vehData.user != null || that.vehData.user != undefined) {
                    if (that.updatevehForm.value.first_name ==
                        that.vehData.user["first_name"]) {
                        that.userdata_id = that.vehData.user["_id"];
                    }
                    else {
                        that.userdata_id = that.userdata._id;
                    }
                }
                else {
                    if (that.userdata._id != undefined) {
                        that.userdata_id = that.userdata._id;
                    }
                    else {
                        that.userdata_id = null;
                    }
                }
            }
            else {
                that.userdata_id = null;
            }
            if (that.updatevehForm.value.brand != "") {
                debugger;
                if (that.vehData.iconType != null ||
                    that.vehData.iconType != undefined ||
                    that.vehData.vehicleType != null ||
                    that.vehData.vehicleType != undefined) {
                    if (that.updatevehForm.value.brand == this.vehData.vehicleType
                        ? this.vehData.vehicleType["brand"]
                        : this.vehData.iconType) {
                        that.vehicleType_id = that.updatevehForm.value.brand._id;
                    }
                    else {
                        that.vehicleType_id = that.vehicleType._id;
                    }
                }
                else {
                    if (that.vehicleType_id != undefined) {
                        that.vehicleType_id = that.vehicleType._id;
                    }
                    else {
                        that.vehicleType_id = null;
                    }
                }
            }
            else {
                that.vehicleType_id = null;
            }
            if (that.updatevehForm.value.SpeedLimit != "") {
                that.speed = that.updatevehForm.value.SpeedLimit;
            }
            else {
                that.speed = 1;
            }
            ///////////////////////////
            that.devicedetail = {
                _id: that.vehData._id,
                devicename: that.updatevehForm.value.device_name,
                drname: that.updatevehForm.value.driver,
                deviceid: that.updatevehForm.value.device_id,
                sim: that.updatevehForm.value.sim_number,
                iconType: null,
                dphone: that.updatevehForm.value.contact_number,
                speed: that.speed,
                vehicleGroup: that.groupstaus_id,
                device_model: that.modeldata_id,
                expdate: new Date(that.updatevehForm.value.ExipreDate).toISOString(),
                user: that.userdata_id,
                vehicleType: that.vehicleType_id,
                updatedBy: that.islogin._id,
            };
            console.log("all details=> " + that.devicedetail);
            that.apiCall.startLoading().present();
            that.apiCall.deviceupdateCall(that.devicedetail).subscribe(function (data) {
                console.log("dataataa---detailssss--->", data);
                that.apiCall.stopLoading();
                var editdata = data;
                console.log("editdata=> " + editdata);
                var toast = that.toastCtrl.create({
                    message: editdata.message,
                    position: "top",
                    duration: 2000,
                });
                toast.onDidDismiss(function () {
                    console.log("Dismissed toast");
                    that.viewCtrl.dismiss(editdata);
                });
                toast.present();
            }, function (err) {
                _this.apiCall.stopLoading();
                var body = err._body;
                var msg = JSON.parse(body);
                console.log("error occured 1=> ", msg);
                var alert = _this.alerCtrl.create({
                    title: "Oops!",
                    message: "Device is already into system. Please delete from system and then try again.",
                    buttons: ["OK"],
                });
                alert.present();
            });
        }
    };
    UpdateDevicePage.prototype.deviceModelata = function (deviceModel) {
        // debugger
        console.log("deviceModel" + this.updatevehForm.value.device_type);
        this.modeldata = this.updatevehForm.value.device_type;
        console.log("modal data device_type=> " + this.modeldata.device_type);
    };
    UpdateDevicePage.prototype.GroupStatusdata = function (status) {
        console.log(status);
        this.groupstaus = status;
        console.log("groupstaus=> " + this.groupstaus._id);
    };
    UpdateDevicePage.prototype.userselectData = function (userselect) {
        console.log(userselect);
        this.userdata = this.updatevehForm.value.first_name;
        console.log("userdata=> " + this.userdata.first_name);
    };
    UpdateDevicePage.prototype.vehicleTypeselectData = function (vehicletype) {
        // debugger
        console.log(this.updatevehForm.value.brand._id);
        this.vehicleType = this.updatevehForm.value.brand;
        console.log("vehType=> " + this.vehicleType._id);
    };
    UpdateDevicePage.prototype.getGroup = function () {
        var _this = this;
        console.log("get group");
        var baseURLp = this.apiCall.mainUrl + "groups/getGroups_list?uid=" + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.groupsCall(baseURLp).subscribe(function (data) {
            _this.apiCall.stopLoading();
            if (data["group_details"] !== undefined) {
                if (data["group_details"].length > 0) {
                    _this.allGroup = data["group_details"];
                    for (var i = 0; i < _this.allGroup.length; i++) {
                        _this.allGroupName = _this.allGroup[i].name;
                        // console.log("allGroupName=> "+this.allGroupName);
                    }
                    console.log("allGroupName=> " + _this.allGroupName);
                }
            }
        }, function (err) {
            console.log(err);
            _this.apiCall.stopLoading();
        });
    };
    UpdateDevicePage.prototype.getDeviceModel = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + "deviceModel/getDeviceModel";
        this.apiCall.getDeviceModelCall(baseURLp).subscribe(function (data) {
            _this.deviceModel = data;
        }, function (err) {
            console.log(err);
        });
        this.apiCall
            .getDevModelById(this.vehData.device_model._id)
            .subscribe(function (data) {
            if (data && data.length) {
                _this.smdData = data[0];
            }
        });
    };
    UpdateDevicePage.prototype.getSelectUser = function () {
        var _this = this;
        console.log("get user");
        var baseURLp = this.apiCall.mainUrl + "users/getAllUsers?dealer=" + this.islogin._id;
        this.apiCall.getAllUsersCall(baseURLp).subscribe(function (data) {
            _this.selectUser = data;
            console.log("selected user=> ", _this.selectUser);
        }, function (error) {
            console.log(error);
        });
    };
    UpdateDevicePage.prototype.getVehicleType = function () {
        var _this = this;
        console.log("get getVehicleType");
        var baseURLp = this.apiCall.mainUrl +
            "vehicleType/getVehicleTypes?user=" +
            this.islogin._id;
        // this.apiCall.startLoading().present();
        this.apiCall.getVehicleTypesCall(baseURLp).subscribe(function (data) {
            // this.apiCall.stopLoading();
            _this.allVehicle = data;
            console.log("all vehicles=> ", _this.allVehicle);
        }, function (err) {
            console.log(err);
            // this.apiCall.stopLoading();
        });
    };
    UpdateDevicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "page-update-device",template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/add-devices/update-device/update-device.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{ "Update Vehicle Details" | translate }}</ion-title>\n    <ion-buttons end>\n          <button ion-button icon-only (click)="sendSms()">\n            <ion-icon name="arrow-round-forward"></ion-icon>\n        </button>\n      <button ion-button icon-only (click)="dismiss()">\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <form [formGroup]="updatevehForm">\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important"\n        >{{ "Registration Number*" | translate }}</ion-label\n      >\n      <ion-input\n        formControlName="device_name"\n        type="text"\n        style="margin-left: 2px"\n      ></ion-input>\n    </ion-item>\n    <ion-item\n      class="logitem1"\n      *ngIf="!updatevehForm.controls.device_name.valid && (updatevehForm.controls.device_name.dirty || submitAttempt)"\n    >\n      <p>{{ "registration number is required!" | translate }}</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important"\n        >{{ "IMEI" | translate }} {{ "id" | translate }}*</ion-label\n      >\n      <ion-input\n        formControlName="device_id"\n        type="text"\n        style="margin-left: 2px"\n      ></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important"\n        >{{ "Sim Number*" | translate }}</ion-label\n      >\n      <ion-input\n        formControlName="sim_number"\n        type="number"\n        style="margin-left: 2px"\n      ></ion-input>\n    </ion-item>\n    <ion-item\n      class="logitem1"\n      *ngIf="!updatevehForm.controls.sim_number.valid && (updatevehForm.controls.sim_number.dirty || submitAttempt)"\n    >\n      <p>{{ "sim number is required." | translate }}</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important"\n        >{{ "Driver Name" | translate }}</ion-label\n      >\n      <ion-input\n        formControlName="driver"\n        type="text"\n        style="margin-left: 2px"\n      ></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important"\n        >{{ "Driver\'s Number" | translate }}</ion-label\n      >\n      <ion-input\n        formControlName="contact_number"\n        type="number"\n        maxlength="10"\n        minlength="10"\n        style="margin-left: 2px"\n      ></ion-input>\n    </ion-item>\n    <ion-item\n      class="logitem1"\n      *ngIf="!updatevehForm.controls.contact_number.valid && (updatevehForm.controls.contact_number.dirty || submitAttempt)"\n    >\n      <p>{{ "mobile number sould be 10 digits!" | translate }}</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important"\n        >{{ "Speed Limit" | translate }}</ion-label\n      >\n      <ion-input\n        formControlName="SpeedLimit"\n        type="number"\n        style="margin-left: 2px"\n      ></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important"\n        >{{ "Expiry Date:" | translate }}*</ion-label\n      >\n      <ion-input\n        type="date"\n        formControlName="ExipreDate"\n        min="{{minDate}}"\n        style="margin-left: 2px"\n      ></ion-input>\n    </ion-item>\n    <ion-item\n      class="logitem1"\n      *ngIf="!updatevehForm.controls.ExipreDate.valid && (updatevehForm.controls.ExipreDate.dirty || submitAttempt)"\n    >\n      <p>{{ "expiry date is required!" | translate }}</p>\n    </ion-item>\n\n    <!-- <ion-item>\n          <ion-label>{{ "Device Model" | translate }}</ion-label>\n          <ion-select formControlName="device_type" style="min-width:49%;">\n              <ion-option *ngFor="let deviceModelname of deviceModel" [value]="deviceModelname.device_type" (ionSelect)="deviceModelata(deviceModelname)">{{deviceModelname.device_type}}</ion-option>\n          </ion-select>\n      </ion-item> -->\n    <ion-item *ngIf="!onEditModel">\n      <ion-label fixed style="min-width: 50% !important"\n        >{{ "Selected Device Model" | translate }}</ion-label\n      >\n      <ion-input\n        formControlName="device_type"\n        type="text"\n        style="margin-left: 20px"\n      ></ion-input>\n      <ion-icon\n        item-end\n        name="create"\n        (click)="(onEditModel = true)"\n        style="font-size: 1.3em"\n      ></ion-icon>\n    </ion-item>\n    <ion-item *ngIf="onEditModel">\n      <ion-label>{{\'Device Model\'| translate}}</ion-label>\n      <select-searchable\n        item-content\n        formControlName="device_type"\n        [items]="deviceModel"\n        itemValueField="device_type"\n        itemTextField="device_type"\n        [canSearch]="true"\n        (onChange)="deviceModelata(device_type)"\n        style="min-width: 49%"\n      >\n      </select-searchable>\n    </ion-item>\n    <ion-item *ngIf="!onEditGroup">\n      <ion-label fixed style="min-width: 50% !important"\n        >{{ "Selected Group" | translate }}</ion-label\n      >\n      <ion-input\n        formControlName="gName"\n        type="text"\n        style="margin-left: 20px"\n      ></ion-input>\n      <ion-icon\n        item-end\n        name="create"\n        (click)="(onEditGroup = true)"\n        style="font-size: 1.3em"\n      ></ion-icon>\n    </ion-item>\n    <ion-item *ngIf="onEditGroup">\n      <ion-label>{{ "Group" | translate }}</ion-label>\n      <ion-select formControlName="gName" style="min-width: 49%">\n        <ion-option\n          *ngFor="let groupname of allGroup"\n          [value]="groupname.name"\n          (ionSelect)="GroupStatusdata(groupname)"\n          >{{groupname.name}}</ion-option\n        >\n      </ion-select>\n    </ion-item>\n    <ion-item *ngIf="!onEditUser">\n      <ion-label fixed style="min-width: 50% !important"\n        >{{ "Selected User" | translate }}</ion-label\n      >\n      <ion-input\n        formControlName="first_name"\n        type="text"\n        style="margin-left: 20px"\n      ></ion-input>\n      <ion-icon\n        item-end\n        name="create"\n        (click)="(onEditUser = true)"\n        style="font-size: 1.3em"\n      ></ion-icon>\n    </ion-item>\n    <ion-item *ngIf="onEditUser">\n      <!-- <ion-label>{{ "Select User" | translate }}</ion-label>\n          <ion-select formControlName="first_name" style="min-width:49%;">\n              <ion-option *ngFor="let user of selectUser" [value]="user.first_name" (ionSelect)="userselectData(user)">{{user.first_name}}</ion-option>\n          </ion-select> -->\n      <ion-label>{{ "Select User" | translate }}</ion-label>\n      <select-searchable\n        item-content\n        formControlName="first_name"\n        [items]="selectUser"\n        itemValueField="first_name"\n        itemTextField="first_name"\n        [canSearch]="true"\n        (onChange)="userselectData(first_name)"\n        style="min-width: 49%"\n      >\n      </select-searchable>\n    </ion-item>\n\n    <ion-item *ngIf="!onEditVehicle">\n      <ion-label fixed style="min-width: 50% !important"\n        >{{ "Selected Vehicle Type" | translate }}</ion-label\n      >\n      <ion-input\n        formControlName="brand"\n        type="text"\n        style="margin-left: 20px"\n      ></ion-input>\n      <ion-icon\n        item-end\n        name="create"\n        (click)="(onEditVehicle = true)"\n        style="font-size: 1.3em"\n      ></ion-icon>\n    </ion-item>\n    <ion-item *ngIf="onEditVehicle">\n      <ion-label>{{\'Vehicle Type\'| translate}}</ion-label>\n      <select-searchable\n        item-content\n        formControlName="brand"\n        [items]="allVehicle"\n        itemValueField="brand"\n        itemTextField="brand"\n        [canSearch]="true"\n        (onChange)="vehicleTypeselectData(brand)"\n        style="min-width: 49%"\n      >\n      </select-searchable>\n    </ion-item>\n  </form>\n</ion-content>\n\n<ion-footer class="footSty">\n  <ion-toolbar>\n    <ion-row>\n      <ion-col style="text-align: center">\n        <button ion-button clear color="light" (click)="updateDevices()">\n          {{ "UPDATE VEHICLE DETAILS" | translate }}\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/add-devices/update-device/update-device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_sms__["a" /* SMS */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_android_permissions__["a" /* AndroidPermissions */]])
    ], UpdateDevicePage);
    return UpdateDevicePage;
}());

//# sourceMappingURL=update-device.js.map

/***/ }),

/***/ 954:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateDevicePageModule", function() { return UpdateDevicePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__update_device__ = __webpack_require__(1044);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var UpdateDevicePageModule = /** @class */ (function () {
    function UpdateDevicePageModule() {
    }
    UpdateDevicePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__update_device__["a" /* UpdateDevicePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__update_device__["a" /* UpdateDevicePage */]),
                __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ]
        })
    ], UpdateDevicePageModule);
    return UpdateDevicePageModule;
}());

//# sourceMappingURL=update-device.module.js.map

/***/ })

});
//# sourceMappingURL=67.js.map