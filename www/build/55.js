webpackJsonp([55],{

/***/ 1062:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDealerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddDealerPage = /** @class */ (function () {
    function AddDealerPage(navCtrl, navParams, formBuilder, apicall, alerCtrl, viewCtrl, toastCtrl, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.apicall = apicall;
        this.alerCtrl = alerCtrl;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.translate = translate;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.addDealerform = formBuilder.group({
            userId: [this.islogin.phn, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            Firstname: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            LastName: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            emailid: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email],
            contact_num: [''],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            cpassword: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            address: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    AddDealerPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter AddDealerPage');
    };
    AddDealerPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddDealerPage.prototype._submit = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.addDealerform.valid) {
            if (this.addDealerform.value.password != this.addDealerform.value.cpassword) {
                var toast = this.toastCtrl.create({
                    message: this.translate.instant('Password mismatched!'),
                    duration: 1500,
                    position: 'middle'
                });
                toast.present();
            }
            else {
                if (this.addDealerform.value.contact_num == "") {
                    var toast = this.toastCtrl.create({
                        message: this.translate.instant('Enter mobile number'),
                        duration: 1500,
                        position: 'middle'
                    });
                    toast.present();
                }
                else {
                    var payload = {
                        address: this.addDealerform.value.address,
                        custumer: false,
                        email: this.addDealerform.value.emailid,
                        first_name: this.addDealerform.value.Firstname,
                        isDealer: true,
                        last_name: this.addDealerform.value.LastName,
                        org_name: this.islogin._orgName,
                        password: this.addDealerform.value.password,
                        phone: this.addDealerform.value.contact_num,
                        supAdmin: this.islogin._id,
                        sysadmin: this.islogin.isSuperAdmin,
                        user_id: this.addDealerform.value.userId
                    };
                    this.apicall.startLoading().present();
                    this.apicall.signupApi(payload)
                        .subscribe(function (data) {
                        _this.apicall.stopLoading();
                        var toast = _this.toastCtrl.create({
                            message: _this.translate.instant('dealeradded', { value: _this.translate.instant('Dealers') }),
                            position: 'top',
                            duration: 1500
                        });
                        toast.onDidDismiss(function () {
                            _this.viewCtrl.dismiss();
                        });
                        toast.present();
                    }, function (err) {
                        _this.apicall.stopLoading();
                        var body = err._body;
                        var msg = JSON.parse(body);
                        var namepass = [];
                        namepass = msg.split(":");
                        var name = namepass[1];
                        var alert = _this.alerCtrl.create({
                            message: name,
                            buttons: [_this.translate.instant('Okay')]
                        });
                        alert.present();
                    });
                }
            }
        }
    };
    AddDealerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-customer-model',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/dealers/add-dealer/add-dealer.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{\'Add Dealer\' | translate}}</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="dismiss()">\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n<ion-content>\n  <form [formGroup]="addDealerform">\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">{{\'User ID*\' | translate}}</ion-label>\n      <ion-input formControlName="userId" type="text"></ion-input>\n    </ion-item>\n\n    <ion-item class="logitem1" *ngIf="!addDealerform.controls.userId.valid && (addDealerform.controls.userId.dirty || submitAttempt)">\n      <p>{{\'Enter User ID\' | translate}}</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">{{\'First Name*\' | translate}}</ion-label>\n      <ion-input formControlName="Firstname" type="text"></ion-input>\n    </ion-item>\n    <ion-item class="logitem1" *ngIf="!addDealerform.controls.Firstname.valid && (addDealerform.controls.Firstname.dirty || submitAttempt)">\n      <p>{{\'Please enter First Name\' | translate}}</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">{{\'Last Name*\' | translate}}</ion-label>\n      <ion-input formControlName="LastName" type="text"></ion-input>\n    </ion-item>\n    <ion-item class="logitem1" *ngIf="!addDealerform.controls.LastName.valid && (addDealerform.controls.LastName.dirty || submitAttempt)">\n      <p>{{\'Please enter Last Name\' | translate}}</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">{{\'Email ID\' | translate}}</ion-label>\n      <ion-input formControlName="emailid" type="email"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">{{\'Mobile Number*\' | translate}}</ion-label>\n      <ion-input formControlName="contact_num" type="number" maxlength="10" minlength="10"></ion-input>\n    </ion-item>\n    <ion-item class="logitem1" *ngIf="!addDealerform.controls.contact_num.valid && (addDealerform.controls.contact_num.dirty || submitAttempt)">\n      <p>{{\'mobile number required and should be 10 digits!\' | translate}}</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">{{\'Password*\' | translate}}</ion-label>\n      <ion-input formControlName="password" type="password"></ion-input>\n    </ion-item>\n    <ion-item class="logitem1" *ngIf="!addDealerform.controls.password.valid && (addDealerform.controls.password.dirty || submitAttempt)">\n      <p>{{\'Invalid Password\' | translate}}</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">{{\'Confirm Password*\' | translate}}</ion-label>\n      <ion-input formControlName="cpassword" type="password"></ion-input>\n    </ion-item>\n    <ion-item class="logitem1" *ngIf="!addDealerform.controls.cpassword.valid && (addDealerform.controls.cpassword.dirty || submitAttempt)">\n      <p>{{\'Please enter Confirm Password\' | translate}}</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed style="min-width: 50% !important;">{{\'Address*\' | translate}}</ion-label>\n      <ion-input formControlName="address" type="text"></ion-input>\n    </ion-item>\n    <ion-item class="logitem1" *ngIf="!addDealerform.controls.address.valid && (addDealerform.controls.address.dirty || submitAttempt)">\n      <p>{{\'Address required!\' | translate}}</p>\n    </ion-item>\n  </form>\n</ion-content>\n<ion-footer class="footSty">\n  <ion-toolbar>\n    <ion-row no-padding>\n      <ion-col width-50 style="text-align: center;">\n        <button ion-button clear color="light" (click)="_submit()">{{\'SUBMIT\' | translate}}</button>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/dealers/add-dealer/add-dealer.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */]])
    ], AddDealerPage);
    return AddDealerPage;
}());

//# sourceMappingURL=add-dealer.js.map

/***/ }),

/***/ 971:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDealerPageModule", function() { return AddDealerPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_dealer__ = __webpack_require__(1062);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AddDealerPageModule = /** @class */ (function () {
    function AddDealerPageModule() {
    }
    AddDealerPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_dealer__["a" /* AddDealerPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__add_dealer__["a" /* AddDealerPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], AddDealerPageModule);
    return AddDealerPageModule;
}());

//# sourceMappingURL=add-dealer.module.js.map

/***/ })

});
//# sourceMappingURL=55.js.map