webpackJsonp([19],{

/***/ 1009:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PoiListPageModule", function() { return PoiListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__poi_list__ = __webpack_require__(1102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dummy_directive__ = __webpack_require__(1103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var PoiListPageModule = /** @class */ (function () {
    function PoiListPageModule() {
    }
    PoiListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__poi_list__["a" /* PoiListPage */],
                __WEBPACK_IMPORTED_MODULE_4__dummy_directive__["a" /* OnCreate */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__poi_list__["a" /* PoiListPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_4__dummy_directive__["a" /* OnCreate */]
            ],
        })
    ], PoiListPageModule);
    return PoiListPageModule;
}());

//# sourceMappingURL=poi-list.module.js.map

/***/ }),

/***/ 1102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PoiListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_geocoder_geocoder__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PoiListPage = /** @class */ (function () {
    function PoiListPage(navCtrl, navParams, apiCall, modalCtrl, toastCtrl, alertCtrl, events, translate, geocoderApi) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.translate = translate;
        this.geocoderApi = geocoderApi;
        this.poilist = [];
        this.allData = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.events.subscribe('reloadpoilist', function () {
            _this.poilist = [];
            _this.getpois();
        });
    }
    PoiListPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter PoiListPage');
    };
    PoiListPage.prototype.ngOnInit = function () {
        this.getpois();
    };
    PoiListPage.prototype.getpois = function () {
        var _this = this;
        this.apiCall.getPoisAPI(this.islogin._id)
            .subscribe(function (data) {
            _this.poilist = data;
            _this.allData._poiListData = [];
            var i = 0, howManyTimes = data.length;
            var that = _this;
            function f() {
                that.allData._poiListData.push({
                    "poiname": (data[i].poi.poiname ? data[i].poi.poiname : null),
                    "radius": data[i].radius ? data[i].radius : 'N/A',
                    "_id": data[i]._id,
                    "start_location": {
                        'lat': data[i].poi.location.coordinates[1],
                        'long': data[i].poi.location.coordinates[0]
                    }
                });
                // debugger
                that.start_address(that.allData._poiListData[i], i);
                that.allData._poiListData[that.allData._poiListData.length - 1].mapid = "a" + i;
                that.allData.map = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["b" /* GoogleMaps */].create(that.allData._poiListData[that.allData._poiListData.length - 1].mapid, {
                    camera: {
                        target: {
                            lat: data[i].poi.location.coordinates[1],
                            lng: data[i].poi.location.coordinates[0]
                        },
                        zoom: 12
                    }
                });
                that.allData.map.addMarker({
                    position: {
                        lat: data[i].poi.location.coordinates[1],
                        lng: data[i].poi.location.coordinates[0]
                    }
                });
                i++;
                if (i < howManyTimes) {
                    setTimeout(f, 200);
                }
            }
            f();
        }, function (err) {
            console.log("error in pois: ", err);
        });
    };
    PoiListPage.prototype.start_address = function (item, index) {
        var that = this;
        that.allData._poiListData[index].StartLocation = "N/A";
        if (!item.start_location) {
            that.allData._poiListData[index].StartLocation = "N/A";
        }
        else if (item.start_location) {
            this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
                .then(function (res) {
                console.log("test", res);
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.allData._poiListData[index].addres = str;
            });
        }
    };
    PoiListPage.prototype.editPOI = function (data) {
        console.log("edited data: " + JSON.stringify(data));
        this.navCtrl.push('AddPoiPage', {
            "param": data
        });
    };
    PoiListPage.prototype.deletePOI = function (data) {
        var _this = this;
        console.log("delete data: ", data);
        var alert = this.alertCtrl.create({
            message: this.translate.instant('Do you want to delete this', { value: 'POI' }),
            buttons: [{
                    text: this.translate.instant('Back')
                }, {
                    text: this.translate.instant('YES PROCEED'),
                    handler: function () {
                        _this.apiCall.startLoading().present();
                        _this.apiCall.deletePOIAPI(data._id)
                            .subscribe(function (data) {
                            _this.apiCall.stopLoading();
                            var toast = _this.toastCtrl.create({
                                message: _this.translate.instant('Deleted successfully.', { value: 'POI' }),
                                position: "top",
                                duration: 2000
                            });
                            toast.present();
                            _this.getpois();
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            console.log("error occured while deleting POI: ", err);
                            var toast = _this.toastCtrl.create({
                                message: _this.translate.instant('Deleted successfully.', { value: 'POI' }),
                                position: "top",
                                duration: 2000
                            });
                            toast.present();
                            _this.getpois();
                        });
                    }
                }]
        });
        alert.present();
    };
    PoiListPage.prototype.addPOI = function () {
        this.navCtrl.push('AddPoiPage');
    };
    PoiListPage.prototype.getStaticImage = function (lat, lng, zoom) {
        return "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lng + "&zoom=" + zoom + "&size=600x300&maptype=roadmap&markers=color:red%7Clabel:Place%7C" + lat + "," + lng + "&key=AIzaSyA2meHC0Wgy8gn8eozMGDs-TI-oBan-V4k";
        // return `https://maps.googleapis.com/maps/api/staticmap?center=Brooklyn+Bridge,New+York,NY&zoom=13&size=600x300&maptype=roadmap
        // &markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318
        // &markers=color:red%7Clabel:C%7C40.718217,-73.998284
        // &key=AIzaSyA2meHC0Wgy8gn8eozMGDs-TI-oBan-V4k`
    };
    PoiListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-poi-list',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/poi-list/poi-list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "POI List" | translate }}</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only clear (tap)="addPOI()">\n        <ion-icon color="light" name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-card *ngFor="let list of allData._poiListData; let i=index">\n    <ion-item>\n      <ion-thumbnail item-start>\n        <!-- <img class="loaction-image" [src]="list.staticImageUrl" /> -->\n        <div id="{{ list.mapid }}" style="height: 100px; width: 100px;"></div>\n      </ion-thumbnail>\n      <h1>{{ list.poiname }}</h1>\n      <p style="color: #687DCA">{{ "Radius:" | translate }} {{ list.radius }}</p>\n      <!-- <p> -->\n      <ion-row>\n        <ion-col col-1>\n          <ion-icon name="pin"></ion-icon>\n        </ion-col>\n        <ion-col col-11 (onCreate)="start_address(list, i)">\n          <!-- <div class="overme"> -->\n            <p style="margin: 0px;">{{ list.addres }}</p>\n          <!-- </div> -->\n        </ion-col>\n      </ion-row>\n      <!-- </p> -->\n      <p>\n        <button ion-button round small (click)="editPOI(list)">\n          {{ "Edit" | translate }}\n        </button>\n        <button ion-button round small color="gpsc" (click)="deletePOI(list)">\n          {{ "Delete" | translate }}\n        </button>\n      </p>\n    </ion-item>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/poi-list/poi-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_5__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], PoiListPage);
    return PoiListPage;
}());

//# sourceMappingURL=poi-list.js.map

/***/ }),

/***/ 1103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnCreate; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OnCreate = /** @class */ (function () {
    function OnCreate() {
        this.onCreate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    OnCreate.prototype.ngOnInit = function () {
        this.onCreate.emit('dummy');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], OnCreate.prototype, "onCreate", void 0);
    OnCreate = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[onCreate]'
        }),
        __metadata("design:paramtypes", [])
    ], OnCreate);
    return OnCreate;
}());

//# sourceMappingURL=dummy-directive.js.map

/***/ })

});
//# sourceMappingURL=19.js.map