webpackJsonp([51],{

/***/ 1072:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpenseTypeDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ExpenseTypeDetailPage = /** @class */ (function () {
    function ExpenseTypeDetailPage(navCtrl, navParams, apiCall, toastCtrl, translate, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.translate = translate;
        this.alertCtrl = alertCtrl;
        this.expensesData = [];
        this.portstemp = [];
        if (!navParams.get('expense')) {
            return;
        }
        this.expense = navParams.get('expense');
        this.type = this.expense._id;
        this.dateFrom = navParams.get('dateFrom');
        this.dateTo = navParams.get('dateTo');
        this.userId = navParams.get('userId');
        this.portstemp = navParams.get('portstemp');
        this.vehId = navParams.get('vehId');
    }
    ExpenseTypeDetailPage.prototype.ionViewWillEnter = function () {
        this.getExpenseList();
    };
    ExpenseTypeDetailPage.prototype.toastMessage = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'bottom'
        });
        toast.present();
    };
    ExpenseTypeDetailPage.prototype.getExpenseList = function () {
        var _this = this;
        this.expensesData = [];
        var _Url;
        if (this.vehId != undefined) {
            _Url = this.apiCall.mainUrl + "expense/tripdetailbyType?user=" + this.userId + "&fdate=" + new Date(this.dateFrom).toISOString() + "&tdate=" + new Date(this.dateTo).toISOString() + "&type=" + this.expense._id + "&vehicle=" + this.vehId;
        }
        else {
            _Url = this.apiCall.mainUrl + "expense/tripdetailbyType?user=" + this.userId + "&fdate=" + new Date(this.dateFrom).toISOString() + "&tdate=" + new Date(this.dateTo).toISOString() + "&type=" + this.expense._id;
        }
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(_Url)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            if (data.message === 'No Expence list found') {
                _this.toastMessage(_this.translate.instant('Expense list not found..'));
                return;
            }
            _this.expensesData = data;
            console.log("response data: ", data);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    ExpenseTypeDetailPage.prototype.delete = function (exp) {
        var _this = this;
        var alert = this.alertCtrl.create({
            message: this.translate.instant('Do you want to delete this', { value: this.translate.instant('Expenses') }),
            buttons: [
                {
                    text: this.translate.instant('YES PROCEED'),
                    handler: function () {
                        var _burl = _this.apiCall.mainUrl + "expense/deleteExpense?_id=" + exp._id;
                        _this.apiCall.startLoading().present();
                        _this.apiCall.getSOSReportAPI(_burl)
                            .subscribe(function (data) {
                            _this.apiCall.stopLoading();
                            if (data) {
                                _this.toastMessage(_this.translate.instant('Deleted successfully.', { value: _this.translate.instant('Expenses') }));
                                _this.navCtrl.pop();
                            }
                            console.log("deleted expense data: ", data);
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            _this.toastMessage(_this.translate.instant('Deleted successfully.', { value: _this.translate.instant('Expenses') }));
                            _this.navCtrl.pop();
                        });
                    }
                },
                {
                    text: this.translate.instant('Back')
                }
            ]
        });
        alert.present();
    };
    ExpenseTypeDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-expense-type-detail',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/expenses/expense-type-detail/expense-type-detail.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{ type | titlecase }} {{ "Expenses" | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col *ngIf="expensesData.length > 0">\n        <ion-card *ngFor="let exp of expensesData">\n          <ion-card-header>\n            <ion-row>\n              <ion-col col-8>\n                <h5>{{ exp.vehicle.Device_Name }}</h5>\n              </ion-col>\n              <ion-col col-4 text-right>\n                <ion-icon\n                  name="trash"\n                  color="danger"\n                  (click)="delete(exp)"\n                ></ion-icon>\n              </ion-col>\n            </ion-row>\n          </ion-card-header>\n          <ion-card-content>\n            <ion-row style="color:#009688;font-size: 11px;font-weight: 350;">\n              <ion-col col-3>{{ "Amount" | translate }}</ion-col>\n              <ion-col col-3>{{ "Currency" | translate }}</ion-col>\n              <ion-col col-3>{{ "Payment Mode" | translate }}</ion-col>\n              <ion-col col-3>{{ "Expense Type" | translate }}</ion-col>\n            </ion-row>\n            <ion-row style="color:gray; font-size:13px; font-weight: 400;">\n              <ion-col col-3>{{ exp.amount ? exp.amount : "N/A" }}</ion-col>\n              <ion-col col-3>{{ exp.currency }}</ion-col>\n              <ion-col col-3>{{ exp.payment_mode }}</ion-col>\n              <ion-col col-3>{{ exp.type }}</ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n      <ion-col *ngIf="expensesData.length === 0">\n        {{ "Expense list not found.." | translate }}\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/expenses/expense-type-detail/expense-type-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], ExpenseTypeDetailPage);
    return ExpenseTypeDetailPage;
}());

//# sourceMappingURL=expense-type-detail.js.map

/***/ }),

/***/ 980:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpenseTypeDetailPageModule", function() { return ExpenseTypeDetailPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__expense_type_detail__ = __webpack_require__(1072);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ExpenseTypeDetailPageModule = /** @class */ (function () {
    function ExpenseTypeDetailPageModule() {
    }
    ExpenseTypeDetailPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__expense_type_detail__["a" /* ExpenseTypeDetailPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__expense_type_detail__["a" /* ExpenseTypeDetailPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], ExpenseTypeDetailPageModule);
    return ExpenseTypeDetailPageModule;
}());

//# sourceMappingURL=expense-type-detail.module.js.map

/***/ })

});
//# sourceMappingURL=51.js.map