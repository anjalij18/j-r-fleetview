webpackJsonp([59],{

/***/ 1056:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDeviceModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_android_permissions__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_sms__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_barcode_scanner__ = __webpack_require__(555);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { GroupModalPage } from "./group-modal/group-modal";




var AddDeviceModalPage = /** @class */ (function () {
    function AddDeviceModalPage(params, viewCtrl, formBuilder, apiCall, toastCtrl, alerCtrl, modalCtrl, androidPermissions, sms, barcodeScanner) {
        this.params = params;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.modalCtrl = modalCtrl;
        this.androidPermissions = androidPermissions;
        this.sms = sms;
        this.barcodeScanner = barcodeScanner;
        this.allGroup = [];
        this.devicedetails = {};
        this.isCustomer = false;
        // console.log("very", this.params);
        // console.log("cust data", params.get("custDet"))
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);
        this.currentYear = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log(this.currentYear);
        // ============== one month later date from current date ================
        var tdate = new Date();
        var eightMonthsFromJan312009 = tdate.setMonth(tdate.getMonth() + 1);
        this.minDate = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log("one one later date=> " + __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD"));
        // =============== end
        this.addvehicleForm = formBuilder.group({
            device_name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            device_id: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            driver: [''],
            contact_num: [''],
            sim_number: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            ExipreDate: [this.currentYear, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            device_type: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            name: [''],
            first_name: [''],
            brand: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            fastag: ['']
        });
        if (params.get("custDet")) {
            this.isCustomer = true;
            this.islogin = params.get("custDet");
            // this.islogin = JSON.parse(localStorage.getItem('details')) || {};
            console.log("custDel=> ", this.islogin);
            this.rootLogin = JSON.parse(localStorage.getItem('details')) || {};
            // this.rootLogin = params.get("custDet");
        }
        else {
            this.islogin = JSON.parse(localStorage.getItem('details')) || {};
            console.log("islogin devices => " + JSON.stringify(this.islogin));
        }
        console.log("cheack imp data", this.rootLogin);
    }
    AddDeviceModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddDeviceModalPage.prototype.ngOnInit = function () {
        this.getGroup();
        this.getDeviceModel();
        this.getSelectUser();
        this.getVehicleType();
        this.getDrivers();
    };
    AddDeviceModalPage.prototype.scan = function () {
        var _this = this;
        this.barcodeScanner.scan().then(function (barcodeData) {
            console.log('Barcode data', barcodeData);
            _this.addvehicleForm.patchValue({
                device_id: barcodeData.text
            });
        }).catch(function (err) {
            console.log('Error', err);
        });
    };
    AddDeviceModalPage.prototype.checkPermissionAndSend = function () {
        var _this = this;
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.SEND_SMS).then(function (success) {
            if (!success.hasPermission) {
                _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.SEND_SMS).
                    then(function (success) {
                    // alert('hi')
                    _this.sendMessage();
                }, function (err) {
                    // alert("error1")
                    // alert(err);
                });
            }
        }, function (err) {
            _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.SEND_SMS).
                then(function (success) {
                // alert('success')
                _this.sendMessage();
            }, function (err) {
                // alert('error2')
                // alert(err);
            });
        });
    };
    AddDeviceModalPage.prototype.sendMessage = function () {
        var _this = this;
        // debugger
        if (this.sms) {
            //CONFIGURATION
            var options = {
                replaceLineBreaks: false,
                android: {
                    intent: 'INTENT' // send SMS with the native android SMS messaging
                    //intent: '' // send SMS without opening any other app
                }
            };
            this.sms.hasPermission()
                .then(function (perm) {
                if (perm) {
                    console.log("sms permission granted!!");
                    _this.sms.send(_this.addvehicleForm.value.sim_number, '', options);
                }
                else {
                    _this.sms.send(_this.addvehicleForm.value.sim_number, '', options);
                    console.log("sms permission not granted!!");
                }
            });
        }
    };
    AddDeviceModalPage.prototype.getDrivers = function () {
        var _this = this;
        this.apiCall.getDriverList(this.islogin._id)
            .subscribe(function (data) {
            _this.driverList = data;
            console.log("driver list => " + JSON.stringify(_this.driverList));
        }, function (err) {
            console.log(err);
        });
    };
    AddDeviceModalPage.prototype.openAddGroupModal = function () {
        var modal = this.modalCtrl.create('GroupModalPage');
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
        });
        modal.present();
    };
    // {
    //     "devicename": "hjvhj",
    //     "deviceid": "097987978686",
    //     "hrdwr": 987678667,
    //     "typdev": "Tracker",
    //     "emailid": "gaurav.gupta@adnate.in",
    //     "Mileage": "10",
    //     "sim_number": 987678667,
    //     "supAdmin": "59cbbdbe508f164aa2fef3d8",
    //     "device_model": "5baf3e857a0c5b647f2ed27a",
    //     "expdate": "2021-01-27T18:30:00.000Z",
    //     "user": "59cbbdbe508f164aa2fef3d8",
    //     "sim_provider": "jhvhgch",
    //     "vehicleType": "5b483afb72a00817af5a5079"
    //  }
    AddDeviceModalPage.prototype.adddevices = function () {
        var _this = this;
        debugger;
        this.submitAttempt = true;
        if (this.addvehicleForm.valid) {
            console.log(this.addvehicleForm.value);
            if (this.params.get("custDet")) {
                if (this.rootLogin.isSuperAdmin == true) {
                    this.devicedetails = {
                        "devicename": this.addvehicleForm.value.device_name,
                        "deviceid": this.addvehicleForm.value.device_id,
                        "driver_name": this.addvehicleForm.value.driver,
                        "contact_number": this.addvehicleForm.value.contact_num,
                        "typdev": "Tracker",
                        "sim_number": this.addvehicleForm.value.sim_number,
                        "user": (this.userdata ? this.userdata._id : this.islogin._id),
                        "emailid": this.islogin.email,
                        // "iconType": this.TypeOf_Device,
                        // "vehicleGroup": this.groupstaus_id,
                        "device_model": this.modeldata_id,
                        "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                        "supAdmin": this.rootLogin._id
                    };
                }
                else {
                    if (this.rootLogin.isDealer == true) {
                        this.devicedetails = {
                            "devicename": this.addvehicleForm.value.device_name,
                            "deviceid": this.addvehicleForm.value.device_id,
                            "driver_name": this.addvehicleForm.value.driver,
                            "contact_number": this.addvehicleForm.value.contact_num,
                            "typdev": "Tracker",
                            "sim_number": this.addvehicleForm.value.sim_number,
                            "user": (this.userdata ? this.userdata._id : this.islogin._id),
                            "emailid": this.islogin.email,
                            // "iconType": this.TypeOf_Device,
                            // "vehicleGroup": this.groupstaus_id,
                            "device_model": this.modeldata_id,
                            "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                            "supAdmin": this.rootLogin.supAdmin,
                            "Dealer": this.rootLogin._id
                        };
                    }
                }
                if (this.addvehicleForm.value.fastag !== null || this.addvehicleForm.value.fastag !== undefined
                    || this.addvehicleForm.value.fastag !== "") {
                    this.devicedetails.fastag = this.addvehicleForm.value.fastag;
                }
                if (this.groupstaus != undefined) {
                    this.groupstaus_id = this.groupstaus._id;
                    this.devicedetails.vehicleGroup = this.groupstaus_id;
                }
                if (this.vehType == undefined) {
                    this.devicedetails;
                }
                else {
                    this.devicedetails.vehicleType = this.vehType._id;
                }
            }
            else {
                if (this.islogin.isSuperAdmin == true) {
                    this.devicedetails = {
                        "devicename": this.addvehicleForm.value.device_name,
                        "deviceid": this.addvehicleForm.value.device_id,
                        "driver_name": this.addvehicleForm.value.driver,
                        "contact_number": this.addvehicleForm.value.contact_num,
                        "typdev": "Tracker",
                        "sim_number": this.addvehicleForm.value.sim_number,
                        "user": (this.userdata ? this.userdata._id : this.islogin._id),
                        "emailid": this.islogin.email,
                        // "vehicleType": this.vehType,
                        // "vehicleGroup": this.groupstaus_id,
                        "device_model": this.modeldata_id,
                        "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                        "supAdmin": this.islogin._id
                    };
                }
                else {
                    if (this.islogin.isDealer == true) {
                        this.devicedetails = {
                            "devicename": this.addvehicleForm.value.device_name,
                            "deviceid": this.addvehicleForm.value.device_id,
                            "driver_name": this.addvehicleForm.value.driver,
                            "contact_number": this.addvehicleForm.value.contact_num,
                            "typdev": "Tracker",
                            "sim_number": this.addvehicleForm.value.sim_number,
                            "user": (this.userdata ? this.userdata._id : this.islogin._id),
                            "emailid": this.islogin.email,
                            // "vehicleType": this.vehType,
                            // "vehicleGroup": this.groupstaus_id,
                            "device_model": this.modeldata_id,
                            "expdate": new Date(this.addvehicleForm.value.ExipreDate).toISOString(),
                            "supAdmin": this.islogin.supAdmin,
                            "Dealer": this.islogin._id
                        };
                    }
                }
                if (this.addvehicleForm.value.fastag != null || this.addvehicleForm.value.fastag != undefined
                    || this.addvehicleForm.value.fastag != "") {
                    this.devicedetails.fastag = this.addvehicleForm.value.fastag;
                }
                if (this.groupstaus != undefined) {
                    this.groupstaus_id = this.groupstaus._id;
                    this.devicedetails.vehicleGroup = this.groupstaus_id;
                }
                if (this.vehType == undefined) {
                    this.devicedetails;
                }
                else {
                    this.devicedetails.vehicleType = this.vehType._id;
                }
            }
            console.log("devicedetails=> " + this.devicedetails);
            this.apiCall.startLoading().present();
            this.apiCall.addDeviceCall(this.devicedetails)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                _this.devicesadd = data;
                console.log("devicesadd=> ", _this.devicesadd);
                var toast = _this.toastCtrl.create({
                    message: 'Vehicle was added successfully',
                    position: 'top',
                    duration: 1500
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    _this.viewCtrl.dismiss(_this.vehType);
                });
                toast.present();
            }, function (err) {
                _this.apiCall.stopLoading();
                var body = err._body;
                var msg = JSON.parse(body);
                console.log("error occured 1=> ", msg);
                var alert = _this.alerCtrl.create({
                    title: 'Oops!',
                    message: 'Device is already into system. Please delete from system and then try again.',
                    buttons: ['OK']
                });
                alert.present();
            });
        }
    };
    AddDeviceModalPage.prototype.getGroup = function () {
        var _this = this;
        console.log("get group");
        var baseURLp = this.apiCall.mainUrl + 'groups/getGroups_list?uid=' + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.groupsCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.allGroup = data["group_details"];
        }, function (err) {
            console.log(err);
            _this.apiCall.stopLoading();
        });
    };
    AddDeviceModalPage.prototype.getDeviceModel = function () {
        var _this = this;
        console.log("getdevices");
        var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDeviceModel';
        this.apiCall.getDeviceModelCall(baseURLp)
            .subscribe(function (data) {
            _this.deviceModel = data;
        }, function (err) {
            console.log(err);
        });
    };
    AddDeviceModalPage.prototype.getSelectUser = function () {
        var _this = this;
        console.log("get user");
        var baseURLp = this.apiCall.mainUrl + 'users/getAllUsers?dealer=' + this.islogin._id;
        this.apiCall.getAllUsersCall(baseURLp)
            .subscribe(function (data) {
            _this.selectUser = data;
        }, function (error) {
            console.log(error);
        });
    };
    AddDeviceModalPage.prototype.getVehicleType = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'vehicleType/getVehicleTypes?user=' + this.islogin._id;
        this.apiCall.getVehicleTypesCall(baseURLp)
            .subscribe(function (data) {
            _this.allVehicle = data;
        }, function (err) {
            console.log(err);
        });
    };
    AddDeviceModalPage.prototype.deviceModelata = function (deviceModel) {
        debugger;
        // if (deviceModel != undefined) {
        this.modeldata = this.addvehicleForm.value.device_type;
        this.modeldata_id = this.modeldata._id;
        // } else {
        //     this.modeldata_id = null;
        // }
    };
    AddDeviceModalPage.prototype.GroupStatusdata = function (status) {
        this.groupstaus = status;
        console.log("groupstaus=> " + this.groupstaus._id);
    };
    AddDeviceModalPage.prototype.userselectData = function (userselect) {
        this.userdata = userselect;
        console.log("userdata=> " + this.userdata.first_name);
        console.log("user", this.userdata._id);
    };
    AddDeviceModalPage.prototype.vehicleTypeselectData = function (vehicletype) {
        debugger;
        // this.vehType = vehicletype;
        this.vehType = this.addvehicleForm.value.brand;
        console.log("vehType=> " + this.vehType._id);
    };
    AddDeviceModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-devices',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/customers/modals/add-device-modal.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>{{\'Add Vehicle\' | translate}}</ion-title>\n        <ion-buttons end>\n            <button ion-button icon-only (click)="dismiss()">\n                <ion-icon name="close-circle"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n\n</ion-header>\n<ion-content>\n    <form [formGroup]="addvehicleForm" style="padding-right: 16px;">\n        <ion-row>\n            <ion-col col-10 style="padding: 0px;">\n                <ion-item>\n                    <ion-label stacked>{{\'Device ID(IMEI)*\' | translate}}</ion-label>\n                    <ion-input formControlName="device_id" type="text"></ion-input>\n                </ion-item>\n                <ion-item class="logitem1"\n                    *ngIf="!addvehicleForm.controls.device_id.valid && (addvehicleForm.controls.device_id.dirty || submitAttempt)">\n                    <p>{{\'device id required!\' | translate}}</p>\n                </ion-item>\n            </ion-col>\n            <ion-col col-2 style="margin: auto; text-align: right;">\n                <ion-icon (click)="scan()" name="barcode" style="font-size: 2.2em;"></ion-icon>\n            </ion-col>\n        </ion-row>\n\n        <ion-item>\n            <ion-label stacked>{{\'Registration Number*\' | translate}}</ion-label>\n            <ion-input formControlName="device_name" type="text"></ion-input>\n        </ion-item>\n        <ion-item class="logitem1"\n            *ngIf="!addvehicleForm.controls.device_name.valid && (addvehicleForm.controls.device_name.dirty || submitAttempt)">\n            <p>{{\'device name required!\' | translate}}</p>\n        </ion-item>\n        <div *ngIf="!addvehicleForm.controls.sim_number.valid">\n            <ion-item>\n                <ion-label stacked>{{\'Sim Number*\'| translate}}</ion-label>\n                <ion-input formControlName="sim_number" type="number"></ion-input>\n            </ion-item>\n            <ion-item class="logitem1"\n                *ngIf="!addvehicleForm.controls.sim_number.valid && (addvehicleForm.controls.sim_number.dirty || submitAttempt)">\n                <!-- <p>{{\'sim number is required & sould be 10 or 13 digits!\' | translate}}</p> -->\n                <p>{{\'sim number is required!\' | translate}}</p>\n            </ion-item>\n        </div>\n\n        <ion-row *ngIf="addvehicleForm.controls.sim_number.valid">\n            <ion-col col-10 style="padding: 0px;">\n                <ion-item>\n                    <ion-label stacked>{{\'Sim Number*\'| translate}}</ion-label>\n                    <ion-input formControlName="sim_number" type="number"></ion-input>\n                </ion-item>\n                <ion-item class="logitem1"\n                    *ngIf="!addvehicleForm.controls.sim_number.valid && (addvehicleForm.controls.sim_number.dirty || submitAttempt)">\n                    <!-- <p>{{\'sim number is required & sould be 10 or 13 digits!\' | translate}}</p> -->\n                    <p>{{\'sim number is required!\' | translate}}</p>\n                </ion-item>\n            </ion-col>\n            <ion-col col-2 style="margin: auto; text-align: right;">\n                <ion-icon name="text" style="font-size: 2.3em;" (click)="checkPermissionAndSend()"></ion-icon>\n            </ion-col>\n        </ion-row>\n\n        <ion-item>\n            <ion-label stacked>{{\'Expire On*\' | translate}}</ion-label>\n            <ion-input type="date" formControlName="ExipreDate" min="{{minDate}}">\n            </ion-input>\n        </ion-item>\n        <ion-item class="logitem1"\n            *ngIf="!addvehicleForm.controls.ExipreDate.valid && (addvehicleForm.controls.ExipreDate.dirty || submitAttempt)">\n            <p>{{\'date of expiry required!\' | translate}}</p>\n        </ion-item>\n\n        <!-- <ion-item>\n            <ion-label stacked>{{\'Device Model\'| translate}}*</ion-label>\n            <ion-select formControlName="device_type">\n                <ion-option *ngFor="let name of deviceModel" [value]="name.device_type"\n                    (ionSelect)="deviceModelata(name)">{{name.device_type}}</ion-option>\n            </ion-select>\n        </ion-item>\n        <ion-item class="logitem1"\n            *ngIf="!addvehicleForm.controls.device_type.valid && (addvehicleForm.controls.device_type.dirty || submitAttempt)">\n            <p>{{\'device model required!\'| translate}}</p>\n        </ion-item> -->\n\n        <ion-item>\n            <ion-label stacked>{{\'Device Model\'| translate}}*</ion-label>\n            <select-searchable item-content formControlName="device_type" [items]="deviceModel"\n                itemValueField="device_type" itemTextField="device_type" [canSearch]="true"\n                (onChange)="deviceModelata(device_type)" style="padding: 0px;margin-top: -1%;">\n            </select-searchable>\n        </ion-item>\n\n        <ion-item class="logitem1"\n            *ngIf="!addvehicleForm.controls.device_type.valid && (addvehicleForm.controls.device_type.dirty || submitAttempt)">\n            <p>{{\'device model required!\'| translate}}</p>\n        </ion-item>\n        <ion-item *ngIf="allGroup.length > 0">\n            <ion-label stacked>{{\'Group\' | translate}}</ion-label>\n            <ion-select formControlName="name">\n                <ion-option *ngFor="let group of allGroup" [value]="group.name" (ionSelect)="GroupStatusdata(group)">\n                    {{group.name}}</ion-option>\n            </ion-select>\n        </ion-item>\n\n        <ion-item *ngIf="!isCustomer">\n            <ion-label stacked>{{\'Select User\'| translate}}</ion-label>\n            <ion-select formControlName="first_name">\n                <ion-option *ngFor="let user of selectUser" [value]="user.first_name"\n                    (ionSelect)="userselectData(user)">{{user.first_name | titlecase}} {{user.last_name | titlecase}}\n                </ion-option>\n            </ion-select>\n        </ion-item>\n\n        <!-- <ion-item>\n            <ion-label stacked>{{\'Vehicle Type\' | translate}}*</ion-label>\n            <ion-select formControlName="brand">\n                <ion-option *ngFor="let veh of allVehicle" [value]="veh.brand" (ionSelect)="vehicleTypeselectData(veh)">\n                    {{veh.brand}}</ion-option>\n            </ion-select>\n        </ion-item> -->\n        <ion-item>\n            <ion-label stacked>{{\'Vehicle Type\'| translate}}*</ion-label>\n            <select-searchable item-content formControlName="brand" [items]="allVehicle"\n                itemValueField="brand" itemTextField="brand" [canSearch]="true"\n                (onChange)="vehicleTypeselectData(brand)" style="padding: 0px;margin-top: -1%;">\n            </select-searchable>\n        </ion-item>\n        <ion-item class="logitem1"\n            *ngIf="!addvehicleForm.controls.brand.valid && (addvehicleForm.controls.brand.dirty || submitAttempt)">\n            <p>{{\'vehicle type is required required!\' | translate}}</p>\n        </ion-item>\n        <!-- <ion-item>\n            <ion-label stacked>{{\'Fast tag Id\' | translate}}</ion-label>\n            <ion-input formControlName="fastag" type="text"></ion-input>\n        </ion-item> -->\n    </form>\n\n</ion-content>\n\n<ion-footer class="footSty">\n    <ion-toolbar>\n        <ion-row no-padding>\n            <ion-col width-50 style="border-right: 1px solid white; text-align: center; padding: 0px !important">\n                <button ion-button style="padding: 0px;" clear color="light"\n                    (click)="openAddGroupModal()">{{\'ADD NEW GROUP\' | translate}}</button>\n            </ion-col>\n            <ion-col width-50 style="text-align: center; padding: 0px !important">\n                <button ion-button style="padding: 0px;" clear color="light"\n                    (click)="adddevices()">{{\'ADD VEHICLE\' | translate}}</button>\n            </ion-col>\n        </ion-row>\n    </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/customers/modals/add-device-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_sms__["a" /* SMS */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]])
    ], AddDeviceModalPage);
    return AddDeviceModalPage;
}());

//problem is that
//abhi aap device add krege na to if you select user than it will be added but agar hum select nahi krte hai
// to error aata hai
//because custDet undefined aa jaa rha hai because humne kuch bheja hi nahi add-devics.ts
//add button ke click pr
//# sourceMappingURL=add-device-modal.js.map

/***/ }),

/***/ 966:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDeviceModalPageModule", function() { return AddDeviceModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_device_modal__ = __webpack_require__(1056);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var AddDeviceModalPageModule = /** @class */ (function () {
    function AddDeviceModalPageModule() {
    }
    AddDeviceModalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_device_modal__["a" /* AddDeviceModalPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__add_device_modal__["a" /* AddDeviceModalPage */]),
                __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ]
        })
    ], AddDeviceModalPageModule);
    return AddDeviceModalPageModule;
}());

//# sourceMappingURL=add-device-modal.module.js.map

/***/ })

});
//# sourceMappingURL=59.js.map