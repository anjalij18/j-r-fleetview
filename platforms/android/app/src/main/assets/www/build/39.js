webpackJsonp([39],{

/***/ 1001:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LivePageModule", function() { return LivePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__live__ = __webpack_require__(1096);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ion_bottom_drawer__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var LivePageModule = /** @class */ (function () {
    function LivePageModule() {
    }
    LivePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__live__["a" /* LivePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__live__["a" /* LivePage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__node_modules_ion_bottom_drawer__["b" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], LivePageModule);
    return LivePageModule;
}());

//# sourceMappingURL=live.module.js.map

/***/ }),

/***/ 1096:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LivePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__(565);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_Subscription__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_Subscription___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_Subscription__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__live_single_device_live_single_device__ = __webpack_require__(550);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_geocoder_geocoder__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var LivePage = /** @class */ (function () {
    function LivePage(navCtrl, navParams, apiCall, actionSheetCtrl, elementRef, socialSharing, alertCtrl, modalCtrl, plt, viewCtrl, translate, toastCtrl, geocoderApi) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.elementRef = elementRef;
        this.socialSharing = socialSharing;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.plt = plt;
        this.viewCtrl = viewCtrl;
        this.translate = translate;
        this.toastCtrl = toastCtrl;
        this.geocoderApi = geocoderApi;
        // countFlag: number = 0;
        this.navigateButtonColor = '#ee612e'; //Default Color
        this.navColor = '#fff';
        this.policeButtonColor = '#ee612e';
        this.policeColor = '#fff';
        this.petrolButtonColor = "#ee612e";
        this.petrolColor = '#fff';
        this.shouldBounce = true;
        this.dockedHeight = 80;
        this.distanceTop = 200;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.states = __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__["a" /* DrawerState */];
        this.minimumHeight = 50;
        this.transition = '0.85s ease-in-out';
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.data = {};
        this.socketSwitch = {};
        this.socketChnl = [];
        this.socketData = {};
        this.allData = {};
        this.titleText = 'multiple';
        this.isEnabled = false;
        this.showMenuBtn = false;
        this.mapHideTraffic = false;
        this.mapData = [];
        this.geodata = [];
        this.geoShape = [];
        this.locations = [];
        this.shwBckBtn = false;
        this.deviceDeatils = {};
        this.showaddpoibtn = false;
        this.zoomLevel = 18;
        this.tempMarkArray = [];
        this.nearbyPolicesArray = [];
        this.nearbyPetrolArray = [];
        this.socketurl = "https://www.oneqlik.in";
        this.displayNames = true;
        this.car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
        this.carIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [12.5, 12.5],
        };
        this.icons = {
            "car": this.carIcon,
            "bike": this.carIcon,
            "truck": this.carIcon,
            "bus": this.carIcon,
            "user": this.carIcon,
            "jcb": this.carIcon,
            "tractor": this.carIcon,
            "ambulance": this.carIcon,
            "auto": this.carIcon
        };
        this.resumeListener = new __WEBPACK_IMPORTED_MODULE_9_rxjs_Subscription__["Subscription"]();
        this.hideMe = false;
        this.expectation = {};
        this.polyLines = [];
        this.marksArray = [];
        this.showStreetMap = true;
        this.streetviewButtonColor = '#ee612e';
        this.streetColor = '#fff';
        if (localStorage.getItem('Total_Vech') !== null) {
            this.t_veicle_count = JSON.parse(localStorage.getItem('Total_Vech'));
        }
        this.callBaseURL();
        var selectedMapKey;
        if (localStorage.getItem('MAP_KEY') != null) {
            selectedMapKey = localStorage.getItem('MAP_KEY');
            if (selectedMapKey == this.translate.instant('Hybrid')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
            else if (selectedMapKey == this.translate.instant('Normal')) {
                this.mapKey = 'MAP_TYPE_NORMAL';
            }
            else if (selectedMapKey == this.translate.instant('Terrain')) {
                this.mapKey = 'MAP_TYPE_TERRAIN';
            }
            else if (selectedMapKey == this.translate.instant('Satellite')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
        }
        else {
            this.mapKey = 'MAP_TYPE_NORMAL';
        }
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> " + JSON.stringify(this.userdetails));
        this.menuActive = false;
        if (localStorage.getItem('DisplayVehicleName') !== null) {
            if (localStorage.getItem('DisplayVehicleName') == 'OFF') {
                this.displayNames = false;
            }
        }
    }
    LivePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.plt.is('ios')) {
            this.shwBckBtn = true;
            this.viewCtrl.showBackButton(false);
        }
        this.plt.ready().then(function () {
            _this.resumeListener = _this.plt.resume.subscribe(function () {
                var today, Christmas;
                today = new Date();
                Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
                var diffMs = (today - Christmas); // milliseconds between now & Christmas
                var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
                if (diffMins >= 5) {
                    localStorage.removeItem("backgroundModeTime");
                    _this.alertCtrl.create({
                        message: _this.translate.instant("Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?"),
                        buttons: [
                            {
                                text: _this.translate.instant('YES PROCEED'),
                                handler: function () {
                                    _this.refreshMe();
                                }
                            },
                            {
                                text: _this.translate.instant('Back'),
                                handler: function () {
                                    _this.navCtrl.setRoot('DashboardPage');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    };
    LivePage.prototype.ionViewWillLeave = function () {
        var _this = this;
        this.plt.ready().then(function () {
            _this.resumeListener.unsubscribe();
        });
    };
    LivePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.navBar.backButtonClick = function (ev) {
            console.log('this will work in Ionic 3 +');
            _this.hideMe = true;
            _this.navCtrl.pop({
                animate: true, animation: 'transition-ios', direction: 'back'
            });
        };
        if (localStorage.getItem("navigationFrom") === null) {
            this.initiateMap(); // at first we will load the map..
        }
        if (localStorage.getItem("CLICKED_ALL") != null) {
            this.showMenuBtn = true;
        }
        if (localStorage.getItem("SCREEN") != null) {
            if (localStorage.getItem("SCREEN") === 'live') {
                this.showMenuBtn = true;
            }
        }
        this.drawerState = __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__["a" /* DrawerState */].Bottom;
        this.onClickShow = false;
        this.showBtn = false;
        this.SelectVehicle = "Select Vehicle";
        this.selectedVehicle = undefined;
        this._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"](this.socketurl + '/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._io.on('connect', function (data) {
            console.log('Ignition IO Connected', data);
        });
        ////////////////
        this._io.on('connect_failed', function () {
            console.log("Sorry, there seems to be an issue with the connection!");
        });
        this._io.on('error', function () {
            console.log("socket io got error!");
        });
        this._io.on('reconnecting', function () {
            console.log("socket io trying to reconnect!");
        });
        this._io.on('reconnect_failed', function () {
            console.log("reconnecting failed!");
        });
        this._io.on('disconnect', function () {
            console.log('client socket-io disconnect!');
        });
    };
    LivePage.prototype.ngOnInit = function () { };
    LivePage.prototype.ngOnDestroy = function () {
        var _this = this;
        localStorage.removeItem("CLICKED_ALL");
        for (var i = 0; i < this.socketChnl.length; i++)
            this._io.removeAllListeners(this.socketChnl[i]);
        this._io.on('disconnect', function () {
            _this._io.open();
        });
    };
    LivePage.prototype.initiateMap = function () {
        if (this.allData.map) {
            this.allData.map.remove();
            this.allData.map = this.newMap();
        }
        else {
            this.allData.map = this.newMap();
        }
        /* Check if traffic mode is On/Off */
        if (localStorage.getItem('TrafficMode') !== null) {
            if (localStorage.getItem('TrafficMode') === 'ON') {
                this.mapHideTraffic = true;
                this.allData.map.setTrafficEnabled(true);
            }
        }
        this.callFunction();
    };
    LivePage.prototype.callFunction = function () {
        this.to = new Date().toISOString();
        var d = new Date();
        var a = d.setHours(0, 0, 0, 0);
        this.from = new Date(a).toISOString();
        this.userDevices();
        // if (Number(this.t_veicle_count) > 500) {
        //   this.userDevices();
        // } else {
        //   this.getDataFromSQLiteDB();
        // }
    };
    LivePage.prototype.newMap = function () {
        var mapOptions = {
            controls: {
                compass: false,
                zoom: false,
                myLocation: true,
                myLocationButton: false,
            },
            mapTypeControlOptions: {
                mapTypeIds: [__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].ROADMAP, 'map_style']
            },
            gestures: {
                rotate: false,
                tilt: false
            },
            mapType: this.mapKey
        };
        var map = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas', mapOptions);
        if (this.plt.is('android')) {
            map.setPadding(20, 20, 20, 20);
        }
        return map;
    };
    LivePage.prototype.goBack = function () {
        this.navCtrl.pop({ animate: true, direction: 'forward' });
    };
    LivePage.prototype.onClickMainMenu = function (item) {
        this.menuActive = !this.menuActive;
    };
    LivePage.prototype.refreshMe = function () {
        this.ngOnDestroy();
        this.ionViewDidEnter();
        // this.ngOnInit();
    };
    LivePage.prototype.refreshMe1 = function () {
        var that = this;
        this.ngOnDestroy();
        this._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"](this.socketurl + '/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        var paramData = that.data;
        this.socketInit(paramData);
    };
    LivePage.prototype.callBaseURL = function () {
        var _this = this;
        // debugger
        if (localStorage.getItem("ENTERED_BASE_URL") === null) {
            var url = "https://www.oneqlik.in/pullData/getUrlnew";
            this.apiCall.getSOSReportAPI(url)
                .subscribe(function (data) {
                console.log("base url: ", data);
                if (data.url) {
                    localStorage.setItem("BASE_URL", JSON.stringify(data.url));
                }
                if (data.socket) {
                    localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
                }
                _this.getSocketUrl();
            });
        }
    };
    LivePage.prototype.getSocketUrl = function () {
        if (localStorage.getItem('SOCKET_URL') !== null) {
            this.socketurl = JSON.parse(localStorage.getItem('SOCKET_URL'));
        }
    };
    LivePage.prototype.userDevices = function () {
        var baseURLp;
        var that = this;
        baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;
        if (this.userdetails.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.userdetails._id;
            this.isSuperAdmin = true;
        }
        else {
            if (this.userdetails.isDealer == true) {
                baseURLp += '&dealer=' + this.userdetails._id;
                this.isDealer = true;
            }
        }
        // that.apiCall.startLoading().present();
        that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (resp) {
            // that.apiCall.stopLoading();
            that.portstemp = resp.devices;
            // console.log("list of vehicles :", that.portstemp)
            that.mapData = [];
            that.mapData = resp.devices.map(function (d) {
                if (d.last_location !== undefined) {
                    return { lat: d.last_location['lat'], lng: d.last_location['long'] };
                }
            });
            var dummyData;
            dummyData = resp.devices.map(function (d) {
                if (d.last_location === undefined)
                    return;
                else {
                    return {
                        "position": {
                            "lat": d.last_location['lat'],
                            "lng": d.last_location['long']
                        },
                        "name": d.Device_Name,
                        "icon": {
                            "url": that.getIconUrl(d),
                            "size": {
                                "width": 20,
                                "height": 40
                            }
                        }
                    };
                }
            });
            dummyData = dummyData.filter(function (element) {
                return element !== undefined;
            });
            console.log("dummy data: ", dummyData);
            // console.log("dummy data: ", this.dummyData());
            var bounds = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["g" /* LatLngBounds */](that.mapData);
            that.allData.map.moveCamera({
                target: bounds,
                zoom: 10
            });
            debugger;
            if (that.isSuperAdmin || that.isDealer) {
                for (var t = 0; t < dummyData.length; t++) {
                    console.log("check position: ", dummyData[t].position);
                }
                that.addCluster(dummyData);
            }
            else {
                if (resp.devices.length > 10) {
                    console.log("customers are greater than 10");
                    that.addCluster(dummyData);
                }
                else {
                    for (var i = 0; i < resp.devices.length; i++) {
                        if (resp.devices[i].status != "Expired") {
                            if (localStorage.getItem("SocketReconnectingStart")) {
                                that.weDidntGetPing(resp.devices[i]);
                            }
                            else {
                                that.socketInit(resp.devices[i]);
                            }
                        }
                    }
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    LivePage.prototype.addCluster = function (data) {
        var _this = this;
        var small, large;
        if (this.plt.is('android')) {
            small = "./assets/markercluster/small.png";
            large = "./assets/markercluster/large.png";
        }
        else if (this.plt.is('ios')) {
            small = "www/assets/markercluster/small.png";
            large = "www/assets/markercluster/large.png";
        }
        var options = {
            markers: data,
            icons: [
                {
                    min: 3,
                    max: 9,
                    url: small,
                    size: {
                        height: 29,
                        width: 29
                    },
                    label: {
                        color: "white"
                    }
                },
                {
                    min: 10,
                    url: large,
                    size: {
                        height: 37,
                        width: 37
                    },
                    label: {
                        color: "white"
                    }
                }
            ],
            boundsDraw: false,
            maxZoomLevel: 15
        };
        // let markerCluster: MarkerCluster = this.allData.map.addMarkerClusterSync(options);
        this.allData.map.addMarkerCluster(options)
            .then(function (markercluster) {
            markercluster.on(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function (params) {
                var marker = params[1];
                marker.setTitle(marker.get("name"));
                marker.setSnippet(marker.get("address"));
                marker.showInfoWindow();
            });
            _this.saveMarkerCluster = markercluster;
        });
        // markerCluster.on(GoogleMapsEvent.MARKER_CLICK).subscribe((params) => {
        //   let marker: Marker = params[1];
        //   marker.setTitle(marker.get("name"));
        //   marker.setSnippet(marker.get("address"));
        //   marker.showInfoWindow();
        // });
    };
    LivePage.prototype.navigateFromCurrentLoc = function () {
        window.open("https://www.google.com/maps/dir/?api=1&destination=" + this.recenterMeLat + "," + this.recenterMeLng + "&travelmode=driving", "_blank");
    };
    LivePage.prototype.calcRoute = function (start, end) {
        this.allData.AIR_PORTS = [];
        var directionsService = new google.maps.DirectionsService();
        var that = this;
        var request = {
            origin: start,
            destination: end,
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING
        };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                var path = new google.maps.MVCArray();
                for (var i = 0, len = response.routes[0].overview_path.length; i < len; i++) {
                    path.push(response.routes[0].overview_path[i]);
                    that.allData.AIR_PORTS.push({
                        lat: path.g[i].lat(), lng: path.g[i].lng()
                    });
                    // debugger
                    if (that.allData.AIR_PORTS.length > 1) {
                        that.allData.map.addPolyline({
                            'points': that.allData.AIR_PORTS,
                            'color': '#4aa9d5',
                            'width': 4,
                            'geodesic': true,
                        }).then(function (poly) {
                            that.polyLines.push(poly);
                            that.getTravelDetails(start, end);
                            //   that.showBtn = true;
                        });
                    }
                }
            }
        });
    };
    // service = new google.maps.DistanceMatrixService();
    LivePage.prototype.getTravelDetails = function (source, dest) {
        var _this = this;
        var that = this;
        that.service = new google.maps.DistanceMatrixService();
        this._id = setInterval(function () {
            if (localStorage.getItem("livepagetravelDetailsObject") != null) {
                if (that.expectation.distance == undefined && that.expectation.duration == undefined) {
                    that.expectation = JSON.parse(localStorage.getItem("livepagetravelDetailsObject"));
                    console.log("expectation: ", that.expectation);
                }
                else {
                    clearInterval(_this._id);
                }
            }
        }, 3000);
        that.service.getDistanceMatrix({
            origins: [source],
            destinations: [dest],
            travelMode: 'DRIVING'
        }, that.callback);
        // that.apiCall.stopLoading();
    };
    LivePage.prototype.callback = function (response, status) {
        var travelDetailsObject;
        if (status == 'OK') {
            var origins = response.originAddresses;
            for (var i = 0; i < origins.length; i++) {
                var results = response.rows[i].elements;
                for (var j = 0; j < results.length; j++) {
                    var element = results[j];
                    var distance = element.distance.text;
                    var duration = element.duration.text;
                    travelDetailsObject = {
                        distance: distance,
                        duration: duration
                    };
                }
            }
            localStorage.setItem("livepagetravelDetailsObject", JSON.stringify(travelDetailsObject));
        }
    };
    LivePage.prototype.onClickMap = function (maptype) {
        var that = this;
        if (maptype == 'SATELLITE' || maptype == 'HYBRID') {
            // this.ngOnDestroy();
            that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].HYBRID);
            // this.someFunc();
        }
        else {
            if (maptype == 'TERRAIN') {
                // this.ngOnDestroy();
                that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].TERRAIN);
                // this.someFunc();
            }
            else {
                if (maptype == 'NORMAL') {
                    // this.ngOnDestroy();
                    that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].NORMAL);
                    // this.someFunc();
                }
            }
        }
    };
    LivePage.prototype.someFunc = function () {
        var runningDevices = [];
        var that = this;
        that._io = undefined;
        that._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"](this.socketurl + '/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        that._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        if (that.isSuperAdmin || that.isDealer) {
        }
        else {
            if (that.selectedVehicle === undefined) {
            }
            else if (that.selectedVehicle !== undefined) {
                that.socketInit(that.selectedVehicle);
            }
        }
    };
    LivePage.prototype.settings = function () {
        var _this = this;
        var that = this;
        var profileModal = this.modalCtrl.create('DeviceSettingsPage', {
            param: that.deviceDeatils
        });
        profileModal.present();
        profileModal.onDidDismiss(function () {
            for (var i = 0; i < that.socketChnl.length; i++)
                that._io.removeAllListeners(that.socketChnl[i]);
            var paramData = _this.navParams.get("device");
            _this.temp(paramData);
        });
    };
    LivePage.prototype.addPOI = function () {
        var _this = this;
        var that = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__live_single_device_live_single_device__["b" /* PoiPage */], {
            param1: that.allData[that.impkey]
        });
        modal.onDidDismiss(function (data) {
            console.log(data);
            var that = _this;
            that.showaddpoibtn = false;
        });
        modal.present();
    };
    LivePage.prototype.onSelectMapOption = function (type) {
        var that = this;
        if (type == 'locateme') {
            that.allData.map.setCameraTarget(that.latlngCenter);
        }
        else {
            if (type == 'mapHideTraffic') {
                // this.ngOnDestroy();
                that.mapHideTraffic = !that.mapHideTraffic;
                if (that.mapHideTraffic) {
                    that.allData.map.setTrafficEnabled(true);
                    // this.someFunc();
                }
                else {
                    that.allData.map.setTrafficEnabled(false);
                    // this.someFunc();
                }
            }
            else {
                if (type == 'showGeofence') {
                    console.log("Show Geofence");
                    if (that.generalPolygon != undefined) {
                        that.generalPolygon.remove();
                        that.generalPolygon = undefined;
                    }
                    else {
                        that.callGeofence();
                    }
                }
            }
        }
    };
    LivePage.prototype.showCluster = function () {
        var that = this;
        this.showStreetMap = !this.showStreetMap;
        if (this.streetviewButtonColor == '#ee612e') {
            this.streetviewButtonColor = '#fff';
            this.streetColor = '#ee612e';
        }
        else if (this.streetviewButtonColor == '#fff') {
            this.streetviewButtonColor = '#ee612e';
            this.streetColor = '#fff';
        }
        debugger;
        // if (this.showStreetMap) {
        this.saveMarkerCluster.remove();
        for (var i = 0; i < this.portstemp.length; i++) {
            if (this.portstemp[i].status != "Expired") {
                if (localStorage.getItem("SocketReconnectingStart")) {
                    that.weDidntGetPing(this.portstemp[i]);
                }
                else {
                    that.socketInit(this.portstemp[i]);
                }
            }
        }
        // } else {
        //   this.allData.map.remove();
        //   this.allData.map = this.newMap();
        //   this.callFunction()
        // }
    };
    LivePage.prototype.callGeofence = function () {
        var _this = this;
        this.geoShape = [];
        this.apiCall.startLoading().present();
        this.apiCall.getGeofenceCall(this.userdetails._id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("geofence data=> " + data.length);
            if (data.length > 0) {
                _this.geoShape = data.map(function (d) {
                    return d.geofence.coordinates[0];
                });
                for (var g = 0; g < _this.geoShape.length; g++) {
                    for (var v = 0; v < _this.geoShape[g].length; v++) {
                        _this.geoShape[g][v] = _this.geoShape[g][v].reverse();
                    }
                }
                for (var t = 0; t < _this.geoShape.length; t++) {
                    _this.drawPolygon(_this.geoShape[t]);
                }
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    message: 'No gofence found..!!',
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LivePage.prototype.drawPolygon = function (polyData) {
        var _this = this;
        var that = this;
        that.geodata = [];
        that.geodata = polyData.map(function (d) {
            return { lat: d[0], lng: d[1] };
        });
        console.log("geodata=> ", that.geodata);
        // let bounds = new LatLngBounds(that.geodata);
        // that.allData.map.moveCamera({
        // target: bounds
        // });
        var GORYOKAKU_POINTS = that.geodata;
        console.log("GORYOKAKU_POINTS=> ", GORYOKAKU_POINTS);
        that.allData.map.addPolygon({
            'points': GORYOKAKU_POINTS,
            'strokeColor': '#AA00FF',
            'fillColor': '#00FFAA',
            'strokeWidth': 2
        }).then(function (polygon) {
            // polygon.on(GoogleMapsEvent.POLYGON_CLICK).subscribe((param) => {
            //   console.log("polygon data=> " + param)
            //   console.log("polygon data=> " + param[1])
            // })
            _this.generalPolygon = polygon;
        });
    };
    LivePage.prototype.gotoAll = function () {
        this.navCtrl.setRoot('LivePage');
        localStorage.setItem("CLICKED_ALL", "CLICKED_ALL");
    };
    LivePage.prototype.shareLive = function () {
        var _this = this;
        var data = {
            id: this.liveDataShare._id,
            imei: this.liveDataShare.Device_ID,
            sh: this.userdetails._id,
            ttl: 60 // set to 1 hour by default
        };
        this.apiCall.startLoading().present();
        this.apiCall.shareLivetrackCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.resToken = data.t;
            _this.liveShare();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LivePage.prototype.getPOIs = function () {
        var _this = this;
        this.allData._poiData = [];
        var _burl = this.apiCall.mainUrl + "poi/getPois?user=" + this.userdetails._id;
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(_burl)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.allData._poiData = data;
            _this.plotPOI();
        });
    };
    LivePage.prototype.plotPOI = function () {
        var toast = this.toastCtrl.create({
            message: "Plotting POI's please wait...",
            position: 'middle'
        });
        var icurl;
        if (this.plt.is('android')) {
            icurl = './assets/imgs/poiFlag.png';
        }
        else if (this.plt.is('ios')) {
            icurl = 'www/assets/imgs/poiFlag.png';
        }
        for (var v = 0; v < this.allData._poiData.length; v++) {
            toast.present();
            if (this.allData._poiData[v].poi.location.coordinates !== undefined) {
                this.allData.map.addMarker({
                    title: this.allData._poiData[v].poi.poiname,
                    position: {
                        lat: this.allData._poiData[v].poi.location.coordinates[1],
                        lng: this.allData._poiData[v].poi.location.coordinates[0]
                    },
                    icon: {
                        url: icurl,
                        size: {
                            width: 20,
                            height: 20
                        }
                    },
                    animation: __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["c" /* GoogleMapsAnimation */].BOUNCE
                });
            }
        }
        toast.dismiss();
    };
    LivePage.prototype.liveShare = function () {
        var that = this;
        var link = this.apiCall.mainUrl + "share/liveShare?t=" + that.resToken;
        that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
    };
    LivePage.prototype.zoomin = function () {
        var that = this;
        that.allData.map.animateCameraZoomIn();
    };
    LivePage.prototype.zoomout = function () {
        var that = this;
        that.allData.map.animateCameraZoomOut();
    };
    LivePage.prototype.getIconUrl = function (data) {
        var that = this;
        var iconUrl;
        if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0) {
            if (that.plt.is('ios')) {
                iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
            }
            else if (that.plt.is('android')) {
                iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
            }
        }
        else {
            if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0) {
                if (that.plt.is('ios')) {
                    iconUrl = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
                else if (that.plt.is('android')) {
                    iconUrl = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
            }
            else {
                var stricon;
                if (data.status.toLowerCase() === 'out of reach') {
                    stricon = "outofreach";
                    if (that.plt.is('ios')) {
                        iconUrl = 'www/assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                    }
                    else if (that.plt.is('android')) {
                        iconUrl = './assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                    }
                }
                else {
                    if (that.plt.is('ios')) {
                        iconUrl = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                    }
                    else if (that.plt.is('android')) {
                        iconUrl = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                    }
                }
            }
        }
        return iconUrl;
    };
    LivePage.prototype.parseMillisecondsIntoReadableTime = function (milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;
        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
        // return h + ':' + m;
        return h + ':' + m + ':' + s;
    };
    LivePage.prototype.otherValues = function (data) {
        var that = this;
        that.vehicle_speed = data.last_speed;
        that.todays_odo = data.today_odo;
        that.total_odo = data.total_odo;
        if (that.userdetails.fuel_unit == 'LITRE') {
            that.fuel = data.currentFuel;
        }
        else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
            that.fuel = data.fuel_percent;
        }
        else {
            that.fuel = data.currentFuel;
        }
        that.voltage = data.battery;
        if (data.last_location) {
            that.getAddress(data.last_location);
        }
        that.last_ping_on = data.last_ping_on;
        var tempvar = new Date(data.lastStoppedAt);
        if (data.lastStoppedAt != null) {
            var fd = tempvar.getTime();
            var td = new Date().getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60;
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            that.lastStoppedAt = rhours + ':' + rminutes;
        }
        else {
            that.lastStoppedAt = '00' + ':' + '00';
        }
        that.distFromLastStop = data.distFromLastStop;
        if (!isNaN(data.timeAtLastStop)) {
            that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
        }
        else {
            that.timeAtLastStop = '00:00:00';
        }
        that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
        that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
        that.last_ACC = data.last_ACC;
        that.acModel = data.ac;
        that.currentFuel = data.currentFuel;
        that.power = data.power;
        that.gpsTracking = data.gpsTracking;
        that.tempreture = data.temp;
        that.door = data.door;
        that.recenterMeLat = data.last_location.lat;
        that.recenterMeLng = data.last_location.long;
    };
    LivePage.prototype.weDidntGetPing = function (data) {
        var that = this;
        if (data._id != undefined && data.last_location != undefined) {
            that.otherValues(data);
            if (data.last_location != undefined) {
                that.allData.map.addMarker({
                    title: data.Device_Name,
                    position: { lat: data.last_location.lat, lng: data.last_location.long },
                    icon: {
                        url: that.getIconUrl(data),
                        size: {
                            width: 20,
                            height: 40
                        }
                    },
                }).then(function (marker) {
                    if (that.selectedVehicle) {
                        that.temporaryMarker = marker;
                    }
                    else {
                        that.tempMarkArray.push(marker);
                    }
                    // that.allData[key].mark = marker;
                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                        .subscribe(function (e) {
                        that.liveVehicleName = data.Device_Name;
                        // that.drawerHidden = false;
                        that.showaddpoibtn = true;
                        that.drawerState = __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__["a" /* DrawerState */].Docked;
                        that.onClickShow = true;
                    });
                    that.getAddress(data.last_location);
                });
            }
        }
    };
    LivePage.prototype.socketInit = function (pdata, center) {
        if (center === void 0) { center = false; }
        var that = this;
        that._io.emit('acc', pdata.Device_ID);
        that.socketChnl.push(pdata.Device_ID + 'acc');
        that._io.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            if (d4 != undefined)
                // console.log('d4 data: ', d4);
                (function (data) {
                    if (data == undefined) {
                        return;
                    }
                    if (that.selectedVehicle !== undefined) {
                        that.reCenterMe();
                        var temp123;
                        if (that.temporaryMarker) {
                            that.temporaryMarker.remove();
                            temp123 = "cleared";
                        }
                        that.temporaryInterval = setInterval(function () {
                            if (that.temporaryMarker) {
                                that.temporaryMarker.remove();
                                clearInterval(that.temporaryInterval);
                                console.log("temporaryInterval cleared!!");
                            }
                            else {
                                if (temp123 === 'cleared') {
                                    if (that.temporaryInterval) {
                                        clearInterval(that.temporaryInterval);
                                    }
                                }
                            }
                        }, 10);
                    }
                    else {
                        // var temp122;
                        if (that.tempMarkArray.length > 0) {
                            for (var t = 0; t < that.tempMarkArray.length; t++) {
                                that.tempMarkArray[t].remove();
                            }
                        }
                    }
                    console.log("on ping: ", data);
                    if (data._id != undefined && data.last_location != undefined) {
                        var key = data._id;
                        that.impkey = data._id;
                        that.deviceDeatils = data;
                        var ic = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](that.icons[data.iconType]);
                        if (!ic) {
                            return;
                        }
                        ic.path = null;
                        if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || (data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0)) {
                            if (that.plt.is('ios')) {
                                ic.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
                            }
                            else if (that.plt.is('android')) {
                                ic.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
                            }
                        }
                        else {
                            if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || (data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0)) {
                                if (that.plt.is('ios')) {
                                    ic.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                                }
                                else if (that.plt.is('android')) {
                                    ic.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                                }
                            }
                            else {
                                var stricon;
                                if (data.status.toLowerCase() === 'out of reach') {
                                    stricon = "outofreach";
                                    if (that.plt.is('ios')) {
                                        ic.url = 'www/assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                                    }
                                    else if (that.plt.is('android')) {
                                        ic.url = './assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                                    }
                                }
                                else {
                                    if (that.plt.is('ios')) {
                                        ic.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                                    }
                                    else if (that.plt.is('android')) {
                                        ic.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                                    }
                                }
                            }
                        }
                        that.otherValues(data);
                        console.log("icon url: ", ic.url);
                        if (data.last_location != undefined) {
                            that.getAddress(data.last_location);
                        }
                        var strStr;
                        if (that.allData[key]) {
                            strStr = ' Address: ' + that.address + '\n Last Updated: ' + __WEBPACK_IMPORTED_MODULE_12_moment__(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
                            that.socketSwitch[key] = data;
                            if (that.allData[key].mark != undefined && that.allData[key].mark1 != undefined) {
                                // that.allData[key].mark.setTitle();
                                that.allData[key].mark.on(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function () {
                                    // alert('clicked');
                                    setTimeout(function () {
                                        that.allData[key].mark.hideInfoWindow();
                                    }, 2000);
                                });
                                that.allData[key].mark.setSnippet(strStr);
                                that.allData[key].mark.setIcon(ic.url);
                                that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                                var temp = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](that.allData[key].poly[1]);
                                that.allData[key].poly[0] = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](temp);
                                that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                if (that.displayNames) {
                                    that.allData[key].mark1.setPosition(that.allData[key].poly[1]);
                                    that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].mark1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                                }
                                else {
                                    that.liveTrack(that.allData.map, that.allData[key].mark, null, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                                }
                            }
                            else {
                                return;
                            }
                        }
                        else {
                            that.allData[key] = {};
                            that.socketSwitch[key] = data;
                            that.allData[key].poly = [];
                            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            if (data.last_location != undefined) {
                                strStr = ' Address: ' + that.address + '\n Last Updated: ' + __WEBPACK_IMPORTED_MODULE_12_moment__(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
                                debugger;
                                if (that.displayNames) {
                                    that.showMergedMarkers(data, key, ic, center);
                                }
                                else {
                                    that.showSimplemarkers(data, key, ic, center);
                                }
                                // that.allData.map.addMarker({
                                //   // title: data.Device_Name,
                                //   position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                                //   icon: {
                                //     url: ic.url,
                                //     size: {
                                //       height: 40,
                                //       width: 20
                                //     }
                                //   },
                                // }).then((marker: Marker) => {
                                //   that.allData[key].mark = marker;
                                //   marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                                //     .subscribe(e => {
                                //       if (that.selectedVehicle == undefined) {
                                //         that.getAddressTitle(marker);
                                //       } else {
                                //         that.liveVehicleName = data.Device_Name;
                                //         // that.drawerHidden = false;
                                //         that.showaddpoibtn = true;
                                //         that.drawerState = DrawerState.Docked;
                                //         that.onClickShow = true;
                                //       }
                                //     });
                                //   // that.getAddress(data.last_location);
                                //   var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                //   that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                                // });
                                // }
                                // that.allData.map.addMarker({
                                //   title: data.Device_Name,
                                //   snippet: strStr,
                                //   position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                                //   icon: {
                                //     url: ic.url,
                                //     size: {
                                //       width: 20,
                                //       height: 40
                                //     }
                                //   },
                                // }).then((marker: Marker) => {
                                //   that.allData[key].mark = marker;
                                //   // if (that.selectedVehicle == undefined) {
                                //   //   marker.showInfoWindow();
                                //   // }
                                //   // if (that.selectedVehicle != undefined) {
                                //   marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                                //     .subscribe(e => {
                                //       if (that.selectedVehicle == undefined) {
                                //         that.getAddressTitle(marker);
                                //       } else {
                                //         that.liveVehicleName = data.Device_Name;
                                //         // that.drawerHidden = false;
                                //         that.showaddpoibtn = true;
                                //         that.drawerState = DrawerState.Docked;
                                //         that.onClickShow = true;
                                //       }
                                //     });
                                //   // that.getAddress(data.last_location);
                                //   var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                //   that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                                // });
                                ///////////////
                                if (that.selectedVehicle) {
                                    // that.addCircleWithAnimation(that.allData[data._id].poly[0].lat, that.allData[data._id].poly[0].lng);
                                }
                                //////////////
                            }
                        }
                    }
                })(d4);
        });
    };
    LivePage.prototype.liveTrack = function (map, mark, mark1, coords, speed, delay, center, id, that) {
        // liveTrack(map, mark, mark1, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {
        var target = 0;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        if (center) {
            map.setCameraTarget(coords[0]);
        }
        function _goToPoint() {
            if (target > coords.length)
                return;
            // console.log("issue coming from livetrack 1", mark)
            var lat = 0;
            var lng = 0;
            if (mark.getPosition().lat !== undefined || mark.getPosition().lng !== undefined) {
                lat = mark.getPosition().lat;
                lng = mark.getPosition().lng;
            }
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](coords[target].lat, coords[target].lng);
            var distance = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
                // mark.setIcon(icons);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    // if (that.selectedVehicle != undefined) {
                    //   map.setCameraTarget(new LatLng(lat, lng));
                    // }
                    that.latlngCenter = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng);
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    if (that.displayNames) {
                        mark1.setPosition(new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    }
                    /////////
                    // that.circleObj.setCenter(new LatLng(lat, lng))
                    ////////
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    head = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    // if (that.selectedVehicle != undefined) {
                    //   map.setCameraTarget(dest);
                    // }
                    that.latlngCenter = dest;
                    mark.setPosition(dest);
                    if (that.displayNames) {
                        mark1.setPosition(dest);
                    }
                    /////////
                    // that.circleObj.setCenter(new LatLng(dest.lat, dest.lng))
                    ////////
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    };
    LivePage.prototype.showSimplemarkers = function (data, key, ic, center) {
        var that = this;
        that.allData.map.addMarker({
            title: data.Device_Name,
            position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
            icon: {
                url: ic,
                size: {
                    height: 40,
                    width: 20
                }
            },
        }).then(function (marker) {
            that.allData[key].mark = marker;
            marker.addEventListener(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                .subscribe(function (e) {
                if (that.selectedVehicle == undefined) {
                    that.getAddressTitle(marker);
                }
                else {
                    that.liveVehicleName = data.Device_Name;
                    // that.drawerHidden = false;
                    that.showaddpoibtn = true;
                    that.drawerState = __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__["a" /* DrawerState */].Docked;
                    that.onClickShow = true;
                }
            });
            // that.getAddress(data.last_location);
            var speed = data.status == "RUNNING" ? data.last_speed : 0;
            that.liveTrack(that.allData.map, that.allData[key].mark, null, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
        });
    };
    LivePage.prototype.showMergedMarkers = function (data, key, ic, center) {
        var that = this;
        /////////////// merge images using canvas
        var c = document.createElement("canvas");
        c.width = 100;
        c.height = 60;
        var ctx = c.getContext("2d");
        ctx.fillStyle = "#1c2f66";
        ctx.fillRect(0, 0, 100, 20);
        // ctx.strokeRect(0, 0, 100, 70);
        ctx.font = "9pt sans-serif";
        ctx.fillStyle = "white";
        ctx.fillText(data.Device_Name, 12, 15);
        var img = c.toDataURL();
        that.allData.map.addMarker({
            // title: data.Device_Name,
            position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
            icon: {
                url: img,
                size: {
                    height: 60,
                    width: 100
                }
            },
        }).then(function (marker1) {
            that.allData[key].mark1 = marker1;
            that.allData.map.addMarker({
                // title: data.Device_Name,
                position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                icon: {
                    url: ic.url,
                    size: {
                        height: 40,
                        width: 20
                    }
                },
            }).then(function (marker) {
                that.allData[key].mark = marker;
                marker.addEventListener(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                    .subscribe(function (e) {
                    if (that.selectedVehicle == undefined) {
                        that.getAddressTitle(marker);
                    }
                    else {
                        that.liveVehicleName = data.Device_Name;
                        // that.drawerHidden = false;
                        that.showaddpoibtn = true;
                        that.drawerState = __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__["a" /* DrawerState */].Docked;
                        that.onClickShow = true;
                    }
                });
                // that.getAddress(data.last_location);
                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].mark1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
            });
        });
    };
    LivePage.prototype.addCircleWithAnimation = function (lat, lng) {
        var that = this;
        var _radius = 500;
        var rMin = _radius * 1 / 9;
        var rMax = _radius;
        var direction = 1;
        var GOOGLE = {
            "lat": lat,
            "lng": lng
        };
        var circleOption = {
            'center': GOOGLE,
            'radius': 500,
            'fillColor': 'rgb(216, 6, 34)',
            'fillOpacity': 0.6,
            'strokeColor': '#950417',
            'strokeOpacity': 1,
            'strokeWidth': 0.5
        };
        // Add circle
        var circle = that.allData.map.addCircleSync(circleOption);
        that.circleObj = circle;
        that.circleTimer = setInterval(function () {
            var radius = circle.getRadius();
            if ((radius > rMax) || (radius < rMin)) {
                direction *= -1;
            }
            var _par = (radius / _radius) - 0.1;
            var opacity = 0.4 * _par;
            var colorString = that.RGBAToHexA(216, 6, 34, opacity);
            circle.setFillColor(colorString);
            circle.setRadius(radius + direction * 10);
        }, 30);
    };
    LivePage.prototype.RGBAToHexA = function (r, g, b, a) {
        r = r.toString(16);
        g = g.toString(16);
        b = b.toString(16);
        a = Math.round(a * 255).toString(16);
        if (r.length == 1)
            r = "0" + r;
        if (g.length == 1)
            g = "0" + g;
        if (b.length == 1)
            b = "0" + b;
        if (a.length == 1)
            a = "0" + a;
        return "#" + r + g + b + a;
    };
    LivePage.prototype.getAddress = function (coordinates) {
        var that = this;
        if (!coordinates) {
            that.address = 'N/A';
            return;
        }
        this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
            .then(function (res) {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            // that.saveAddressToServer(str, coordinates.lat, coordinates.long);
            that.address = str;
        });
    };
    LivePage.prototype.showNearby = function (key) {
        var _this = this;
        // for police stations
        var url, icurl;
        if (key === 'police') {
            if (this.policeButtonColor == '#ee612e') {
                this.policeButtonColor = '#f4f4f4';
                this.policeColor = '#ee612e';
            }
            else {
                if (this.policeButtonColor == '#f4f4f4') {
                    this.policeButtonColor = '#ee612e';
                    this.policeColor = '#fff';
                }
            }
            if (this.plt.is('android')) {
                icurl = './assets/imgs/police-station.png';
            }
            else if (this.plt.is('ios')) {
                icurl = 'www/assets/imgs/police-station.png';
            }
            url = this.apiCall.mainUrl + "googleAddress/getNearBY?lat=" + this.recenterMeLat + "&long=" + this.recenterMeLng + "&radius=1000&type=police&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";
        }
        else if (key === 'petrol') {
            if (this.petrolButtonColor == '#ee612e') {
                this.petrolButtonColor = '#f4f4f4';
                this.petrolColor = '#ee612e';
            }
            else {
                if (this.petrolButtonColor == '#f4f4f4') {
                    this.petrolButtonColor = '#ee612e';
                    this.petrolColor = '#fff';
                }
            }
            if (this.plt.is('android')) {
                icurl = './assets/imgs/gas-pump.png';
            }
            else if (this.plt.is('ios')) {
                icurl = 'www/assets/imgs/gas-pump.png';
            }
            url = this.apiCall.mainUrl + "googleAddress/getNearBY?lat=" + this.recenterMeLat + "&long=" + this.recenterMeLng + "&radius=1000&type=gas_station&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";
        }
        console.log("google maps activity: ", url);
        this.apiCall.getSOSReportAPI(url).subscribe(function (resp) {
            console.log(resp.status);
            if (resp.status === 'OK') {
                console.log(resp.results);
                console.log(resp);
                var mapData = resp.results.map(function (d) {
                    return { lat: d.geometry.location.lat, lng: d.geometry.location.lng };
                });
                var bounds = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["g" /* LatLngBounds */](mapData);
                for (var t = 0; t < resp.results.length; t++) {
                    _this.allData.map.addMarker({
                        title: resp.results[t].name,
                        position: {
                            lat: resp.results[t].geometry.location.lat,
                            lng: resp.results[t].geometry.location.lng
                        },
                        icon: {
                            url: icurl,
                            size: {
                                height: 35,
                                width: 35
                            }
                        }
                    }).then(function (mark) {
                    });
                }
                _this.allData.map.moveCamera({
                    target: bounds
                });
                _this.allData.map.setPadding(20, 20, 20, 20);
                _this.zoomLevel = 10;
            }
            else if (resp.status === 'ZERO_RESULTS') {
                if (key === 'police') {
                    _this.showToastMsg('No nearby police stations found.');
                }
                else if (key === 'petrol') {
                    _this.showToastMsg('No nearby petrol pump found.');
                }
            }
            else if (resp.status === 'OVER_QUERY_LIMIT') {
                _this.showToastMsg('Query limit exeed. Please try after some time.');
            }
            else if (resp.status === 'REQUEST_DENIED') {
                _this.showToastMsg('Please check your API key.');
            }
            else if (resp.status === 'INVALID_REQUEST') {
                _this.showToastMsg('Invalid request. Please try again.');
            }
            else if (resp.status === 'UNKNOWN_ERROR') {
                _this.showToastMsg('An unknown error occured. Please try again.');
            }
        });
    };
    LivePage.prototype.showToastMsg = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'bottom'
        }).present();
    };
    LivePage.prototype.reCenterMe = function () {
        this.allData.map.animateCamera({
            target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
            // zoom: this.allData.map.getCameraZoom(),
            tilt: 30,
            // bearing: this.allData.map.getCameraBearing(),
            duration: 1800
        }).then(function () {
        });
        // // console.log("getzoom level: " + this.allData.map.getCameraZoom());
        // this.allData.map.moveCamera({
        //   target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
        //   zoom: this.allData.map.getCameraZoom()
        // }).then(() => {
        // })
    };
    LivePage.prototype.getAddressTitle = function (marker) {
        // if(marker)
        console.log("issue coming from getAddressTitle");
        var payload = {
            "lat": marker.getPosition().lat,
            "long": marker.getPosition().lng,
            "api_id": "1"
        };
        var addr;
        this.apiCall.getAddressApi(payload)
            .subscribe(function (data) {
            console.log("got address: " + data.results);
            if (data.results[2] != undefined || data.results[2] != null) {
                addr = data.results[2].formatted_address;
            }
            else {
                addr = 'N/A';
            }
            marker.setSnippet(addr);
        });
    };
    LivePage.prototype.info = function (mark, cb) {
        mark.addListener('click', cb);
    };
    LivePage.prototype.setDocHeight = function () {
        var that = this;
        that.distanceTop = 200;
        that.drawerState = __WEBPACK_IMPORTED_MODULE_8_ion_bottom_drawer__["a" /* DrawerState */].Top;
    };
    LivePage.prototype.temp = function (data) {
        var _this = this;
        // debugger
        localStorage.setItem("navigationFrom", "MultiLiveScreen");
        console.log("check selected vehicle: ", this.selectedVehicle);
        console.log("check temp function data: ", data);
        if (data.status === 'Expired') {
            var profileModal = this.modalCtrl.create('ExpiredPage', {
                param: data
            });
            profileModal.present();
            profileModal.onDidDismiss(function () {
                _this.selectedVehicle = undefined;
            });
        }
        else {
            this.navCtrl.push('LiveSingleDevice', { device: data }, {
                animate: true, animation: 'transition-ios'
            });
        }
        // this.navCtrl.push('LiveSingleDevice', { device: data }, {
        //   animate: true, animation: 'transition-ios'
        // });
        // this.titleText = 'single';
        // if (data.status == 'Expired') {
        //   let profileModal = this.modalCtrl.create('ExpiredPage', {
        //     param: data
        //   });
        //   profileModal.present();
        //   profileModal.onDidDismiss(() => {
        //     this.selectedVehicle = undefined;
        //   });
        // } else {
        //   let that = this;
        //   that.data = data;
        //   that.liveDataShare = data;
        //   // that.drawerHidden = true;
        //   that.drawerState = DrawerState.Bottom;
        //   that.onClickShow = false;
        //   if (that.allData.map != undefined) {
        //     // that.allData.map.clear();
        //     that.allData.map.remove();
        //   }
        //   // console.log("on select change data=> " + JSON.stringify(data));
        //   debugger
        //   for (var i = 0; i < that.socketChnl.length; i++)
        //     that._io.removeAllListeners(that.socketChnl[i]);
        //   that.allData = {};
        //   that.socketChnl = [];
        //   that.socketSwitch = {};
        //   if (data) {
        //     if (data.last_location) {
        //       let mapOptions = {
        //         // backgroundColor: 'white',
        //         controls: {
        //           compass: false,
        //           zoom: false,
        //           mapToolbar: true,
        //           myLocation: true,
        //           myLocationButton: false,
        //         },
        //         gestures: {
        //           rotate: false,
        //           tilt: false
        //         },
        //         mapType: that.mapKey
        //       }
        //       let map = GoogleMaps.create('map_canvas', mapOptions);
        //       map.animateCamera({
        //         target: { lat: 20.5937, lng: 78.9629 },
        //         zoom: 15,
        //         duration: 1000,
        //         padding: 0  // default = 20px
        //       });
        //       map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
        //       // map.moveCamera({
        //       //   target: {
        //       //     lat: data.last_location['lat'], lng: data.last_location['long']
        //       //   }
        //       // });
        //       map.setPadding(20, 20, 20, 20);
        //       // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
        //       that.allData.map = map;
        //       // that.weDidntGetPing(data);
        //       // that.socketInit(data);
        //       // that.socketInitForSingleVehicle(data);
        //       that.weDidntGetPingForSingleVehicle(data);
        //     } else {
        //       that.allData.map = that.newMap();
        //       // that.socketInit(data);
        //       that.socketInitForSingleVehicle(data);
        //     }
        //     if (that.selectedVehicle != undefined) {
        //       // that.drawerHidden = false;
        //       that.drawerState = DrawerState.Docked;
        //       that.onClickShow = true;
        //     }
        //   }
        //   that.showBtn = true;
        // }
    };
    LivePage.prototype.socketInitForSingleVehicle = function (pdata, center) {
        if (center === void 0) { center = false; }
        this._io.emit('acc', pdata.Device_ID);
        this.socketChnl.push(pdata.Device_ID + 'acc');
        var that = this;
        this._io.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            if (d4 != undefined)
                console.log("ping data: => ", d4);
            (function (data) {
                if (data == undefined) {
                    return;
                }
                localStorage.setItem("gotPing", "true");
                that.reCenterMe();
                localStorage.setItem("pdata", JSON.stringify(data));
                if (data._id != undefined && data.last_location != undefined) {
                    var key = data._id;
                    that.impkey = data._id;
                    that.deviceDeatils = data;
                    that.otherValues(data);
                    var strStr;
                    // debugger
                    if (that.allData[key]) {
                        strStr = ' Address: ' + that.address + '\n Last Updated: ' + __WEBPACK_IMPORTED_MODULE_12_moment__(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
                        that.socketSwitch[key] = data;
                        console.log("check mark is avalible or not? ", that.allData[key].mark);
                        if (that.allData[key].mark !== undefined) {
                            that.allData[key].mark.on(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function () {
                                // alert('clicked');
                                setTimeout(function () {
                                    that.allData[key].mark.hideInfoWindow();
                                }, 2000);
                            });
                            that.allData[key].mark.setSnippet(strStr);
                            that.allData[key].mark.setIcon(that.getIconUrl(data));
                            that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                            var temp = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](that.allData[key].poly[1]);
                            that.allData[key].poly[0] = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](temp);
                            that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                            var speed = data.status == "RUNNING" ? data.last_speed : 0;
                            that.getAddress(data.last_location);
                            that.liveTrackForSingleVehicle(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                        }
                        else {
                            return;
                        }
                    }
                    else {
                        that.allData[key] = {};
                        that.socketSwitch[key] = data;
                        that.allData[key].poly = [];
                        if (data.sec_last_location.lat !== null && data.sec_last_location.long !== null) {
                            that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
                        }
                        else {
                            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                        }
                        that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                        if (data.last_location != undefined) {
                            console.log("we got pung: ", that.getIconUrl(data));
                            // let position: ILatLng = {
                            //   lat: that.allData[key].poly[0].lat,
                            //   lng: that.allData[key].poly[0].lng
                            // };
                            // debugger
                            if (that.temporaryMarker123 !== undefined) {
                                that.temporaryMarker123.setTitle(data.Device_Name);
                                // that.temporaryMarker.setIcon(that.getIconUrl(data));
                                that.temporaryMarker123.setPosition(that.allData[key].poly[0]);
                                that.allData[key].mark = that.temporaryMarker123;
                                // that.temporaryMarker.remove();
                                that.allData[key].mark.addEventListener(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                                    .subscribe(function (e) {
                                    that.liveVehicleName = data.Device_Name;
                                    // that.showActionSheet = true;
                                    that.showaddpoibtn = true;
                                    that.getAddressTitle(that.allData[key].mark);
                                });
                                that.getAddress(data.last_location);
                                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                that.liveTrackForSingleVehicle(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                            }
                            else {
                                that.allData.map.addMarker({
                                    title: data.Device_Name,
                                    position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                                    icon: {
                                        url: that.getIconUrl(data),
                                        size: {
                                            height: 40,
                                            width: 20
                                        }
                                    },
                                }).then(function (marker) {
                                    that.allData[key].mark = marker;
                                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                                        .subscribe(function (e) {
                                        that.liveVehicleName = data.Device_Name;
                                        // that.showActionSheet = true;
                                        that.showaddpoibtn = true;
                                        that.getAddressTitle(marker);
                                    });
                                    that.getAddress(data.last_location);
                                    var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                    that.liveTrackForSingleVehicle(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                                });
                            }
                            ///////////////
                            // that.addCircleWithAnimation(that.allData[data._id].poly[0].lat, that.allData[data._id].poly[0].lng);
                            //////////////
                        }
                    }
                }
            })(d4);
        });
    };
    LivePage.prototype.liveTrackForSingleVehicle = function (map, mark, coords, speed, delay, center, id, that) {
        var target = 0;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        // if (center) {
        //   map.setCameraTarget(coords[0]);
        // }
        function _goToPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](coords[target].lat, coords[target].lng);
            var distance = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
            }
            function _moveMarker() {
                // debugger
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.mapTrail) {
                        that.showTrail(lat, lng, map);
                    }
                    else {
                        console.log(that.allData.polylineID);
                        // console.log(that.allData.polylineID.length)
                        if (that.allData.polylineID) {
                            that.tempArray = [];
                            that.allData.polylineID.remove();
                        }
                    }
                    // map.moveCamera({
                    //   target: new LatLng(lat, lng)
                    // });
                    // map.setCameraTarget(new LatLng(lat, lng));
                    that.latlngCenter = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng);
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    /////////
                    // that.circleObj.setCenter(new LatLng(lat, lng))
                    ////////
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    head = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                        // map.moveCamera({
                        //   target: {
                        //     lat: dest.lat,
                        //     lng: dest.lng
                        //   },
                        //   tilt: 30,
                        //   bearing: head
                        // });
                    }
                    if (that.mapTrail) {
                        that.showTrail(dest.lat, dest.lng, map);
                    }
                    else {
                        if (that.allData.polylineID) {
                            that.tempArray = [];
                            that.allData.polylineID.remove();
                        }
                    }
                    // map.moveCamera({
                    //   target: {
                    //     lat: dest.lat,
                    //     lng: dest.lng
                    //   }
                    // });
                    // map.setCameraTarget(dest);
                    that.latlngCenter = dest;
                    mark.setPosition(dest);
                    /////////
                    // that.circleObj.setCenter(new LatLng(dest.lat, dest.lng))
                    ////////
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    };
    LivePage.prototype.weDidntGetPingForSingleVehicle = function (data) {
        console.log("I came here in weDidntGetPing function");
        var that = this;
        if (data._id != undefined && data.last_location != undefined) {
            that.otherValues(data);
            if (data.last_location != undefined) {
                console.log("we did not get pung: ", that.getIconUrl(data));
                debugger;
                // if (localStorage.getItem('gotPing') === null) {
                that.allData.map.addMarker({
                    title: data.Device_Name,
                    position: { lat: data.last_location.lat, lng: data.last_location.long },
                    icon: {
                        url: that.getIconUrl(data),
                        size: {
                            height: 40,
                            width: 20
                        }
                    },
                }).then(function (marker) {
                    // debugger
                    that.temporaryMarker123 = marker;
                    // that.allData[key].mark = marker;
                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                        .subscribe(function (e) {
                        that.liveVehicleName = data.Device_Name;
                        // that.showActionSheet = true;
                        that.showaddpoibtn = true;
                        that.getAddressTitle(marker);
                    });
                    that.getAddress(data.last_location);
                    that.socketInitForSingleVehicle(data);
                });
                // }
            }
        }
    };
    ///////////// CORDOVA SQLite CODE ////////////////////////////
    LivePage.prototype.doFurtherLogic = function (array) {
        var that = this;
        that.portstemp = array;
        var dummyData;
        dummyData = array.map(function (d) {
            if (d.last_lat === undefined && d.last_lng === undefined)
                return;
            else {
                return {
                    "position": {
                        "lat": d.last_lat,
                        "lng": d.last_lng
                    },
                    "name": d.Device_Name,
                    "icon": {
                        "url": that.getIconUrl(d),
                        "size": {
                            "width": 20,
                            "height": 40
                        }
                    }
                };
            }
        });
        dummyData = dummyData.filter(function (element) {
            return element !== undefined;
        });
        console.log("dummy data: ", dummyData);
        if (that.isSuperAdmin || that.isDealer) {
            that.addCluster(dummyData);
        }
        else {
            if (array.length > 10) {
                console.log("customers are greater than 10");
                that.addCluster(dummyData);
            }
            else {
                for (var i = 0; i < array.length; i++) {
                    if (array[i].status != "Expired") {
                        that.addMarkersFirst(array[i]);
                    }
                }
            }
            // for (var i = 0; i < array.length; i++) {
            //   if (array[i].status !== "Expired") {
            //     that.addMarkersFirst(array[i]);
            //   }
            // }
        }
    };
    LivePage.prototype.addMarkersFirst = function (obj) {
        var that = this;
        that.allData.markersArray = [];
        that.allData.markersArray1 = [];
        if (that.displayNames) {
            var that_1 = this;
            /////////////// merge images using canvas
            var c = document.createElement("canvas");
            c.width = 100;
            c.height = 60;
            var ctx = c.getContext("2d");
            ctx.fillStyle = "#1c2f66";
            ctx.fillRect(0, 0, 100, 20);
            // ctx.strokeRect(0, 0, 100, 70);
            ctx.font = "9pt sans-serif";
            ctx.fillStyle = "white";
            ctx.fillText(obj.Device_Name, 12, 15);
            var img = c.toDataURL();
            that_1.allData.map.addMarker({
                // title: data.Device_Name,
                position: { lat: obj.last_lat, lng: obj.last_lng },
                icon: {
                    url: that_1.getIconUrl(obj),
                    size: {
                        height: 40,
                        width: 20
                    }
                },
            }).then(function (marker) {
                that_1.allData.markersArray.push({
                    key: obj._id,
                    mark: marker
                });
                that_1.allData.map.addMarker({
                    position: { lat: obj.last_lat, lng: obj.last_lng },
                    icon: {
                        url: img,
                        size: {
                            height: 60,
                            width: 100
                        }
                    },
                }).then(function (marker1) {
                    // that.allData.markersArray1 = marker1;
                    that_1.allData.markersArray1.push({
                        key: obj._id,
                        mark: marker1
                    });
                    if (obj.last_lat !== 0 && obj.last_lng !== 0) {
                        var last_location = {
                            lat: obj.last_lat,
                            long: obj.last_lng
                        };
                        that_1.getAddress(last_location);
                    }
                    if (obj.status === 'RUNNING') {
                        that_1.getPingOperation(obj);
                    }
                });
            });
        }
        else {
            that.allData.map.addMarker({
                title: obj.Device_Name,
                position: { lat: obj.last_lat, lng: obj.last_lng },
                icon: {
                    url: that.getIconUrl(obj),
                    size: {
                        height: 40,
                        width: 20
                    }
                },
            }).then(function (marker) {
                that.allData.markersArray.push({
                    key: obj._id,
                    mark: marker
                });
                if (obj.last_lat !== 0 && obj.last_lng !== 0) {
                    var last_location = {
                        lat: obj.last_lat,
                        long: obj.last_lng
                    };
                    that.getAddress(last_location);
                }
                // debugger
                if (obj.status === 'RUNNING') {
                    that.getPingOperation(obj);
                }
            });
        }
    };
    LivePage.prototype.getPingOperation = function (pdata, center) {
        if (center === void 0) { center = false; }
        var that = this;
        that._io.emit('acc', pdata.Device_ID);
        that.socketChnl.push(pdata.Device_ID + 'acc');
        that._io.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        return;
                    }
                    console.log("on ping: ", data);
                    if (data._id != undefined && data.last_location != undefined) {
                        var key = data._id;
                        that.impkey = data._id;
                        that.deviceDeatils = data;
                        that.otherValues(data);
                        console.log("icon url: ", that.getIconUrl(data));
                        if (data.last_location != undefined) {
                            that.getAddress(data.last_location);
                        }
                        var strStr;
                        if (that.allData[key]) {
                            strStr = ' Address: ' + that.address + '\n Last Updated: ' + __WEBPACK_IMPORTED_MODULE_12_moment__(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
                            that.socketSwitch[key] = data;
                            if (that.allData[key].mark !== undefined) {
                                // that.allData[key].mark.setTitle();
                                that.allData[key].mark.on(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function () {
                                    // alert('clicked');
                                    setTimeout(function () {
                                        that.allData[key].mark.hideInfoWindow();
                                    }, 2000);
                                });
                                that.allData[key].mark.setSnippet(strStr);
                                that.allData[key].mark.setIcon(that.getIconUrl(data));
                                that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                                var temp = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](that.allData[key].poly[1]);
                                that.allData[key].poly[0] = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](temp);
                                that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                if (that.displayNames) {
                                    that.allData[key].mark1.setPosition(that.allData[key].poly[1]);
                                    that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].mark1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                                }
                                else {
                                    that.liveTrackForSingleVehicle(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                                }
                            }
                            else {
                                return;
                            }
                        }
                        else {
                            debugger;
                            that.allData[key] = {};
                            that.socketSwitch[key] = data;
                            that.allData[key].poly = [];
                            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            strStr = ' Address: ' + that.address + '\n Last Updated: ' + __WEBPACK_IMPORTED_MODULE_12_moment__(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
                            if (that.displayNames) {
                                for (var t = 0; t < that.allData.markersArray.length; t++) {
                                    if (that.allData.markersArray[t].key === key && that.allData.markersArray[t].mark !== undefined) {
                                        that.allData[key].mark = undefined;
                                        that.allData[key].mark = that.allData.markersArray[t].mark;
                                        // that.allData.markersArray = that.allData.markersArray.filter(function (element) {
                                        //   return element.key === key;
                                        // });
                                        console.log("mark key foud");
                                        for (var j = 0; j < that.allData.markersArray1.length; j++) {
                                            if (that.allData.markersArray1[j].key === key && that.allData.markersArray1[j].mark !== undefined) {
                                                that.allData[key].mark1 = undefined;
                                                that.allData[key].mark1 = that.allData.markersArray1[j].mark;
                                                // that.allData.markersArray1 = that.allData.markersArray1.filter(function (element) {
                                                //   return element.key === key;
                                                // });
                                                console.log("mark1 key foud");
                                                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                                that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].mark1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                for (var t = 0; t < that.allData.markersArray.length; t++) {
                                    if (that.allData.markersArray[t].key === key && that.allData.markersArray[t].mark !== undefined) {
                                        that.allData[key].mark = undefined;
                                        that.allData[key].mark = that.allData.markersArray[t].mark;
                                        that.allData.markersArray = that.allData.markersArray.filter(function (element) {
                                            return element.key === key;
                                        });
                                        that.allData[key].mark.addEventListener(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                                            .subscribe(function (e) {
                                            if (that.selectedVehicle === undefined) {
                                                that.getAddressTitle(that.allData[key].mark);
                                            }
                                        });
                                        that.getAddress(data.last_location);
                                        var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                        that.liveTrackForSingleVehicle(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                                    }
                                }
                            }
                        }
                    }
                })(d4);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], LivePage.prototype, "navBar", void 0);
    LivePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-live',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/live/live.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n\n    <ion-title>{{ "LIVE TRACKING" | translate }}</ion-title>\n    <ion-buttons end *ngIf="showBtn">\n      <button ion-button (click)="ionViewDidEnter()">\n        {{ "All Vehicles" | translate }}\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n  <ion-item>\n    <ion-label>{{ "Select Vehicle" | translate }}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="temp(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n</ion-header>\n\n<ion-content style="background-color: #f2f1f0" *ngIf=\'!hideMe\'>\n\n  <div id="map_canvas">\n\n    <ion-fab top left>\n      <button ion-fab color="light" mini>\n        <ion-icon color="gpsc" name="map"></ion-icon>\n      </button>\n      <ion-fab-list side="bottom">\n        <button ion-fab (click)="onClickMap(\'SATELLITE\')" color="gpsc">\n          S\n        </button>\n        <button ion-fab (click)="onClickMap(\'TERRAIN\')" color="gpsc">\n          T\n        </button>\n        <button ion-fab (click)="onClickMap(\'NORMAL\')" color="gpsc">\n          N\n        </button>\n      </ion-fab-list>\n    </ion-fab>\n\n    <ion-fab top right>\n      <button color="gpsc" mini ion-fab (click)="onSelectMapOption(\'mapHideTraffic\')">\n        <img src="assets/icon/trafficON.png" *ngIf="mapHideTraffic" />\n        <img src="assets/icon/trafficOFF.png" *ngIf="!mapHideTraffic" />\n      </button>\n    </ion-fab>\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 20%">\n      <button ion-fab mini (click)="onSelectMapOption(\'showGeofence\')" color="gpsc">\n        <img src="assets/imgs/geo.png" />\n      </button>\n    </ion-fab>\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 33%">\n      <button ion-fab mini (click)="getPOIs()" color="gpsc">\n        <ion-icon name="pin"></ion-icon>\n      </button>\n    </ion-fab>\n    <ion-fab *ngIf="!isSuperAdmin || !isDealer" style="right: calc(10px + env(safe-area-inset-right)); margin-top: 33%">\n      <button ion-fab mini (click)="showCluster()" [ngStyle]="{\'background-color\': streetviewButtonColor}">\n        <ion-icon name="football" [ngStyle]="{\'color\': streetColor}"></ion-icon>\n      </button>\n    </ion-fab>\n  </div>\n</ion-content>\n\n<div *ngIf="onClickShow" class="divPlan">\n  <ion-bottom-drawer [(state)]="drawerState" [dockedHeight]="dockedHeight" [shouldBounce]="shouldBounce"\n    [distanceTop]="distanceTop" [minimumHeight]="minimumHeight" (click)="setDocHeight()">\n    <div class="drawer-content">\n      <p padding-left style="font-size: 13px; padding-bottom: 3px; color:cornflowerblue;">\n        {{ "Last Updated On" | translate }} &mdash;\n        {{ last_ping_on | date: "medium" }}\n      </p>\n      <p padding-left style="font-size: 13px" *ngIf="!address">N/A</p>\n      <p padding-left style="font-size: 13px" *ngIf="address">{{ address }}</p>\n      <hr />\n      <ion-row>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/ign_red.png" *ngIf="last_ACC == \'0\'" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/ign_green.png" *ngIf="last_ACC == \'1\'" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/ign_idle.png" *ngIf="last_ACC == null" width="20" height="20" />\n          <p>{{ "IGN" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/ac_na.png" *ngIf="acModel == null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/ac_present.png" *ngIf="acModel == \'1\'" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/ac_empty.png" *ngIf="acModel == \'0\'" width="20" height="20" />\n          <p>{{ "AC" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/fuel_available.png" *ngIf="currentFuel != null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/fuel_empty.png" *ngIf="currentFuel == null" width="20" height="20" />\n          <p>{{ "FUEL" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/power_na.png" *ngIf="power == null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/pwer_disconnected.png" *ngIf="power == \'0\'" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/power_connected.png" *ngIf="power == \'1\'" width="20" height="20" />\n          <p>{{ "POWER" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/gps_na.png" *ngIf="gpsTracking == null" width="30" height="20" />\n          <img src="assets/imgs/statusIcons/gps_notPresent.png" *ngIf="gpsTracking == \'0\'" width="30" height="20" />\n          <img src="assets/imgs/statusIcons/gps_present.png" *ngIf="gpsTracking == \'1\'" width="30" height="20" />\n          <p>{{ "GPS" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <ion-icon name="thermometer" *ngIf="tempreture == null" style="color: gray; font-size: 1.6em;"></ion-icon>\n          <ion-icon name="thermometer" *ngIf="tempreture == \'0\'" color="danger" style="font-size: 1.6em;"></ion-icon>\n          <ion-icon name="thermometer" *ngIf="tempreture == \'1\'" color="secondary" style="font-size: 1.6em;"></ion-icon>\n          <p>{{ "Temp" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/car_no_data.png" *ngIf="door == null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/car_door_open.png" *ngIf="door == 0" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/car_door_close.png" *ngIf="door == 1" width="20" height="20" />\n          <p>{{ "Door" | translate }}</p>\n        </ion-col>\n      </ion-row>\n      <hr />\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!total_odo">\n            N/A\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="total_odo">\n            {{ total_odo | number: "1.0-2" }}\n          </p>\n          <p style="font-size: 13px">{{ "Odometer" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!vehicle_speed">\n            0 {{ "Km/hr" | translate }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="vehicle_speed">\n            {{ vehicle_speed }} {{ "Km/hr" | translate }}\n          </p>\n          <p style="font-size: 13px">\n            {{ "Speed" | translate }}\n          </p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="fuel">\n            {{ fuel }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!fuel">N/A</p>\n          <p style="font-size: 13px">{{ "Fuel" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="voltage">\n            {{ voltage | number: "1.0-2" }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!voltage">N/A</p>\n          <p style="font-size: 13px">{{ "Voltage" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="tempreture">\n            {{ tempreture | number: "1.0-2" }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!tempreture">N/A</p>\n          <p style="font-size: 13px">{{ "Temp" | translate }}</p>\n        </ion-col>\n      </ion-row>\n      <hr />\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!distFromLastStop">\n            0{{ "Kms" | translate }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold" *ngIf="distFromLastStop">\n            {{ distFromLastStop | number: "1.0-2" }}{{ "Kms" | translate }}\n          </p>\n          <p style="font-size: 13px">{{ "From Last Stop" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <img src="assets/imgs/milestone-png-1.png" width="30" height="35" style="margin-top: 9%" />\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!todays_odo">\n            0{{ "Kms" | translate }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold" *ngIf="todays_odo">\n            {{ todays_odo | number: "1.0-2" }}{{ "Kms" | translate }}\n          </p>\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n        </ion-col>\n      </ion-row>\n      <hr />\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!timeAtLastStop">\n            N/A\n          </p>\n          <p style="font-size: 14px; font-weight: bold" *ngIf="timeAtLastStop">\n            {{ timeAtLastStop }}\n          </p>\n          <p style="font-size: 13px">{{ "At Last Stop" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <img src="assets/imgs/parkinglogo.png" width="30" height="30" style="margin-top: 9%" />\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!today_stopped">\n            N/A\n          </p>\n          <p style="font-size: 14px; font-weight: bold" *ngIf="today_stopped">\n            {{ today_stopped }}\n          </p>\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n        </ion-col>\n      </ion-row>\n      <hr />\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!lastStoppedAt">\n            N/A\n          </p>\n          <p style="font-size: 14px; font-weight: bold" *ngIf="lastStoppedAt">\n            {{ lastStoppedAt }}\n          </p>\n          <p style="font-size: 13px">{{ "From Last Stop" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <img src="assets/imgs/2530791.png" width="30" height="35" style="margin-top: 9%" />\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!today_running">\n            N/A\n          </p>\n          <p style="font-size: 14px; font-weight: bold" *ngIf="today_running">\n            {{ today_running }}\n          </p>\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-bottom-drawer>\n</div>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/live/live.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_11__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], LivePage);
    return LivePage;
}());

//# sourceMappingURL=live.js.map

/***/ })

});
//# sourceMappingURL=39.js.map