webpackJsonp([17],{

/***/ 1028:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YourTripsPageModule", function() { return YourTripsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__your_trips__ = __webpack_require__(1124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__dummy_directive__ = __webpack_require__(1125);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var YourTripsPageModule = /** @class */ (function () {
    function YourTripsPageModule() {
    }
    YourTripsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__your_trips__["a" /* YourTripsPage */],
                __WEBPACK_IMPORTED_MODULE_5__dummy_directive__["a" /* OnCreate */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__your_trips__["a" /* YourTripsPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_5__dummy_directive__["a" /* OnCreate */]
            ],
        })
    ], YourTripsPageModule);
    return YourTripsPageModule;
}());

//# sourceMappingURL=your-trips.module.js.map

/***/ }),

/***/ 1124:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YourTripsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var YourTripsPage = /** @class */ (function () {
    function YourTripsPage(apicall, navCtrl, navParams, toastCtrl, geocoderApi) {
        this.apicall = apicall;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.geocoderApi = geocoderApi;
        this.portstemp = [];
        this._tripList = [];
        this.skip = 1;
        this.limit = 10;
        this._tripListKey = [];
        this.twoMonthsLater = __WEBPACK_IMPORTED_MODULE_3_moment__().subtract(2, 'month').format("YYYY-MM-DD");
        this.today = __WEBPACK_IMPORTED_MODULE_3_moment__().format("YYYY-MM-DD");
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        this.getdevices();
    }
    YourTripsPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter YourTripsPage');
    };
    YourTripsPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apicall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicall.startLoading().present();
        this.apicall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicall.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apicall.stopLoading();
            console.log(err);
        });
    };
    YourTripsPage.prototype.getID = function (val) {
        this.trip_id = val._id;
    };
    YourTripsPage.prototype.getTrips = function (infiniteScroll) {
        var _this = this;
        var url;
        var that = this;
        if (!infiniteScroll) {
            this._tripListKey = [];
            this._tripList = [];
        }
        if (this.trip_id != undefined) {
            url = this.apicall.mainUrl + "user_trip/getLastTrip?device=" + this.trip_id + "&tripInfo=all_trip&user=" + this.islogin._id + "&fromDate=" + new Date(this.datetimeStart).toISOString() + "&toDate=" + new Date(this.datetimeEnd).toISOString() + "&skip=" + that.skip + "&limit=" + that.limit;
        }
        else {
            url = this.apicall.mainUrl + "user_trip/getLastTrip?&tripInfo=all_trip&user=" + this.islogin._id + "&fromDate=" + new Date(this.datetimeStart).toISOString() + "&toDate=" + new Date(this.datetimeEnd).toISOString() + "&skip=" + that.skip + "&limit=" + that.limit;
        }
        if (!infiniteScroll) {
            this.apicall.startLoading().present();
            this.apicall.getSOSReportAPI(url)
                .subscribe(function (data) {
                _this.apicall.stopLoading();
                console.log("trip data: ", data);
                if (!data.message) {
                    _this.innerFunc(data, infiniteScroll);
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: "Trip(s) not found..!",
                        duration: 1500,
                        position: "bottom"
                    });
                    toast.present();
                }
            }, function (err) {
                _this.apicall.stopLoading();
            });
        }
        else {
            this.apicall.getSOSReportAPI(url)
                .subscribe(function (data) {
                console.log("trip data: ", data);
                debugger;
                if (!data.message) {
                    _this.innerFunc(data, infiniteScroll);
                }
            });
        }
    };
    YourTripsPage.prototype.innerFunc = function (data, infiniteScroll) {
        var outerthis = this;
        outerthis.startAdd = undefined;
        outerthis.endAdd = undefined;
        var i = 0, howManyTimes = data.length;
        function f() {
            var date1 = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(data[i].start_time), 'DD/MM/YYYY').format("LLLL");
            var str = date1.split(', ');
            var date2 = str[0];
            var date3 = str[1].split(' ');
            var date4 = str[2].split(' ');
            var date5 = date4[0];
            var date6 = date3[0];
            var date7 = date3[1];
            var date8 = date6 + ' ' + date5;
            var date9 = date4[1];
            outerthis._tripList.push({
                "date1": date2,
                "date2": date8,
                "date3": date7,
                "dtime": date9,
                "trip_name": data[i].trip_name,
                "trip_status": data[i].trip_status,
                "start_location": {
                    "lat": data[i].start_lat,
                    "long": data[i].start_long
                },
                "end_location": {
                    "lat": data[i].end_lat,
                    "long": data[i].end_long
                }
            });
            outerthis.start_address(outerthis._tripList[i], i);
            outerthis.end_address(outerthis._tripList[i], i);
            if (infiniteScroll) {
                outerthis._tripListKey.push(outerthis._tripList);
                // infiniteScroll.complete();
            }
            else {
                // infiniteScroll.complete();
                outerthis._tripListKey = outerthis._tripList;
            }
            console.log(outerthis._tripListKey);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    YourTripsPage.prototype.start_address = function (item, index) {
        var that = this;
        that._tripList[index].StartLocation = "N/A";
        if (!item.start_location) {
            that._tripList[index].StartLocation = "N/A";
        }
        else if (item.start_location) {
            this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
                .then(function (res) {
                console.log("test", res);
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
                that._tripList[index].StartLocation = str;
            });
        }
    };
    YourTripsPage.prototype.end_address = function (item, index) {
        var that = this;
        that._tripList[index].EndLocation = "N/A";
        if (!item.end_location) {
            that._tripList[index].EndLocation = "N/A";
        }
        else if (item.end_location) {
            this.geocoderApi.reverseGeocode(Number(item.end_location.lat), Number(item.end_location.long))
                .then(function (res) {
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
                that._tripList[index].EndLocation = str;
            });
        }
    };
    YourTripsPage.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apicall.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            console.log("check if address is stored in db or not? ", respData);
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    YourTripsPage.prototype.doInfinite = function (infiniteScroll) {
        var that = this;
        that.skip = that.skip + 1;
        setTimeout(function () {
            that.getTrips(infiniteScroll);
            infiniteScroll.complete();
        }, 200);
    };
    YourTripsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-your-trips',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/your-trips/your-trips.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{"Your Trips" | translate}}</ion-title>\n  </ion-navbar>\n  <ion-item style="background-color: #fafafa;">\n    <ion-label style="margin-top: 15px;">{{"Select Vehicle" | translate}}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getID(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n  <ion-row style="background-color: #fafafa;" padding-left padding-right>\n    <ion-col width-20>\n\n      <ion-label>\n        <span style="font-size: 13px">{{"From Date" | translate}}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [min]="twoMonthsLater" [max]="today" [(ngModel)]="datetimeStart"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{"To Date" | translate}}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right">\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getTrips();"></ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n\n<ion-content>\n  <ion-card *ngFor="let t of _tripListKey; let i=index">\n    <ion-row>\n      <ion-col col-4 style="background-color: #ee612e;">\n        <ion-row>\n          <ion-col col-12 style="text-align: center;">\n            <ion-icon name="car" color="light" style="font-size: 1.8em;"></ion-icon>\n          </ion-col>\n        </ion-row>\n        <ion-row style="text-align: center;">\n          <ion-col col-12 style="color:white; font-size: 1.8em; font-weight: 300">\n            {{t.date3}}\n          </ion-col>\n          <ion-col col-12 style="color: #f2f2f2; font-size: 1.1em;">\n            {{t.date1}}\n          </ion-col>\n          <ion-col col-12 style="color:white;font-size: 1.1em;">\n            <b>{{t.date2}}</b>\n          </ion-col>\n        </ion-row>\n        <ion-row style="border-top: 2px solid white; text-align: center; color:white;">\n          <ion-col col-12>\n            {{t.dtime}}\n          </ion-col>\n        </ion-row>\n      </ion-col>\n      <ion-col col-8>\n        <ion-row>\n          <ion-col col-12 style="font-size: 1.2em">\n            {{t.trip_name}}\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-12 style="font-size: 1.5em">\n            {{t.saddkey}} - {{t.eaddkey}}\n          </ion-col>\n          <ion-col col-12>\n            <ion-row class="card11">\n              <ion-col col-1>\n                <ion-icon name="pin" color="secondary"></ion-icon>\n              </ion-col>\n              <ion-col col-11 (onCreate)="start_address(t, i)"  style="font-size: 0.8em;">\n                <div class="overme">\n                  {{ t.StartLocation }}\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n          <ion-col col-12>\n            <ion-row class="card11">\n              <ion-col col-1>\n                <ion-icon name="pin" color="danger"></ion-icon>\n              </ion-col>\n              <ion-col col-11 (onCreate)="end_address(t, i)"  style="font-size: 0.8em;">\n                <div class="overme">\n                  {{ t.EndLocation }}\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-12 style="text-align: right;">\n            <button ion-button small clear color="secondary" *ngIf="t.trip_status == \'Started\'">Ongoing</button>\n            <button ion-button small clear color="gpsc" *ngIf="t.trip_status != \'Started\'">{{t.trip_status}}</button>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n    </ion-infinite-scroll-content>\n</ion-infinite-scroll>\n\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/your-trips/your-trips.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], YourTripsPage);
    return YourTripsPage;
}());

//# sourceMappingURL=your-trips.js.map

/***/ }),

/***/ 1125:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnCreate; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OnCreate = /** @class */ (function () {
    function OnCreate() {
        this.onCreate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    OnCreate.prototype.ngOnInit = function () {
        this.onCreate.emit('dummy');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], OnCreate.prototype, "onCreate", void 0);
    OnCreate = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[onCreate]'
        }),
        __metadata("design:paramtypes", [])
    ], OnCreate);
    return OnCreate;
}());

//# sourceMappingURL=dummy-directive.js.map

/***/ })

});
//# sourceMappingURL=17.js.map