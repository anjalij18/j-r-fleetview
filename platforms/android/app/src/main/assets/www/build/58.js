webpackJsonp([58],{

/***/ 1057:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GroupModalPage = /** @class */ (function () {
    function GroupModalPage(renderer, viewCtrl, formBuilder, apiCall, alerCtrl, toastCtrl) {
        this.renderer = renderer;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.apiCall = apiCall;
        this.alerCtrl = alerCtrl;
        this.toastCtrl = toastCtrl;
        this.GroupStatus = {};
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup', true);
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
        this.GroupStatus = [
            {
                name: "Active",
                value: true
            },
            {
                name: "InActive",
                value: false
            }
        ];
        this.GroupType = [
            {
                vehicle: "assets/imgs/car_blue_icon.png",
                name: "car"
            },
            {
                vehicle: "assets/imgs/bike_blue_icon.png",
                name: "bike"
            },
            {
                vehicle: "assets/imgs/truck2.png",
                name: "truck"
            }
        ];
        this.groupForm = formBuilder.group({
            group_name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            status: [''],
            grouptype: [''],
            address: [''],
            emailid: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email],
            mobno: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"]]
        });
    }
    GroupModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    GroupModalPage.prototype.clickedGroupDiv = function (group, index) {
        this.TypeOf_Device = group.name,
            console.log(this.TypeOf_Device);
        console.log(index);
        var selected = [false, false, false];
        for (var i = 0; i < selected.length; i++)
            document.getElementById("" + i + "").className = "group daysDeselected";
        console.log(document.getElementById("" + i + "").className);
        document.getElementById(index).className = "group daysSelected";
        selected[index] = true;
        console.log(selected);
    };
    GroupModalPage.prototype.GroupStatusdata = function (status) {
        console.log("group status=> " + status);
        this.groupstaus = status;
        console.log("group status id=> " + this.groupstaus.value);
    };
    GroupModalPage.prototype.addGroup = function () {
        var that = this;
        that.submitAttempt = true;
        if (that.groupForm.valid) {
            var devicedetails = {
                "name": that.groupForm.value.group_name,
                "status": that.groupstaus.value,
                "address": that.groupForm.value.address,
                "email": that.groupForm.value.email,
                "mobileNo": that.groupForm.value.mobno,
                "uid": that.islogin._id,
                "logopath": "car"
            };
            console.log(devicedetails);
            that.apiCall.startLoading().present();
            that.apiCall.addGroupCall(devicedetails)
                .subscribe(function (data) {
                that.apiCall.stopLoading();
                that.devicesadd = data;
                console.log("response from device=> " + that.devicesadd);
                var toast = that.toastCtrl.create({
                    message: 'Group was added successfully',
                    position: 'top',
                    duration: 2000
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    that.viewCtrl.dismiss();
                });
                toast.present();
            }, function (err) {
                that.apiCall.stopLoading();
                var body = err._body;
                var msg = JSON.parse(body);
                var alert = that.alerCtrl.create({
                    title: 'Oops!',
                    message: msg.message,
                    buttons: ['OK']
                });
                alert.present();
            });
        }
    };
    GroupModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-group-modal',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/customers/modals/group-modal/group-modal.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>{{\'addgroup\' | translate}}</ion-title>\n        <ion-buttons end>\n            <button ion-button icon-only (click)="dismiss()">\n                <ion-icon name="close-circle"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n<ion-content>\n\n    <form [formGroup]="groupForm">\n\n            <ion-item>\n                <ion-label fixed style="min-width: 50% !important;">{{\'Group Name\' | translate}}</ion-label>\n                <ion-input formControlName="group_name" type="text"></ion-input>\n            </ion-item>\n            <ion-item class="logitem1" *ngIf="!groupForm.controls.group_name.valid && (groupForm.controls.group_name.dirty || submitAttempt)">\n                <p>{{\'group name is required\' | translate}}</p>\n            </ion-item>\n\n\n            <ion-item>\n                <ion-label>{{\'Group Status\' | translate}}</ion-label>\n                <ion-select formControlName="status" style="min-width:50%;">\n                    <ion-option *ngFor="let statusname of GroupStatus" [value]="statusname.name" (ionSelect)="GroupStatusdata(statusname)">{{statusname.name}}</ion-option>\n                </ion-select>\n            </ion-item>\n\n\n            <ion-item>\n                <ion-label fixed style="min-width: 50% !important;">{{\'Address\' | translate}}</ion-label>\n                <ion-textarea formControlName="address"></ion-textarea>\n            </ion-item>\n\n\n            <ion-item>\n                <ion-label fixed style="min-width: 50% !important;">{{\'Email Id\' | translate}}</ion-label>\n                <ion-input type="email" formControlName="emailid"></ion-input>\n            </ion-item>\n            <ion-item class="logitem1" *ngIf="!groupForm.controls.emailid.valid && (groupForm.controls.emailid.dirty || submitAttempt)">\n                <p>{{\'please enter valid email id!\' | translate}}</p>\n            </ion-item>\n\n            <ion-item>\n                <ion-label fixed style="min-width: 50% !important;">{{\'Mobile Number\' | translate}}</ion-label>\n                <ion-input type="number" minlength="10" maxlength="10" formControlName="mobno"></ion-input>\n            </ion-item>\n            <ion-item class="logitem1" *ngIf="!groupForm.controls.mobno.valid && (groupForm.controls.mobno.dirty || submitAttempt)">\n                <p>{{\'mobile number should be 10 digits!\' | translate}}</p>\n            </ion-item>\n        </form>\n<!-- </div> -->\n<!-- </div> -->\n</ion-content>\n<ion-footer class="footSty">\n\n    <ion-toolbar>\n        <ion-row no-padding>\n            <ion-col width-50 style="text-align: center;">\n                <button ion-button clear color="light" (click)="addGroup()">{{\'ADD GROUP\' | translate}}</button>\n            </ion-col>\n        </ion-row>\n    </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/customers/modals/group-modal/group-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], GroupModalPage);
    return GroupModalPage;
}());

//# sourceMappingURL=group-modal.js.map

/***/ }),

/***/ 967:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupModalPageModule", function() { return GroupModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__group_modal__ = __webpack_require__(1057);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var GroupModalPageModule = /** @class */ (function () {
    function GroupModalPageModule() {
    }
    GroupModalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__group_modal__["a" /* GroupModalPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__group_modal__["a" /* GroupModalPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ]
        })
    ], GroupModalPageModule);
    return GroupModalPageModule;
}());

//# sourceMappingURL=group-modal.module.js.map

/***/ })

});
//# sourceMappingURL=58.js.map