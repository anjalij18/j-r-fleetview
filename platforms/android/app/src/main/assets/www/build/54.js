webpackJsonp([54],{

/***/ 1065:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DealerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dealer_perm_dealer_perm__ = __webpack_require__(556);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__AddPointsModalPage__ = __webpack_require__(557);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DealerPage = /** @class */ (function () {
    function DealerPage(navCtrl, navParams, apiCall, modalCtrl, toastCtrl, alerCtrl, events, translate, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.events = events;
        this.translate = translate;
        this.loadingCtrl = loadingCtrl;
        this.islogin = {};
        this.page = 1;
        this.limit = 5;
        this.DealerArraySearch = [];
        // DealerArray: any = [];
        this.ndata = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    DealerPage.prototype.ionViewDidEnter = function () {
        this.getDealersListCall();
    };
    DealerPage.prototype.getDealersListCall = function () {
        var _this = this;
        this.page = 1;
        this.DealerArraySearch = [];
        this.apiCall.startLoading().present();
        this.apiCall.getDealersCall(this.islogin._id, this.page, this.limit, this.searchKey)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            // this.DealerArray = data;
            _this.DealerArraySearch = data;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("getting error from server=> ", err);
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant('No Dealer(s) found'),
                duration: 2000,
                position: "bottom"
            });
            toast.present();
            toast.onDidDismiss(function () {
                _this.navCtrl.setRoot("AddDevicesPage");
            });
        });
    };
    DealerPage.prototype.addDealersModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create('AddDealerPage');
        modal.onDidDismiss(function () {
            _this.getDealersListCall();
        });
        modal.present();
    };
    DealerPage.prototype._editDealer = function (item) {
        var _this = this;
        var modal = this.modalCtrl.create('EditDealerPage', {
            param: item
        });
        modal.onDidDismiss(function () {
            _this.getDealersListCall();
        });
        modal.present();
    };
    DealerPage.prototype.DeleteDealer = function (_id) {
        var _this = this;
        var alert = this.alerCtrl.create({
            message: this.translate.instant('doyouwanttodeletedealer', { value: this.translate.instant('Dealers') }),
            buttons: [{
                    text: this.translate.instant('NO')
                },
                {
                    text: this.translate.instant('Yes'),
                    handler: function () {
                        _this.deleteDeal(_id);
                    }
                }]
        });
        alert.present();
    };
    DealerPage.prototype.deleteDeal = function (_id) {
        var _this = this;
        var deletePayload = {
            'userId': _id,
            'deleteuser': true
        };
        this.apiCall.startLoading().present();
        this.apiCall.deleteDealerCall(deletePayload).
            subscribe(function (data) {
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant("Deleted successfully.", { value: _this.translate.instant('Dealers') }),
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                _this.getDealersListCall();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    DealerPage.prototype.callSearch = function (ev) {
        var _this = this;
        this.page = 1;
        this.searchKey = ev.target.value;
        var loading = this.loadingCtrl.create({
            content: "Searching dealer please wait..."
        });
        loading.present();
        this.apiCall.getDealersCall(this.islogin._id, this.page, this.limit, this.searchKey)
            .subscribe(function (data) {
            loading.dismiss();
            _this.DealerArraySearch = data;
        }, function (err) {
            loading.dismiss();
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant("No Dealer found for search key '") + ev.target.value + "' ..",
                duration: 1500,
                position: "bottom"
            });
            toast.present();
            toast.onDidDismiss(function () { });
        });
    };
    DealerPage.prototype.onClear = function (ev) {
        this.searchKey = undefined;
        this.getDealersListCall();
        ev.target.value = '';
    };
    DealerPage.prototype.switchDealer = function (_id) {
        var _this = this;
        localStorage.setItem('superAdminData', JSON.stringify(this.islogin));
        localStorage.setItem('custumer_status', 'OFF');
        localStorage.setItem('dealer_status', 'ON');
        this.apiCall.getcustToken(_id)
            .subscribe(function (res) {
            var custToken = res;
            var logindata = JSON.stringify(custToken);
            var logindetails = JSON.parse(logindata);
            var userDetails = window.atob(logindetails.custumer_token.split('.')[1]);
            var details = JSON.parse(userDetails);
            localStorage.setItem("loginflag", "loginflag");
            localStorage.setItem('details', JSON.stringify(details));
            var dealerSwitchObj = {
                "logindata": logindata,
                "details": userDetails,
                'condition_chk': details.isDealer
            };
            var temp = localStorage.getItem('isDealervalue');
            _this.events.publish("event_sidemenu", JSON.stringify(dealerSwitchObj));
            _this.events.publish("sidemenu:event", temp);
            _this.navCtrl.setRoot('AddDevicesPage');
        }, function (err) {
            console.log(err);
        });
    };
    DealerPage.prototype.addPonits = function (item) {
        console.log("add points: ", item);
        this.presentModal(item);
    };
    DealerPage.prototype.presentModal = function (param) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__AddPointsModalPage__["a" /* AddPointsModalPage */], { params: param });
        modal.present();
        modal.onDidDismiss(function (data) {
            console.log("onDidDismiss", data);
            //   this.getIdlePoints(data);
        });
    };
    DealerPage.prototype.dealerStatus = function (data) {
        var _this = this;
        var msg;
        if (data.status) {
            msg = this.translate.instant('deactivateDealer', { value: this.translate.instant('Dealers') });
        }
        else {
            msg = this.translate.instant('deactivateDealer', { value: this.translate.instant('Dealers') });
        }
        var alert = this.alerCtrl.create({
            message: msg,
            buttons: [{
                    text: this.translate.instant('Yes'),
                    handler: function () {
                        _this.user_status(data);
                    }
                },
                {
                    text: this.translate.instant('NO'),
                    handler: function () {
                        _this.getDealersListCall();
                    }
                }]
        });
        alert.present();
    };
    DealerPage.prototype.user_status = function (data) {
        var _this = this;
        var stat;
        if (data.status) {
            stat = false;
        }
        else {
            stat = true;
        }
        var ddata = {
            "uId": data._id,
            "loggedIn_id": this.islogin._id,
            "status": stat
        };
        this.apiCall.startLoading().present();
        this.apiCall.user_statusCall(ddata)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant('Dealer status updated successfully!'),
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                _this.getDealersListCall();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    DealerPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        // debugger
        var that = this;
        that.page = that.page + 1;
        // setTimeout(() => {
        // that.ndata = [];
        this.apiCall.getDealersCall(that.islogin._id, that.page, that.limit, that.searchKey)
            .subscribe(function (data) {
            // that.ndata = data;
            // for (let i = 0; i < that.ndata.length; i++) {
            //     that.DealerData.push(that.ndata[i]);
            // }
            for (var i = 0; i < data.length; i++) {
                that.DealerArraySearch.push(data[i]);
            }
            // that.DealerArraySearch = that.DealerData;
            infiniteScroll.complete();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error found=> " + err);
        });
        // }, 500);
    };
    DealerPage.prototype.dealerPerm = function (dealer) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__dealer_perm_dealer_perm__["a" /* DealerPermModalPage */], {
            param: dealer
        });
        modal.present();
        modal.onDidDismiss(function (data) {
            console.log("onDidDismiss", data);
            _this.getDealersListCall();
        });
    };
    DealerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-customers',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/dealers/dealers.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{\'Dealers\' | translate}}</ion-title>\n        <ion-buttons end>\n            <button ion-button icon-only (click)="addDealersModal()">\n                <ion-icon name="add"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n    <ion-searchbar (ionInput)="callSearch($event)" (ionClear)="onClear($event)"></ion-searchbar>\n</ion-header>\n\n<ion-content>\n    <ion-list>\n        <div *ngFor="let item of DealerArraySearch">\n\n            <ion-item>\n                <ion-thumbnail item-start>\n                    <img src="assets/imgs/user.png" alt="item.Device_Name">\n                    <ion-icon name="trash" style="margin-left: 41%;margin-top: 10%;font-size: 30px;color: #b9002f;"\n                        (tap)="DeleteDealer(item._id)"></ion-icon>\n                </ion-thumbnail>\n                <ion-row>\n                    <ion-col col-8 style="padding: 0px;">\n                        <p>\n                            <span ion-text color="dark">{{\'Name:\' | translate}} </span>\n                            {{item.first_name}}&nbsp;{{item.last_name}}\n                        </p>\n                    </ion-col>\n                    <ion-col col-4 style="padding: 0px;">\n                        <ion-badge color="light" (tap)="addPonits(item)">Add Points</ion-badge>\n                    </ion-col>\n                </ion-row>\n                <div (tap)="switchDealer(item._id)">\n                    <ion-row>\n                        <ion-col col-12 style="padding: 0px;">\n                            <p>\n                                <span ion-text color="dark">{{\'Email:\' | translate}} </span>\n                                {{item.email}}\n                            </p>\n                        </ion-col>\n                    </ion-row>\n                    <ion-row>\n                        <ion-col col-12 style="padding: 0px;">\n                            <p *ngIf="item.pass">\n                                <span ion-text color="dark">{{\'Password:\' | translate}} </span>\n                                {{item.pass}}\n                            </p>\n                            <p *ngIf="!item.pass">\n                                <span ion-text color="dark">{{\'Password:\' | translate}} </span>\n                                Not Saved\n                            </p>\n                        </ion-col>\n                    </ion-row>\n                    <ion-row>\n                        <ion-col col-12 style="padding: 0px;">\n                            <p>\n                                <span ion-text color="dark">{{\'Phone:\' | translate}} </span>\n                                <a href="tel:{{item.phone}}">{{item.phone}}</a>\n                            </p>\n                        </ion-col>\n                    </ion-row>\n                    <ion-row>\n                        <ion-col col-12 style="padding: 0px;">\n                            <p>\n                                <span ion-text color="dark">{{\'Created On:\' | translate}} </span>\n                                {{item.created_on | date:\'shortDate\'}}\n                            </p>\n                        </ion-col>\n                    </ion-row>\n                    <ion-row>\n                        <ion-col col-12 style="padding: 0px;">\n                            <p>\n                                <span ion-text color="dark">{{\'Total Vehicles:\' | translate}} </span>\n                                <span ion-text color="danger">{{item.total_vehicle}}</span>\n                            </p>\n                        </ion-col>\n                    </ion-row>\n                </div>\n                \n\n                <p>\n                    <ion-row>\n                        <ion-col col-3 style="padding: 0px;">\n                            <button ion-button small (click)="_editDealer(item)">{{\'Edit\' | translate}}</button>\n                        </ion-col>\n                        <ion-col col-4 style="text-align: center;padding: 0px;">\n                            <button ion-button small (click)="dealerStatus(item)" *ngIf="item.status == true"\n                                color="mypink">{{\'active\' | translate}}</button>\n                            <button ion-button small color="danger" (click)="dealerStatus(item)"\n                                *ngIf="item.status != true">{{\'inactive\' | translate}}</button>\n                        </ion-col>\n                        <ion-col col-5 style="padding: 0px;">\n                            <button ion-button small color="primary"\n                                (click)="dealerPerm(item)">{{\'Permission\' | translate}}</button>\n                        </ion-col>\n                    </ion-row>\n                </p>\n            </ion-item>\n\n        </div>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n        <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="{{\'Loading more data...\' | translate}}">\n        </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/dealers/dealers.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], DealerPage);
    return DealerPage;
}());

//# sourceMappingURL=dealers.js.map

/***/ }),

/***/ 974:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DealerPageModule", function() { return DealerPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dealers__ = __webpack_require__(1065);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DealerPageModule = /** @class */ (function () {
    function DealerPageModule() {
    }
    DealerPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__dealers__["a" /* DealerPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__dealers__["a" /* DealerPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ]
        })
    ], DealerPageModule);
    return DealerPageModule;
}());

//# sourceMappingURL=dealers.module.js.map

/***/ })

});
//# sourceMappingURL=54.js.map