webpackJsonp([24],{

/***/ 1026:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TripReviewPageModule", function() { return TripReviewPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__trip_review__ = __webpack_require__(1121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var TripReviewPageModule = /** @class */ (function () {
    function TripReviewPageModule() {
    }
    TripReviewPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__trip_review__["a" /* TripReviewPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_3__trip_review__["a" /* TripReviewPage */]),
                __WEBPACK_IMPORTED_MODULE_2_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], TripReviewPageModule);
    return TripReviewPageModule;
}());

//# sourceMappingURL=trip-review.module.js.map

/***/ }),

/***/ 1121:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TripReviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TripReviewPage = /** @class */ (function () {
    function TripReviewPage(navParams, apiCall, alertCtrl, plt) {
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.plt = plt;
        this.target = 0;
        this.allData = {};
        this.measurementUnit = 'MKS';
        debugger;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("pramas=> " + JSON.parse(JSON.stringify(navParams.get("params"))));
        this.tripData = navParams.get("params");
        this.Device_Id = this.tripData.Device_ID;
    }
    TripReviewPage.prototype.ngOnInit = function () {
        // localStorage.removeItem("tripTarget");
        // this.triphistory();
    };
    TripReviewPage.prototype.ionViewDidEnter = function () {
        this.getDefaultUserSettings();
        localStorage.removeItem("tripTarget");
        this.triphistory();
    };
    TripReviewPage.prototype.getDefaultUserSettings = function () {
        var _this = this;
        var b_url = this.apiCall.mainUrl + "users/get_user_setting";
        var Var = { uid: this.islogin._id };
        this.apiCall.urlpasseswithdata(b_url, Var)
            .subscribe(function (resp) {
            console.log("check lang key: ", resp);
            if (resp.unit_measurement !== undefined) {
                _this.measurementUnit = resp.unit_measurement;
            }
            else {
                if (localStorage.getItem('MeasurementType') !== null) {
                    var measureType = localStorage.getItem('MeasurementType');
                    _this.measurementUnit = measureType;
                }
                else {
                    _this.measurementUnit = 'MKS';
                }
            }
        }, function (err) {
            console.log(err);
            if (localStorage.getItem('MeasurementType') !== null) {
                var measureType = localStorage.getItem('MeasurementType');
                _this.measurementUnit = measureType;
            }
            else {
                _this.measurementUnit = 'MKS';
            }
        });
    };
    TripReviewPage.prototype.onShare = function () {
        var tempalert = this.alertCtrl.create({
            title: 'Share Trip',
            message: 'Enter your Mobile Number so that we can share you a trip',
            inputs: [
                {
                    name: 'mobno',
                    placeholder: 'Mobile Number',
                    type: 'number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Share',
                    handler: function (data) {
                        var sNumber = data.mobno.toString();
                        if (sNumber.length == 10) {
                            console.log(data.mobno);
                        }
                        else {
                            alert("Plaese provide valid mobile number");
                        }
                    }
                }
            ]
        });
        tempalert.present();
    };
    TripReviewPage.prototype.triphistory = function () {
        debugger;
        var that = this;
        that.apiCall.startLoading().present();
        that.apiCall.tripReviewCall(that.Device_Id, that.tripData.start_time, that.tripData.end_time)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            that.points = data;
            that.mapData = [];
            that.mapData = that.points.map(function (d) {
                return { lat: d.lat, lng: d.lng };
            });
            // that.points.reverse();
            var latLngBounds = new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["g" /* LatLngBounds */](that.mapData);
            that.dataArrayCoords = [];
            for (var i = that.points.length - 1; i > 0; i--) {
                if (that.points[i].lat && that.points[i].lng) {
                    var arr = [];
                    var startdatetime = new Date(that.points[i].insertionTime);
                    arr.push(that.points[i].lat);
                    arr.push(that.points[i].lng);
                    arr.push({ "time": startdatetime.toLocaleString() });
                    arr.push({ "speed": that.points[i].speed });
                    that.dataArrayCoords.push(arr);
                }
            }
            setInterval(function () {
                that.dataArrayCoords;
            }, 0);
            // console.log(that.dataArrayCoords);
            that.playbackData = [];
            for (var j = 0; j < that.points.length; j++) {
                var startdatetime1 = new Date(that.points[j].insertionTime);
                that.playbackData.push({ "lat": that.points[j].latDecimal, "lng": that.points[j].longDecimal, 'speed': that.points[j].speed + "Km/hr", 'isettiontime': startdatetime1.toLocaleString() });
                that.speed1 = that.playbackData[j].speed;
                setInterval(function () {
                    that.speed1;
                }, 0);
            }
            if (that.allData.map != undefined) {
                that.allData.map.remove();
            }
            that.allData.map = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["b" /* GoogleMaps */].create('trip_map_canvas');
            that.allData.map.moveCamera({
                'target': latLngBounds
            });
            that.allData.map.addMarker({
                title: 'S',
                position: that.mapData[0],
                icon: 'green',
                styles: {
                    'text-align': 'center',
                    'font-style': 'italic',
                    'font-weight': 'bold',
                    'color': 'red'
                },
            }).then(function (marker) {
                marker.showInfoWindow();
                that.allData.map.addMarker({
                    title: 'D',
                    position: that.mapData[that.mapData.length - 1],
                    icon: 'red',
                    styles: {
                        'text-align': 'center',
                        'font-style': 'italic',
                        'font-weight': 'bold',
                        'color': 'green'
                    },
                }).then(function (marker) {
                    marker.showInfoWindow();
                    // animateMarker(marker, dataArrayCoords, speed, trackerType)
                });
            });
            // console.log("latlang.............", that.mapData)
            if (that.mapData.length > 1) {
                that.allData.map.addPolyline({
                    points: that.mapData,
                    color: 'blue',
                    width: 4,
                    geodesic: true
                });
            }
        }, function (err) {
            console.log(err);
            that.apiCall.stopLoading();
        });
    };
    ;
    TripReviewPage.prototype.Playback = function () {
        var that = this;
        if (localStorage.getItem("tripTarget") != null) {
            that.target = JSON.parse(localStorage.getItem("tripTarget"));
        }
        that.playing = !that.playing; // This would alternate the state each time
        var coord = that.dataArrayCoords[that.target];
        that.coordreplaydata = coord;
        var lat = coord[0];
        var lng = coord[1];
        that.startPos = [lat, lng];
        that.speed = 200; // km/h
        if (that.playing) {
            console.log("if initialize=> ", that.playing);
            that.allData.map.setCameraTarget({ lat: lat, lng: lng });
            if (that.allData.mark == undefined) {
                var markicon;
                if (this.plt.is('android')) {
                    markicon = './assets/imgs/vehicles/runningcar.png';
                }
                else if (this.plt.is('ios')) {
                    markicon = 'www/assets/imgs/vehicles/runningcar.png';
                }
                that.allData.map.addMarker({
                    icon: {
                        url: markicon,
                        size: {
                            width: 20,
                            height: 40
                        }
                    },
                    styles: {
                        'text-align': 'center',
                        'font-style': 'italic',
                        'font-weight': 'bold',
                        'color': 'green'
                    },
                    position: new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["f" /* LatLng */](that.startPos[0], that.startPos[1]),
                }).then(function (marker) {
                    that.allData.mark = marker;
                    that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
                });
            }
            else {
                that.allData.mark.setPosition(new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["f" /* LatLng */](that.startPos[0], that.startPos[1]));
                that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
            }
        }
        else {
            that.allData.mark.setPosition(new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["f" /* LatLng */](that.startPos[0], that.startPos[1]));
        }
    };
    TripReviewPage.prototype.liveTrack = function (map, mark, coords, target, startPos, speed, delay) {
        var that = this;
        var target = target;
        if (!startPos.length)
            coords.push([startPos[0], startPos[1]]);
        function _gotoPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000;
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["f" /* LatLng */](coords[target][0], coords[target][1]);
            var distance = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); //in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target][0] - lat) / numStep;
            var deltaLng = (coords[target][1] - lng) / numStep;
            function changeMarker(mark, deg) {
                console.log("check marker: ", mark);
                if (Number.isNaN(parseInt(deg))) {
                    console.log("check degree: " + parseInt(deg));
                }
                else {
                    console.log("check not: " + parseInt(deg));
                    if (mark) {
                        mark.setRotation(deg);
                    }
                }
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                if (i < distance) {
                    if ((head !== 0) || (head !== NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    setTimeout(_moveMarker, delay);
                }
                else {
                    if ((head !== 0) || (head !== NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(dest);
                    target++;
                    setTimeout(_gotoPoint, delay);
                }
            }
            a++;
            if (a > coords.length) {
            }
            else {
                that.speedMarker = coords[target][3].speed;
                that.updatetimedate = coords[target][2].time;
                if (that.playing) {
                    _moveMarker();
                    target = target;
                    localStorage.setItem("tripTarget", target);
                }
                else { }
            }
        }
        var a = 0;
        _gotoPoint();
    };
    TripReviewPage.prototype.inter = function (fastforwad) {
        var that = this;
        if (fastforwad == 'fast') {
            that.speed = 2 * that.speed;
        }
        else if (fastforwad == 'slow') {
            if (that.speed > 50) {
                that.speed = that.speed / 2;
            }
            else {
            }
        }
        else {
            that.speed = 200;
        }
    };
    TripReviewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-trip-review',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/trip-report/trip-review/trip-review.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>{{"Trip Review" | translate}}</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content>\n    <div id="trip_map_canvas">\n        <ion-fab top right>\n            <button ion-fab mini (click)="onShare()" color="gpsc">\n                <ion-icon name="share" color="black"></ion-icon>\n            </button>\n        </ion-fab>\n    </div>\n</ion-content>\n\n<ion-footer class="footSty">\n\n    <ion-row style="background-color: #dfdfdf; padding: 0px !important;">\n        <ion-col width-50 style="padding: 0px">\n            <p style="color:black;font-size:14px; text-align:center;">\n                <ion-icon name="time" style="color:#33cd5f;font-size:15px;"></ion-icon>&nbsp;\n                <span *ngIf="updatetimedate">{{updatetimedate}}&nbsp;</span>\n                <span *ngIf="!updatetimedate">0:0&nbsp;</span>\n            </p>\n        </ion-col>\n\n        <ion-col width-50 style="padding: 0px">\n            <p style="color:black;font-size:14px;text-align:center;">\n                <ion-icon name="speedometer" style="color:#cd4343"></ion-icon>&nbsp;\n                <span *ngIf="speedMarker">{{speedMarker}}&nbsp;{{ (measurementUnit == \'MKS\') ? ("Km/hr" | translate) : ("Miles/hr" | translate)}}</span>\n                <span *ngIf="!speedMarker">0&nbsp;{{ (measurementUnit == \'MKS\') ? ("Km/hr" | translate) : ("Miles/hr" | translate)}}</span>\n            </p>\n        </ion-col>\n    </ion-row>\n\n    <ion-toolbar>\n        <div style="text-align: center;">\n            <button ion-button clear>\n                <ion-icon color="light" name="rewind" style="font-size:26px;margin-right: 17px" (click)="inter(\'slow\')"></ion-icon>\n                <ion-icon color="light" name="arrow-dropright-circle" style="font-size:26px;margin-right: 15px" class="play" *ngIf="!playing"\n                    (click)="Playback()"></ion-icon>\n                <ion-icon color="light" name="pause" style="font-size:26px;margin-right: 15px" class="pause" *ngIf="playing" (click)="Playback()"></ion-icon>\n                <ion-icon color="light" name="fastforward" style="font-size:26px;margin-right: 17px" (click)="inter(\'fast\')"></ion-icon>\n            </button>\n        </div>\n    </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/trip-report/trip-review/trip-review.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"]])
    ], TripReviewPage);
    return TripReviewPage;
}());

//# sourceMappingURL=trip-review.js.map

/***/ })

});
//# sourceMappingURL=24.js.map