webpackJsonp([29],{

/***/ 1017:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutePageModule", function() { return RoutePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__route__ = __webpack_require__(1110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { CreateRoutePage } from '../create-route/create-route';
var RoutePageModule = /** @class */ (function () {
    function RoutePageModule() {
    }
    RoutePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__route__["a" /* RoutePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__route__["a" /* RoutePage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
            // CreateRoutePage
            ]
        })
    ], RoutePageModule);
    return RoutePageModule;
}());

//# sourceMappingURL=route.module.js.map

/***/ }),

/***/ 1110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoutePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { EditRouteDetailsPage } from './edit-route-details/edit-route-details';
// import { RouteMapShowPage } from '../route-map-show/route-map-show';
// import { CreateRoutePage } from './create-route/create-route';
var RoutePage = /** @class */ (function () {
    function RoutePage(navCtrl, navParams, apiCall, modalCtrl, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log(this.islogin._id);
    }
    RoutePage.prototype.ngOnInit = function () {
        this.getRoutes();
    };
    RoutePage.prototype.getRoutes = function () {
        var _this = this;
        console.log("getRoutes");
        var baseURLp = this.apiCall.mainUrl + 'trackRoute/user/' + this.islogin._id;
        //console.log(baseURLp);
        this.apiCall.startLoading().present();
        this.apiCall.getRoutesCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.routesdata = data.reverse();
            console.log(_this.routesdata);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    // createRoute() {
    //   this.navCtrl.push(CreateRoutePage)
    // }
    // searchUser(ev: any) {
    //   // Reset items back to all of the items
    //   // this.getcustomer();
    //   // set val to the value of the searchbar
    //   let val = ev.target.value;
    //   // if the value is an empty string don't filter the items
    //   if (val && val.trim() != '') {
    //     this.routesdata = this.routesdata.filter((item) => {
    //       return (item.first_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    //     })
    //   }
    // }
    RoutePage.prototype.ShowRoute = function (routes) {
        console.log('data pass route', routes);
        this.navCtrl.push('RouteMapShowPage', {
            param: routes
        });
    };
    // openroute_edit(routes) {
    //   console.log('Opening Modal open update deviceModal');
    //   let modal = this.modalCtrl.create(EditRouteDetailsPage, {
    //     param: routes
    //   })
    //   modal.present();
    //   // $rootScope.routesdetail = angular.copy(routes1);
    //   // console.log($rootScope.routesdetail);
    // };
    RoutePage.prototype.deleteFunc = function (_id) {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.trackRouteCall(_id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            if (data) {
                // console.log(this.DeletedDevice);
                var toast = _this.toastCtrl.create({
                    message: 'Route was deleted successfully',
                    position: 'bottom',
                    duration: 1500
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    _this.getRoutes();
                });
                toast.present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("deleteing route error=> " + err);
            var toast = _this.toastCtrl.create({
                message: 'Route was deleted successfully',
                position: 'bottom',
                duration: 1500
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.getRoutes();
            });
            toast.present();
            // var body = err._body;
            // var msg = JSON.parse(body);
            // let alert = this.alertCtrl.create({
            //   title: 'Oops!',
            //   message: msg.message,
            //   buttons: ['OK']
            // });
            // alert.present();
        });
    };
    RoutePage.prototype.DelateRoute = function (data) {
        var _this = this;
        console.log(data._id);
        var alert = this.alertCtrl.create({
            message: 'Do you want to delete this route?',
            buttons: [{
                    text: 'NO'
                },
                {
                    text: 'YES',
                    handler: function () {
                        _this.deleteFunc(data._id);
                    }
                }]
        });
        alert.present();
    };
    RoutePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-route',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/route/route.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "Routes Details" | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-list>\n    <ion-item *ngFor="let routes of routesdata" (click)="ShowRoute(routes)">\n      <ion-thumbnail item-start>\n        <img src="assets/imgs/icon_1024.png" />\n      </ion-thumbnail>\n      <h2>{{ routes.name }}</h2>\n\n      <p *ngIf="routes.source" class="overme">\n        <ion-icon name="pin" color="secondary"></ion-icon>&nbsp;{{\n          routes.source\n        }}\n      </p>\n\n      <p *ngIf="routes.destination" class="overme">\n        <ion-icon name="pin" color="danger"></ion-icon>&nbsp;{{\n          routes.destination\n        }}\n      </p>\n\n      <p *ngIf="!routes.source">N/A</p>\n      <p *ngIf="!routes.destination">N/A</p>\n      <ion-row item-end>\n        <ion-col width-10>\n          <div style="margin-top:100%;">\n            <ion-icon\n              name="trash"\n              color="danger"\n              style="margin-right:15%;text-align: right;"\n              (click)="DelateRoute(routes)"\n            ></ion-icon>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/route/route.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], RoutePage);
    return RoutePage;
}());

//# sourceMappingURL=route.js.map

/***/ })

});
//# sourceMappingURL=29.js.map