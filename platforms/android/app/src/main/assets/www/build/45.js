webpackJsonp([45],{

/***/ 1089:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeofencePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GeofencePage = /** @class */ (function () {
    function GeofencePage(navCtrl, navParams, apiCall, toastCtrl, alerCtrl, modalCtrl, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.modalCtrl = modalCtrl;
        this.events = events;
        this.devices = [];
        this.creation_type = "circular";
        this.allData = {};
        this.allData._poiListData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.setsmsforotp = localStorage.getItem('setsms');
        this.isdevice = localStorage.getItem('cordinates');
        this.events.subscribe('reloadDetails', function () { });
    }
    GeofencePage.prototype.ngOnInit = function () {
        var _this = this;
        if (localStorage.getItem("SCREEN") != null) {
            this.navBar.backButtonClick = function (e) {
                console.log("back button poped");
                if (localStorage.getItem("SCREEN") != null) {
                    if (localStorage.getItem("SCREEN") === 'live') {
                        _this.navCtrl.setRoot('LivePage');
                    }
                    else {
                        if (localStorage.getItem("SCREEN") === 'dashboard') {
                            _this.navCtrl.setRoot('AddDevicesPage');
                        }
                    }
                }
            };
        }
        if (this.creation_type == 'circular') {
            this.devices = [];
            this.getCircularGeofence();
        }
        // this.getgeofence();
    };
    GeofencePage.prototype.ngOnDestroy = function () {
        if (this.allData.map) {
            this.allData.map.remove();
        }
    };
    GeofencePage.prototype.radioChecked = function (key) {
        if (key == "circular") {
            this.devices = [];
            this.getCircularGeofence();
        }
        else {
            if (key == "polygon") {
                this.allData._poiListData = [];
                if (this.allData.map) {
                    this.allData.map.remove();
                }
                this.getgeofence();
            }
        }
    };
    GeofencePage.prototype.getCircularGeofence = function () {
        var _this = this;
        this.apiCall.getPoisAPI(this.islogin._id)
            .subscribe(function (data) {
            // this.circularlist = data;
            _this.allData._poiListData = [];
            var i = 0, howManyTimes = data.length;
            var that = _this;
            function f() {
                that.allData._poiListData.push({
                    "geonamename": data[i].poi.poiname,
                    "radius": data[i].radius ? data[i].radius : 'N/A',
                    "address": data[i].poi.address ? data[i].poi.address : 'N/A',
                    "_id": data[i]._id
                });
                that.allData._poiListData[that.allData._poiListData.length - 1].mapid = "a" + i;
                that.allData.map = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["b" /* GoogleMaps */].create(that.allData._poiListData[that.allData._poiListData.length - 1].mapid, {
                    camera: {
                        target: {
                            lat: data[i].poi.location.coordinates[1],
                            lng: data[i].poi.location.coordinates[0]
                        },
                        zoom: 13
                    }
                });
                that.allData.map.addMarker({
                    position: {
                        lat: data[i].poi.location.coordinates[1],
                        lng: data[i].poi.location.coordinates[0]
                    }
                });
                var ltln = { "lat": data[i].poi.location.coordinates[1], "lng": data[i].poi.location.coordinates[0] };
                that.allData.map.addCircleSync({
                    'center': ltln,
                    'radius': data[i].radius,
                    'strokeColor': '#ee612e',
                    'strokeWidth': 2,
                    'fillColor': 'rgb(255, 128, 128, 0.5)'
                });
                // let circle: Circle = that.allData.map.addCircleSync({
                //   'center': ltln,
                //   'radius': data[i].radius,
                //   'strokeColor': '#ee612e',
                //   'strokeWidth': 2,
                //   'fillColor': 'rgb(255, 128, 128, 0.5)'
                // });
                i++;
                if (i < howManyTimes) {
                    setTimeout(f, 200);
                }
            }
            f();
        }, function (err) {
            // debugger
            if (JSON.parse(err._body).message === "no poi list found") {
                var toast = _this.toastCtrl.create({
                    message: "Oops..POI list not found.",
                    duration: 1800,
                    position: 'bottom'
                });
                toast.present();
            }
            else {
                return;
            }
            // console.log("error in pois: ", JSON.parse(JSON.stringify(err)).message)
            console.log("error in pois: ", JSON.parse(err._body).message);
        });
    };
    GeofencePage.prototype.addgeofence = function () {
        this.navCtrl.push('AddGeofencePage');
    };
    GeofencePage.prototype.getgeofence = function () {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.getallgeofenceCall(this.islogin._id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.devices = data;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error => ", err);
        });
    };
    GeofencePage.prototype.deleteGeo = function (_id) {
        var _this = this;
        // this.apiCall.startLoading().present();
        this.apiCall.deleteGeoCall(_id).
            subscribe(function (data) {
            // this.apiCall.stopLoading();
            _this.DeletedDevice = data;
            var toast = _this.toastCtrl.create({
                message: 'Deleted Geofence Area successfully.',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.getgeofence();
            });
            toast.present();
        }, function (err) {
            // this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alerCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    GeofencePage.prototype.DelateGeofence = function (_id) {
        var _this = this;
        var alert = this.alerCtrl.create({
            message: 'Do you want to delete this geofence area?',
            buttons: [{
                    text: 'No'
                },
                {
                    text: 'YES',
                    handler: function () {
                        _this.deleteGeo(_id);
                    }
                }]
        });
        alert.present();
    };
    GeofencePage.prototype.DisplayDataOnMap = function (item) {
        var _this = this;
        // console.log(item);
        // var baseURLp = 'http://13.126.36.205:3000/geofencing/geofencestatus?gid=' + item._id + '&status=' + item.status + '&entering=' + item.entering + '&exiting=' + item.exiting;
        this.apiCall.geofencestatusCall(item._id, item.status, item.entering, item.exiting)
            .subscribe(function (data) {
            _this.statusofgeofence = data;
            // console.log(this.statusofgeofence);
        }, function (err) {
            console.log(err);
        });
    };
    GeofencePage.prototype.geofenceShow = function (item) {
        this.navCtrl.push('GeofenceShowPage', {
            param: item
        });
    };
    GeofencePage.prototype.deletePOI = function (data) {
        var _this = this;
        console.log("delete data: ", data);
        var alert = this.alerCtrl.create({
            message: "Do you want to delete this Geofence?",
            buttons: [{
                    text: 'BACK'
                }, {
                    text: 'YES PROCEED',
                    handler: function () {
                        _this.apiCall.startLoading().present();
                        _this.apiCall.deletePOIAPI(data._id)
                            .subscribe(function (data) {
                            _this.apiCall.stopLoading();
                            var toast = _this.toastCtrl.create({
                                message: "Geofence deleted successfully!",
                                position: "top",
                                duration: 2000
                            });
                            toast.present();
                            _this.getCircularGeofence();
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            console.log("error occured while deleting POI: ", err);
                            var toast = _this.toastCtrl.create({
                                message: "Geofence deleted successfully!",
                                position: "top",
                                duration: 2000
                            });
                            toast.present();
                            _this.getCircularGeofence();
                        });
                    }
                }]
        });
        alert.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], GeofencePage.prototype, "navBar", void 0);
    GeofencePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-geofence',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/geofence/geofence.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "Geofences" | translate }}</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (tap)="addgeofence()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div style="padding: 10px;">\n    <div\n      style="padding: 0px 10px 10px;margin-top: -10px; background: white; border-radius: 5px;"\n    >\n      <input\n        type="radio"\n        name="demo"\n        [(ngModel)]="creation_type"\n        value="circular"\n        id="radio-one"\n        class="form-radio"\n        (click)="radioChecked(\'circular\')"\n      /><label for="radio-one">{{ "Circular" | translate }}</label>\n      <input\n        type="radio"\n        name="demo"\n        [(ngModel)]="creation_type"\n        value="polygon"\n        id="radio-one"\n        class="form-radio"\n        (click)="radioChecked(\'polygon\')"\n      /><label for="radio-two">{{ "Polygon" | translate }}</label>\n    </div>\n  </div>\n  <div *ngIf="devices.length > 0">\n    <ion-card *ngFor="let item of devices">\n      <ion-item (click)="geofenceShow(item)">\n        <h2>{{ item.geoname }}</h2>\n        <p style="margin-top: 3%;">\n          {{ "Number Of Vehicles" | translate }} - {{ item.devicesWithin.length }}\n        </p>\n        <ion-icon\n          item-end\n          name="trash"\n          color="danger"\n          (tap)="DelateGeofence(item._id)"\n        >\n        </ion-icon>\n      </ion-item>\n      <!-- <ion-toggle color="danger"></ion-toggle> -->\n      <ion-row>\n        <ion-col width-33>\n          <div style="padding-left: 10px;">\n            <ion-toggle\n              [(ngModel)]="item.status"\n              (ionChange)="DisplayDataOnMap(item)"\n              color="danger"\n            ></ion-toggle>\n          </div>\n\n          <!-- <ion-toggle [(ngModel)]="item.status" (ionChange)="DisplayDataOnMap(item)" color="danger" style="margin-left:11%;"></ion-toggle> -->\n          <p style="color:#e618af;font-size:10px;margin-left: 10px;">\n            {{ "Display on map" | translate }}\n          </p>\n        </ion-col>\n        <ion-col width-33 style="text-align: center;">\n          <div style="padding-left: 10px;">\n            <ion-toggle\n              [(ngModel)]="item.entering"\n              (ionChange)="DisplayDataOnMap(item)"\n              color="danger"\n            ></ion-toggle>\n          </div>\n          <!-- <ion-toggle [(ngModel)]="item.entering" (ionChange)="DisplayDataOnMap(item)" color="danger" style="margin-left: 20%;"></ion-toggle> -->\n          <p style="color:green;font-size: 10px;margin-left: -8%;">\n            {{ "Notification Entering" | translate }}\n          </p>\n        </ion-col>\n        <ion-col width-33>\n          <div style="padding-left: 10px;">\n            <ion-toggle\n              [(ngModel)]="item.exiting"\n              (ionChange)="DisplayDataOnMap(item)"\n              color="danger"\n            ></ion-toggle>\n          </div>\n          <!-- <ion-toggle [(ngModel)]="item.exiting" (ionChange)="DisplayDataOnMap(item)" color="danger" style="margin-left: 20%;"></ion-toggle> -->\n          <p style="color:green;font-size: 10px; margin-left: 11%;">\n            {{ "Notification Exiting" | translate }}\n          </p>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </div>\n  <div *ngIf="allData._poiListData.length > 0">\n    <ion-card *ngFor="let list of allData._poiListData">\n      <ion-item>\n        <ion-thumbnail item-start>\n          <div id="{{ list.mapid }}" style="height: 100px; width: 100px;"></div>\n        </ion-thumbnail>\n        <h1>{{ list.geonamename }}</h1>\n        <p style="color: #687DCA">\n          {{ "Radius:" | translate }} {{ list.radius }}\n        </p>\n        <p><ion-icon name="pin"></ion-icon>&nbsp;&nbsp;{{ list.address }}</p>\n        <p>\n          <!-- <button ion-button round small (click)="editPOI(list)">Edit</button> -->\n          <button\n            ion-button\n            round\n            small\n            style="background: #a60033; color: white"\n            (click)="deletePOI(list)"\n          >\n            {{ "Delete" | translate }}\n          </button>\n        </p>\n      </ion-item>\n    </ion-card>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/geofence/geofence.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], GeofencePage);
    return GeofencePage;
}());

//# sourceMappingURL=geofence.js.map

/***/ }),

/***/ 994:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeofencePageModule", function() { return GeofencePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__geofence__ = __webpack_require__(1089);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var GeofencePageModule = /** @class */ (function () {
    function GeofencePageModule() {
    }
    GeofencePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__geofence__["a" /* GeofencePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__geofence__["a" /* GeofencePage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], GeofencePageModule);
    return GeofencePageModule;
}());

//# sourceMappingURL=geofence.module.js.map

/***/ })

});
//# sourceMappingURL=45.js.map