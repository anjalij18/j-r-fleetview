webpackJsonp([26],{

/***/ 1020:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SirenAlertPageModule", function() { return SirenAlertPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__siren_alert__ = __webpack_require__(1113);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SirenAlertPageModule = /** @class */ (function () {
    function SirenAlertPageModule() {
    }
    SirenAlertPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__siren_alert__["a" /* SirenAlertPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__siren_alert__["a" /* SirenAlertPage */]),
            ],
        })
    ], SirenAlertPageModule);
    return SirenAlertPageModule;
}());

//# sourceMappingURL=siren-alert.module.js.map

/***/ }),

/***/ 1113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SirenAlertPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_native_audio__ = __webpack_require__(195);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SirenAlertPage = /** @class */ (function () {
    function SirenAlertPage(navCtrl, navParams, nativeAudio) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.nativeAudio = nativeAudio;
        this.nativeAudio.preloadComplex('parkingViolation', 'assets/sound/notif.mp3', 1, 1, 0);
        if (navParams.get('vehName') !== null) {
            console.log("vehicle name is: ", navParams.get('vehName'));
        }
    }
    SirenAlertPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter SirenAlertPage');
        // can optionally pass a callback to be called when the file is done playing
        // this.nativeAudio.play('parkingViolation', () => {
        //   this.dismiss();
        //   console.log('uniqueId1 is done playing')
        // });
        this.nativeAudio.loop('parkingViolation');
    };
    SirenAlertPage.prototype.dismiss = function () {
        this.navCtrl.pop();
        this.nativeAudio.stop('parkingViolation');
    };
    SirenAlertPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-siren-alert',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/siren-alert/siren-alert.html"*/'\n<ion-content padding style="background: #a60033;">\n  <ion-row style="padding-top: 150px;">\n    <ion-col col-3></ion-col>\n    <ion-col col-6>\n      <img src="assets/imgs/alarmicon.png" style="height: 150px; width: 180px;" />\n    </ion-col>\n    <ion-col col-3></ion-col>\n  </ion-row>\n\n  <div>\n    <ion-row>\n      <ion-col col-2></ion-col>\n      <ion-col col-8 style="text-align: center;">\n        <p style="margin: 0px; font-size: 1.8em; color: white; font-weight: 600;">Parking Violation</p>\n      </ion-col>\n      <ion-col col-2></ion-col>\n    </ion-row>\n    <ion-row style="padding-top: 16px;">\n      <ion-col col-3></ion-col>\n      <ion-col col-6>\n        <!-- <button ion-button round block color="gpsc">Lock Engine</button> -->\n        <button ion-button round block color="light" (click)="dismiss()">Exit</button>\n\n      </ion-col>\n      <ion-col col-3></ion-col>\n    </ion-row>\n    <!-- <ion-row>\n      <ion-col col-3></ion-col>\n      <ion-col col-6>\n        <button ion-button round block color="gpsc">View</button>\n      </ion-col>\n      <ion-col col-3></ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-3></ion-col>\n      <ion-col col-6>\n        <button ion-button round block color="light" (click)="dismiss()">Exit</button>\n      </ion-col>\n      <ion-col col-3></ion-col>\n    </ion-row> -->\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/siren-alert/siren-alert.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_native_audio__["a" /* NativeAudio */]])
    ], SirenAlertPage);
    return SirenAlertPage;
}());

//# sourceMappingURL=siren-alert.js.map

/***/ })

});
//# sourceMappingURL=26.js.map