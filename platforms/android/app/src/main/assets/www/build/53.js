webpackJsonp([53],{

/***/ 1066:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditDealerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EditDealerPage = /** @class */ (function () {
    function EditDealerPage(apiCall, viewCtrl, formBuilder, navPar, toastCtrl, alerCtrl, translate) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.translate = translate;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this._dealerData = navPar.get("param");
        console.log("dealer details=> " + JSON.stringify(this._dealerData));
        if (this._dealerData.expire_date == undefined) {
            var tru = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(), 'DD/MM/YYYY').format('YYYY-MM-DD');
            var tempdate = new Date(tru);
            tempdate.setDate(tempdate.getDate() + 365);
            this.yearLater = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(tempdate), 'DD-MM-YYYY').format('YYYY-MM-DD');
        }
        else {
            this.yearLater = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(this._dealerData.expire_date), 'DD/MM/YYYY').format('YYYY-MM-DD');
        }
        this.editDealerForm = formBuilder.group({
            userid: [this._dealerData.user_id, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            first_name: [this._dealerData.first_name, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            last_name: [this._dealerData.last_name, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            email: [(this._dealerData.email ? this._dealerData.email : 'N/A'), __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            phone: [(this._dealerData.phone ? this._dealerData.phone : 'N/A'), __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            address: [(this._dealerData.address ? this._dealerData.address : 'N/A'), __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            expirationdate: [this.yearLater]
        });
    }
    EditDealerPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter EditDealerPage');
    };
    EditDealerPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    EditDealerPage.prototype._submit = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.editDealerForm.valid) {
            var payload = {
                address: this.editDealerForm.value.address,
                contactid: this._dealerData._id,
                expire_date: new Date(this.editDealerForm.value.expirationdate).toISOString(),
                first_name: this.editDealerForm.value.first_name,
                last_name: this.editDealerForm.value.last_name,
                status: this._dealerData.status,
                user_id: this.editDealerForm.value.userid,
                email: this.editDealerForm.value.email
            };
            this.apiCall.startLoading().present();
            this.apiCall.editUserDetailsCall(payload)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                if (data) {
                    var toast = _this.toastCtrl.create({
                        message: _this.translate.instant('dealerUpdated', { value: _this.translate.instant('Dealers') }),
                        duration: 1500,
                        position: 'middle'
                    });
                    toast.present();
                    _this.viewCtrl.dismiss();
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                console.log("error occured=> ", err);
            });
        }
    };
    EditDealerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-cust',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/dealers/edit-dealer/edit-dealer.html"*/'<ion-header>\n  <ion-navbar>\n      <ion-title>{{\'Edit Dealer Details\' | translate}}</ion-title>\n      <ion-buttons end>\n          <button ion-button ion-only (click)="dismiss()">\n              <ion-icon name="close-circle"></ion-icon>\n          </button>\n      </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <form [formGroup]="editDealerForm">\n\n      <ion-item>\n          <ion-label fixed style="min-width: 50% !important;">{{\'User ID*\' | translate}}</ion-label>\n          <ion-input formControlName="userid" type="text"></ion-input>\n      </ion-item>\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.userid.valid && (editDealerForm.controls.userid.dirty || submitAttempt)">\n          <p>{{\'Enter User ID\' | translate}}</p>\n      </ion-item>\n\n      <ion-item>\n          <ion-label fixed style="min-width: 50% !important;">{{\'First Name*\' | translate}}</ion-label>\n          <ion-input formControlName="first_name" type="text"></ion-input>\n      </ion-item>\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.first_name.valid && (editDealerForm.controls.first_name.dirty || submitAttempt)">\n          <p>{{\'Enter First Name\' | translate}}</p>\n      </ion-item>\n\n      <ion-item>\n          <ion-label fixed style="min-width: 50% !important;">{{\'Last Name*\' | translate}}</ion-label>\n          <ion-input formControlName="last_name" type="text"></ion-input>\n      </ion-item>\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.last_name.valid && (editDealerForm.controls.last_name.dirty || submitAttempt)">\n          <p>{{\'Enter Last Name\' | translate}}</p>\n      </ion-item>\n\n      <ion-item>\n          <ion-label fixed style="min-width: 50% !important;">{{\'Email ID\' | translate}}</ion-label>\n          <ion-input formControlName="email" type="email"></ion-input>\n      </ion-item>\n\n      <ion-item>\n          <ion-label fixed style="min-width: 50% !important;">{{\'Mobile Number\' | translate}}</ion-label>\n          <ion-input formControlName="phone" type="number" maxlength="10" minlength="10"></ion-input>\n      </ion-item>\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.phone.valid && (editDealerForm.controls.phone.dirty || submitAttempt)">\n          <p>{{\'mobile number required and should be 10 digits!\' | translate}}</p>\n      </ion-item>\n\n      <ion-item>\n        <ion-label fixed style="min-width: 50% !important;">{{\'Expire On\' | translate}}</ion-label>\n        <ion-input type="date" formControlName="expirationdate" min="{{minDate}}" style="margin-left: -3px;"></ion-input>\n    </ion-item>\n\n      <ion-item>\n          <ion-label fixed style="min-width: 50% !important;">{{\'Address\' | translate}}</ion-label>\n          <ion-input formControlName="address" type="text"></ion-input>\n      </ion-item>\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.address.valid && (editDealerForm.controls.address.dirty || submitAttempt)">\n          <p>{{\'Enter Address\' | translate}}</p>\n      </ion-item>\n\n  </form>\n  <!-- <button ion-button block (click)="updateCustomer()">UPDATE DETAILS</button> -->\n</ion-content>\n<ion-footer class="footSty">\n  <ion-toolbar>\n      <ion-row>\n          <ion-col style="text-align: center;">\n              <button ion-button clear color="light" (click)="_submit()">{{\'UPDATE DEALER DETAILS\' | translate}}</button>\n          </ion-col>\n      </ion-row>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/dealers/edit-dealer/edit-dealer.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */]])
    ], EditDealerPage);
    return EditDealerPage;
}());

//# sourceMappingURL=edit-dealer.js.map

/***/ }),

/***/ 975:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditDealerPageModule", function() { return EditDealerPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__edit_dealer__ = __webpack_require__(1066);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var EditDealerPageModule = /** @class */ (function () {
    function EditDealerPageModule() {
    }
    EditDealerPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__edit_dealer__["a" /* EditDealerPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__edit_dealer__["a" /* EditDealerPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], EditDealerPageModule);
    return EditDealerPageModule;
}());

//# sourceMappingURL=edit-dealer.module.js.map

/***/ })

});
//# sourceMappingURL=53.js.map