webpackJsonp([37],{

/***/ 1005:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaintenanceReminderPageModule", function() { return MaintenanceReminderPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintenance_reminder__ = __webpack_require__(1099);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var MaintenanceReminderPageModule = /** @class */ (function () {
    function MaintenanceReminderPageModule() {
    }
    MaintenanceReminderPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__maintenance_reminder__["a" /* MaintenanceReminderPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__maintenance_reminder__["a" /* MaintenanceReminderPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], MaintenanceReminderPageModule);
    return MaintenanceReminderPageModule;
}());

//# sourceMappingURL=maintenance-reminder.module.js.map

/***/ }),

/***/ 1099:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaintenanceReminderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MaintenanceReminderPage = /** @class */ (function () {
    function MaintenanceReminderPage(navCtrl, navParams, apiCall, modalCtrl, toastCtrl, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.translate = translate;
        this.portstemp = [];
        this.reminderTypes = [];
        this.remData = [];
        this.isParamData = false;
        // twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
        this.today = __WEBPACK_IMPORTED_MODULE_2_moment__().add(5, 'year').format("YYYY-MM-DD");
        this.twoMonthsLater = __WEBPACK_IMPORTED_MODULE_2_moment__().subtract(5, 'year').format("YYYY-MM-DD");
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // console.log("email => " + this.islogin._id);
        this.reminderTypes = [{
                viewValue: this.translate.instant("Service"),
                value: "Service",
            },
            {
                viewValue: this.translate.instant("Oil Change"),
                value: "Oil Change",
            },
            {
                viewValue: this.translate.instant("Tyres"),
                value: "Tyres",
            },
            {
                viewValue: this.translate.instant("Maintenance"),
                value: "Maintenance",
            },
            {
                viewValue: this.translate.instant("Auto Repair"),
                value: "Auto Repair",
            },
            {
                viewValue: this.translate.instant("Body Work"),
                value: "Body Work",
            },
            {
                viewValue: this.translate.instant("Diagnostics"),
                value: "Diagnostics",
            },
            {
                viewValue: this.translate.instant("Tune Up"),
                value: "Tune Up",
            },
            {
                viewValue: this.translate.instant("Brake Job"),
                value: "Brake Job",
            },
            {
                viewValue: this.translate.instant("Oil & Oil Filter Change"),
                value: "Oil & Oil Filter Change",
            },
            {
                viewValue: this.translate.instant("Tyer Care"),
                value: "Tyer Care",
            },
            {
                viewValue: this.translate.instant("Towing"),
                value: "Towing",
            },
            {
                viewValue: this.translate.instant("Wheel Balance & Alignment"),
                value: "Wheel Balance & Alignment",
            },
            {
                viewValue: this.translate.instant("Fleet"),
                value: "Fleet",
            },
            {
                viewValue: this.translate.instant("Auto Tracking"),
                value: "Auto Tracking",
            },
            {
                viewValue: this.translate.instant("A/C Repair"),
                value: "A/C Repair",
            },
            {
                viewValue: this.translate.instant("Others"),
                value: "Others",
            }
        ];
        // this.datetimeStart = moment({ hours: 0 }).format();
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_2_moment__().subtract(1, 'months').format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_2_moment__().format(); //new Date(a).toISOString();
        console.log('end date', this.datetimeEnd);
        if (navParams.get('param')) {
            this.isParamData = true;
            console.log('reminder navparams: ', navParams.get('param'));
            this.selectedVehicle = navParams.get('param');
            this.onChangeVehicle();
        }
    }
    MaintenanceReminderPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter MaintenanceReminderPage');
    };
    MaintenanceReminderPage.prototype.ngOnInit = function () {
        if (!this.isParamData) {
            this.getdevices();
            this.getReminders();
        }
    };
    MaintenanceReminderPage.prototype.onSeletChange = function (rem) {
        console.log("reminder changed: ", this.reminderType);
        this.remData = [];
        this.getReminders();
    };
    MaintenanceReminderPage.prototype.onChangeVehicle = function () {
        console.log("vehicle changed: ", this.selectedVehicle);
        this.remData = [];
        this.getReminders();
    };
    MaintenanceReminderPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.portstemp = data.devices;
        }, function (err) {
            console.log(err);
        });
    };
    MaintenanceReminderPage.prototype.getReminders = function () {
        var _this = this;
        var Burl = this.apiCall.mainUrl + 'reminder/reminderdatatable';
        var payload = {};
        if (this.islogin.isSuperAdmin === true) {
            if (this.reminderType != undefined && this.selectedVehicle != undefined) {
                payload = {
                    "draw": 2,
                    "columns": [
                        {
                            "data": "_id"
                        },
                        {
                            "data": "user.first_name"
                        },
                        {
                            "data": "user.last_name"
                        },
                        {
                            "data": "device.Device_Name"
                        },
                        {
                            "data": "reminder_date"
                        },
                        {
                            "data": "notification_type.SMS"
                        },
                        {
                            "data": "notification_type.EMAIL"
                        },
                        {
                            "data": "notification_type.PUSH_NOTIFICATION"
                        },
                        {
                            "data": "reminder_type"
                        },
                        {
                            "data": "prior_reminder"
                        },
                        {
                            "data": "note"
                        }
                    ],
                    "order": [
                        {
                            "column": 0,
                            "dir": "asc"
                        }
                    ],
                    "start": 0,
                    "length": 25,
                    "search": {
                        "value": "",
                        "regex": false
                    },
                    "op": {},
                    "select": [],
                    "find": {
                        "$and": [
                            {
                                "$or": [
                                    {
                                        "user": this.islogin._id
                                    },
                                    {
                                        "created_by": this.islogin._id
                                    },
                                    {}
                                ]
                            },
                            {
                                "device": {
                                    "$in": [
                                        this.selectedVehicle._id
                                    ]
                                }
                            },
                            {
                                "reminder_date": {
                                    "$gte": new Date(this.datetimeStart).toISOString(),
                                }
                            },
                            {
                                "reminder_type": this.reminderType
                            }
                        ]
                    }
                };
            }
            else {
                if (this.reminderType != undefined && this.selectedVehicle == undefined) {
                    payload = {
                        "draw": 2,
                        "columns": [
                            {
                                "data": "_id"
                            },
                            {
                                "data": "user.first_name"
                            },
                            {
                                "data": "user.last_name"
                            },
                            {
                                "data": "device.Device_Name"
                            },
                            {
                                "data": "reminder_date"
                            },
                            {
                                "data": "notification_type.SMS"
                            },
                            {
                                "data": "notification_type.EMAIL"
                            },
                            {
                                "data": "notification_type.PUSH_NOTIFICATION"
                            },
                            {
                                "data": "reminder_type"
                            },
                            {
                                "data": "prior_reminder"
                            },
                            {
                                "data": "note"
                            }
                        ],
                        "order": [
                            {
                                "column": 0,
                                "dir": "asc"
                            }
                        ],
                        "start": 0,
                        "length": 25,
                        "search": {
                            "value": "",
                            "regex": false
                        },
                        "op": {},
                        "select": [],
                        "find": {
                            "$and": [
                                {
                                    "$or": [
                                        {
                                            "user": this.islogin._id
                                        },
                                        {
                                            "created_by": this.islogin._id
                                        },
                                        {}
                                    ]
                                },
                                {
                                    "reminder_date": {
                                        "$gte": new Date(this.datetimeStart).toISOString(),
                                    }
                                },
                                {
                                    "reminder_type": this.reminderType
                                }
                            ]
                        }
                    };
                }
                else {
                    if (this.reminderType == undefined && this.selectedVehicle != undefined) {
                        payload = {
                            "draw": 2,
                            "columns": [
                                {
                                    "data": "_id"
                                },
                                {
                                    "data": "user.first_name"
                                },
                                {
                                    "data": "user.last_name"
                                },
                                {
                                    "data": "device.Device_Name"
                                },
                                {
                                    "data": "reminder_date"
                                },
                                {
                                    "data": "notification_type.SMS"
                                },
                                {
                                    "data": "notification_type.EMAIL"
                                },
                                {
                                    "data": "notification_type.PUSH_NOTIFICATION"
                                },
                                {
                                    "data": "reminder_type"
                                },
                                {
                                    "data": "prior_reminder"
                                },
                                {
                                    "data": "note"
                                }
                            ],
                            "order": [
                                {
                                    "column": 0,
                                    "dir": "asc"
                                }
                            ],
                            "start": 0,
                            "length": 25,
                            "search": {
                                "value": "",
                                "regex": false
                            },
                            "op": {},
                            "select": [],
                            "find": {
                                "$and": [
                                    {
                                        "$or": [
                                            {
                                                "user": this.islogin._id
                                            },
                                            {
                                                "created_by": this.islogin._id
                                            },
                                            {}
                                        ]
                                    },
                                    {
                                        "device": {
                                            "$in": [
                                                this.selectedVehicle._id
                                            ]
                                        }
                                    },
                                    {
                                        "reminder_date": {
                                            "$gte": new Date(this.datetimeStart).toISOString(),
                                        }
                                    }
                                ]
                            }
                        };
                    }
                    else {
                        if (this.reminderType == undefined && this.selectedVehicle == undefined) {
                            payload = {
                                "draw": 2,
                                "columns": [
                                    {
                                        "data": "_id"
                                    },
                                    {
                                        "data": "user.first_name"
                                    },
                                    {
                                        "data": "user.last_name"
                                    },
                                    {
                                        "data": "device.Device_Name"
                                    },
                                    {
                                        "data": "reminder_date"
                                    },
                                    {
                                        "data": "notification_type.SMS"
                                    },
                                    {
                                        "data": "notification_type.EMAIL"
                                    },
                                    {
                                        "data": "notification_type.PUSH_NOTIFICATION"
                                    },
                                    {
                                        "data": "reminder_type"
                                    },
                                    {
                                        "data": "prior_reminder"
                                    },
                                    {
                                        "data": "note"
                                    },
                                    {
                                        "data": "status"
                                    }
                                ],
                                "order": [
                                    {
                                        "column": 0,
                                        "dir": "asc"
                                    }
                                ],
                                "start": 0,
                                "length": 25,
                                "search": {
                                    "value": "",
                                    "regex": false
                                },
                                "op": {},
                                "select": [],
                                "find": {
                                    "$and": [
                                        {
                                            "$or": [
                                                {
                                                    "user": this.islogin._id
                                                },
                                                {
                                                    "created_by": this.islogin._id
                                                }
                                            ]
                                        },
                                        {
                                            "reminder_date": {
                                                "$gte": new Date(this.datetimeStart).toISOString(),
                                            }
                                        }
                                    ]
                                }
                            };
                        }
                    }
                }
            }
        }
        else {
            if (this.reminderType != undefined && this.selectedVehicle != undefined) {
                payload = {
                    "draw": 2,
                    "columns": [
                        {
                            "data": "_id"
                        },
                        {
                            "data": "user.first_name"
                        },
                        {
                            "data": "user.last_name"
                        },
                        {
                            "data": "device.Device_Name"
                        },
                        {
                            "data": "reminder_date"
                        },
                        {
                            "data": "notification_type.SMS"
                        },
                        {
                            "data": "notification_type.EMAIL"
                        },
                        {
                            "data": "notification_type.PUSH_NOTIFICATION"
                        },
                        {
                            "data": "reminder_type"
                        },
                        {
                            "data": "prior_reminder"
                        },
                        {
                            "data": "note"
                        }
                    ],
                    "order": [
                        {
                            "column": 0,
                            "dir": "asc"
                        }
                    ],
                    "start": 0,
                    "length": 25,
                    "search": {
                        "value": "",
                        "regex": false
                    },
                    "op": {},
                    "select": [],
                    "find": {
                        "$and": [
                            {
                                "$or": [
                                    {
                                        "user": this.islogin._id
                                    },
                                    {
                                        "created_by": this.islogin.supAdmin
                                    }
                                ]
                            },
                            {
                                "device": {
                                    "$in": [
                                        this.selectedVehicle._id
                                    ]
                                }
                            },
                            {
                                "reminder_date": {
                                    "$gte": new Date(this.datetimeStart).toISOString(),
                                }
                            },
                            {
                                "reminder_type": this.reminderType
                            }
                        ]
                    }
                };
            }
            else {
                if (this.reminderType != undefined && this.selectedVehicle == undefined) {
                    payload = {
                        "draw": 2,
                        "columns": [
                            {
                                "data": "_id"
                            },
                            {
                                "data": "user.first_name"
                            },
                            {
                                "data": "user.last_name"
                            },
                            {
                                "data": "device.Device_Name"
                            },
                            {
                                "data": "reminder_date"
                            },
                            {
                                "data": "notification_type.SMS"
                            },
                            {
                                "data": "notification_type.EMAIL"
                            },
                            {
                                "data": "notification_type.PUSH_NOTIFICATION"
                            },
                            {
                                "data": "reminder_type"
                            },
                            {
                                "data": "prior_reminder"
                            },
                            {
                                "data": "note"
                            }
                        ],
                        "order": [
                            {
                                "column": 0,
                                "dir": "asc"
                            }
                        ],
                        "start": 0,
                        "length": 25,
                        "search": {
                            "value": "",
                            "regex": false
                        },
                        "op": {},
                        "select": [],
                        "find": {
                            "$and": [
                                {
                                    "$or": [
                                        {
                                            "user": this.islogin._id
                                        },
                                        {
                                            "created_by": this.islogin.supAdmin
                                        }
                                    ]
                                },
                                {
                                    "reminder_date": {
                                        "$gte": new Date(this.datetimeStart).toISOString(),
                                    }
                                },
                                {
                                    "reminder_type": this.reminderType
                                }
                            ]
                        }
                    };
                }
                else {
                    if (this.reminderType == undefined && this.selectedVehicle != undefined) {
                        payload = {
                            "draw": 2,
                            "columns": [
                                {
                                    "data": "_id"
                                },
                                {
                                    "data": "user.first_name"
                                },
                                {
                                    "data": "user.last_name"
                                },
                                {
                                    "data": "device.Device_Name"
                                },
                                {
                                    "data": "reminder_date"
                                },
                                {
                                    "data": "notification_type.SMS"
                                },
                                {
                                    "data": "notification_type.EMAIL"
                                },
                                {
                                    "data": "notification_type.PUSH_NOTIFICATION"
                                },
                                {
                                    "data": "reminder_type"
                                },
                                {
                                    "data": "prior_reminder"
                                },
                                {
                                    "data": "note"
                                }
                            ],
                            "order": [
                                {
                                    "column": 0,
                                    "dir": "asc"
                                }
                            ],
                            "start": 0,
                            "length": 25,
                            "search": {
                                "value": "",
                                "regex": false
                            },
                            "op": {},
                            "select": [],
                            "find": {
                                "$or": [
                                    {
                                        "user": this.islogin._id
                                    },
                                    {
                                        "created_by": this.islogin.supAdmin
                                    }
                                ],
                                "device": {
                                    "$in": [
                                        this.selectedVehicle._id
                                    ]
                                },
                                "reminder_date": {
                                    "$gte": new Date(this.datetimeStart).toISOString(),
                                }
                            }
                        };
                    }
                    else {
                        if (this.reminderType == undefined && this.selectedVehicle == undefined) {
                            payload = {
                                "draw": 2,
                                "columns": [
                                    {
                                        "data": "_id"
                                    },
                                    {
                                        "data": "user.first_name"
                                    },
                                    {
                                        "data": "user.last_name"
                                    },
                                    {
                                        "data": "device.Device_Name"
                                    },
                                    {
                                        "data": "reminder_date"
                                    },
                                    {
                                        "data": "notification_type.SMS"
                                    },
                                    {
                                        "data": "notification_type.EMAIL"
                                    },
                                    {
                                        "data": "notification_type.PUSH_NOTIFICATION"
                                    },
                                    {
                                        "data": "reminder_type"
                                    },
                                    {
                                        "data": "prior_reminder"
                                    },
                                    {
                                        "data": "note"
                                    },
                                    {
                                        "data": "status"
                                    }
                                ],
                                "order": [
                                    {
                                        "column": 0,
                                        "dir": "asc"
                                    }
                                ],
                                "start": 0,
                                "length": 25,
                                "search": {
                                    "value": "",
                                    "regex": false
                                },
                                "op": {},
                                "select": [],
                                "find": {
                                    "$and": [
                                        {
                                            "$or": [
                                                {
                                                    "user": this.islogin._id
                                                },
                                                {
                                                    "created_by": this.islogin.supAdmin
                                                }
                                            ]
                                        },
                                        {
                                            "reminder_date": {
                                                "$gte": new Date(this.datetimeStart).toISOString(),
                                            }
                                        }
                                    ]
                                }
                            };
                        }
                    }
                }
            }
        }
        // let current_date = new Date();
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(Burl, payload)
            .subscribe(function (resData) {
            _this.apiCall.stopLoading();
            console.log("response reminder: " + resData);
            if (resData.data.length > 0) {
                for (var i = 0; i < resData.data.length; i++) {
                    debugger;
                    // let someti = new Date(resData.data[i].reminder_date);
                    // if (someti < current_date) {
                    //   return;
                    // }
                    var date = __WEBPACK_IMPORTED_MODULE_2_moment__(new Date(resData.data[i].reminder_date)).format('YYYY-MM-DD');
                    var now = __WEBPACK_IMPORTED_MODULE_2_moment__().format('YYYY-MM-DD');
                    if (now > date) {
                        // date is past
                        console.log("im past");
                    }
                    else {
                        console.log("im future");
                        // date is future
                        var date1 = __WEBPACK_IMPORTED_MODULE_2_moment__(new Date(resData.data[i].reminder_date), 'DD/MM/YYYY').format("llll");
                        var str = date1.split(', ');
                        var day = str[0];
                        var date3 = str[1].split(' ');
                        var date4 = str[2].split(' ');
                        var time = date4[1] + ' ' + date4[2];
                        var year = date4[0];
                        var month = date3[0];
                        var dateNum = date3[1];
                        _this.remData.push({
                            datFormats: {
                                'day': day,
                                'time': time,
                                'year': year,
                                'month': month,
                                'dateNum': dateNum
                            },
                            vehiclName: (resData.data[i].device ? resData.data[i].device.Device_Name : null),
                            reminderType: (resData.data[i].reminder_type ? resData.data[i].reminder_type : null),
                            notifType: (resData.data[i].notification_type ? resData.data[i].notification_type : null),
                            note: (resData.data[i].note ? resData.data[i].note : null),
                            prior_reminder: (resData.data[i].prior_reminder ? resData.data[i].prior_reminder : null)
                        });
                    }
                }
            }
            else {
                _this.toastCtrl.create({
                    message: 'Reminders not found...',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    MaintenanceReminderPage.prototype.onAddReminder = function () {
        var _this = this;
        var modal;
        if (this.navParams.get('param')) {
            modal = this.modalCtrl.create('AddReminderPage', {
                param: this.navParams.get('param')
            });
        }
        else {
            modal = this.modalCtrl.create('AddReminderPage', {
                param: null
            });
        }
        modal.present();
        modal.onDidDismiss(function () {
            _this.remData = [];
            _this.getReminders();
        });
    };
    MaintenanceReminderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-maintenance-reminder',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/maintenance-reminder/maintenance-reminder.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{"Maintenance Reminder" | translate}}</ion-title>\n \n  </ion-navbar>\n\n  <ion-list no-padding style="margin: 0px;">\n    <ion-item style="background-color: #fafafa;" *ngIf="!isParamData">\n      <ion-label style="margin-top: 15px;">{{\'Select Vehicle\' | translate}}</ion-label>\n      <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n        itemTextField="Device_Name" [canSearch]="true" (onChange)="onChangeVehicle(selectedVehicle)">\n      </select-searchable>\n    </ion-item>\n    <ion-item style="background-color: #fafafa;">\n      <ion-label style="margin-top: 15px;">{{"Reminder Type" | translate}}</ion-label>\n      <ion-select style="font-size: 12px;" [(ngModel)]="reminderType" (ionChange)="onSeletChange(reminderType)">\n        <ion-option *ngFor="let rem of reminderTypes" [value]="rem.value">{{rem.viewValue}}</ion-option>\n      </ion-select>\n    </ion-item>\n  </ion-list>\n  <ion-row style="background-color: #fafafa;" padding-left padding-right>\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{\'From Date\' | translate}}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YYYY" [(ngModel)]="datetimeStart"\n        style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;" [min]="twoMonthsLater" [max]="today">\n      </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{\'To Date\' | translate}}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YYYY" [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right">\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getReminders();"></ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n\n<ion-content>\n  <div *ngIf="remData.length > 0">\n    <ion-row *ngFor="let item of remData" style="padding: 10px;">\n      <ion-col col-2 style="padding-top: 16px;">\n        <ion-row>\n          <ion-col col-12 text-center style="font-size: 1.1em; letter-spacing: 1.5px; color: gray; font-weight: 500;">\n            {{item.datFormats.day | uppercase}}</ion-col>\n          <ion-col col-12 text-center style="font-size: 2.3em;">{{item.datFormats.dateNum}}</ion-col>\n          <ion-col col-12 text-center style="font-size: 0.8em; color: gray; font-weight: 200;">\n            {{item.datFormats.month}}, {{item.datFormats.year}}</ion-col>\n        </ion-row>\n      </ion-col>\n      <ion-col col-10 style="background-image: linear-gradient(to right top, #89be4c, #9ec963, #b2d57a, #c5e091, #d7eca8); color: white;border-radius: 7px;">\n        <ion-row style="padding: 10px;">\n          <ion-col col-1>\n            <ion-icon name="car"></ion-icon>\n          </ion-col>\n          <ion-col col-5>{{item.vehiclName}}</ion-col>\n          <ion-col col-6 text-right style="font-size: 1em;">{{item.reminderType}}</ion-col>\n        </ion-row>\n        <ion-row style="padding: 10px;">\n          <ion-col col-8>\n            <ion-row>\n              <ion-col col-5 style="font-size: 1.3em;">{{item.datFormats.time}}</ion-col>\n          \n              <ion-col col-2 *ngIf="item.notifType.SMS">\n                <img style="width: 20px;" src="assets/imgs/reminder/sms-of-surveillance-system.png" /></ion-col>\n              <ion-col col-2 *ngIf="item.notifType.EMAIL">\n                <img style="width: 20px;" src="assets/imgs/reminder/envelope.png" /></ion-col>\n              <ion-col col-2 *ngIf="item.notifType.PUSH_NOTIFICATION">\n                <img style="width: 20px;" src="assets/imgs/reminder/mobile.png" /></ion-col>\n             \n            </ion-row>\n          </ion-col>\n          <ion-col col-4 text-right>\n            <img style="width: 40px;" *ngIf="item.reminderType == \'oil_change\'" src="assets/imgs/reminder/oil.png" />\n            <img style="width: 40px;" *ngIf="item.reminderType == \'Maintenance\'"\n              src="assets/imgs/reminder/maintenance.png" />\n            <img style="width: 40px;" *ngIf="item.reminderType == \'Tyres\'" src="assets/imgs/reminder/racing.png" />\n            <img style="width: 40px;" *ngIf="item.reminderType == \'Service\'"\n              src="assets/imgs/reminder/car-service.png" />\n            <img style="width: 40px;" *ngIf="item.reminderType == \'Others\'" src="assets/imgs/reminder/any-bleach.png" />\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n    \n  </div>\n\n</ion-content>\n<ion-fab right bottom>\n  <button ion-fab color="gpsc" (click)="onAddReminder()">\n    <ion-icon name="add"></ion-icon>\n  </button>\n</ion-fab>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/maintenance-reminder/maintenance-reminder.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */]])
    ], MaintenanceReminderPage);
    return MaintenanceReminderPage;
}());

//# sourceMappingURL=maintenance-reminder.js.map

/***/ })

});
//# sourceMappingURL=37.js.map