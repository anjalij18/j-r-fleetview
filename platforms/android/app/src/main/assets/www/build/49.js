webpackJsonp([49],{

/***/ 1074:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FastagListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__msg_utility_msg_utility__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FastagListPage = /** @class */ (function () {
    function FastagListPage(navCtrl, navParams, apiCall, toastCtrl, modalCtrl, formBuilder, navParam) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.navParam = navParam;
        this.fastagList = [];
        this.page = 1;
        this.DealerArraySearch = [];
        this.limit = 10000;
        this.CustomerArraySearch = [];
        this.DCustomerArraySearch = [];
        this.selectus = [];
        debugger;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.msgForm = this.formBuilder.group({
            title: [this.islogin.fn],
            msg: [this.islogin.ln],
        });
        // this.selectus = this.navParam.get('selecteduser');
        // console.log("select", this.selectus);
    }
    FastagListPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter FastagListPage');
    };
    FastagListPage.prototype.ngOnInit = function () {
        this.getDealer();
        this.getcust();
        this.getAllDealers();
        // this.selectus = this.navParam.get('selecteduser');
        // console.log("select", this.selectus)
    };
    FastagListPage.prototype.addFastag = function () {
        this.navCtrl.push('FastagPage', {});
    };
    FastagListPage.prototype.getcust = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'users/getCustomer?uid=' + this.islogin._id + '&pageNo=' + this.page + '&size=' + this.limit;
        //this.apiCall.startLoading().present();
        this.apiCall.getCustomersCall(baseURLp)
            .subscribe(function (data) {
            //this.apiCall.stopLoading();
            _this.CustomerData = data;
            _this.CustomerArraySearch = [];
            _this.CustomerArraySearch = _this.CustomerData;
        }, function (err) {
            _this.apiCall.stopLoading();
            var a = JSON.parse(err._body);
            var b = a.message;
            var toast = _this.toastCtrl.create({
                message: b,
                duration: 2000,
                position: "bottom"
            });
            toast.present();
            // toast.onDidDismiss(() => {
            //   this.navCtrl.setRoot('DashboardPage');
            // });
        });
    };
    FastagListPage.prototype.getmodel = function (d) {
        var _this = this;
        debugger;
        var pModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__msg_utility_msg_utility__["a" /* MsgUtilityPage */], {
            param: d
        });
        pModal.onDidDismiss(function (data) {
            console.log("chff", data);
            _this.selectus = data;
            _this.cond = true;
            // this.selectus = this.navParam.get('selecteduser');
            // console.log("select", this.selectus)
        });
        //   pModal.onDidDismiss(d).then(data=>{
        //     console.log('data came back from modal');
        //     console.log(data);
        // })
        pModal.present();
    };
    FastagListPage.prototype.sendmsg = function () {
        var _this = this;
        var data = {
            "messageTitle": this.titleMsg,
            "msgContent": this.fullMsg,
            "notificationType": "NOTIFICATION",
            "numberArr": this.selectus,
        };
        this.apiCall.startLoading().present();
        this.apiCall.announcementData(data)
            .subscribe(function (resp) {
            _this.apiCall.stopLoading();
            console.log(resp);
        });
    };
    FastagListPage.prototype.getDealer = function () {
        var _this = this;
        this.page = 1;
        this.DealerArraySearch = [];
        //this.apiCall.startLoading().present();
        this.apiCall.getDealers(this.islogin._id, this.page, this.limit)
            .subscribe(function (data) {
            //this.apiCall.stopLoading();
            // this.DealerArray = data;
            _this.DealerArraySearch = data;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("getting error from server=> ", err);
            var toast = _this.toastCtrl.create({
                message: 'No Dealer(s) found',
                duration: 2000,
                position: "bottom"
            });
            toast.present();
            toast.onDidDismiss(function () {
                _this.navCtrl.setRoot("DashboardPage");
            });
        });
    };
    FastagListPage.prototype.getList = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + 'fastTag/getRequest?id=' + this.islogin.supAdmin + '&role=supAdmin';
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(url)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            console.log('respData: ', respData);
            if (respData.length > 0) {
                _this.fastagList = respData;
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    FastagListPage.prototype.getAllDealers = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'users/getAllDealerVehicles?supAdmin=' + this.islogin._id;
        var toast = this.toastCtrl.create({
            message: 'Loading dealers..',
            position: 'bottom',
            duration: 1500
        });
        toast.present();
        this.apiCall.getAllDealerCall(baseURLp)
            .subscribe(function (data) {
            _this.selectDealer = data;
            console.log("dealers list", _this.selectDealer);
            // toast.dismiss();
        }, function (error) {
            console.log(error);
        });
    };
    FastagListPage.prototype.dealerOnChnage = function (dealer) {
        console.log(dealer);
        console.log(dealer.dealer_id);
        this.dealerId = dealer.dealer_id;
        this.getDealercust();
        debugger;
        // this.getmodel(this.DCustomerArraySearch);
        //this.dealerdata = dealer;
        // this.dealerdata = this.addcustomerform.value.dealer_firstname;
        // console.log("dealer", this.dealerdata);
        // console.log("dealer id=> " + this.dealerdata.dealer_id);
    };
    FastagListPage.prototype.getDealercust = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'users/getCustomer?uid=' + this.dealerId + '&pageNo=' + this.page + '&size=' + this.limit;
        //this.apiCall.startLoading().present();
        this.apiCall.getCustomersCall(baseURLp)
            .subscribe(function (data) {
            console.log("first click initial", _this.DCustomerArraySearch);
            //this.apiCall.stopLoading();
            _this.CustomerDataa = data;
            _this.DCustomerArraySearch = [];
            _this.DCustomerArraySearch = _this.CustomerDataa;
            console.log("first click", _this.DCustomerArraySearch);
            _this.getmodel(_this.DCustomerArraySearch);
        }, function (err) {
            _this.apiCall.stopLoading();
            var a = JSON.parse(err._body);
            var b = a.message;
            var toast = _this.toastCtrl.create({
                message: b,
                duration: 2000,
                position: "bottom"
            });
            toast.present();
            // toast.onDidDismiss(() => {
            //   this.navCtrl.setRoot('DashboardPage');
            // });
        });
    };
    FastagListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fastag-list',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/fastag-list/fastag-list.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{"Message Utility" | translate}}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <span>SEND TO *:</span>&nbsp;&nbsp;&nbsp;\n  <input type="radio" id="male" name="gender" value="male" [checked]="cond" (tap)="getmodel(DealerArraySearch)">\n  <label for="male" (tap)="getmodel(DealerArraySearch)">Dealers</label><br>\n  <input type="radio" id="female" name="gender" value="female" style="margin-left: 83px;" [checked]="cond" (tap)="getmodel(CustomerArraySearch)">\n  <label for="female" (tap)="getmodel(CustomerArraySearch)">Direct Customer</label><br>\n  <input type="radio" id="other" name="gender" value="other" [(ngModel)]="model" style="margin-left: 83px;">\n  <label for="other">Dealer\'s Customer</label>\n\n  <div *ngIf="model === \'other\'">\n    <ion-item style="padding: 0px;">\n      <ion-label style="margin-top: 15px;">{{\'Select Dealers\' | translate}}</ion-label>\n      <select-searchable item-content [(ngModel)]="dealer_firstname" [items]="selectDealer"\n      itemValueField="dealer_firstname" itemTextField="dealer_firstname" [canSearch]="true"\n      (onChange)="dealerOnChnage(dealer_firstname)" style="padding: 0px;margin-top: -1%;">\n  </select-searchable>\n      <!-- <ion-select formControlName="dealer_firstname" style="min-width:49%;">\n          <ion-option *ngFor="let dealer of selectDealer" [value]="dealer.dealer_firstname"\n              (ionSelect)="dealerOnChnage(dealer)">{{dealer.dealer_firstname | titlecase}}\n              {{dealer.dealer_lastname | titlecase}}</ion-option>\n      </ion-select> -->\n\n  </ion-item>\n  </div>\n\n  <div *ngIf="model !== \'other\'">\n\n  </div>\n\n    <textarea [(ngModel)]="titleMsg" placeholder="Enter the title of the message" style="height: 55px;width: 100%;margin-top: 20px;">\n\n    </textarea>\n\n\n\n    <textarea [(ngModel)]="fullMsg" placeholder="Enter the full message here" style="height: 255px;width: 100%;margin-top: 20px;">\n\n    </textarea>\n\n\n  <!-- <button (click)="sendmsg()" style="margin-left: 122px;\n  /* padding: 20px; */\n  padding: inherit;\n  margin-top: 19px"> Send </button> -->\n\n<ion-footer class="footSty">\n  <ion-toolbar>\n      <ion-row no-padding>\n          <ion-col width-50 style="text-align: center;">\n              <button ion-button clear color="light" (click)="sendmsg()"> SEND </button>\n          </ion-col>\n      </ion-row>\n  </ion-toolbar>\n</ion-footer>\n\n  <!-- <form [formGroup]="msgForm">\n    <ion-item>\n      <ion-label floating>{{\'Enter Title\' | translate}}</ion-label>\n      <ion-input [formControl]="msgForm.controls[\'title\']" type="text"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label floating>{{\'Message\' | translate}}</ion-label>\n      <ion-input [formControl]="msgForm.controls[\'msg\']" type="text"></ion-input>\n    </ion-item>\n    <ion-row>\n      <ion-col text-center>\n        <button ion-button block color="gpsc" (click)="onSignIn()">\n          {{\' Update Account\' | translate}}\n        </button>\n      </ion-col>\n    </ion-row>\n\n  </form> -->\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/fastag-list/fastag-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], FastagListPage);
    return FastagListPage;
}());

//# sourceMappingURL=fastag-list.js.map

/***/ }),

/***/ 982:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FastagListPageModule", function() { return FastagListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fastag_list__ = __webpack_require__(1074);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var FastagListPageModule = /** @class */ (function () {
    function FastagListPageModule() {
    }
    FastagListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fastag_list__["a" /* FastagListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__fastag_list__["a" /* FastagListPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild(),
                __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], FastagListPageModule);
    return FastagListPageModule;
}());

//# sourceMappingURL=fastag-list.module.js.map

/***/ })

});
//# sourceMappingURL=49.js.map