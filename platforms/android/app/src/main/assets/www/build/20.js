webpackJsonp([20],{

/***/ 1079:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FuelConsumptionReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_pager_pager__ = __webpack_require__(561);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var FuelConsumptionReportPage = /** @class */ (function () {
    function FuelConsumptionReportPage(navCtrl, navParams, apiCall, toastCtrl, geocoderApi, translate, modalCtrl, pagerService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.geocoderApi = geocoderApi;
        this.translate = translate;
        this.modalCtrl = modalCtrl;
        this.pagerService = pagerService;
        this.portstemp = [];
        this._listData = [];
        this.fuelDataArr = [];
        this.reportType = [];
        this.repkey = this.translate.instant('Time');
        this.twoMonthsLater = __WEBPACK_IMPORTED_MODULE_3_moment__().subtract(2, 'month').format("YYYY-MM-DD");
        this.today = __WEBPACK_IMPORTED_MODULE_3_moment__().format("YYYY-MM-DD");
        // pager object
        this.pager = {};
        this.measurementUnit = 'MKS';
        // this.reportType = ["Time", "Daily", "Trip"];
        this.reportType = [this.translate.instant('Time'), this.translate.instant('Daily'), this.translate.instant('Trip')];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
    }
    FuelConsumptionReportPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter FuelConsumptionReportPage');
        this.getDefaultUserSettings();
    };
    FuelConsumptionReportPage.prototype.getDefaultUserSettings = function () {
        var _this = this;
        var b_url = this.apiCall.mainUrl + "users/get_user_setting";
        var Var = { uid: this.islogin._id };
        this.apiCall.urlpasseswithdata(b_url, Var)
            .subscribe(function (resp) {
            console.log("check lang key: ", resp);
            if (resp.unit_measurement !== undefined) {
                _this.measurementUnit = resp.unit_measurement;
            }
            else {
                if (localStorage.getItem('MeasurementType') !== null) {
                    var measureType = localStorage.getItem('MeasurementType');
                    _this.measurementUnit = measureType;
                }
                else {
                    _this.measurementUnit = 'MKS';
                }
            }
        }, function (err) {
            console.log(err);
            if (localStorage.getItem('MeasurementType') !== null) {
                var measureType = localStorage.getItem('MeasurementType');
                _this.measurementUnit = measureType;
            }
            else {
                _this.measurementUnit = 'MKS';
            }
        });
    };
    FuelConsumptionReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    FuelConsumptionReportPage.prototype.getFuelNotifs = function (fuelData, key) {
        this.getData(fuelData, key);
    };
    FuelConsumptionReportPage.prototype.getData = function (fuelData, key) {
        var _this = this;
        // this.eventType = key;
        // const paramData = this.navParams.get('paramMaps');
        var _url = this.apiCall.mainUrl + "gps/getFuelNotifs";
        var payload = {
            "start_time": fuelData.start_time,
            "end_time": fuelData.end_time,
            "imei": fuelData.imei,
            "event": key
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(_url, payload)
            .subscribe(function (resp) {
            _this.apiCall.stopLoading();
            console.log("response: ", resp);
            // this.fuelData = resp;
            _this.modalCtrl.create('FuelEventsComponent', {
                paramMaps: resp,
            }).present();
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    FuelConsumptionReportPage.prototype.reportWise = function (repId) {
        console.log("repId: ", repId);
    };
    FuelConsumptionReportPage.prototype.getId = function (veh) {
        this.vehId = veh;
        // this.vehId = veh.Device_ID;
    };
    FuelConsumptionReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    FuelConsumptionReportPage.prototype.getReport = function () {
        var _this = this;
        var payload = {};
        var _basU = this.apiCall.mainUrl + "gps/updatedFuelReport";
        if (this.repkey === this.translate.instant('Daily') || this.repkey === this.translate.instant('Trip')) {
            if (this.vehId == undefined) {
                var toast = this.toastCtrl.create({
                    message: this.translate.instant('Please Select Vehicle..'),
                    duration: 1500,
                    position: "bottom"
                });
                toast.present();
            }
            else {
                if (this.repkey === this.translate.instant('Daily')) {
                    payload = {
                        "from": new Date(this.datetimeStart).toISOString(),
                        "to": new Date(this.datetimeEnd).toISOString(),
                        "user": this.islogin._id,
                        "daywise": true,
                        "imei": this.vehId.Device_ID,
                        "vehicle": this.vehId._id
                    };
                }
                else if (this.repkey === this.translate.instant('Trip')) {
                    payload = {
                        "from": new Date(this.datetimeStart).toISOString(),
                        "to": new Date(this.datetimeEnd).toISOString(),
                        "user": this.islogin._id,
                        "trip": true,
                        "imei": this.vehId.Device_ID,
                        "vehicle": this.vehId._id
                    };
                }
                this.apiCall.startLoading().present();
                this.apiCall.urlpasseswithdata(_basU, payload)
                    .subscribe(function (res) {
                    _this.apiCall.stopLoading();
                    // console.log("report data: ", res)
                    _this.fuelDataArr = [];
                    if (res.message === undefined) {
                        _this.fuelDataArr = res;
                        // initialize to page 1
                        // this.setPage(1);
                    }
                    else {
                        var toast = _this.toastCtrl.create({
                            message: "No data found..",
                            duration: 1500,
                            position: 'bottom'
                        });
                        toast.present();
                    }
                }, function (err) {
                    _this.apiCall.stopLoading();
                });
            }
        }
        else if (this.repkey === this.translate.instant('Time')) {
            if (this.vehId != undefined) {
                payload = {
                    "from": new Date(this.datetimeStart).toISOString(),
                    "to": new Date(this.datetimeEnd).toISOString(),
                    "imei": this.vehId.Device_ID,
                    "vehicle": this.vehId._id
                };
            }
            else {
                payload = {
                    "from": new Date(this.datetimeStart).toISOString(),
                    "to": new Date(this.datetimeEnd).toISOString(),
                    "user": this.islogin._id
                };
            }
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(_basU, payload)
                .subscribe(function (res) {
                _this.apiCall.stopLoading();
                console.log("report data: ", res);
                _this.fuelDataArr = [];
                if (res.message === undefined) {
                    _this.fuelDataArr = res;
                    // initialize to page 1
                    _this.setPage(1);
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: "No data found..",
                        duration: 1500,
                        position: 'bottom'
                    });
                    toast.present();
                }
            });
        }
    };
    FuelConsumptionReportPage.prototype.setPage = function (page) {
        debugger;
        // get pager object from service
        this.pager = this.pagerService.getPager(this.fuelDataArr.length, page);
        if (page >= 2) {
            var someArr = this.fuelDataArr.slice(this.pager.startIndex, this.pager.endIndex + 1);
            for (var i = 0; i < someArr.length; i++) {
                this.pagedItems.push(someArr[i]);
            }
        }
        else {
            // get current page of items
            this.pagedItems = this.fuelDataArr.slice(this.pager.startIndex, this.pager.endIndex + 1);
        }
        console.log("pagedItems: ", this.pagedItems);
    };
    FuelConsumptionReportPage.prototype.start_address = function (item, index) {
        var that = this;
        that.fuelDataArr[index].StartLocation = "N/A";
        if (!item.start_location) {
            that.fuelDataArr[index].StartLocation = "N/A";
        }
        else if (item.start_location) {
            this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.lng))
                .then(function (res) {
                console.log("test", res);
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, item.start_location.lat, item.start_location.lng);
                that.fuelDataArr[index].StartLocation = str;
            });
        }
    };
    FuelConsumptionReportPage.prototype.end_address = function (item, index) {
        var that = this;
        that.fuelDataArr[index].EndLocation = "N/A";
        if (!item.end_location) {
            that.fuelDataArr[index].EndLocation = "N/A";
        }
        else if (item.end_location) {
            this.geocoderApi.reverseGeocode(Number(item.end_location.lat), Number(item.end_location.lng))
                .then(function (res) {
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, item.end_location.lat, item.end_location.lng);
                that.fuelDataArr[index].EndLocation = str;
            });
        }
    };
    FuelConsumptionReportPage.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apiCall.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            console.log("check if address is stored in db or not? ", respData);
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    FuelConsumptionReportPage.prototype.secondsToHms = function (d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);
        var hDisplay = h > 0 ? h + (h == 1 ? " hr, " : " hrs, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? " min, " : " mins, ") : "";
        var sDisplay = s > 0 ? s + (s == 1 ? " sec" : " sec") : "";
        return hDisplay + mDisplay + sDisplay;
    };
    FuelConsumptionReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fuel-consumption-report',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/fuel/fuel-consumption-report/fuel-consumption-report.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "Fuel Consumption Report" | translate }}</ion-title>\n  </ion-navbar>\n  <ion-item style="background-color: #fafafa;">\n    <ion-label style="margin-top: 15px;">{{\n      "Select Vehicle" | translate\n    }}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getId(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n  <ion-item style="background-color: #fafafa;">\n    <ion-label>{{ "Report Type" | translate }}</ion-label>\n    <ion-select [(ngModel)]="repkey">\n      <ion-option *ngFor="let rep of reportType" [value]="rep" (ionSelect)="reportWise(rep)"\n        [selected]="repkey === rep">{{ rep }}</ion-option>\n    </ion-select>\n  </ion-item>\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{ "From Date" | translate }}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [min]="twoMonthsLater"\n          [max]="today" [(ngModel)]="datetimeStart" style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{ "To Date" | translate }}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right;">\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getReport()"></ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n\n<ion-content>\n  <ion-card *ngFor="let item of fuelDataArr; let i = index">\n    <ion-item style="border-bottom: 2px solid #dedede;" [ngClass]="{ paddClass: repkey === translate.instant(\'Trip\') }">\n\n      <ion-row>\n        <ion-col col-3>\n          <img src="assets/imgs/car_blue_icon.png" style="width: 40px; height: 40px; margin-top: -10px;" />\n        </ion-col>\n        <ion-col col-9>\n          <p style="color:black; font-size:16px; ">\n            {{ item.vehicle_name }}\n          </p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-5>\n          <p style="color:gray;font-size:11px;  text-align:left;font-weight:400;">\n            <span *ngIf="item.start_time">{{ item.start_time | date: "mediumDate" }},{{\n                          item.start_time | date: "shortTime"\n                        }}</span>\n            <span *ngIf="!item.start_time">N/A</span>&nbsp;\n          </p>\n\n          <p style="color:#53ab53;margin-left:0%;font-size:11px;font-weight: bold;">\n            {{\'Start Time\' | translate}}\n          </p>\n\n        </ion-col>\n        <ion-col col-5>\n          <p style="color:gray;font-size:11px;text-align:left;font-weight:400;">\n            <span *ngIf="item.end_time">{{\n                          item.end_time | date: "mediumDate"\n                        }},{{\n                          item.end_time | date: "shortTime"\n                        }}</span>\n            <span *ngIf="!item.end_time">N/A</span>&nbsp;\n          </p>\n\n          <p style="text-align:left;font-size: 11px;color:red; font-weight:bold;">\n            {{\'End Time\' | translate}}\n          </p>\n\n        </ion-col>\n        <ion-col col-2>\n          <p style="color:gray;font-size:11px; font-weight: 400;">\n            <span *ngIf="item.fuel_consumed">{{ item.fuel_consumed | number: "1.0-2" }}</span>\n            <span *ngIf="!item.fuel_consumed">00.00\n              {{ (measurementUnit == \'MKS\') ? ("Litre" | translate) : ("Gallon" | translate)}}</span>&nbsp;\n          </p>\n\n          <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: bold;">\n            {{\n                  "Fuel Con" | translate\n                }}\n          </p>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-top:2%;">\n        <ion-col col-5>\n          <p style="color:gray;font-size:11px; margin-top: -9px; font-weight:400;">\n            <span *ngIf="item.in_event_count > 0" tappable\n              (click)="getFuelNotifs(item, \'IN\')">{{ item.in_event_count }}</span>\n            <span *ngIf="item.in_event_count === 0">{{ item.in_event_count }}</span>&nbsp;\n          </p>\n          <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: bold;">\n            {{\n                  "In event count" | translate\n                }}\n          </p>\n        </ion-col>\n        <ion-col col-5>\n          <p style="color:gray;font-size:11px; margin-top: -9px; font-weight: 400;">\n            <span *ngIf="item.out_event_count > 0" tappable\n              (click)="getFuelNotifs(item, \'OUT\')">{{ item.out_event_count }}</span>\n            <span *ngIf="item.out_event_count === 0">{{ item.out_event_count }}</span>&nbsp;\n          </p>\n\n          <p style="font-size: 11px;color:#11c1f3;  font-weight:bold;">\n            {{\n                  "Out event count" | translate\n                }}\n          </p>\n\n        </ion-col>\n        <ion-col col-2>\n          <p style="color:gray;font-size:11px; margin-top: -9px; font-weight:400;">\n            <span *ngIf="item.distance">{{\n                    item.distance | number: "1.0-2"\n                  }} {{ (measurementUnit == \'MKS\') ? ("Kms" | translate) : ("Miles" | translate)}}</span>\n            <span *ngIf="!item.distance">0</span>&nbsp;\n          </p>\n\n          <p style="font-size: 11px;color:#e6c917; font-weight:bold;">\n            {{\'Distance\' | translate}}\n          </p>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-top:1%;font-size: 0.8em;">\n        <ion-col col-1>\n          <ion-icon name="pin" style="color:#33c45c;font-size:15px;"></ion-icon>\n        </ion-col>\n        <ion-col col-11 (onCreate)="start_address(item, i)" class="colSt2">\n          <div class="overme">\n            {{ item.StartLocation }}\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row style="font-size: 0.8em;">\n        <ion-col col-1>\n          <ion-icon name="pin" style="color:#e14444;font-size:15px;"></ion-icon>\n        </ion-col>\n        <ion-col col-11 (onCreate)="end_address(item, i)" class="colSt2">\n          <div class="overme">\n            {{ item.EndLocation }}\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n\n  </ion-card>\n  <!-- <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="{{ \'Loading more data...\' | translate}}">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll> -->\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/fuel/fuel-consumption-report/fuel-consumption-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_6__providers_pager_pager__["a" /* PagerProvider */]])
    ], FuelConsumptionReportPage);
    return FuelConsumptionReportPage;
}());

//# sourceMappingURL=fuel-consumption-report.js.map

/***/ }),

/***/ 1080:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnCreate; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OnCreate = /** @class */ (function () {
    function OnCreate() {
        this.onCreate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    OnCreate.prototype.ngOnInit = function () {
        this.onCreate.emit('dummy');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], OnCreate.prototype, "onCreate", void 0);
    OnCreate = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[onCreate]'
        }),
        __metadata("design:paramtypes", [])
    ], OnCreate);
    return OnCreate;
}());

//# sourceMappingURL=dummy-directive.js.map

/***/ }),

/***/ 986:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelConsumptionReportPageModule", function() { return FuelConsumptionReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fuel_consumption_report__ = __webpack_require__(1079);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__dummy_directive__ = __webpack_require__(1080);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var FuelConsumptionReportPageModule = /** @class */ (function () {
    function FuelConsumptionReportPageModule() {
    }
    FuelConsumptionReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fuel_consumption_report__["a" /* FuelConsumptionReportPage */],
                __WEBPACK_IMPORTED_MODULE_5__dummy_directive__["a" /* OnCreate */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__fuel_consumption_report__["a" /* FuelConsumptionReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild(),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_5__dummy_directive__["a" /* OnCreate */]
            ],
        })
    ], FuelConsumptionReportPageModule);
    return FuelConsumptionReportPageModule;
}());

//# sourceMappingURL=fuel-consumption-report.module.js.map

/***/ })

});
//# sourceMappingURL=20.js.map