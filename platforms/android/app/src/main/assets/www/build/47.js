webpackJsonp([47],{

/***/ 1086:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddGeofencePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_geocoder__ = __webpack_require__(193);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AddGeofencePage = /** @class */ (function () {
    function AddGeofencePage(apiCall, alertCtrl, toastCtrl, fb, navCtrl, 
    // public geoLocation: Geolocation,
    viewCtrl, nativeGeocoder, events, plt) {
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.fb = fb;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.nativeGeocoder = nativeGeocoder;
        this.events = events;
        this.plt = plt;
        this.finalcordinate = [];
        this.cord = [];
        this.autocompleteItems = [];
        this.autocomplete = {};
        this.storedLatLng = [];
        this.fence = 200;
        this.polyineArray = [];
        this.keepMark = [];
        //console.log("maptest", this.map);
        //console.log("maptest1", this.map.moveCamera);
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.geofenceForm = fb.group({
            geofence_name: [''],
            check: [false],
            check1: [false]
        });
        this.acService = new google.maps.places.AutocompleteService();
    }
    AddGeofencePage.prototype.ngOnInit = function () {
        // this.runGeofenceMaps();
        this.drawGeofence();
        this.autocompleteItems = [];
        this.autocomplete = {
            query: '',
            creation_type: 'circular'
        };
    };
    AddGeofencePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddGeofencePage.prototype.enteringFunc = function () {
        console.log(this.geofenceForm.value.check);
        this.entering = this.geofenceForm.value.check;
    };
    AddGeofencePage.prototype.exitingFunc = function () {
        console.log(this.geofenceForm.value.check1);
        this.exiting = this.geofenceForm.value.check1;
    };
    AddGeofencePage.prototype.creategoefence = function () {
        var _this = this;
        var that = this;
        that.submitAttempt = true;
        // console.log("circle coordinates: ", that.circleData.getCenter().lat);
        // console.log("circle radius: ", that.circleData.getRadius());
        if (that.autocomplete.creation_type == 'polygon') {
            for (var r = 0; r < that.storedLatLng.length; r++) {
                var a = [];
                a.push(parseFloat(that.storedLatLng[r].lng));
                a.push(parseFloat(that.storedLatLng[r].lat));
                that.cord.push(a);
            }
            that.finalcordinate.push(that.cord);
            if (this.geofenceForm.valid) {
                if (this.entering || this.exiting) {
                    if (this.geofenceForm.value.geofence_name && that.finalcordinate.length) {
                        var data = {
                            "uid": this.islogin._id,
                            "geoname": this.geofenceForm.value.geofence_name,
                            "entering": this.entering,
                            "exiting": this.exiting,
                            "geofence": that.finalcordinate
                        };
                        this.apiCall.startLoading().present();
                        this.apiCall.addgeofenceCall(data)
                            .subscribe(function (data) {
                            _this.apiCall.stopLoading();
                            _this.devicesadd = data;
                            console.log(_this.devicesadd);
                            var toast = _this.toastCtrl.create({
                                message: 'Geofence created successfully',
                                position: 'bottom',
                                duration: 2000
                            });
                            toast.onDidDismiss(function () {
                                console.log('Dismissed toast');
                                _this.events.publish('reloadDetails');
                                _this.navCtrl.pop();
                            });
                            toast.present();
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            var alert = _this.alertCtrl.create({
                                message: 'Please draw valid geofence..',
                                buttons: [{
                                        text: 'OK', handler: function () {
                                            that.storedLatLng = [];
                                            that.finalcordinate = [];
                                            that.cord = [];
                                            _this.drawGeofence();
                                        }
                                    }]
                            });
                            alert.present();
                            console.log(err);
                        });
                    }
                    else {
                        var toast = this.toastCtrl.create({
                            message: 'Select Geofence On Map or make sure fence name field is not empty!',
                            position: 'top',
                            duration: 2000
                        });
                        toast.present();
                    }
                }
                else {
                    var alert_1 = this.alertCtrl.create({
                        message: 'All fields are required!',
                        buttons: ['Try Again']
                    });
                    alert_1.present();
                }
            }
        }
        else if (that.autocomplete.creation_type == 'circular') {
            if (this.geofenceForm.valid) {
                if (this.entering || this.exiting) {
                    if (this.geofenceForm.value.geofence_name && that.circleData != undefined) {
                        var payload = {
                            "poi": [
                                {
                                    "location": {
                                        "type": "Point",
                                        "coordinates": [
                                            that.circleData.getCenter().lng,
                                            that.circleData.getCenter().lat
                                        ]
                                    },
                                    "poiname": that.geofenceForm.value.geofence_name,
                                    "status": "Active",
                                    "user": that.islogin._id,
                                    "address": "N/A",
                                    "radius": that.circleData.getRadius()
                                }
                            ]
                        };
                        this.apiCall.startLoading().present();
                        this.apiCall.addPOIAPI(payload)
                            .subscribe(function (data) {
                            console.log("response adding poi: ", data);
                            _this.apiCall.stopLoading();
                            _this.events.publish('reloadDetails');
                            _this.navCtrl.pop();
                            var toast = _this.toastCtrl.create({
                                message: "Geofence created successfully!",
                                duration: 1500,
                                position: 'top'
                            });
                            toast.present();
                        }, function (err) {
                            console.log("error adding poi: ", err);
                            _this.apiCall.stopLoading();
                        });
                    }
                    else {
                        var toast = this.toastCtrl.create({
                            message: 'Select Geofence On Map or make sure fence name field is not empty!',
                            position: 'top',
                            duration: 2000
                        });
                        toast.present();
                    }
                }
                else {
                    var alert_2 = this.alertCtrl.create({
                        message: 'All fields are required!',
                        buttons: ['Try Again']
                    });
                    alert_2.present();
                }
            }
        }
    };
    AddGeofencePage.prototype.updateSearch = function () {
        // debugger
        console.log('modal > updateSearch');
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        var that = this;
        var config = {
            //types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
            input: that.autocomplete.query,
            componentRestrictions: {}
        };
        this.acService.getPlacePredictions(config, function (predictions, status) {
            console.log('modal > getPlacePredictions > status > ', status);
            console.log("lat long not find ", predictions);
            that.autocompleteItems = [];
            predictions.forEach(function (prediction) {
                that.autocompleteItems.push(prediction);
            });
            console.log("autocompleteItems=> " + that.autocompleteItems);
        });
    };
    AddGeofencePage.prototype.chooseItem = function (item) {
        var _this = this;
        var that = this;
        that.autocomplete.query = item.description;
        console.log("console items=> " + JSON.stringify(item));
        that.autocompleteItems = [];
        var options = {
            useLocale: true,
            maxResults: 5
        };
        this.nativeGeocoder.forwardGeocode(item.description, options)
            .then(function (coordinates) {
            console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude);
            that.newLat = coordinates[0].latitude;
            that.newLng = coordinates[0].longitude;
            var pos = {
                target: new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](that.newLat, that.newLng),
                zoom: 15,
                tilt: 30
            };
            _this.map.moveCamera(pos);
            _this.map.addMarker({
                title: '',
                position: new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](that.newLat, that.newLng),
            }).then(function (data) {
                console.log("Marker added");
            });
        })
            .catch(function (error) { return console.log(error); });
    };
    AddGeofencePage.prototype.callThis = function (fence) {
        var that = this;
        that.circleData.setRadius(fence);
    };
    AddGeofencePage.prototype.radioChecked = function (key) {
        var _this = this;
        var that = this;
        console.log("radio btn checked: ", key);
        that.autocomplete.creation_type = key;
        if (key == "polygon") {
            if (that.circleData) {
                that.circleData.remove();
                that.fence = 200;
            }
            that.storedLatLng = [];
        }
        else {
            if (key == "circular") {
                // this.geoLocation.getCurrentPosition().then((resp) => {
                //   this.latlat = resp.coords.latitude;
                //   this.lnglng = resp.coords.longitude
                //   let pos: CameraPosition<ILatLng> = {
                //     target: new LatLng(resp.coords.latitude, resp.coords.longitude),
                //     zoom: 16,
                //     tilt: 30
                //   };
                //   this.map.moveCamera(pos);
                //   if (that.circleData != undefined) {
                //     that.circleData.remove();
                //   }
                //   let ltln: ILatLng = { "lat": this.latlat, "lng": this.lnglng };
                //   let circle: Circle = this.map.addCircleSync({
                //     'center': ltln,
                //     'radius': that.fence,
                //     'strokeColor': '#ee612e',
                //     'strokeWidth': 2,
                //     'fillColor': 'rgb(255, 128, 128, 0.5)'
                //   });
                //   that.circleData = circle;
                //   console.log("circle data: => " + that.circleData)
                // });
                // Get the location of you
                this.map.getMyLocation()
                    .then(function (location) {
                    console.log(JSON.stringify(location, null, 2));
                    _this.latlat = location.latLng.lat;
                    _this.lnglng = location.latLng.lng;
                    // Move the map camera to the location with animation
                    _this.map.animateCamera({
                        target: location.latLng,
                        zoom: 17,
                        tilt: 30
                    }).then(function () {
                        if (that.circleData != undefined) {
                            that.circleData.remove();
                        }
                        var ltln = { "lat": _this.latlat, "lng": _this.lnglng };
                        var circle = _this.map.addCircleSync({
                            'center': ltln,
                            'radius': that.fence,
                            'strokeColor': '#ee612e',
                            'strokeWidth': 2,
                            'fillColor': 'rgb(255, 128, 128, 0.5)'
                        });
                        that.circleData = circle;
                        console.log("circle data: => " + that.circleData);
                        // this.map.addMarker({
                        //   title: 'Add POI here(Drag Me)',
                        //   position: location.latLng,
                        //   animation: GoogleMapsAnimation.BOUNCE,
                        //   draggable: true
                        // }).then((marker: Marker) => {
                        //   this.markerObj = marker;
                        //   marker.on(GoogleMapsEvent.MARKER_DRAG_END)
                        //     .subscribe(() => {
                        //       this.markerObj = undefined;
                        //       this.markerObj = marker;
                        //     });
                        // })
                    });
                });
                if (that.collectPoly) {
                    that.collectPoly.remove();
                }
                if (that.polyineArray.length > 0) {
                    for (var i = 0; i < that.polyineArray.length; i++) {
                        that.polyineArray[i].remove();
                    }
                }
                if (that.keepMark.length > 0) {
                    for (var r = 0; r < that.keepMark.length; r++) {
                        that.keepMark[r].remove();
                    }
                }
                that.storedLatLng = [];
            }
        }
    };
    AddGeofencePage.prototype.drawGeofence = function () {
        var _this = this;
        if (this.map != undefined) {
            this.map.remove();
        }
        this.mapElement = document.getElementById('mapGeofence');
        // this.map = this.googleMaps.create(this.mapElement);
        this.map = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["b" /* GoogleMaps */].create(this.mapElement);
        // Wait the MAP_READY before using any methods.
        this.map.one(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MAP_READY)
            .then(function () {
            console.log('Map is ready!');
            var that = _this;
            if (that.liveMark) {
                that.liveMark.remove();
            }
            _this.map.getMyLocation()
                .then(function (location) {
                console.log(JSON.stringify(location, null, 2));
                _this.latlat = location.latLng.lat;
                _this.lnglng = location.latLng.lng;
                // Move the map camera to the location with animation
                _this.map.animateCamera({
                    target: location.latLng,
                    zoom: 17,
                    tilt: 30
                }).then(function () {
                    _this.map.addMarker({
                        title: '',
                        position: new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](location.latLng.lat, location.latLng.lng),
                    }).then(function (marker) {
                        console.log("Marker added");
                        that.liveMark = marker;
                    });
                    if (that.autocomplete.creation_type == 'circular') {
                        if (that.circleData != undefined) {
                            that.circleData.remove();
                        }
                        // that.storedLatLng.push(data[0]);
                        // Add circle
                        var ltln = { "lat": location.latLng.lat, "lng": location.latLng.lng };
                        var circle = _this.map.addCircleSync({
                            'center': ltln,
                            'radius': that.fence,
                            'strokeColor': '#ee612e',
                            'strokeWidth': 2,
                            'fillColor': 'rgb(255, 128, 128, 0.5)'
                        });
                        that.circleData = circle;
                        console.log("circle data: => " + that.circleData);
                    }
                });
            });
            that.map.on(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MAP_CLICK).subscribe(function (data) {
                if (that.autocomplete.creation_type == 'circular') {
                    if (that.circleData != undefined) {
                        that.circleData.remove();
                    }
                    // that.storedLatLng.push(data[0]);
                    // Add circle
                    var ltln = { "lat": data[0].lat, "lng": data[0].lng };
                    var circle = _this.map.addCircleSync({
                        'center': ltln,
                        'radius': that.fence,
                        'strokeColor': '#ee612e',
                        'strokeWidth': 2,
                        'fillColor': 'rgb(255, 128, 128, 0.5)'
                    });
                    that.circleData = circle;
                    console.log("circle data: => " + that.circleData);
                    // this.map.moveCamera({
                    //   target: circle.getBounds()
                    // });
                }
                else {
                    if (that.autocomplete.creation_type == 'polygon') {
                        var markicon;
                        if (that.plt.is('ios')) {
                            markicon = 'www/assets/imgs/circle1.png';
                        }
                        else if (that.plt.is('android')) {
                            markicon = './assets/imgs/circle1.png';
                        }
                        that.storedLatLng.push(data[0]);
                        that.map.addMarker({
                            position: data[0],
                            icon: {
                                url: markicon,
                                size: {
                                    width: 13,
                                    height: 13
                                }
                            }
                        }).then(function (mark) {
                            that.keepMark.push(mark);
                            mark.on(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function (latLng) {
                                that.storedLatLng.push(that.storedLatLng[0]); // store first lat lng at last also
                                var GORYOKAKU_POINTS = that.storedLatLng;
                                that.map.addPolygon({
                                    'points': GORYOKAKU_POINTS,
                                    'strokeColor': '#000',
                                    'fillColor': '#ffff00',
                                    'strokeWidth': 5
                                }).then(function (polygon) {
                                    // console.log("GORYOKAKU_POINTS=> " + JSON.stringify(GORYOKAKU_POINTS));
                                    that.collectPoly = polygon;
                                });
                            });
                        });
                        debugger;
                        // console.log(JSON.stringify(that.storedLatLng));
                        if (that.storedLatLng.length > 1) {
                            that.map.addPolyline({
                                points: that.storedLatLng,
                                color: '#000000',
                                width: 3,
                                geodesic: true
                            }).then(function (poly) {
                                that.polyineArray.push(poly);
                            });
                        }
                    }
                }
            });
        });
    };
    AddGeofencePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-add-geofence',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/geofence/add-geofence/add-geofence.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{ "Add Geofence" | translate }}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div #mapGeofence id="mapGeofence" data-tap-disabled="true">\n    <ion-searchbar style="padding: 8px 8px 5px 8px;" [(ngModel)]="autocomplete.query" (ionInput)="updateSearch()"\n      placeholder="{{ \'Search Places...\' | translate }}">\n    </ion-searchbar>\n    <ion-list [ngClass]="autocompleteItems.length > 0 ? \'searchclass1\' : \'searchclass2\'">\n      <ion-item *ngFor="let item of autocompleteItems" (click)="chooseItem(item)">\n        {{ item.description }}\n      </ion-item>\n    </ion-list>\n    <div style="padding: 10px;">\n      <div style="padding: 0px 10px 10px;margin-top: -10px; background: white; border-radius: 5px;">\n        <input type="radio" name="demo" [(ngModel)]="autocomplete.creation_type" value="circular" id="radio-one"\n          class="form-radio" (click)="radioChecked(\'circular\')" /><label\n          for="radio-one">{{ "Circular" | translate }}</label>\n        <input type="radio" name="demo" [(ngModel)]="autocomplete.creation_type" value="polygon" id="radio-one"\n          class="form-radio" (click)="radioChecked(\'polygon\')" /><label\n          for="radio-two">{{ "Polygon" | translate }}</label>\n      </div>\n    </div>\n    <div *ngIf="autocomplete.creation_type == \'circular\'" style="padding-left: 5px; padding-right: 5px"\n      [ngClass]="autocompleteItems.length > 0 ? \'searchclass2\' : \'searchclass1\'">\n      <ion-list style="margin: 0px; border-radius: 25px; background: #d5dbe3;">\n        <ion-item style="background: transparent;">\n          <ion-range style="padding: 0px; margin: 0px;" min="200" max="5000" pin="false" [(ngModel)]="fence"\n            color="secondary" (ionChange)="callThis(fence)">\n            <ion-icon range-left small color="gpsc" name="remove-circle"></ion-icon>\n            <span range-left style="font-size: 10px;">{{\n              "200m" | translate\n            }}</span>\n            <span range-right style="font-size: 10px;">{{\n              "500m" | translate\n            }}</span>\n            <ion-icon range-right color="gpsc" name="add-circle"></ion-icon>\n          </ion-range>\n        </ion-item>\n      </ion-list>\n      <ion-row>\n        <ion-col col-6 style="padding-left: 10px;">{{\n          "Radius:" | translate\n        }}</ion-col>\n        <ion-col col-6 style="text-align: right;padding-right: 10px;">\n          <ion-badge color="gpsc">{{ fence }}</ion-badge>\n        </ion-col>\n      </ion-row>\n    </div>\n  </div>\n\n  <form [formGroup]="geofenceForm">\n    <ion-item class="logitem">\n      <ion-label style="color: black;font-size: 0.9em;" fixed>{{\n        "Fence Name*" | translate\n      }}</ion-label>\n      <ion-input style="font-size: 0.9em;" formControlName="geofence_name" type="text"></ion-input>\n    </ion-item>\n    <ion-row>\n      <ion-col col-4 padding-left>\n        <ion-label style="font-size: 1em">{{\n          "Alert Settings" | translate\n        }}</ion-label>\n      </ion-col>\n      <ion-col col-4>\n        <ion-row style="padding: 10px">\n          <ion-col col-6 style="text-align: right;">\n            <ion-checkbox color="gpsc" checked="true" formControlName="check" (ionChange)="enteringFunc()">\n            </ion-checkbox>\n          </ion-col>\n          <ion-col col-6 float-left>{{ "Enter" | translate }}</ion-col>\n        </ion-row>\n      </ion-col>\n      <ion-col col-4>\n        <ion-row style="padding: 10px">\n          <ion-col col-6 style="text-align: right;">\n            <ion-checkbox color="gpsc" checked="true" formControlName="check1" (ionChange)="exitingFunc()">\n            </ion-checkbox>\n          </ion-col>\n          <ion-col col-6 float-left>{{ "Exit" | translate }}</ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </form>\n</ion-content>\n\n<ion-footer class="footSty">\n  <ion-toolbar>\n    <ion-row>\n      <ion-col style="text-align: center">\n        <button ion-button clear color="light" (click)="creategoefence()">\n          {{ "Create Geofence" | translate }}\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/geofence/add-geofence/add-geofence.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"]])
    ], AddGeofencePage);
    return AddGeofencePage;
}());

//# sourceMappingURL=add-geofence.js.map

/***/ }),

/***/ 991:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddGeofencePageModule", function() { return AddGeofencePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_geofence__ = __webpack_require__(1086);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { NativeGeocoder } from '@ionic-native/native-geocoder';
var AddGeofencePageModule = /** @class */ (function () {
    function AddGeofencePageModule() {
    }
    AddGeofencePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_geofence__["a" /* AddGeofencePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__add_geofence__["a" /* AddGeofencePage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            providers: [
            // NativeGeocoder
            ]
        })
    ], AddGeofencePageModule);
    return AddGeofencePageModule;
}());

//# sourceMappingURL=add-geofence.module.js.map

/***/ })

});
//# sourceMappingURL=47.js.map