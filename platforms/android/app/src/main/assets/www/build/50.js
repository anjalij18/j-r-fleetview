webpackJsonp([50],{

/***/ 1073:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpensesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { TranslateService } from '@ngx-translate/core';
var ExpensesPage = /** @class */ (function () {
    function ExpensesPage(navCtrl, apiCall, toastCtrl) {
        this.navCtrl = navCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.portstemp = [];
        this._vehId = {};
        this.expensesData = [];
        this.total = 0;
        this.measurementUnit = 'MKS';
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__().startOf('month').format("YYYY-MM-DD"); // moment({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format();
        if (localStorage.getItem('default_curreny') !== null) {
            this.currency_code = localStorage.getItem('default_curreny');
        }
    }
    ExpensesPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    ExpensesPage.prototype.ionViewDidEnter = function () {
        this.getDefaultUserSettings();
    };
    ExpensesPage.prototype.getDefaultUserSettings = function () {
        var _this = this;
        var b_url = this.apiCall.mainUrl + "users/get_user_setting";
        var Var = { uid: this.islogin._id };
        this.apiCall.urlpasseswithdata(b_url, Var)
            .subscribe(function (resp) {
            console.log("check lang key: ", resp);
            if (resp.unit_measurement !== undefined) {
                _this.measurementUnit = resp.unit_measurement;
            }
            else {
                if (localStorage.getItem('MeasurementType') !== null) {
                    var measureType = localStorage.getItem('MeasurementType');
                    _this.measurementUnit = measureType;
                }
                else {
                    _this.measurementUnit = 'MKS';
                }
            }
        }, function (err) {
            console.log(err);
            if (localStorage.getItem('MeasurementType') !== null) {
                var measureType = localStorage.getItem('MeasurementType');
                _this.measurementUnit = measureType;
            }
            else {
                _this.measurementUnit = 'MKS';
            }
        });
    };
    ExpensesPage.prototype.onTypeDetail = function (exp) {
        console.log(exp);
        this.navCtrl.push('ExpenseTypeDetailPage', {
            expense: exp,
            dateFrom: this.datetimeStart,
            dateTo: this.datetimeEnd,
            userId: this.islogin._id,
            portstemp: this.portstemp,
            vehId: this._vehId._id
        });
    };
    ExpensesPage.prototype.getExpenceTypes = function () {
        var _this = this;
        var _bUrl;
        this.total = 0;
        if (this._vehId._id != undefined) {
            _bUrl = this.apiCall.mainUrl + "expense/expensebycateogry?user=" + this.islogin._id + "&fdate=" + new Date(this.datetimeStart).toISOString() + "&tdate=" + new Date(this.datetimeEnd).toISOString() + "&vehicle=" + this._vehId._id;
        }
        else {
            _bUrl = this.apiCall.mainUrl + "expense/expensebycateogry?user=" + this.islogin._id + "&fdate=" + new Date(this.datetimeStart).toISOString() + "&tdate=" + new Date(this.datetimeEnd).toISOString();
        }
        // const _bUrl = this.apiCall.mainUrl + "expense/expensebycateogry?user=" + this.islogin._id + "&fdate=" + new Date(this.datetimeStart).toISOString() + "&tdate=" + new Date(this.datetimeEnd).toISOString();
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(_bUrl)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.expensesData = [];
            _this.expensesData = data.expenseobj;
            for (var j = 0; j < data.expenseobj.length; j++) {
                _this.total += data.expenseobj[j].total;
            }
            console.log("expense type=> " + data);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    ExpensesPage.prototype.addExpence = function () {
        this.navCtrl.push('AddExpensePage', { vehicleList: this.portstemp });
    };
    ExpensesPage.prototype.getExpenseList = function () {
        this.getExpenceTypes();
    };
    ExpensesPage.prototype.ionViewWillEnter = function () {
        this.showBtn = false;
        this._vehId = {};
        this.selectedVehicle = undefined;
        console.log("view will enter");
        this.getExpenseList();
    };
    ExpensesPage.prototype.toastMessage = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'bottom'
        });
        toast.present();
    };
    ExpensesPage.prototype.getid = function (veh) {
        this._vehId = veh;
        this.showBtn = true;
    };
    ExpensesPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        // this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            // this.apiCall.stopLoading();
            console.log(err);
        });
    };
    ExpensesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-expenses',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/expenses/expenses.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "Expenses" | translate }}</ion-title>\n    <ion-buttons end *ngIf="showBtn">\n      <button ion-button (click)="ionViewWillEnter()">\n        {{ "All Vehicles" | translate }}\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n  <ion-item style="background-color: #fafafa;">\n    <ion-label style="margin-top: 15px;">{{\n      "Select Vehicle" | translate\n    }}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getid(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n  <ion-row style="background-color: #fafafa;" padding-left padding-right>\n    <ion-col width-20 class="cust">\n      <ion-label>\n        <span style="font-size: 13px">{{ "From Date" | translate }}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20 class="cust">\n      <ion-label>\n        <span style="font-size: 13px">{{ "To Date" | translate }}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right">\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getExpenseList()">\n        </ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-row>\n          <ion-col col-6 text-center *ngFor="let exp of expensesData; let i = index" offest-sm="2" no-padding\n            (click)="onTypeDetail(exp)">\n            <ion-card style="border-radius: 16px;">\n              <ion-card-header style="padding: 16px 0px 0px 0px;">\n                <img src="assets/imgs/001-wage.png" *ngIf="exp._id === \'salary\'" />\n                <img src="assets/imgs/002-gas.png" *ngIf="exp._id === \'fuel\'" />\n                <img src="assets/imgs/settings.png" *ngIf="exp._id === \'tools\'" />\n                <img src="assets/imgs/carpenter.png" *ngIf="exp._id === \'labor\'" />\n                <img src="assets/imgs/003-customer-support.png" *ngIf="exp._id === \'service\'" />\n                <img src="assets/imgs/toll-road.png" *ngIf="exp._id === \'TOLE\'" />\n                <img src="assets/imgs/004-wheel.png" *ngIf="exp._id === \'other\'" />\n                <ion-card-title style="font-size: 1.7rem;" color="gpsc">{{\n                  exp._id | titlecase\n                }}</ion-card-title>\n              </ion-card-header>\n              <ion-card-content>\n                <p style="margin: 0px; padding: 0px; font-size: 1em">\n                  {{ exp.total | number: "2." }} {{ exp.currency }}\n                </p>\n              </ion-card-content>\n            </ion-card>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-fab right bottom>\n    <button ion-fab color="gpsc" (click)="addExpence()">\n      <ion-icon name="add"></ion-icon>\n    </button>\n  </ion-fab>\n</ion-content>\n<ion-footer class="footSty">\n  <ion-toolbar>\n    <ion-row no-padding>\n      <ion-col width-50 style="text-align: center; color: #fff; font-size: 1.5em;">\n        <b>{{ "Total" | translate }} {{ total | number: "2." }} {{currency_code}}</b>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/expenses/expenses.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], ExpensesPage);
    return ExpensesPage;
}());

//# sourceMappingURL=expenses.js.map

/***/ }),

/***/ 981:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpensesPageModule", function() { return ExpensesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__expenses__ = __webpack_require__(1073);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ExpensesPageModule = /** @class */ (function () {
    function ExpensesPageModule() {
    }
    ExpensesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__expenses__["a" /* ExpensesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__expenses__["a" /* ExpensesPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], ExpensesPageModule);
    return ExpensesPageModule;
}());

//# sourceMappingURL=expenses.module.js.map

/***/ })

});
//# sourceMappingURL=50.js.map