webpackJsonp([52],{

/***/ 1069:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddExpensePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddExpensePage = /** @class */ (function () {
    function AddExpensePage(navCtrl, navParams, apiCall, toastCtrl, translate) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.translate = translate;
        this.portstemp = [];
        this.veh_id = {};
        this.currencyArray = [];
        this.currencyJson = [];
        this.currencyValue = 'INR';
        this.expenseType = [
            { "value": "fuel", "viewValue": "Fuel" },
            { "value": "labor", "viewValue": "Labor" },
            { "value": "service", "viewValue": "Service" },
            { "value": "tools", "viewValue": "Tools" },
            { "value": "toll", "viewValue": "Toll" },
            { "value": "salary", "viewValue": "Salary" },
            { "value": "other", "viewValue": "Others" }
        ];
        this.payMode = [
            { "value": "CASH", "viewValue": "Cash" },
            { "value": "CHEQUE", "viewValue": "Cheque" },
            { "value": "ONLINE", "viewValue": "Online" },
            { "value": "OTHER", "viewValue": "Other" }
        ];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        if (!this.navParams.get('vehicleList')) {
            this.navCtrl.pop();
            return;
        }
        this.portstemp = this.navParams.get("vehicleList");
        debugger;
        if (localStorage.getItem('default_curreny') !== null) {
            this.currencyValue = localStorage.getItem('default_curreny');
        }
        this.apiCall.getCurrency().subscribe(function (data) {
            _this.currencyJson = data;
            _this.getCurrencies();
        });
    }
    AddExpensePage.prototype.ngOnInit = function () {
    };
    AddExpensePage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter AddExpensePage');
        // this.getDefaultUserSettings();
    };
    // measurementUnit: string = 'MKS';
    // getDefaultUserSettings() {
    //   var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    //   var Var = { uid: this.islogin._id };
    //   this.apiCall.urlpasseswithdata(b_url, Var)
    //     .subscribe(resp => {
    //       console.log("check lang key: ", resp)
    //       if (resp.unit_measurement !== undefined) {
    //         this.measurementUnit = resp.unit_measurement;
    //         if (this.measurementUnit === 'MKS') {
    //           this.currencyValue = 'INR';
    //         } else if (this.measurementUnit === 'FPS') {
    //           this.currencyValue = 'USD';
    //         }
    //       } else {
    //         if (localStorage.getItem('MeasurementType') !== null) {
    //           let measureType = localStorage.getItem('MeasurementType');
    //           this.measurementUnit = measureType;
    //           if (this.measurementUnit === 'MKS') {
    //             this.currencyValue = 'INR';
    //           } else if (this.measurementUnit === 'FPS') {
    //             this.currencyValue = 'USD';
    //           }
    //         } else {
    //           this.measurementUnit = 'MKS';
    //           this.currencyValue = 'INR';
    //         }
    //       }
    //     },
    //       err => {
    //         console.log(err);
    //         if (localStorage.getItem('MeasurementType') !== null) {
    //           let measureType = localStorage.getItem('MeasurementType');
    //           this.measurementUnit = measureType;
    //           if (this.measurementUnit === 'MKS') {
    //             this.currencyValue = 'INR';
    //           } else if (this.measurementUnit === 'FPS') {
    //             this.currencyValue = 'USD';
    //           }
    //         } else {
    //           this.measurementUnit = 'MKS';
    //           this.currencyValue = 'INR';
    //         }
    //       });
    // }
    AddExpensePage.prototype.getCurrencies = function () {
        this.currency = [];
        var currencyObj = this.currencyJson;
        for (var i in currencyObj) {
            if (this.veh_id.currency != undefined) {
                var devicecurr = this.veh_id.device.currency;
                if (i == devicecurr) {
                    this.currencyValue = i;
                }
            }
            else {
                // if(this.measurementUnit === 'MKS') {
                // if (i == 'INR') {
                if (i == this.currencyValue) {
                    this.currencyValue = i;
                }
                // } else {
                //   if(this.measurementUnit === 'FPS') {
                //     if (i == 'USD') {
                //       this.currencyValue = i;
                //     }
                //   }
                // }
            }
            var tempObj = {
                currencyVal: i,
                country: i + '-' + currencyObj[i]
            };
            this.currencyArray.push(tempObj);
        }
    };
    AddExpensePage.prototype.onChnageEvent = function (veh) {
        var that = this;
        that.veh_id = veh;
        this.getCurrencies();
    };
    AddExpensePage.prototype.submit = function () {
        var _this = this;
        if (this.amt == undefined || this.currencyValue == undefined || this.expenseTypeValue == undefined || this.ddate == undefined || this.vehName == undefined || this.currency == undefined) {
            this.toastMessage(this.translate.instant('Please fill all the fields and try again'));
            return;
        }
        var _burl = this.apiCall.mainUrl + 'expense/addExpense';
        var payload = {
            "user": this.islogin._id,
            "currency": this.currencyValue,
            "vehicle": this.veh_id._id,
            "payment_mode": this.payModeValue,
            "amount": Number(this.amt),
            "odometer": this.veh_id.total_odo,
            "type": this.expenseTypeValue,
            "date": new Date(this.ddate).toISOString()
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(_burl, payload)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            if (data.message === "added Successfully") {
                _this.toastMessage(_this.translate.instant('dealeradded', { value: 'Expense' }));
                _this.navCtrl.pop();
            }
            else {
                _this.toastMessage(_this.translate.instant('Process failed, something went wrong'));
            }
        });
    };
    AddExpensePage.prototype.toastMessage = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'bottom'
        });
        toast.present();
    };
    AddExpensePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-expense',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/expenses/add-expense/add-expense.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{\'Add Expense\' | translate}}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-item>\n      <ion-label>{{\'Select Vehicle\' | translate}}</ion-label>\n      <ion-select [(ngModel)]="vehName" required>\n        <ion-option\n          *ngFor="let veh of portstemp"\n          [value]="veh.Device_Name"\n          (ionSelect)="onChnageEvent(veh)"\n        >\n          {{ veh.Device_Name }}</ion-option\n        >\n      </ion-select>\n    </ion-item>\n    <ion-item>\n      <ion-label>{{\'Expense Type\' | translate}}</ion-label>\n      <ion-select [(ngModel)]="expenseTypeValue" required>\n        <ion-option *ngFor="let type of expenseType" [value]="type.value">{{\n          type.viewValue\n        }}</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item>\n      <ion-label>{{\'Enter Amount\' | translate}}</ion-label>\n      <ion-input\n        type="number"\n        text-right\n        [(ngModel)]="amt"\n        required\n      ></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>{{\'Currency\' | translate}}</ion-label>\n      <ion-select [(ngModel)]="currencyValue" required>\n        <ion-option\n          *ngFor="let currency of currencyArray"\n          [value]="currency.currencyVal"\n          >{{ currency.country }}</ion-option\n        >\n      </ion-select>\n    </ion-item>\n    <ion-item>\n      <ion-label>{{\'Payment Mode\' | translate}}</ion-label>\n      <ion-select [(ngModel)]="payModeValue" required>\n        <ion-option *ngFor="let mode of payMode" [value]="mode.value">{{\n          mode.viewValue\n        }}</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item>\n      <ion-label>{{\'Date\' | translate}}</ion-label>\n      <ion-datetime\n        displayFormat="DD/MM/YYYY"\n        pickerFormat="DD/MM/YYYY"\n        [(ngModel)]="ddate"\n        name="ddate"\n        required\n      >\n      </ion-datetime>\n    </ion-item>\n    <ion-item>\n      <ion-label>{{\'Reference\' | translate}}</ion-label>\n      <!-- <ion-input type="text" name="reference" [ngModel]="reference" required #reference="ngModel"></ion-input> -->\n      <ion-input\n        type="text"\n        text-right\n        [(ngModel)]="reference"\n        required\n      ></ion-input>\n    </ion-item>\n  </ion-list>\n</ion-content>\n\n<ion-footer class="footSty">\n  <ion-toolbar>\n    <ion-row no-padding>\n      <ion-col width-50 style="text-align: center;">\n        <button ion-button clear color="light" (click)="submit()">\n          {{\'SUBMIT\' | translate}}\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/expenses/add-expense/add-expense.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */]])
    ], AddExpensePage);
    return AddExpensePage;
}());

//# sourceMappingURL=add-expense.js.map

/***/ }),

/***/ 978:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddExpensePageModule", function() { return AddExpensePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_expense__ = __webpack_require__(1069);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AddExpensePageModule = /** @class */ (function () {
    function AddExpensePageModule() {
    }
    AddExpensePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_expense__["a" /* AddExpensePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__add_expense__["a" /* AddExpensePage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], AddExpensePageModule);
    return AddExpensePageModule;
}());

//# sourceMappingURL=add-expense.module.js.map

/***/ })

});
//# sourceMappingURL=52.js.map