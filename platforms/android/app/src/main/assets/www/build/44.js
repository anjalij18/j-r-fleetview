webpackJsonp([44],{

/***/ 1088:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__update_group_update_group__ = __webpack_require__(563);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GroupsPage = /** @class */ (function () {
    function GroupsPage(navCtrl, navParams, apigroupcall, alertCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apigroupcall = apigroupcall;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("_id=> " + this.islogin._id);
    }
    GroupsPage_1 = GroupsPage;
    GroupsPage.prototype.ngOnInit = function () {
        this.getgroup();
    };
    GroupsPage.prototype.getgroup = function () {
        var _this = this;
        console.log("getgroup");
        var baseURLp = this.apigroupcall.mainUrl + 'groups/getGroups_list?uid=' + this.islogin._id;
        this.apigroupcall.startLoading().present();
        this.apigroupcall.getGroupCall(baseURLp)
            .subscribe(function (data) {
            _this.apigroupcall.stopLoading();
            _this.groupData = data;
            _this.allGroup = data["group_details"];
            console.log("GroupData=> " + JSON.stringify(_this.allGroup));
            // console.log("customerlist=> ", this.customerslist)
            _this.GroupArray = [];
            for (var i = 0; i < _this.allGroup.length; i++) {
                _this.allGroupName = _this.allGroup[i].name;
                _this.datetime = _this.allGroup[i].last_modified;
                _this.Groupdevice = _this.allGroup[i].devices.length;
                _this.status = _this.allGroup[i].status;
                _this.groupId = _this.allGroup[i]._id;
                _this.GroupArray.push({ 'groupname': _this.allGroupName, 'status': _this.status, 'vehicle': _this.Groupdevice, '_id': _this.groupId, 'datetime': _this.datetime });
                console.log(_this.GroupArray);
            }
        }, function (err) {
            _this.apigroupcall.stopLoading();
            console.log("error found=> " + err);
        });
    };
    GroupsPage.prototype.deleteItem = function (item) {
        var that = this;
        console.log("delete");
        console.log(item);
        var alert = this.alertCtrl.create({
            message: 'Do you want to delete this vehicle ?',
            buttons: [{
                    text: 'YES PROCEED',
                    handler: function () {
                        console.log(item._id);
                        that.deleteDevice(item._id);
                    }
                },
                {
                    text: 'NO'
                }]
        });
        alert.present();
    };
    GroupsPage.prototype.deleteDevice = function (d_id) {
        var _this = this;
        this.apigroupcall.startLoading().present();
        this.apigroupcall.deleteGroupCall(d_id)
            .subscribe(function (data) {
            _this.apigroupcall.stopLoading();
            var DeletedDevice = data;
            console.log(DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: 'Vehicle deleted successfully!',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.navCtrl.push(GroupsPage_1);
            });
            toast.present();
        }, function (err) {
            _this.apigroupcall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    GroupsPage.prototype.openAddGroupModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create('GroupModalPage');
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.getgroup();
        });
        modal.present();
    };
    GroupsPage.prototype.openUpdateGroupModal = function (item) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__update_group_update_group__["a" /* UpdateGroup */], {
            params: item
        });
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.getgroup();
        });
        modal.present();
    };
    GroupsPage = GroupsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-groups',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/groups/groups.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Groups</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="openAddGroupModal()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-list >\n    <ion-item *ngFor="let item of GroupArray">\n      <ion-thumbnail item-start>\n        <img src="assets/imgs/user.png" alt="item.Device_Name">\n      </ion-thumbnail>\n      <h2>{{item.groupname}}</h2>\n      <p>{{item.datetime | date: \'short\'}}</p>\n      <p><span style="color:rgb(255,64,129);">{{item.vehicle}}</span>&nbsp;&nbsp;Vehicles</p>\n      <button ion-button outline item-end (click)="openUpdateGroupModal(item)">Edit</button>\n      <button ion-button clear item-end *ngIf="item.status">Active</button>\n      <button ion-button clear item-end *ngIf="!item.status">InActive</button>\n    </ion-item>\n  </ion-list>\n\n  <!-- <ion-list>\n    <div *ngFor="let item of GroupArray">\n     \n\n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/user.png" alt="item.Device_Name">\n        </ion-thumbnail>\n\n        <ion-row>\n          <ion-col col-8>\n\n            <p>\n              <span ion-text color="dark" style="text-align:left;"></span> {{item.groupname}}</p>\n\n          </ion-col>\n          <ion-col col-4>\n\n            <p style="text-align:left;font-size: 14px;">&nbsp;&nbsp;\n              <span style="color:#23c797;" *ngIf="item.status == true">Active&nbsp;</span>\n            </p>\n            <p style="text-align:left;font-size: 14px;">&nbsp;&nbsp;\n              <span style="color:#c74423;" *ngIf="item.status != true">InActive&nbsp;</span>\n            </p>\n\n          </ion-col>\n\n        </ion-row>\n\n\n        <p style="font-size:12px;">{{item.datetime | date: \'medium\'}}</p>\n\n        <p>\n          <span style="color:rgb(255,64,129);">{{item.vehicle}}</span>&nbsp;&nbsp;Vehicles</p>\n     \n        <p>\n          <button ion-button small (click)="openUpdateGroupModal(item)">Edit</button>\n          <button ion-button small (click)="deleteItem(item)">Delete</button>\n        </p>\n      </ion-item>\n  \n    </div>\n  </ion-list> -->\n\n\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/groups/groups.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"]])
    ], GroupsPage);
    return GroupsPage;
    var GroupsPage_1;
}());

//# sourceMappingURL=groups.js.map

/***/ }),

/***/ 993:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupsPageModule", function() { return GroupsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__groups__ = __webpack_require__(1088);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GroupsPageModule = /** @class */ (function () {
    function GroupsPageModule() {
    }
    GroupsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__groups__["a" /* GroupsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__groups__["a" /* GroupsPage */]),
            ],
        })
    ], GroupsPageModule);
    return GroupsPageModule;
}());

//# sourceMappingURL=groups.module.js.map

/***/ })

});
//# sourceMappingURL=44.js.map