webpackJsonp([68],{

/***/ 1043:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImmobilizePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { FormBuilder } from '@angular/forms';
var ImmobilizePage = /** @class */ (function () {
    function ImmobilizePage(params, renderer, viewCtrl) {
        this.renderer = renderer;
        this.viewCtrl = viewCtrl;
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup', true);
        // console.log('UserId', params.get('userId'));
        this.dataParam = params.get("param");
        this.respData = params.get("udata");
        this.msg = params.get("msg");
    }
    ImmobilizePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ImmobilizePage.prototype.yes = function () {
    };
    ImmobilizePage.prototype.no = function () {
        this.viewCtrl.dismiss();
    };
    ImmobilizePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-immobilize',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/add-devices/immobilize/immobilize.html"*/'<ion-content padding class="guide-modal">\n\n    <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n    <div>\n      <p>{{msg}}</p>\n      <ion-row>\n          <ion-col col-3>\n              <button ion-button small (tap)="yes()">{{ "Yes" | translate }}</button>\n          </ion-col>\n          <ion-col col-3>\n                <button ion-button small (tap)="no()">{{ "NO" | translate }}</button>\n          </ion-col>\n      </ion-row>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/j-r-fleetview/src/pages/add-devices/immobilize/immobilize.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], ImmobilizePage);
    return ImmobilizePage;
}());

//# sourceMappingURL=immobilize.js.map

/***/ }),

/***/ 953:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImmobilizePageModule", function() { return ImmobilizePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__immobilize__ = __webpack_require__(1043);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ImmobilizePageModule = /** @class */ (function () {
    function ImmobilizePageModule() {
    }
    ImmobilizePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__immobilize__["a" /* ImmobilizePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__immobilize__["a" /* ImmobilizePage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ]
        })
    ], ImmobilizePageModule);
    return ImmobilizePageModule;
}());

//# sourceMappingURL=immobilize.module.js.map

/***/ })

});
//# sourceMappingURL=68.js.map